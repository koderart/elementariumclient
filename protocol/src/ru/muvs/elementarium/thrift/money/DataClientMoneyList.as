/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.money {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataClientMoneyList implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataClientMoneyList");
    private static const MONEYS_FIELD_DESC:TField = new TField("moneys", TType.LIST, 1);

    private var _moneys:Array;
    public static const MONEYS:int = 1;


    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[MONEYS] = new FieldMetaData("moneys", TFieldRequirementType.REQUIRED, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.money.DataMoney)));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataClientMoneyList, metaDataMap);
    }

    public function DataClientMoneyList() {
    }

    public function get moneys():Array {
      return this._moneys;
    }

    public function set moneys(moneys:Array):void {
      this._moneys = moneys;
    }

    public function unsetMoneys():void {
      this.moneys = null;
    }

    // Returns true if field moneys is set (has been assigned a value) and false otherwise
    public function isSetMoneys():Boolean {
      return this.moneys != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case MONEYS:
        if (value == null) {
          unsetMoneys();
        } else {
          this.moneys = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case MONEYS:
        return this.moneys;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case MONEYS:
        return isSetMoneys();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case MONEYS:
            if (field.type == TType.LIST) {
              {
                var _list1:TList = iprot.readListBegin();
                this.moneys = new Array();
                for (var _i2:int = 0; _i2 < _list1.size; ++_i2)
                {
                  var _elem3:ru.muvs.elementarium.thrift.money.DataMoney;
                  _elem3 = new ru.muvs.elementarium.thrift.money.DataMoney();
                  _elem3.read(iprot);
                  this.moneys.push(_elem3);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.moneys != null) {
        oprot.writeFieldBegin(MONEYS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.moneys.length));
          for each (var elem4:* in this.moneys)          {
            elem4.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataClientMoneyList(");
      var first:Boolean = true;

      ret += "moneys:";
      if (this.moneys == null) {
        ret += "null";
      } else {
        ret += this.moneys;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      if (moneys == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'moneys' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
    }

  }

}
