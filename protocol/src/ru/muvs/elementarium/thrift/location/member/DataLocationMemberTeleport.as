/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.location.member {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataLocationMemberTeleport implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataLocationMemberTeleport");
    private static const LOCATION_MEMBER_ID_FIELD_DESC:TField = new TField("locationMemberId", TType.I32, 1);

    private var _locationMemberId:int;
    public static const LOCATIONMEMBERID:int = 1;

    private var __isset_locationMemberId:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[LOCATIONMEMBERID] = new FieldMetaData("locationMemberId", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataLocationMemberTeleport, metaDataMap);
    }

    public function DataLocationMemberTeleport() {
    }

    public function get locationMemberId():int {
      return this._locationMemberId;
    }

    public function set locationMemberId(locationMemberId:int):void {
      this._locationMemberId = locationMemberId;
      this.__isset_locationMemberId = true;
    }

    public function unsetLocationMemberId():void {
      this.__isset_locationMemberId = false;
    }

    // Returns true if field locationMemberId is set (has been assigned a value) and false otherwise
    public function isSetLocationMemberId():Boolean {
      return this.__isset_locationMemberId;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        if (value == null) {
          unsetLocationMemberId();
        } else {
          this.locationMemberId = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        return this.locationMemberId;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        return isSetLocationMemberId();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case LOCATIONMEMBERID:
            if (field.type == TType.I32) {
              this.locationMemberId = iprot.readI32();
              this.__isset_locationMemberId = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_locationMemberId) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'locationMemberId' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(LOCATION_MEMBER_ID_FIELD_DESC);
      oprot.writeI32(this.locationMemberId);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataLocationMemberTeleport(");
      var first:Boolean = true;

      ret += "locationMemberId:";
      ret += this.locationMemberId;
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'locationMemberId' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
    }

  }

}
