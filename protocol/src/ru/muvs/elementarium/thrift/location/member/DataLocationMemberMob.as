/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.location.member {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataLocationMemberMob implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataLocationMemberMob");
    private static const LOCATION_MEMBER_ID_FIELD_DESC:TField = new TField("locationMemberId", TType.I32, 1);
    private static const BATTLE_FIELD_DESC:TField = new TField("battle", TType.BOOL, 2);
    private static const LEFT_FIELD_DESC:TField = new TField("left", TType.I32, 3);

    private var _locationMemberId:int;
    public static const LOCATIONMEMBERID:int = 1;
    private var _battle:Boolean;
    public static const BATTLE:int = 2;
    private var _left:int;
    public static const LEFT:int = 3;

    private var __isset_locationMemberId:Boolean = false;
    private var __isset_battle:Boolean = false;
    private var __isset_left:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[LOCATIONMEMBERID] = new FieldMetaData("locationMemberId", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[BATTLE] = new FieldMetaData("battle", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.BOOL));
      metaDataMap[LEFT] = new FieldMetaData("left", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataLocationMemberMob, metaDataMap);
    }

    public function DataLocationMemberMob() {
    }

    public function get locationMemberId():int {
      return this._locationMemberId;
    }

    public function set locationMemberId(locationMemberId:int):void {
      this._locationMemberId = locationMemberId;
      this.__isset_locationMemberId = true;
    }

    public function unsetLocationMemberId():void {
      this.__isset_locationMemberId = false;
    }

    // Returns true if field locationMemberId is set (has been assigned a value) and false otherwise
    public function isSetLocationMemberId():Boolean {
      return this.__isset_locationMemberId;
    }

    public function get battle():Boolean {
      return this._battle;
    }

    public function set battle(battle:Boolean):void {
      this._battle = battle;
      this.__isset_battle = true;
    }

    public function unsetBattle():void {
      this.__isset_battle = false;
    }

    // Returns true if field battle is set (has been assigned a value) and false otherwise
    public function isSetBattle():Boolean {
      return this.__isset_battle;
    }

    public function get left():int {
      return this._left;
    }

    public function set left(left:int):void {
      this._left = left;
      this.__isset_left = true;
    }

    public function unsetLeft():void {
      this.__isset_left = false;
    }

    // Returns true if field left is set (has been assigned a value) and false otherwise
    public function isSetLeft():Boolean {
      return this.__isset_left;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        if (value == null) {
          unsetLocationMemberId();
        } else {
          this.locationMemberId = value;
        }
        break;

      case BATTLE:
        if (value == null) {
          unsetBattle();
        } else {
          this.battle = value;
        }
        break;

      case LEFT:
        if (value == null) {
          unsetLeft();
        } else {
          this.left = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        return this.locationMemberId;
      case BATTLE:
        return this.battle;
      case LEFT:
        return this.left;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case LOCATIONMEMBERID:
        return isSetLocationMemberId();
      case BATTLE:
        return isSetBattle();
      case LEFT:
        return isSetLeft();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case LOCATIONMEMBERID:
            if (field.type == TType.I32) {
              this.locationMemberId = iprot.readI32();
              this.__isset_locationMemberId = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case BATTLE:
            if (field.type == TType.BOOL) {
              this.battle = iprot.readBool();
              this.__isset_battle = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case LEFT:
            if (field.type == TType.I32) {
              this.left = iprot.readI32();
              this.__isset_left = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_locationMemberId) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'locationMemberId' was not found in serialized data! Struct: " + toString());
      }
      if (!__isset_battle) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'battle' was not found in serialized data! Struct: " + toString());
      }
      if (!__isset_left) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'left' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(LOCATION_MEMBER_ID_FIELD_DESC);
      oprot.writeI32(this.locationMemberId);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(BATTLE_FIELD_DESC);
      oprot.writeBool(this.battle);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(LEFT_FIELD_DESC);
      oprot.writeI32(this.left);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataLocationMemberMob(");
      var first:Boolean = true;

      ret += "locationMemberId:";
      ret += this.locationMemberId;
      first = false;
      if (!first) ret +=  ", ";
      ret += "battle:";
      ret += this.battle;
      first = false;
      if (!first) ret +=  ", ";
      ret += "left:";
      ret += this.left;
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'locationMemberId' because it's a primitive and you chose the non-beans generator.
      // alas, we cannot check 'battle' because it's a primitive and you chose the non-beans generator.
      // alas, we cannot check 'left' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
    }

  }

}
