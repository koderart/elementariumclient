/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.message.message.text 
{
import org.apache.thrift.Set;
import flash.utils.Dictionary;
  public class DataTextType   {
    public static const INFO:int = 1;
    public static const ERROR:int = 2;

    public static const VALID_VALUES:Set = new Set(INFO, ERROR);
    public static const VALUES_TO_NAMES:Dictionary = new Dictionary();
    {
      VALUES_TO_NAMES[INFO] = "INFO";
      VALUES_TO_NAMES[ERROR] = "ERROR";

    }
  }
}
