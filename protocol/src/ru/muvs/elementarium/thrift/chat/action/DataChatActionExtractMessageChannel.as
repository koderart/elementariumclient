/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.chat.action {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.chat.channel.DataChatChannelType;
import ru.muvs.elementarium.thrift.chat.message.DataChatMessage;

  public class DataChatActionExtractMessageChannel implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataChatActionExtractMessageChannel");
    private static const TYPE_FIELD_DESC:TField = new TField("type", TType.I32, 1);
    private static const MESSAGE_FIELD_DESC:TField = new TField("message", TType.STRUCT, 2);

    private var _type:int;
    public static const TYPE:int = 1;
    private var _message:ru.muvs.elementarium.thrift.chat.message.DataChatMessage;
    public static const MESSAGE:int = 2;

    private var __isset_type:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[TYPE] = new FieldMetaData("type", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[MESSAGE] = new FieldMetaData("message", TFieldRequirementType.REQUIRED, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.chat.message.DataChatMessage));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataChatActionExtractMessageChannel, metaDataMap);
    }

    public function DataChatActionExtractMessageChannel() {
    }

    public function get type():int {
      return this._type;
    }

    public function set type(type:int):void {
      this._type = type;
      this.__isset_type = true;
    }

    public function unsetType():void {
      this.__isset_type = false;
    }

    // Returns true if field type is set (has been assigned a value) and false otherwise
    public function isSetType():Boolean {
      return this.__isset_type;
    }

    public function get message():ru.muvs.elementarium.thrift.chat.message.DataChatMessage {
      return this._message;
    }

    public function set message(message:ru.muvs.elementarium.thrift.chat.message.DataChatMessage):void {
      this._message = message;
    }

    public function unsetMessage():void {
      this.message = null;
    }

    // Returns true if field message is set (has been assigned a value) and false otherwise
    public function isSetMessage():Boolean {
      return this.message != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case TYPE:
        if (value == null) {
          unsetType();
        } else {
          this.type = value;
        }
        break;

      case MESSAGE:
        if (value == null) {
          unsetMessage();
        } else {
          this.message = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case TYPE:
        return this.type;
      case MESSAGE:
        return this.message;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case TYPE:
        return isSetType();
      case MESSAGE:
        return isSetMessage();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case TYPE:
            if (field.type == TType.I32) {
              this.type = iprot.readI32();
              this.__isset_type = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case MESSAGE:
            if (field.type == TType.STRUCT) {
              this.message = new ru.muvs.elementarium.thrift.chat.message.DataChatMessage();
              this.message.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_type) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'type' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(TYPE_FIELD_DESC);
      oprot.writeI32(this.type);
      oprot.writeFieldEnd();
      if (this.message != null) {
        oprot.writeFieldBegin(MESSAGE_FIELD_DESC);
        this.message.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataChatActionExtractMessageChannel(");
      var first:Boolean = true;

      ret += "type:";
      var type_name:String = ru.muvs.elementarium.thrift.chat.channel.DataChatChannelType.VALUES_TO_NAMES[this.type];
      if (type_name != null) {
        ret += type_name;
        ret += " (";
      }
      ret += this.type;
      if (type_name != null) {
        ret += ")";
      }
      first = false;
      if (!first) ret +=  ", ";
      ret += "message:";
      if (this.message == null) {
        ret += "null";
      } else {
        ret += this.message;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'type' because it's a primitive and you chose the non-beans generator.
      if (message == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'message' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
      if (isSetType() && !ru.muvs.elementarium.thrift.chat.channel.DataChatChannelType.VALID_VALUES.contains(type)){
        throw new TProtocolError(TProtocolError.UNKNOWN, "The field 'type' has been assigned the invalid value " + type);
      }
    }

  }

}
