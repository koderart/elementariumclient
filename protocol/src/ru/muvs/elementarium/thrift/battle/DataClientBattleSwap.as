/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.battle.member.DataBattleMember;

  public class DataClientBattleSwap implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataClientBattleSwap");
    private static const MEMBER_FIELD_DESC:TField = new TField("member", TType.STRUCT, 1);
    private static const LEFT_FIELD_DESC:TField = new TField("left", TType.I32, 2);

    private var _member:ru.muvs.elementarium.thrift.battle.member.DataBattleMember;
    public static const MEMBER:int = 1;
    private var _left:int;
    public static const LEFT:int = 2;

    private var __isset_left:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[MEMBER] = new FieldMetaData("member", TFieldRequirementType.REQUIRED, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.member.DataBattleMember));
      metaDataMap[LEFT] = new FieldMetaData("left", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataClientBattleSwap, metaDataMap);
    }

    public function DataClientBattleSwap() {
    }

    public function get member():ru.muvs.elementarium.thrift.battle.member.DataBattleMember {
      return this._member;
    }

    public function set member(member:ru.muvs.elementarium.thrift.battle.member.DataBattleMember):void {
      this._member = member;
    }

    public function unsetMember():void {
      this.member = null;
    }

    // Returns true if field member is set (has been assigned a value) and false otherwise
    public function isSetMember():Boolean {
      return this.member != null;
    }

    public function get left():int {
      return this._left;
    }

    public function set left(left:int):void {
      this._left = left;
      this.__isset_left = true;
    }

    public function unsetLeft():void {
      this.__isset_left = false;
    }

    // Returns true if field left is set (has been assigned a value) and false otherwise
    public function isSetLeft():Boolean {
      return this.__isset_left;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case MEMBER:
        if (value == null) {
          unsetMember();
        } else {
          this.member = value;
        }
        break;

      case LEFT:
        if (value == null) {
          unsetLeft();
        } else {
          this.left = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case MEMBER:
        return this.member;
      case LEFT:
        return this.left;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case MEMBER:
        return isSetMember();
      case LEFT:
        return isSetLeft();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case MEMBER:
            if (field.type == TType.STRUCT) {
              this.member = new ru.muvs.elementarium.thrift.battle.member.DataBattleMember();
              this.member.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case LEFT:
            if (field.type == TType.I32) {
              this.left = iprot.readI32();
              this.__isset_left = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_left) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'left' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.member != null) {
        oprot.writeFieldBegin(MEMBER_FIELD_DESC);
        this.member.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(LEFT_FIELD_DESC);
      oprot.writeI32(this.left);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataClientBattleSwap(");
      var first:Boolean = true;

      ret += "member:";
      if (this.member == null) {
        ret += "null";
      } else {
        ret += this.member;
      }
      first = false;
      if (!first) ret +=  ", ";
      ret += "left:";
      ret += this.left;
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      if (member == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'member' was not present! Struct: " + toString());
      }
      // alas, we cannot check 'left' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
    }

  }

}
