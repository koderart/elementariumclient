/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.action {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.battle.area.DataAreaCell;

  public class DataBattleActionExtractReplaceCell implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataBattleActionExtractReplaceCell");
    private static const SOURCE_FIELD_DESC:TField = new TField("source", TType.I32, 1);
    private static const DESTINATION_FIELD_DESC:TField = new TField("destination", TType.I32, 2);
    private static const CELL_FIELD_DESC:TField = new TField("cell", TType.STRUCT, 3);

    private var _source:int;
    public static const SOURCE:int = 1;
    private var _destination:int;
    public static const DESTINATION:int = 2;
    private var _cell:ru.muvs.elementarium.thrift.battle.area.DataAreaCell;
    public static const CELL:int = 3;

    private var __isset_source:Boolean = false;
    private var __isset_destination:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[SOURCE] = new FieldMetaData("source", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[DESTINATION] = new FieldMetaData("destination", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[CELL] = new FieldMetaData("cell", TFieldRequirementType.REQUIRED, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.area.DataAreaCell));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataBattleActionExtractReplaceCell, metaDataMap);
    }

    public function DataBattleActionExtractReplaceCell() {
    }

    public function get source():int {
      return this._source;
    }

    public function set source(source:int):void {
      this._source = source;
      this.__isset_source = true;
    }

    public function unsetSource():void {
      this.__isset_source = false;
    }

    // Returns true if field source is set (has been assigned a value) and false otherwise
    public function isSetSource():Boolean {
      return this.__isset_source;
    }

    public function get destination():int {
      return this._destination;
    }

    public function set destination(destination:int):void {
      this._destination = destination;
      this.__isset_destination = true;
    }

    public function unsetDestination():void {
      this.__isset_destination = false;
    }

    // Returns true if field destination is set (has been assigned a value) and false otherwise
    public function isSetDestination():Boolean {
      return this.__isset_destination;
    }

    public function get cell():ru.muvs.elementarium.thrift.battle.area.DataAreaCell {
      return this._cell;
    }

    public function set cell(cell:ru.muvs.elementarium.thrift.battle.area.DataAreaCell):void {
      this._cell = cell;
    }

    public function unsetCell():void {
      this.cell = null;
    }

    // Returns true if field cell is set (has been assigned a value) and false otherwise
    public function isSetCell():Boolean {
      return this.cell != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case SOURCE:
        if (value == null) {
          unsetSource();
        } else {
          this.source = value;
        }
        break;

      case DESTINATION:
        if (value == null) {
          unsetDestination();
        } else {
          this.destination = value;
        }
        break;

      case CELL:
        if (value == null) {
          unsetCell();
        } else {
          this.cell = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case SOURCE:
        return this.source;
      case DESTINATION:
        return this.destination;
      case CELL:
        return this.cell;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case SOURCE:
        return isSetSource();
      case DESTINATION:
        return isSetDestination();
      case CELL:
        return isSetCell();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case SOURCE:
            if (field.type == TType.I32) {
              this.source = iprot.readI32();
              this.__isset_source = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case DESTINATION:
            if (field.type == TType.I32) {
              this.destination = iprot.readI32();
              this.__isset_destination = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case CELL:
            if (field.type == TType.STRUCT) {
              this.cell = new ru.muvs.elementarium.thrift.battle.area.DataAreaCell();
              this.cell.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_source) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'source' was not found in serialized data! Struct: " + toString());
      }
      if (!__isset_destination) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'destination' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(SOURCE_FIELD_DESC);
      oprot.writeI32(this.source);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(DESTINATION_FIELD_DESC);
      oprot.writeI32(this.destination);
      oprot.writeFieldEnd();
      if (this.cell != null) {
        oprot.writeFieldBegin(CELL_FIELD_DESC);
        this.cell.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataBattleActionExtractReplaceCell(");
      var first:Boolean = true;

      ret += "source:";
      ret += this.source;
      first = false;
      if (!first) ret +=  ", ";
      ret += "destination:";
      ret += this.destination;
      first = false;
      if (!first) ret +=  ", ";
      ret += "cell:";
      if (this.cell == null) {
        ret += "null";
      } else {
        ret += this.cell;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'source' because it's a primitive and you chose the non-beans generator.
      // alas, we cannot check 'destination' because it's a primitive and you chose the non-beans generator.
      if (cell == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'cell' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
    }

  }

}
