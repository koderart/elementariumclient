/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.member {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataBattleMemberMob implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataBattleMemberMob");
    private static const ID_FIELD_DESC:TField = new TField("id", TType.I32, 1);

    private var _id:int;
    public static const ID:int = 1;

    private var __isset_id:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[ID] = new FieldMetaData("id", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataBattleMemberMob, metaDataMap);
    }

    public function DataBattleMemberMob() {
    }

    public function get id():int {
      return this._id;
    }

    public function set id(id:int):void {
      this._id = id;
      this.__isset_id = true;
    }

    public function unsetId():void {
      this.__isset_id = false;
    }

    // Returns true if field id is set (has been assigned a value) and false otherwise
    public function isSetId():Boolean {
      return this.__isset_id;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case ID:
        if (value == null) {
          unsetId();
        } else {
          this.id = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case ID:
        return this.id;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case ID:
        return isSetId();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case ID:
            if (field.type == TType.I32) {
              this.id = iprot.readI32();
              this.__isset_id = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_id) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'id' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(ID_FIELD_DESC);
      oprot.writeI32(this.id);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataBattleMemberMob(");
      var first:Boolean = true;

      ret += "id:";
      ret += this.id;
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'id' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
    }

  }

}
