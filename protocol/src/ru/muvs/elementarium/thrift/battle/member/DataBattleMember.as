/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.member {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlot;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;
import ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool;

import ru.muvs.elementarium.thrift.battle.member.DataBattleMemberType;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePoint;
import ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill;

public class DataBattleMember implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataBattleMember");
    private static const ID_FIELD_DESC:TField = new TField("id", TType.I32, 1);
    private static const SIDE_FIELD_DESC:TField = new TField("side", TType.I32, 2);
    private static const TYPE_FIELD_DESC:TField = new TField("type", TType.I32, 3);
    private static const PLAYER_FIELD_DESC:TField = new TField("player", TType.STRUCT, 4);
    private static const MOB_FIELD_DESC:TField = new TField("mob", TType.STRUCT, 5);
    private static const POINTS_FIELD_DESC:TField = new TField("points", TType.LIST, 6);
    private static const SKILLS_FIELD_DESC:TField = new TField("skills", TType.LIST, 7);
    private static const SLOTS_FIELD_DESC:TField = new TField("slots", TType.LIST, 8);
    private static const BELTS_FIELD_DESC:TField = new TField("belts", TType.LIST, 9);
    private static const MAGIC_COOLS_FIELD_DESC:TField = new TField("magicCools", TType.LIST, 10);
    private static const POTION_COOLS_FIELD_DESC:TField = new TField("potionCools", TType.LIST, 11);
    private static const BUFFS_FIELD_DESC:TField = new TField("buffs", TType.LIST, 12);

    private var _id:int;
    public static const ID:int = 1;
    private var _side:int;
    public static const SIDE:int = 2;
    private var _type:int;
    public static const TYPE:int = 3;
    private var _player:ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer;
    public static const PLAYER:int = 4;
    private var _mob:ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob;
    public static const MOB:int = 5;
    private var _points:Array;
    public static const POINTS:int = 6;
    private var _skills:Array;
    public static const SKILLS:int = 7;
    private var _slots:Array;
    public static const SLOTS:int = 8;
    private var _belts:Array;
    public static const BELTS:int = 9;
    private var _magicCools:Array;
    public static const MAGICCOOLS:int = 10;
    private var _potionCools:Array;
    public static const POTIONCOOLS:int = 11;
    private var _buffs:Array;
    public static const BUFFS:int = 12;

    private var __isset_id:Boolean = false;
    private var __isset_side:Boolean = false;
    private var __isset_type:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[ID] = new FieldMetaData("id", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[SIDE] = new FieldMetaData("side", TFieldRequirementType.OPTIONAL, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[TYPE] = new FieldMetaData("type", TFieldRequirementType.OPTIONAL, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[PLAYER] = new FieldMetaData("player", TFieldRequirementType.OPTIONAL, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer));
      metaDataMap[MOB] = new FieldMetaData("mob", TFieldRequirementType.OPTIONAL, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob));
      metaDataMap[POINTS] = new FieldMetaData("points", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.point.DataBattlePoint)));
      metaDataMap[SKILLS] = new FieldMetaData("skills", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill)));
      metaDataMap[SLOTS] = new FieldMetaData("slots", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlot)));
      metaDataMap[BELTS] = new FieldMetaData("belts", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt)));
      metaDataMap[MAGICCOOLS] = new FieldMetaData("magicCools", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool)));
      metaDataMap[POTIONCOOLS] = new FieldMetaData("potionCools", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.cool.DataBattlePotionCool)));
      metaDataMap[BUFFS] = new FieldMetaData("buffs", TFieldRequirementType.OPTIONAL, 
          new ListMetaData(TType.LIST, 
              new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff)));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataBattleMember, metaDataMap);
    }

    public function DataBattleMember() {
    }

    public function get id():int {
      return this._id;
    }

    public function set id(id:int):void {
      this._id = id;
      this.__isset_id = true;
    }

    public function unsetId():void {
      this.__isset_id = false;
    }

    // Returns true if field id is set (has been assigned a value) and false otherwise
    public function isSetId():Boolean {
      return this.__isset_id;
    }

    public function get side():int {
      return this._side;
    }

    public function set side(side:int):void {
      this._side = side;
      this.__isset_side = true;
    }

    public function unsetSide():void {
      this.__isset_side = false;
    }

    // Returns true if field side is set (has been assigned a value) and false otherwise
    public function isSetSide():Boolean {
      return this.__isset_side;
    }

    public function get type():int {
      return this._type;
    }

    public function set type(type:int):void {
      this._type = type;
      this.__isset_type = true;
    }

    public function unsetType():void {
      this.__isset_type = false;
    }

    // Returns true if field type is set (has been assigned a value) and false otherwise
    public function isSetType():Boolean {
      return this.__isset_type;
    }

    public function get player():ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer {
      return this._player;
    }

    public function set player(player:ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer):void {
      this._player = player;
    }

    public function unsetPlayer():void {
      this.player = null;
    }

    // Returns true if field player is set (has been assigned a value) and false otherwise
    public function isSetPlayer():Boolean {
      return this.player != null;
    }

    public function get mob():ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob {
      return this._mob;
    }

    public function set mob(mob:ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob):void {
      this._mob = mob;
    }

    public function unsetMob():void {
      this.mob = null;
    }

    // Returns true if field mob is set (has been assigned a value) and false otherwise
    public function isSetMob():Boolean {
      return this.mob != null;
    }

    public function get points():Array {
      return this._points;
    }

    public function set points(points:Array):void {
      this._points = points;
    }

    public function unsetPoints():void {
      this.points = null;
    }

    // Returns true if field points is set (has been assigned a value) and false otherwise
    public function isSetPoints():Boolean {
      return this.points != null;
    }

    public function get skills():Array {
      return this._skills;
    }

    public function set skills(skills:Array):void {
      this._skills = skills;
    }

    public function unsetSkills():void {
      this.skills = null;
    }

    // Returns true if field skills is set (has been assigned a value) and false otherwise
    public function isSetSkills():Boolean {
      return this.skills != null;
    }

    public function get slots():Array {
      return this._slots;
    }

    public function set slots(slots:Array):void {
      this._slots = slots;
    }

    public function unsetSlots():void {
      this.slots = null;
    }

    // Returns true if field slots is set (has been assigned a value) and false otherwise
    public function isSetSlots():Boolean {
      return this.slots != null;
    }

    public function get belts():Array {
      return this._belts;
    }

    public function set belts(belts:Array):void {
      this._belts = belts;
    }

    public function unsetBelts():void {
      this.belts = null;
    }

    // Returns true if field belts is set (has been assigned a value) and false otherwise
    public function isSetBelts():Boolean {
      return this.belts != null;
    }

    public function get magicCools():Array {
      return this._magicCools;
    }

    public function set magicCools(magicCools:Array):void {
      this._magicCools = magicCools;
    }

    public function unsetMagicCools():void {
      this.magicCools = null;
    }

    // Returns true if field magicCools is set (has been assigned a value) and false otherwise
    public function isSetMagicCools():Boolean {
      return this.magicCools != null;
    }

    public function get potionCools():Array {
      return this._potionCools;
    }

    public function set potionCools(potionCools:Array):void {
      this._potionCools = potionCools;
    }

    public function unsetPotionCools():void {
      this.potionCools = null;
    }

    // Returns true if field potionCools is set (has been assigned a value) and false otherwise
    public function isSetPotionCools():Boolean {
      return this.potionCools != null;
    }

    public function get buffs():Array {
      return this._buffs;
    }

    public function set buffs(buffs:Array):void {
      this._buffs = buffs;
    }

    public function unsetBuffs():void {
      this.buffs = null;
    }

    // Returns true if field buffs is set (has been assigned a value) and false otherwise
    public function isSetBuffs():Boolean {
      return this.buffs != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case ID:
        if (value == null) {
          unsetId();
        } else {
          this.id = value;
        }
        break;

      case SIDE:
        if (value == null) {
          unsetSide();
        } else {
          this.side = value;
        }
        break;

      case TYPE:
        if (value == null) {
          unsetType();
        } else {
          this.type = value;
        }
        break;

      case PLAYER:
        if (value == null) {
          unsetPlayer();
        } else {
          this.player = value;
        }
        break;

      case MOB:
        if (value == null) {
          unsetMob();
        } else {
          this.mob = value;
        }
        break;

      case POINTS:
        if (value == null) {
          unsetPoints();
        } else {
          this.points = value;
        }
        break;

      case SKILLS:
        if (value == null) {
          unsetSkills();
        } else {
          this.skills = value;
        }
        break;

      case SLOTS:
        if (value == null) {
          unsetSlots();
        } else {
          this.slots = value;
        }
        break;

      case BELTS:
        if (value == null) {
          unsetBelts();
        } else {
          this.belts = value;
        }
        break;

      case MAGICCOOLS:
        if (value == null) {
          unsetMagicCools();
        } else {
          this.magicCools = value;
        }
        break;

      case POTIONCOOLS:
        if (value == null) {
          unsetPotionCools();
        } else {
          this.potionCools = value;
        }
        break;

      case BUFFS:
        if (value == null) {
          unsetBuffs();
        } else {
          this.buffs = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case ID:
        return this.id;
      case SIDE:
        return this.side;
      case TYPE:
        return this.type;
      case PLAYER:
        return this.player;
      case MOB:
        return this.mob;
      case POINTS:
        return this.points;
      case SKILLS:
        return this.skills;
      case SLOTS:
        return this.slots;
      case BELTS:
        return this.belts;
      case MAGICCOOLS:
        return this.magicCools;
      case POTIONCOOLS:
        return this.potionCools;
      case BUFFS:
        return this.buffs;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case ID:
        return isSetId();
      case SIDE:
        return isSetSide();
      case TYPE:
        return isSetType();
      case PLAYER:
        return isSetPlayer();
      case MOB:
        return isSetMob();
      case POINTS:
        return isSetPoints();
      case SKILLS:
        return isSetSkills();
      case SLOTS:
        return isSetSlots();
      case BELTS:
        return isSetBelts();
      case MAGICCOOLS:
        return isSetMagicCools();
      case POTIONCOOLS:
        return isSetPotionCools();
      case BUFFS:
        return isSetBuffs();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case ID:
            if (field.type == TType.I32) {
              this.id = iprot.readI32();
              this.__isset_id = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case SIDE:
            if (field.type == TType.I32) {
              this.side = iprot.readI32();
              this.__isset_side = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case TYPE:
            if (field.type == TType.I32) {
              this.type = iprot.readI32();
              this.__isset_type = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case PLAYER:
            if (field.type == TType.STRUCT) {
              this.player = new ru.muvs.elementarium.thrift.battle.member.DataBattleMemberPlayer();
              this.player.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case MOB:
            if (field.type == TType.STRUCT) {
              this.mob = new ru.muvs.elementarium.thrift.battle.member.DataBattleMemberMob();
              this.mob.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case POINTS:
            if (field.type == TType.LIST) {
              {
                var _list12:TList = iprot.readListBegin();
                this.points = new Array();
                for (var _i13:int = 0; _i13 < _list12.size; ++_i13)
                {
                  var _elem14:ru.muvs.elementarium.thrift.battle.point.DataBattlePoint;
                  _elem14 = new ru.muvs.elementarium.thrift.battle.point.DataBattlePoint();
                  _elem14.read(iprot);
                  this.points.push(_elem14);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case SKILLS:
            if (field.type == TType.LIST) {
              {
                var _list15:TList = iprot.readListBegin();
                this.skills = new Array();
                for (var _i16:int = 0; _i16 < _list15.size; ++_i16)
                {
                  var _elem17:ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill;
                  _elem17 = new ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill();
                  _elem17.read(iprot);
                  this.skills.push(_elem17);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case SLOTS:
            if (field.type == TType.LIST) {
              {
                var _list18:TList = iprot.readListBegin();
                this.slots = new Array();
                for (var _i19:int = 0; _i19 < _list18.size; ++_i19)
                {
                  var _elem20:ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlot;
                  _elem20 = new ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlot();
                  _elem20.read(iprot);
                  this.slots.push(_elem20);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case BELTS:
            if (field.type == TType.LIST) {
              {
                var _list21:TList = iprot.readListBegin();
                this.belts = new Array();
                for (var _i22:int = 0; _i22 < _list21.size; ++_i22)
                {
                  var _elem23:ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;
                  _elem23 = new ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt();
                  _elem23.read(iprot);
                  this.belts.push(_elem23);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case MAGICCOOLS:
            if (field.type == TType.LIST) {
              {
                var _list24:TList = iprot.readListBegin();
                this.magicCools = new Array();
                for (var _i25:int = 0; _i25 < _list24.size; ++_i25)
                {
                  var _elem26:ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool;
                  _elem26 = new ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool();
                  _elem26.read(iprot);
                  this.magicCools.push(_elem26);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case POTIONCOOLS:
            if (field.type == TType.LIST) {
              {
                var _list27:TList = iprot.readListBegin();
                this.potionCools = new Array();
                for (var _i28:int = 0; _i28 < _list27.size; ++_i28)
                {
                  var _elem29:ru.muvs.elementarium.thrift.battle.cool.DataBattlePotionCool;
                  _elem29 = new ru.muvs.elementarium.thrift.battle.cool.DataBattlePotionCool();
                  _elem29.read(iprot);
                  this.potionCools.push(_elem29);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case BUFFS:
            if (field.type == TType.LIST) {
              {
                var _list30:TList = iprot.readListBegin();
                this.buffs = new Array();
                for (var _i31:int = 0; _i31 < _list30.size; ++_i31)
                {
                  var _elem32:ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;
                  _elem32 = new ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff();
                  _elem32.read(iprot);
                  this.buffs.push(_elem32);
                }
                iprot.readListEnd();
              }
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_id) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'id' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(ID_FIELD_DESC);
      oprot.writeI32(this.id);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(SIDE_FIELD_DESC);
      oprot.writeI32(this.side);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(TYPE_FIELD_DESC);
      oprot.writeI32(this.type);
      oprot.writeFieldEnd();
      if (this.player != null) {
        oprot.writeFieldBegin(PLAYER_FIELD_DESC);
        this.player.write(oprot);
        oprot.writeFieldEnd();
      }
      if (this.mob != null) {
        oprot.writeFieldBegin(MOB_FIELD_DESC);
        this.mob.write(oprot);
        oprot.writeFieldEnd();
      }
      if (this.points != null) {
        oprot.writeFieldBegin(POINTS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.points.length));
          for each (var elem33:* in this.points)          {
            elem33.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.skills != null) {
        oprot.writeFieldBegin(SKILLS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.skills.length));
          for each (var elem34:* in this.skills)          {
            elem34.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.slots != null) {
        oprot.writeFieldBegin(SLOTS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.slots.length));
          for each (var elem35:* in this.slots)          {
            elem35.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.belts != null) {
        oprot.writeFieldBegin(BELTS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.belts.length));
          for each (var elem36:* in this.belts)          {
            elem36.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.magicCools != null) {
        oprot.writeFieldBegin(MAGIC_COOLS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.magicCools.length));
          for each (var elem37:* in this.magicCools)          {
            elem37.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.potionCools != null) {
        oprot.writeFieldBegin(POTION_COOLS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.potionCools.length));
          for each (var elem38:* in this.potionCools)          {
            elem38.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      if (this.buffs != null) {
        oprot.writeFieldBegin(BUFFS_FIELD_DESC);
        {
          oprot.writeListBegin(new TList(TType.STRUCT, this.buffs.length));
          for each (var elem39:* in this.buffs)          {
            elem39.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataBattleMember(");
      var first:Boolean = true;

      ret += "id:";
      ret += this.id;
      first = false;
      if (isSetSide()) {
        if (!first) ret +=  ", ";
        ret += "side:";
        ret += this.side;
        first = false;
      }
      if (isSetType()) {
        if (!first) ret +=  ", ";
        ret += "type:";
        var type_name:String = ru.muvs.elementarium.thrift.battle.member.DataBattleMemberType.VALUES_TO_NAMES[this.type];
        if (type_name != null) {
          ret += type_name;
          ret += " (";
        }
        ret += this.type;
        if (type_name != null) {
          ret += ")";
        }
        first = false;
      }
      if (isSetPlayer()) {
        if (!first) ret +=  ", ";
        ret += "player:";
        if (this.player == null) {
          ret += "null";
        } else {
          ret += this.player;
        }
        first = false;
      }
      if (isSetMob()) {
        if (!first) ret +=  ", ";
        ret += "mob:";
        if (this.mob == null) {
          ret += "null";
        } else {
          ret += this.mob;
        }
        first = false;
      }
      if (isSetPoints()) {
        if (!first) ret +=  ", ";
        ret += "points:";
        if (this.points == null) {
          ret += "null";
        } else {
          ret += this.points;
        }
        first = false;
      }
      if (isSetSkills()) {
        if (!first) ret +=  ", ";
        ret += "skills:";
        if (this.skills == null) {
          ret += "null";
        } else {
          ret += this.skills;
        }
        first = false;
      }
      if (isSetSlots()) {
        if (!first) ret +=  ", ";
        ret += "slots:";
        if (this.slots == null) {
          ret += "null";
        } else {
          ret += this.slots;
        }
        first = false;
      }
      if (isSetBelts()) {
        if (!first) ret +=  ", ";
        ret += "belts:";
        if (this.belts == null) {
          ret += "null";
        } else {
          ret += this.belts;
        }
        first = false;
      }
      if (isSetMagicCools()) {
        if (!first) ret +=  ", ";
        ret += "magicCools:";
        if (this.magicCools == null) {
          ret += "null";
        } else {
          ret += this.magicCools;
        }
        first = false;
      }
      if (isSetPotionCools()) {
        if (!first) ret +=  ", ";
        ret += "potionCools:";
        if (this.potionCools == null) {
          ret += "null";
        } else {
          ret += this.potionCools;
        }
        first = false;
      }
      if (isSetBuffs()) {
        if (!first) ret +=  ", ";
        ret += "buffs:";
        if (this.buffs == null) {
          ret += "null";
        } else {
          ret += this.buffs;
        }
        first = false;
      }
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'id' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
      if (isSetType() && !ru.muvs.elementarium.thrift.battle.member.DataBattleMemberType.VALID_VALUES.contains(type)){
        throw new TProtocolError(TProtocolError.UNKNOWN, "The field 'type' has been assigned the invalid value " + type);
      }
    }

  }

}
