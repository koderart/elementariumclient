/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.area {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataAreaCell implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataAreaCell");
    private static const POSITION_X_FIELD_DESC:TField = new TField("positionX", TType.I32, 1);
    private static const POSITION_Y_FIELD_DESC:TField = new TField("positionY", TType.I32, 2);
    private static const TYPE_FIELD_DESC:TField = new TField("type", TType.I32, 3);

    private var _positionX:int;
    public static const POSITIONX:int = 1;
    private var _positionY:int;
    public static const POSITIONY:int = 2;
    private var _type:int;
    public static const TYPE:int = 3;

    private var __isset_positionX:Boolean = false;
    private var __isset_positionY:Boolean = false;
    private var __isset_type:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[POSITIONX] = new FieldMetaData("positionX", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[POSITIONY] = new FieldMetaData("positionY", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[TYPE] = new FieldMetaData("type", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataAreaCell, metaDataMap);
    }

    public function DataAreaCell() {
    }

    public function get positionX():int {
      return this._positionX;
    }

    public function set positionX(positionX:int):void {
      this._positionX = positionX;
      this.__isset_positionX = true;
    }

    public function unsetPositionX():void {
      this.__isset_positionX = false;
    }

    // Returns true if field positionX is set (has been assigned a value) and false otherwise
    public function isSetPositionX():Boolean {
      return this.__isset_positionX;
    }

    public function get positionY():int {
      return this._positionY;
    }

    public function set positionY(positionY:int):void {
      this._positionY = positionY;
      this.__isset_positionY = true;
    }

    public function unsetPositionY():void {
      this.__isset_positionY = false;
    }

    // Returns true if field positionY is set (has been assigned a value) and false otherwise
    public function isSetPositionY():Boolean {
      return this.__isset_positionY;
    }

    public function get type():int {
      return this._type;
    }

    public function set type(type:int):void {
      this._type = type;
      this.__isset_type = true;
    }

    public function unsetType():void {
      this.__isset_type = false;
    }

    // Returns true if field type is set (has been assigned a value) and false otherwise
    public function isSetType():Boolean {
      return this.__isset_type;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case POSITIONX:
        if (value == null) {
          unsetPositionX();
        } else {
          this.positionX = value;
        }
        break;

      case POSITIONY:
        if (value == null) {
          unsetPositionY();
        } else {
          this.positionY = value;
        }
        break;

      case TYPE:
        if (value == null) {
          unsetType();
        } else {
          this.type = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case POSITIONX:
        return this.positionX;
      case POSITIONY:
        return this.positionY;
      case TYPE:
        return this.type;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case POSITIONX:
        return isSetPositionX();
      case POSITIONY:
        return isSetPositionY();
      case TYPE:
        return isSetType();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case POSITIONX:
            if (field.type == TType.I32) {
              this.positionX = iprot.readI32();
              this.__isset_positionX = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case POSITIONY:
            if (field.type == TType.I32) {
              this.positionY = iprot.readI32();
              this.__isset_positionY = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case TYPE:
            if (field.type == TType.I32) {
              this.type = iprot.readI32();
              this.__isset_type = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_positionX) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'positionX' was not found in serialized data! Struct: " + toString());
      }
      if (!__isset_positionY) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'positionY' was not found in serialized data! Struct: " + toString());
      }
      if (!__isset_type) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'type' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(POSITION_X_FIELD_DESC);
      oprot.writeI32(this.positionX);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(POSITION_Y_FIELD_DESC);
      oprot.writeI32(this.positionY);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(TYPE_FIELD_DESC);
      oprot.writeI32(this.type);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataAreaCell(");
      var first:Boolean = true;

      ret += "positionX:";
      ret += this.positionX;
      first = false;
      if (!first) ret +=  ", ";
      ret += "positionY:";
      ret += this.positionY;
      first = false;
      if (!first) ret +=  ", ";
      ret += "type:";
      ret += this.type;
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'positionX' because it's a primitive and you chose the non-beans generator.
      // alas, we cannot check 'positionY' because it's a primitive and you chose the non-beans generator.
      // alas, we cannot check 'type' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
    }

  }

}
