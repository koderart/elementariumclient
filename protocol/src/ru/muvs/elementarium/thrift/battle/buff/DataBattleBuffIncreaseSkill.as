/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.buff {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill;

  public class DataBattleBuffIncreaseSkill implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataBattleBuffIncreaseSkill");
    private static const SKILL_FIELD_DESC:TField = new TField("skill", TType.STRUCT, 1);

    private var _skill:ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill;
    public static const SKILL:int = 1;


    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[SKILL] = new FieldMetaData("skill", TFieldRequirementType.REQUIRED, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataBattleBuffIncreaseSkill, metaDataMap);
    }

    public function DataBattleBuffIncreaseSkill() {
    }

    public function get skill():ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill {
      return this._skill;
    }

    public function set skill(skill:ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill):void {
      this._skill = skill;
    }

    public function unsetSkill():void {
      this.skill = null;
    }

    // Returns true if field skill is set (has been assigned a value) and false otherwise
    public function isSetSkill():Boolean {
      return this.skill != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case SKILL:
        if (value == null) {
          unsetSkill();
        } else {
          this.skill = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case SKILL:
        return this.skill;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case SKILL:
        return isSetSkill();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case SKILL:
            if (field.type == TType.STRUCT) {
              this.skill = new ru.muvs.elementarium.thrift.battle.skill.DataBattleSkill();
              this.skill.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.skill != null) {
        oprot.writeFieldBegin(SKILL_FIELD_DESC);
        this.skill.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataBattleBuffIncreaseSkill(");
      var first:Boolean = true;

      ret += "skill:";
      if (this.skill == null) {
        ret += "null";
      } else {
        ret += this.skill;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      if (skill == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'skill' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
    }

  }

}
