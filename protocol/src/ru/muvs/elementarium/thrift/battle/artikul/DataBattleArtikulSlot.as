/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.battle.artikul {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlotType;

  public class DataBattleArtikulSlot implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataBattleArtikulSlot");
    private static const TYPE_FIELD_DESC:TField = new TField("type", TType.I32, 1);
    private static const ARTIKUL_FIELD_DESC:TField = new TField("artikul", TType.STRUCT, 2);

    private var _type:int;
    public static const TYPE:int = 1;
    private var _artikul:ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikul;
    public static const ARTIKUL:int = 2;

    private var __isset_type:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[TYPE] = new FieldMetaData("type", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[ARTIKUL] = new FieldMetaData("artikul", TFieldRequirementType.REQUIRED, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikul));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataBattleArtikulSlot, metaDataMap);
    }

    public function DataBattleArtikulSlot() {
    }

    public function get type():int {
      return this._type;
    }

    public function set type(type:int):void {
      this._type = type;
      this.__isset_type = true;
    }

    public function unsetType():void {
      this.__isset_type = false;
    }

    // Returns true if field type is set (has been assigned a value) and false otherwise
    public function isSetType():Boolean {
      return this.__isset_type;
    }

    public function get artikul():ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikul {
      return this._artikul;
    }

    public function set artikul(artikul:ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikul):void {
      this._artikul = artikul;
    }

    public function unsetArtikul():void {
      this.artikul = null;
    }

    // Returns true if field artikul is set (has been assigned a value) and false otherwise
    public function isSetArtikul():Boolean {
      return this.artikul != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case TYPE:
        if (value == null) {
          unsetType();
        } else {
          this.type = value;
        }
        break;

      case ARTIKUL:
        if (value == null) {
          unsetArtikul();
        } else {
          this.artikul = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case TYPE:
        return this.type;
      case ARTIKUL:
        return this.artikul;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case TYPE:
        return isSetType();
      case ARTIKUL:
        return isSetArtikul();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case TYPE:
            if (field.type == TType.I32) {
              this.type = iprot.readI32();
              this.__isset_type = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case ARTIKUL:
            if (field.type == TType.STRUCT) {
              this.artikul = new ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikul();
              this.artikul.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_type) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'type' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(TYPE_FIELD_DESC);
      oprot.writeI32(this.type);
      oprot.writeFieldEnd();
      if (this.artikul != null) {
        oprot.writeFieldBegin(ARTIKUL_FIELD_DESC);
        this.artikul.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataBattleArtikulSlot(");
      var first:Boolean = true;

      ret += "type:";
      var type_name:String = ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlotType.VALUES_TO_NAMES[this.type];
      if (type_name != null) {
        ret += type_name;
        ret += " (";
      }
      ret += this.type;
      if (type_name != null) {
        ret += ")";
      }
      first = false;
      if (!first) ret +=  ", ";
      ret += "artikul:";
      if (this.artikul == null) {
        ret += "null";
      } else {
        ret += this.artikul;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'type' because it's a primitive and you chose the non-beans generator.
      if (artikul == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'artikul' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
      if (isSetType() && !ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulSlotType.VALID_VALUES.contains(type)){
        throw new TProtocolError(TProtocolError.UNKNOWN, "The field 'type' has been assigned the invalid value " + type);
      }
    }

  }

}
