/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.login {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;

import ru.muvs.elementarium.thrift.player.DataPlayer;

  public class DataClientLoginApple implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataClientLoginApple");
    private static const STATUS_FIELD_DESC:TField = new TField("status", TType.I32, 1);
    private static const PLAYER_FIELD_DESC:TField = new TField("player", TType.STRUCT, 2);

    private var _status:int;
    public static const STATUS:int = 1;
    private var _player:ru.muvs.elementarium.thrift.player.DataPlayer;
    public static const PLAYER:int = 2;

    private var __isset_status:Boolean = false;

    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[STATUS] = new FieldMetaData("status", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.I32));
      metaDataMap[PLAYER] = new FieldMetaData("player", TFieldRequirementType.OPTIONAL, 
          new StructMetaData(TType.STRUCT, ru.muvs.elementarium.thrift.player.DataPlayer));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataClientLoginApple, metaDataMap);
    }

    public function DataClientLoginApple() {
    }

    public function get status():int {
      return this._status;
    }

    public function set status(status:int):void {
      this._status = status;
      this.__isset_status = true;
    }

    public function unsetStatus():void {
      this.__isset_status = false;
    }

    // Returns true if field status is set (has been assigned a value) and false otherwise
    public function isSetStatus():Boolean {
      return this.__isset_status;
    }

    public function get player():ru.muvs.elementarium.thrift.player.DataPlayer {
      return this._player;
    }

    public function set player(player:ru.muvs.elementarium.thrift.player.DataPlayer):void {
      this._player = player;
    }

    public function unsetPlayer():void {
      this.player = null;
    }

    // Returns true if field player is set (has been assigned a value) and false otherwise
    public function isSetPlayer():Boolean {
      return this.player != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case STATUS:
        if (value == null) {
          unsetStatus();
        } else {
          this.status = value;
        }
        break;

      case PLAYER:
        if (value == null) {
          unsetPlayer();
        } else {
          this.player = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case STATUS:
        return this.status;
      case PLAYER:
        return this.player;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case STATUS:
        return isSetStatus();
      case PLAYER:
        return isSetPlayer();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case STATUS:
            if (field.type == TType.I32) {
              this.status = iprot.readI32();
              this.__isset_status = true;
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case PLAYER:
            if (field.type == TType.STRUCT) {
              this.player = new ru.muvs.elementarium.thrift.player.DataPlayer();
              this.player.read(iprot);
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      if (!__isset_status) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'status' was not found in serialized data! Struct: " + toString());
      }
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(STATUS_FIELD_DESC);
      oprot.writeI32(this.status);
      oprot.writeFieldEnd();
      if (this.player != null) {
        oprot.writeFieldBegin(PLAYER_FIELD_DESC);
        this.player.write(oprot);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataClientLoginApple(");
      var first:Boolean = true;

      ret += "status:";
      var status_name:String = DataClientLoginAppleStatus.VALUES_TO_NAMES[this.status];
      if (status_name != null) {
        ret += status_name;
        ret += " (";
      }
      ret += this.status;
      if (status_name != null) {
        ret += ")";
      }
      first = false;
      if (isSetPlayer()) {
        if (!first) ret +=  ", ";
        ret += "player:";
        if (this.player == null) {
          ret += "null";
        } else {
          ret += this.player;
        }
        first = false;
      }
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      // alas, we cannot check 'status' because it's a primitive and you chose the non-beans generator.
      // check that fields of type enum have valid values
      if (isSetStatus() && !DataClientLoginAppleStatus.VALID_VALUES.contains(status)){
        throw new TProtocolError(TProtocolError.UNKNOWN, "The field 'status' has been assigned the invalid value " + status);
      }
    }

  }

}
