/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.payment {

import org.apache.thrift.Set;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import org.apache.thrift.*;
import org.apache.thrift.meta_data.*;
import org.apache.thrift.protocol.*;


  public class DataServerPaymentApple implements TBase   {
    private static const STRUCT_DESC:TStruct = new TStruct("DataServerPaymentApple");
    private static const IN_APP_FIELD_DESC:TField = new TField("inApp", TType.STRING, 1);
    private static const RECEIPT_FIELD_DESC:TField = new TField("receipt", TType.STRING, 2);
    private static const TRANSACTION_ID_FIELD_DESC:TField = new TField("transactionId", TType.STRING, 3);

    private var _inApp:String;
    public static const INAPP:int = 1;
    private var _receipt:String;
    public static const RECEIPT:int = 2;
    private var _transactionId:String;
    public static const TRANSACTIONID:int = 3;


    public static const metaDataMap:Dictionary = new Dictionary();
    {
      metaDataMap[INAPP] = new FieldMetaData("inApp", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.STRING));
      metaDataMap[RECEIPT] = new FieldMetaData("receipt", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.STRING));
      metaDataMap[TRANSACTIONID] = new FieldMetaData("transactionId", TFieldRequirementType.REQUIRED, 
          new FieldValueMetaData(TType.STRING));
    }
    {
      FieldMetaData.addStructMetaDataMap(DataServerPaymentApple, metaDataMap);
    }

    public function DataServerPaymentApple() {
    }

    public function get inApp():String {
      return this._inApp;
    }

    public function set inApp(inApp:String):void {
      this._inApp = inApp;
    }

    public function unsetInApp():void {
      this.inApp = null;
    }

    // Returns true if field inApp is set (has been assigned a value) and false otherwise
    public function isSetInApp():Boolean {
      return this.inApp != null;
    }

    public function get receipt():String {
      return this._receipt;
    }

    public function set receipt(receipt:String):void {
      this._receipt = receipt;
    }

    public function unsetReceipt():void {
      this.receipt = null;
    }

    // Returns true if field receipt is set (has been assigned a value) and false otherwise
    public function isSetReceipt():Boolean {
      return this.receipt != null;
    }

    public function get transactionId():String {
      return this._transactionId;
    }

    public function set transactionId(transactionId:String):void {
      this._transactionId = transactionId;
    }

    public function unsetTransactionId():void {
      this.transactionId = null;
    }

    // Returns true if field transactionId is set (has been assigned a value) and false otherwise
    public function isSetTransactionId():Boolean {
      return this.transactionId != null;
    }

    public function setFieldValue(fieldID:int, value:*):void {
      switch (fieldID) {
      case INAPP:
        if (value == null) {
          unsetInApp();
        } else {
          this.inApp = value;
        }
        break;

      case RECEIPT:
        if (value == null) {
          unsetReceipt();
        } else {
          this.receipt = value;
        }
        break;

      case TRANSACTIONID:
        if (value == null) {
          unsetTransactionId();
        } else {
          this.transactionId = value;
        }
        break;

      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function getFieldValue(fieldID:int):* {
      switch (fieldID) {
      case INAPP:
        return this.inApp;
      case RECEIPT:
        return this.receipt;
      case TRANSACTIONID:
        return this.transactionId;
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    // Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise
    public function isSet(fieldID:int):Boolean {
      switch (fieldID) {
      case INAPP:
        return isSetInApp();
      case RECEIPT:
        return isSetReceipt();
      case TRANSACTIONID:
        return isSetTransactionId();
      default:
        throw new ArgumentError("Field " + fieldID + " doesn't exist!");
      }
    }

    public function read(iprot:TProtocol):void {
      var field:TField;
      iprot.readStructBegin();
      while (true)
      {
        field = iprot.readFieldBegin();
        if (field.type == TType.STOP) { 
          break;
        }
        switch (field.id)
        {
          case INAPP:
            if (field.type == TType.STRING) {
              this.inApp = iprot.readString();
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case RECEIPT:
            if (field.type == TType.STRING) {
              this.receipt = iprot.readString();
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          case TRANSACTIONID:
            if (field.type == TType.STRING) {
              this.transactionId = iprot.readString();
            } else { 
              TProtocolUtil.skip(iprot, field.type);
            }
            break;
          default:
            TProtocolUtil.skip(iprot, field.type);
            break;
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();


      // check for required fields of primitive type, which can't be checked in the validate method
      validate();
    }

    public function write(oprot:TProtocol):void {
      validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (this.inApp != null) {
        oprot.writeFieldBegin(IN_APP_FIELD_DESC);
        oprot.writeString(this.inApp);
        oprot.writeFieldEnd();
      }
      if (this.receipt != null) {
        oprot.writeFieldBegin(RECEIPT_FIELD_DESC);
        oprot.writeString(this.receipt);
        oprot.writeFieldEnd();
      }
      if (this.transactionId != null) {
        oprot.writeFieldBegin(TRANSACTION_ID_FIELD_DESC);
        oprot.writeString(this.transactionId);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

    public function toString():String {
      var ret:String = new String("DataServerPaymentApple(");
      var first:Boolean = true;

      ret += "inApp:";
      if (this.inApp == null) {
        ret += "null";
      } else {
        ret += this.inApp;
      }
      first = false;
      if (!first) ret +=  ", ";
      ret += "receipt:";
      if (this.receipt == null) {
        ret += "null";
      } else {
        ret += this.receipt;
      }
      first = false;
      if (!first) ret +=  ", ";
      ret += "transactionId:";
      if (this.transactionId == null) {
        ret += "null";
      } else {
        ret += this.transactionId;
      }
      first = false;
      ret += ")";
      return ret;
    }

    public function validate():void {
      // check for required fields
      if (inApp == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'inApp' was not present! Struct: " + toString());
      }
      if (receipt == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'receipt' was not present! Struct: " + toString());
      }
      if (transactionId == null) {
        throw new TProtocolError(TProtocolError.UNKNOWN, "Required field 'transactionId' was not present! Struct: " + toString());
      }
      // check that fields of type enum have valid values
    }

  }

}
