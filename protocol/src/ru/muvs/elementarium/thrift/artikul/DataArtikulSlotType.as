/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package ru.muvs.elementarium.thrift.artikul 
{
import org.apache.thrift.Set;
import flash.utils.Dictionary;
  public class DataArtikulSlotType   {
    public static const TALISMAN_LEFT:int = 1;
    public static const TALISMAN_RIGHT:int = 2;
    public static const HEAD:int = 3;
    public static const CHEST:int = 4;
    public static const BELT:int = 5;
    public static const SHOULDERS:int = 6;
    public static const HAND_LEFT:int = 7;
    public static const HAND_RIGHT:int = 8;
    public static const HAND_BOTH:int = 9;
    public static const BRACERS:int = 10;
    public static const GLOVES:int = 11;
    public static const LEGS:int = 12;
    public static const BOOTS:int = 13;
    public static const BAG:int = 14;
    public static const MINING:int = 15;

    public static const VALID_VALUES:Set = new Set(TALISMAN_LEFT, TALISMAN_RIGHT, HEAD, CHEST, BELT, SHOULDERS, HAND_LEFT, HAND_RIGHT, HAND_BOTH, BRACERS, GLOVES, LEGS, BOOTS, BAG, MINING);
    public static const VALUES_TO_NAMES:Dictionary = new Dictionary();
    {
      VALUES_TO_NAMES[TALISMAN_LEFT] = "TALISMAN_LEFT";
      VALUES_TO_NAMES[TALISMAN_RIGHT] = "TALISMAN_RIGHT";
      VALUES_TO_NAMES[HEAD] = "HEAD";
      VALUES_TO_NAMES[CHEST] = "CHEST";
      VALUES_TO_NAMES[BELT] = "BELT";
      VALUES_TO_NAMES[SHOULDERS] = "SHOULDERS";
      VALUES_TO_NAMES[HAND_LEFT] = "HAND_LEFT";
      VALUES_TO_NAMES[HAND_RIGHT] = "HAND_RIGHT";
      VALUES_TO_NAMES[HAND_BOTH] = "HAND_BOTH";
      VALUES_TO_NAMES[BRACERS] = "BRACERS";
      VALUES_TO_NAMES[GLOVES] = "GLOVES";
      VALUES_TO_NAMES[LEGS] = "LEGS";
      VALUES_TO_NAMES[BOOTS] = "BOOTS";
      VALUES_TO_NAMES[BAG] = "BAG";
      VALUES_TO_NAMES[MINING] = "MINING";

    }
  }
}
