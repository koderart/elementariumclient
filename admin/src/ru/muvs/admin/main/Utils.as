package ru.muvs.admin.main {

import flash.utils.ByteArray;

import ru.muvs.admin.main.vo.battle.battleenum.BuffEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionDamageEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionHealEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionOperandEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionOperatorEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactActionTargetEnum;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactConditionEnum;

import ru.muvs.admin.main.vo.enum.*;
import ru.muvs.admin.main.vo.enum.FractionEnum;

import spark.components.gridClasses.GridColumn;

public class Utils {

    public static function clone( source:Object ):*
    {
        var myBA:ByteArray = new ByteArray();
        myBA.writeObject( source );
        myBA.position = 0;
        return( myBA.readObject() );
    }

    public static function objectToClass( toClass:Class, obj:Object ):*
    {
        var item:* = new toClass();
        for (var prop:String in obj) {
            item[prop] = obj[prop];
        }
        return item;
    }

    public static function objectsToClassArray( toClass:Class, arr:Array ):Array
    {
        var resultArray:Array = [];
        
        for each (var obj:Object in arr) {
            resultArray.push(objectToClass(toClass, obj));
        }
        
        return resultArray;
    }
    
    public static function testFileName( str:String ):Boolean
    {
        var pattern:RegExp = /^[-_A-Za-z0-9]+$/g;
        return pattern.exec(str);
    }

    public static function colorLabel(obj:Object):String {
        return ColorEnum.LABELS[obj];
    }

    public static  function slotLabel(obj:Object):String {
        return SlotEnum.LABELS[obj];
    }

    public static function slotObjLabel(obj:Object, column:GridColumn):String {
        return SlotEnum.LABELS[obj.artikulSlot];
    }

    public static  function moneyLabel(obj:Object):String {
        return MoneyEnum.LABELS[obj];
    }

    public static function moneyObjLabel(obj:Object, column:GridColumn):String {
        return MoneyEnum.LABELS[obj.moneyType];
    }

    public static function elementLabel(obj:Object):String {
        return ElementEnum.LABELS[obj];
    }

    public static function arenaLabel(obj:Object):String {
        return ArenaEnum.LABELS[obj];
    }
    
    public static function arenaObjLabel(obj:Object):String {
        return ArenaEnum.LABELS[obj.arena];
    }

    public static function elementOppositeLabel(obj:Object):String {
        switch (obj) {
            case ElementEnum.DARK:
                return ElementEnum.LABELS[ElementEnum.NATURE];
                break;
            case ElementEnum.FIRE:
                return ElementEnum.LABELS[ElementEnum.LIGHT];
                break;
            case ElementEnum.LIGHT:
                return ElementEnum.LABELS[ElementEnum.DARK];
                break;
            case ElementEnum.NATURE:
                return ElementEnum.LABELS[ElementEnum.WATER];
                break;
            case ElementEnum.WATER:
                return ElementEnum.LABELS[ElementEnum.FIRE];
                break;
        }
        return ElementEnum.LABELS[obj];
    }

    public static function elementObjLabel(obj:Object, column:GridColumn):String {
        return ElementEnum.LABELS[obj.element];
    }

    public static function fractionLabel(obj:Object):String {
        return FractionEnum.LABELS[obj];
    }

    public static function fractionObjLabel(obj:Object, column:GridColumn):String {
        return FractionEnum.LABELS[obj.fraction];
    }

    public static function bonusActionLabel(obj:Object):String {
        return BonusActionEnum.LABELS[obj];
    }

    public static function questGoalLabel(obj:Object):String {
        return QuestGoalEnum.LABELS[obj];
    }

    public static function questGoalObjLabel(obj:Object, column:GridColumn):String {
        return QuestGoalEnum.LABELS[obj.GOAL];
    }

    public static function questLockLabel(obj:Object):String {
        return QuestLockEnum.LABELS[obj];
    }

    public static function questLockObjLabel(obj:Object, column:GridColumn):String {
        return QuestLockEnum.LABELS[obj.LOCK];
    }

    public static function achieveGoalLabel(obj:Object):String {
        return AchieveGoalEnum.LABELS[obj];
    }

    public static function achieveGoalObjLabel(obj:Object, column:GridColumn):String {
        return AchieveGoalEnum.LABELS[obj.GOAL];
    }

    public static function achieveLockLabel(obj:Object):String {
        return AchieveLockEnum.LABELS[obj];
    }

    public static function achieveLockObjLabel(obj:Object, column:GridColumn):String {
        return AchieveLockEnum.LABELS[obj.LOCK];
    }

    public static function challengeGoalLabel(obj:Object):String {
        return ChallengeGoalEnum.LABELS[obj];
    }

    public static function challengeGoalObjLabel(obj:Object, column:GridColumn):String {
        return ChallengeGoalEnum.LABELS[obj.GOAL];
    }

    public static function challengeLockLabel(obj:Object):String {
        return ChallengeLockEnum.LABELS[obj];
    }

    public static function challengeLockObjLabel(obj:Object, column:GridColumn):String {
        return ChallengeLockEnum.LABELS[obj.LOCK];
    }

    public static function bonusActionObjLabel(obj:Object, column:GridColumn):String {
        return BonusActionEnum.LABELS[obj.ACTION];
    }
    
    public static function bonusLockLabel(obj:Object):String {
        return BonusLockEnum.LABELS[obj];
    }

    public static function bonusLockObjLabel(obj:Object, column:GridColumn):String {
        return BonusLockEnum.LABELS[obj.LOCK];
    }

    public static function locationTeleportLockLabel(obj:Object):String {
        return LocationTeleportLockEnum.LABELS[obj];
    }

    public static function locationTeleportLockObjLabel(obj:Object, column:GridColumn):String {
        return LocationTeleportLockEnum.LABELS[obj.LOCK];
    }

    public static function locationPickupLockLabel(obj:Object):String {
        return LocationPickupLockEnum.LABELS[obj];
    }

    public static function locationPickupLockObjLabel(obj:Object, column:GridColumn):String {
        return LocationPickupLockEnum.LABELS[obj.LOCK];
    }

    public static function battleBuffLabel(obj:Object):String {
        return BuffEnum.LABELS[obj];
    }

    public static function battleBuffObjLabel(obj:Object, column:GridColumn):String {
        return BuffEnum.LABELS[obj.battleBuff];
    }

    public static function battleImpactConditionLabel(obj:Object):String {
        return ImpactConditionEnum.LABELS[obj];
    }

    public static function battleImpactConditionObjLabel(obj:Object, column:GridColumn):String {
        return ImpactConditionEnum.LABELS[obj.impactCondition];
    }

    public static function battleImpactActionLabel(obj:Object):String {
        return ImpactActionEnum.LABELS[obj];
    }

    public static function battleImpactActionObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionEnum.LABELS[obj.impactAction];
    }

    public static function battleImpactActionTargetLabel(obj:Object):String {
        return ImpactActionTargetEnum.LABELS[obj];
    }

    public static function battleImpactActionTargetObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionTargetEnum.LABELS[obj.actionTarget];
    }

    public static function battleImpactActionDamageLabel(obj:Object):String {
        return ImpactActionDamageEnum.LABELS[obj];
    }

    public static function battleImpactActionDamageObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionDamageEnum.LABELS[obj.damage];
    }

    public static function battleImpactActionHealLabel(obj:Object):String {
        return ImpactActionHealEnum.LABELS[obj];
    }

    public static function battleImpactActionHealObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionHealEnum.LABELS[obj.heal];
    }

    public static function battleImpactActionOperatorLabel(obj:Object):String {
        return ImpactActionOperatorEnum.LABELS[obj];
    }

    public static function battleImpactActionOperatorObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionOperatorEnum.LABELS[obj.actionOperator];
    }

    public static function battleImpactActionOperandLabel(obj:Object):String {
        return ImpactActionOperandEnum.LABELS[obj];
    }

    public static function battleImpactActionOperandObjLabel(obj:Object, column:GridColumn):String {
        return ImpactActionOperandEnum.LABELS[obj.actionOperand];
    }

    public static function locationMemberLabel(obj:Object):String {
        return LocationMemberEnum.LABELS[obj];
    }

    public static function locationMemberObjLabel(obj:Object, column:GridColumn):String {
        return LocationMemberEnum.LABELS[obj.locationMember];
    }

    public static function locationDecorationLabel(obj:Object):String {
        return LocationDecorationEnum.LABELS[obj];
    }

    public static function locationDecorationObjLabel(obj:Object, column:GridColumn):String {
        return LocationDecorationEnum.LABELS[obj.locationMember];
    }

    public static function locationBackgroundLabel(obj:Object):String {
        return LocationBackgroundEnum.LABELS[obj];
    }

    public static function locationBackgroundObjLabel(obj:Object, column:GridColumn):String {
        return LocationBackgroundEnum.LABELS[obj.background];
    }

    public static function locationLayerLabel(obj:Object):String {
        return LocationLayerEnum.LABELS[obj];
    }

    public static function locationLayerObjLabel(obj:Object, column:GridColumn):String {
        return LocationLayerEnum.LABELS[obj.layer];
    }

    public static function objTitleLabel(obj:Object):String {
        return obj.title.ru;
    }

    public static function levelLabel(obj:Object):String {
        if (obj.levelId) {
            return obj.number;
        } else {
            return "";
        }
    }

    public static function levelObjLabel(obj:Object, column:GridColumn):String {
        if (obj.level && obj.level.levelId) {
            return obj.level.number;
        } else {
            return "";
        }
    }


}
}
