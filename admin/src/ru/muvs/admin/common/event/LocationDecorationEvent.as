package ru.muvs.admin.common.event
{
	import flash.events.Event;

    public class LocationDecorationEvent extends Event
	{
		public static const DELETE:String = "LocationDecorationEvent.DELETE";
		public static const EDIT:String = "LocationDecorationEvent.EDIT";
		
		public function LocationDecorationEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}