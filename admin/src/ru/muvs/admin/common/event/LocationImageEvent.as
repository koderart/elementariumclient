package ru.muvs.admin.common.event
{
	import flash.events.Event;

    public class LocationImageEvent extends Event
	{
		public static const DELETE:String = "LocationImageEvent.DELETE";
		
		public function LocationImageEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}