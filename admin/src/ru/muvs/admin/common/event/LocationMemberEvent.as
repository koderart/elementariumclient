package ru.muvs.admin.common.event
{
	import flash.events.Event;

    public class LocationMemberEvent extends Event
	{
		public static const DELETE:String = "LocationMemberEvent.DELETE";
		public static const EDIT:String = "LocationMemberEvent.EDIT";
		
		public function LocationMemberEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}