﻿package utils
{
    import com.junkbyte.console.Cc;

    import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.Loader;
    import flash.display.Sprite;
    import flash.events.IOErrorEvent;
    import flash.geom.Point;
	import flash.geom.Rectangle;	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	public class SpritesheetBlitter extends Sprite
	{
		private var _spritesheetImageLoader:Loader;
		private var _spritesheetBitmap:Bitmap;
		private var _spritesheetImage:String;

		private var _animDescFileLoader:URLLoader;
		private var _animDesc:XML;
		private var _animDescFile:String;
		
		private var _currFrame:uint;
		private var _timestampNextFrame:Number;

        private var _modelBitmap:Bitmap;

        public function SpritesheetBlitter(url:String) 
		{
			_spritesheetImage = url+".png";
			_animDescFile = url+".xml";
			
			_spritesheetImageLoader = new Loader();
			_spritesheetImageLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, handleImageLoaded);
			_spritesheetImageLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorLoaderHandler);
			_spritesheetImageLoader.load(new URLRequest(_spritesheetImage));
		}

		
		public function setCurrentSequence(timeStamp:Number):void
		{
            _currFrame = 0;
            _timestampNextFrame = timeStamp;
            getNextFrameUpdateTimestamp();
		}
		
		private function getNextFrameUpdateTimestamp():void
		{
			_timestampNextFrame = _timestampNextFrame + 50;
		}
		
		public function drawCurrFrame(canvasBitmapData:BitmapData, timeStamp:Number):void	
		{
            if (timeStamp > _timestampNextFrame)
            {
                var _x:Number = int(_animDesc.SubTexture[_currFrame].@x);
                var _y:Number = int(_animDesc.SubTexture[_currFrame].@y);
                var _width:Number = int(_animDesc.SubTexture[_currFrame].@width);
                var _height:Number = int(_animDesc.SubTexture[_currFrame].@height);

                var _frameX:Number = int(_animDesc.SubTexture[_currFrame].@frameX);
                var _frameY:Number = int(_animDesc.SubTexture[_currFrame].@frameY);
                var _frameWidth:Number = int(_animDesc.SubTexture[_currFrame].@frameWidth);
                var _frameHeight:Number = int(_animDesc.SubTexture[_currFrame].@frameHeight);

                if (!_frameWidth) _frameWidth = _width;
                if (!_frameHeight) _frameHeight = _height;
                
                var destPoint:Point = new Point(-1*_frameX,-1*_frameY);
                var frameRect:Rectangle = new Rectangle(_x, _y, _width, _height);

                canvasBitmapData.fillRect(new Rectangle(0, 0, _frameWidth, _frameHeight), 0x00FFFFFF);
                canvasBitmapData.copyPixels(_spritesheetBitmap.bitmapData, frameRect, destPoint ,null);
                
                findNextFrame(timeStamp);
            }
		}
		
		private function findNextFrame(timeStamp:Number):void
		{
			var frameSkipCount:uint = 0;
			do
			{
				advanceOneFrame(timeStamp);
				frameSkipCount++;
			}
			while(timeStamp > _timestampNextFrame)
		}
		
		private function advanceOneFrame(timeStamp:Number):void
		{
			_currFrame++;
			if ( _currFrame >=  _animDesc.SubTexture.length())
			{
                _currFrame = 0;
			}
			getNextFrameUpdateTimestamp();
		}

		private function handleImageLoaded(e:Event):void 
		{
			e.target.removeEventListener(Event.COMPLETE, handleImageLoaded);
            
			_spritesheetBitmap = _spritesheetImageLoader.content as Bitmap;
			
			_animDescFileLoader = new URLLoader();
			_animDescFileLoader.load(new URLRequest(_animDescFile));
			_animDescFileLoader.addEventListener(Event.COMPLETE, handleAnimDescLoaded);
            _animDescFileLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorLoaderHandler);
		}
		
		private function handleAnimDescLoaded(e:Event):void 
		{
			e.target.removeEventListener(Event.COMPLETE, handleAnimDescLoaded);
            
			_animDesc = new XML(_animDescFileLoader.data);
            
            var w:int = (_animDesc.SubTexture[0].@frameWidth);
            if (!w) w = _animDesc.SubTexture[0].@width;
            var h:int = (_animDesc.SubTexture[0].@frameHeight);
            if (!h) h = _animDesc.SubTexture[0].@height;
            
            var modelBitmapData:BitmapData = new BitmapData(w, h, true, 0);
            _modelBitmap = new Bitmap(modelBitmapData);
            addChild(_modelBitmap);

            setCurrentSequence(new Date().getTime());
            stage.addEventListener(Event.ENTER_FRAME, handleEnterFrame);

        }

        private function ioErrorLoaderHandler(e:IOErrorEvent):void
        {
            this.graphics.clear();
            graphics.beginFill(0xFF0000, 0.5);
            graphics.drawRect(0, 0, 100, 100);
            graphics.endFill();
        }
        
        private function handleEnterFrame(e:Event):void
        {
            drawCurrFrame(_modelBitmap.bitmapData, new Date().getTime());
        }
    }

}