<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.0</string>
        <key>fileName</key>
        <string>C:/PRG/Elementarium2/spritesheets_src/common/common.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>x1</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>2048</int>
                    <key>height</key>
                    <int>2048</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>2</double>
                <key>extension</key>
                <string>x2</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>2048</int>
                    <key>height</key>
                    <int>2048</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>../../webclient/assets/textures/{v}/common_{n}.atf</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">atf</enum>
        <key>borderPadding</key>
        <uint>1</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../webclient/assets/textures/{v}/common_{n}.xml</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ReduceBorderArtifacts</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">achievement/achieve_paperback.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>137,25,273,51</rect>
                <key>scale9Paddings</key>
                <rect>137,25,273,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">achievement/achieve_reward_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,25,101,49</rect>
                <key>scale9Paddings</key>
                <rect>51,25,101,49</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">arena/1vs1.png</key>
            <key type="filename">arena/2vs2.png</key>
            <key type="filename">arena/3vs3.png</key>
            <key type="filename">arena/4vs4.png</key>
            <key type="filename">arena/5vs5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,28,56,56</rect>
                <key>scale9Paddings</key>
                <rect>28,28,56,56</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/arrow/left1.png</key>
            <key type="filename">button/arrow/right1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,8,11</rect>
                <key>scale9Paddings</key>
                <rect>4,5,8,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/arrow/left2.png</key>
            <key type="filename">button/arrow/right2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,10,11</rect>
                <key>scale9Paddings</key>
                <rect>5,5,10,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/big/over_center.png</key>
            <key type="filename">button/big/up_center.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,11,9,23</rect>
                <key>scale9Paddings</key>
                <rect>5,11,9,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/big/over_left.png</key>
            <key type="filename">button/big/over_right.png</key>
            <key type="filename">button/big/up_left.png</key>
            <key type="filename">button/big/up_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,11,8,23</rect>
                <key>scale9Paddings</key>
                <rect>4,11,8,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/checkbox/disabled.png</key>
            <key type="filename">button/checkbox/down.png</key>
            <key type="filename">button/checkbox/over.png</key>
            <key type="filename">button/checkbox/up.png</key>
            <key type="filename">window/text_back.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/checkbox/selected_disabled.png</key>
            <key type="filename">button/checkbox/selected_down.png</key>
            <key type="filename">button/checkbox/selected_over.png</key>
            <key type="filename">button/checkbox/selected_up.png</key>
            <key type="filename">scrollbar/v_decrement.png</key>
            <key type="filename">scrollbar/v_increment.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,9,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,9,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/circle/cancel.png</key>
            <key type="filename">button/circle/ok.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,27,27</rect>
                <key>scale9Paddings</key>
                <rect>13,13,27,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/common/close.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,6,13,13</rect>
                <key>scale9Paddings</key>
                <rect>7,6,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/common/disabled_bg.png</key>
            <key type="filename">button/common/down_bg.png</key>
            <key type="filename">button/common/over_bg.png</key>
            <key type="filename">button/common/up_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,5,23,11</rect>
                <key>scale9Paddings</key>
                <rect>11,5,23,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/common/frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,13,13</rect>
                <key>scale9Paddings</key>
                <rect>7,7,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">button/common2/over.png</key>
            <key type="filename">button/common2/up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,7,13,14</rect>
                <key>scale9Paddings</key>
                <rect>7,7,13,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">chat/minimize_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,9,9</rect>
                <key>scale9Paddings</key>
                <rect>4,4,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">chat/new_mesage.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,4,10,8</rect>
                <key>scale9Paddings</key>
                <rect>5,4,10,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">chat/send_button.png</key>
            <key type="filename">chat/send_down_button.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,11,11</rect>
                <key>scale9Paddings</key>
                <rect>5,5,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">chat/text_frame.png</key>
            <key type="filename">window/inner_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,11</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">currency/coin_icon.png</key>
            <key type="filename">currency/exp_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,7</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">currency/coin_icon_big.png</key>
            <key type="filename">currency/crystal_icon_big.png</key>
            <key type="filename">currency/exp_icon_big.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,12,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,12,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">currency/crystal_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">elements/big/dark.png</key>
            <key type="filename">elements/big/fire.png</key>
            <key type="filename">elements/big/light.png</key>
            <key type="filename">elements/big/nature.png</key>
            <key type="filename">elements/big/water.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,14,27,28</rect>
                <key>scale9Paddings</key>
                <rect>13,14,27,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">elements/small/dark.png</key>
            <key type="filename">elements/small/fire.png</key>
            <key type="filename">elements/small/light.png</key>
            <key type="filename">elements/small/nature.png</key>
            <key type="filename">elements/small/water.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,5,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,5,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/stats_button.png</key>
            <key type="filename">inventory/stats_selected_button.png</key>
            <key type="filename">match3/battle_log_frame.png</key>
            <key type="filename">match3/battle_select_frame.png</key>
            <key type="filename">repair/item_lighting.png</key>
            <key type="filename">tooltipspell/background.png</key>
            <key type="filename">window/background_border3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tooltip_back.png</key>
            <key type="filename">window/background3.png</key>
            <key type="filename">window/background4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,18,35,35</rect>
                <key>scale9Paddings</key>
                <rect>18,18,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tooltip_frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,26,51,52</rect>
                <key>scale9Paddings</key>
                <rect>26,26,51,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tooltip_item_frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,34,34</rect>
                <key>scale9Paddings</key>
                <rect>17,17,34,34</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tooltip_itemrarity.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>57,3,114,6</rect>
                <key>scale9Paddings</key>
                <rect>57,3,114,6</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">location/ghost_green.png</key>
            <key type="filename">location/ghost_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,51,73,102</rect>
                <key>scale9Paddings</key>
                <rect>36,51,73,102</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">location/walk_marker_01.png</key>
            <key type="filename">location/walk_marker_02.png</key>
            <key type="filename">location/walk_marker_03.png</key>
            <key type="filename">location/walk_marker_04.png</key>
            <key type="filename">location/walk_marker_05.png</key>
            <key type="filename">location/walk_marker_06.png</key>
            <key type="filename">location/walk_marker_07.png</key>
            <key type="filename">location/walk_marker_08.png</key>
            <key type="filename">location/walk_marker_09.png</key>
            <key type="filename">location/walk_marker_10.png</key>
            <key type="filename">location/walk_marker_11.png</key>
            <key type="filename">location/walk_marker_12.png</key>
            <key type="filename">location/walk_marker_13.png</key>
            <key type="filename">location/walk_marker_14.png</key>
            <key type="filename">location/walk_marker_15.png</key>
            <key type="filename">location/walk_marker_16.png</key>
            <key type="filename">location/walk_marker_17.png</key>
            <key type="filename">location/walk_marker_18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,13,33,26</rect>
                <key>scale9Paddings</key>
                <rect>16,13,33,26</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">location/way_point.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,12,34,24</rect>
                <key>scale9Paddings</key>
                <rect>17,12,34,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">mainmenu/fullscreen.png</key>
            <key type="filename">mainmenu/fullscreen_down.png</key>
            <key type="filename">mainmenu/fullscreen_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,5,12,10</rect>
                <key>scale9Paddings</key>
                <rect>6,5,12,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/gems/gem_1.png</key>
            <key type="filename">match3/gems/gem_2.png</key>
            <key type="filename">match3/gems/gem_3.png</key>
            <key type="filename">match3/gems/gem_4.png</key>
            <key type="filename">match3/gems/gem_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,17,35,35</rect>
                <key>scale9Paddings</key>
                <rect>17,17,35,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/stats_info_battle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>55,54,109,108</rect>
                <key>scale9Paddings</key>
                <rect>55,54,109,108</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">particle/blue_star_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">payment/backlight_select.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,27,100,53</rect>
                <key>scale9Paddings</key>
                <rect>50,27,100,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">payment/crystal_lot_1.png</key>
            <key type="filename">payment/crystal_lot_2.png</key>
            <key type="filename">payment/crystal_lot_3.png</key>
            <key type="filename">payment/money_lot_1.png</key>
            <key type="filename">payment/money_lot_2.png</key>
            <key type="filename">payment/money_lot_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,30,105,60</rect>
                <key>scale9Paddings</key>
                <rect>53,30,105,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">payment/flag_green.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,9,26,18</rect>
                <key>scale9Paddings</key>
                <rect>13,9,26,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">quest/ready_marker.png</key>
            <key type="filename">quest/taken_marker.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,5,7,11</rect>
                <key>scale9Paddings</key>
                <rect>3,5,7,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">registration/spell_frame.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,29</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">repair/anvil.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,19,70,39</rect>
                <key>scale9Paddings</key>
                <rect>35,19,70,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">repair/price_slot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,7,58,15</rect>
                <key>scale9Paddings</key>
                <rect>29,7,58,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scrollbar/h_decrement.png</key>
            <key type="filename">scrollbar/h_increment.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scrollbar/h_track.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,4,18,9</rect>
                <key>scale9Paddings</key>
                <rect>9,4,18,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scrollbar/h_tumb.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,2,7,4</rect>
                <key>scale9Paddings</key>
                <rect>4,2,7,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scrollbar/v_track.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,9,9,18</rect>
                <key>scale9Paddings</key>
                <rect>4,9,9,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scrollbar/v_tumb.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,4,4,7</rect>
                <key>scale9Paddings</key>
                <rect>2,4,4,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">textinput/input_field.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>7,9,13,19</rect>
                <key>scale9Paddings</key>
                <rect>7,9,13,19</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tooltipspell/blue_mark.png</key>
            <key type="filename">tooltipspell/gray_mark.png</key>
            <key type="filename">tooltipspell/green_mark.png</key>
            <key type="filename">tooltipspell/ready_mark.png</key>
            <key type="filename">tooltipspell/red_mark.png</key>
            <key type="filename">tooltipspell/violet_mark.png</key>
            <key type="filename">tooltipspell/yellow_mark.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,7,7</rect>
                <key>scale9Paddings</key>
                <rect>3,3,7,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,34,68,68</rect>
                <key>scale9Paddings</key>
                <rect>34,34,68,68</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,60,119,119</rect>
                <key>scale9Paddings</key>
                <rect>60,60,119,119</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,10,12</rect>
                <key>scale9Paddings</key>
                <rect>5,6,10,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background_border2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,10,10</rect>
                <key>scale9Paddings</key>
                <rect>5,5,10,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background_dark_gradient.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>73,73,146,146</rect>
                <key>scale9Paddings</key>
                <rect>73,73,146,146</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background_header_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,10,10,20</rect>
                <key>scale9Paddings</key>
                <rect>5,10,10,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/border_down.png</key>
            <key type="filename">window/border_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,2,1,4</rect>
                <key>scale9Paddings</key>
                <rect>1,2,1,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/border_left.png</key>
            <key type="filename">window/border_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,1,4,1</rect>
                <key>scale9Paddings</key>
                <rect>2,1,4,1</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/decor_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,22,38,43</rect>
                <key>scale9Paddings</key>
                <rect>19,22,38,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_boder_left.png</key>
            <key type="filename">window/header_boder_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,6,5,12</rect>
                <key>scale9Paddings</key>
                <rect>2,6,5,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_boder_middle.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,6,12</rect>
                <key>scale9Paddings</key>
                <rect>3,6,6,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_gradient2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,8,2,17</rect>
                <key>scale9Paddings</key>
                <rect>1,8,2,17</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_texture.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,5,5,11</rect>
                <key>scale9Paddings</key>
                <rect>3,5,5,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_texture2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,6,10,13</rect>
                <key>scale9Paddings</key>
                <rect>5,6,10,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_texture3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,5,40,10</rect>
                <key>scale9Paddings</key>
                <rect>20,5,40,10</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header_texture_blue.png</key>
            <key type="filename">window/header_texture_gray.png</key>
            <key type="filename">window/header_texture_green.png</key>
            <key type="filename">window/header_texture_orange.png</key>
            <key type="filename">window/header_texture_purple.png</key>
            <key type="filename">window/header_texture_red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,6,5,11</rect>
                <key>scale9Paddings</key>
                <rect>3,6,5,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/horizontal_separator.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>29,3,57,7</rect>
                <key>scale9Paddings</key>
                <rect>29,3,57,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/horizontal_separator2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,3,67,7</rect>
                <key>scale9Paddings</key>
                <rect>34,3,67,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/left_down_frame_corner.png</key>
            <key type="filename">window/right_up_frame_corner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,17,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,17,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/left_up_frame_corner.png</key>
            <key type="filename">window/npc_avatar_frame.png</key>
            <key type="filename">window/right_down_frame_corner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/light_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,5,5</rect>
                <key>scale9Paddings</key>
                <rect>3,3,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/light_border_with_header.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,11,11,21</rect>
                <key>scale9Paddings</key>
                <rect>6,11,11,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/main_board_corner.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/main_board_tile_bottom.png</key>
            <key type="filename">window/main_board_tile_top.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,4,3,8</rect>
                <key>scale9Paddings</key>
                <rect>1,4,3,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/main_board_tile_left.png</key>
            <key type="filename">window/main_board_tile_right.png</key>
            <key type="filename">window/title_bottom_line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,1,8,2</rect>
                <key>scale9Paddings</key>
                <rect>4,1,8,2</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/one_pixel_border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>2,2,4,4</rect>
                <key>scale9Paddings</key>
                <rect>2,2,4,4</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/separator_left.png</key>
            <key type="filename">window/separator_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,4,29,8</rect>
                <key>scale9Paddings</key>
                <rect>14,4,29,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/title_defeat.jpg</key>
            <key type="filename">window/title_level.jpg</key>
            <key type="filename">window/title_win.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>91,26,182,52</rect>
                <key>scale9Paddings</key>
                <rect>91,26,182,52</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/vertical_separator.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,34,7,67</rect>
                <key>scale9Paddings</key>
                <rect>3,34,7,67</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/yes_mark.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,11,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,11,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
