<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.2.3</string>
        <key>fileName</key>
        <string>C:/PRG/Elementarium2/spritesheets_src/gems_bang/gems_bang.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>x1</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>2048</int>
                    <key>height</key>
                    <int>2048</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>2</double>
                <key>extension</key>
                <string>x2</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>2048</int>
                    <key>height</key>
                    <int>2048</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename>../../webclient/assets/textures/{v}/gems_bang_{n}.atf</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">atf</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../webclient/assets/textures/{v}/gems_bang_{n}.xml</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">DXT5</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">gem_blue_00.png</key>
            <key type="filename">gem_blue_01.png</key>
            <key type="filename">gem_blue_02.png</key>
            <key type="filename">gem_blue_03.png</key>
            <key type="filename">gem_blue_04.png</key>
            <key type="filename">gem_blue_05.png</key>
            <key type="filename">gem_blue_06.png</key>
            <key type="filename">gem_blue_07.png</key>
            <key type="filename">gem_blue_08.png</key>
            <key type="filename">gem_blue_09.png</key>
            <key type="filename">gem_blue_10.png</key>
            <key type="filename">gem_blue_11.png</key>
            <key type="filename">gem_blue_12.png</key>
            <key type="filename">gem_blue_13.png</key>
            <key type="filename">gem_blue_14.png</key>
            <key type="filename">gem_blue_15.png</key>
            <key type="filename">gem_blue_16.png</key>
            <key type="filename">gem_blue_17.png</key>
            <key type="filename">gem_blue_18.png</key>
            <key type="filename">gem_blue_19.png</key>
            <key type="filename">gem_blue_20.png</key>
            <key type="filename">gem_blue_21.png</key>
            <key type="filename">gem_blue_22.png</key>
            <key type="filename">gem_blue_23.png</key>
            <key type="filename">gem_blue_24.png</key>
            <key type="filename">gem_blue_25.png</key>
            <key type="filename">gem_blue_26.png</key>
            <key type="filename">gem_blue_27.png</key>
            <key type="filename">gem_blue_28.png</key>
            <key type="filename">gem_blue_29.png</key>
            <key type="filename">gem_blue_30.png</key>
            <key type="filename">gem_blue_31.png</key>
            <key type="filename">gem_blue_32.png</key>
            <key type="filename">gem_blue_33.png</key>
            <key type="filename">gem_blue_34.png</key>
            <key type="filename">gem_blue_35.png</key>
            <key type="filename">gem_green_00.png</key>
            <key type="filename">gem_green_01.png</key>
            <key type="filename">gem_green_02.png</key>
            <key type="filename">gem_green_03.png</key>
            <key type="filename">gem_green_04.png</key>
            <key type="filename">gem_green_05.png</key>
            <key type="filename">gem_green_06.png</key>
            <key type="filename">gem_green_07.png</key>
            <key type="filename">gem_green_08.png</key>
            <key type="filename">gem_green_09.png</key>
            <key type="filename">gem_green_10.png</key>
            <key type="filename">gem_green_11.png</key>
            <key type="filename">gem_green_12.png</key>
            <key type="filename">gem_green_13.png</key>
            <key type="filename">gem_green_14.png</key>
            <key type="filename">gem_green_15.png</key>
            <key type="filename">gem_green_16.png</key>
            <key type="filename">gem_green_17.png</key>
            <key type="filename">gem_green_18.png</key>
            <key type="filename">gem_green_19.png</key>
            <key type="filename">gem_green_20.png</key>
            <key type="filename">gem_green_21.png</key>
            <key type="filename">gem_green_22.png</key>
            <key type="filename">gem_green_23.png</key>
            <key type="filename">gem_green_24.png</key>
            <key type="filename">gem_green_25.png</key>
            <key type="filename">gem_green_26.png</key>
            <key type="filename">gem_green_27.png</key>
            <key type="filename">gem_green_28.png</key>
            <key type="filename">gem_green_29.png</key>
            <key type="filename">gem_green_30.png</key>
            <key type="filename">gem_green_31.png</key>
            <key type="filename">gem_green_32.png</key>
            <key type="filename">gem_green_33.png</key>
            <key type="filename">gem_green_34.png</key>
            <key type="filename">gem_green_35.png</key>
            <key type="filename">gem_light/1.png</key>
            <key type="filename">gem_light/2.png</key>
            <key type="filename">gem_light/3.png</key>
            <key type="filename">gem_light/4.png</key>
            <key type="filename">gem_light/5.png</key>
            <key type="filename">gem_light/6.png</key>
            <key type="filename">gem_light/7.png</key>
            <key type="filename">gem_light/8.png</key>
            <key type="filename">gem_light/9.png</key>
            <key type="filename">gem_purple_00.png</key>
            <key type="filename">gem_purple_01.png</key>
            <key type="filename">gem_purple_02.png</key>
            <key type="filename">gem_purple_03.png</key>
            <key type="filename">gem_purple_04.png</key>
            <key type="filename">gem_purple_05.png</key>
            <key type="filename">gem_purple_06.png</key>
            <key type="filename">gem_purple_07.png</key>
            <key type="filename">gem_purple_08.png</key>
            <key type="filename">gem_purple_09.png</key>
            <key type="filename">gem_purple_10.png</key>
            <key type="filename">gem_purple_11.png</key>
            <key type="filename">gem_purple_12.png</key>
            <key type="filename">gem_purple_13.png</key>
            <key type="filename">gem_purple_14.png</key>
            <key type="filename">gem_purple_15.png</key>
            <key type="filename">gem_purple_16.png</key>
            <key type="filename">gem_purple_17.png</key>
            <key type="filename">gem_purple_18.png</key>
            <key type="filename">gem_purple_19.png</key>
            <key type="filename">gem_purple_20.png</key>
            <key type="filename">gem_purple_21.png</key>
            <key type="filename">gem_purple_22.png</key>
            <key type="filename">gem_purple_23.png</key>
            <key type="filename">gem_purple_24.png</key>
            <key type="filename">gem_purple_25.png</key>
            <key type="filename">gem_purple_26.png</key>
            <key type="filename">gem_purple_27.png</key>
            <key type="filename">gem_purple_28.png</key>
            <key type="filename">gem_purple_29.png</key>
            <key type="filename">gem_purple_30.png</key>
            <key type="filename">gem_purple_31.png</key>
            <key type="filename">gem_purple_32.png</key>
            <key type="filename">gem_purple_33.png</key>
            <key type="filename">gem_purple_34.png</key>
            <key type="filename">gem_purple_35.png</key>
            <key type="filename">gem_red_00.png</key>
            <key type="filename">gem_red_01.png</key>
            <key type="filename">gem_red_02.png</key>
            <key type="filename">gem_red_03.png</key>
            <key type="filename">gem_red_04.png</key>
            <key type="filename">gem_red_05.png</key>
            <key type="filename">gem_red_06.png</key>
            <key type="filename">gem_red_07.png</key>
            <key type="filename">gem_red_08.png</key>
            <key type="filename">gem_red_09.png</key>
            <key type="filename">gem_red_10.png</key>
            <key type="filename">gem_red_11.png</key>
            <key type="filename">gem_red_12.png</key>
            <key type="filename">gem_red_13.png</key>
            <key type="filename">gem_red_14.png</key>
            <key type="filename">gem_red_15.png</key>
            <key type="filename">gem_red_16.png</key>
            <key type="filename">gem_red_17.png</key>
            <key type="filename">gem_red_18.png</key>
            <key type="filename">gem_red_19.png</key>
            <key type="filename">gem_red_20.png</key>
            <key type="filename">gem_red_21.png</key>
            <key type="filename">gem_red_22.png</key>
            <key type="filename">gem_red_23.png</key>
            <key type="filename">gem_red_24.png</key>
            <key type="filename">gem_red_25.png</key>
            <key type="filename">gem_red_26.png</key>
            <key type="filename">gem_red_27.png</key>
            <key type="filename">gem_red_28.png</key>
            <key type="filename">gem_red_29.png</key>
            <key type="filename">gem_red_30.png</key>
            <key type="filename">gem_red_31.png</key>
            <key type="filename">gem_red_32.png</key>
            <key type="filename">gem_red_33.png</key>
            <key type="filename">gem_red_34.png</key>
            <key type="filename">gem_red_35.png</key>
            <key type="filename">gem_yellow_00.png</key>
            <key type="filename">gem_yellow_01.png</key>
            <key type="filename">gem_yellow_02.png</key>
            <key type="filename">gem_yellow_03.png</key>
            <key type="filename">gem_yellow_04.png</key>
            <key type="filename">gem_yellow_05.png</key>
            <key type="filename">gem_yellow_06.png</key>
            <key type="filename">gem_yellow_07.png</key>
            <key type="filename">gem_yellow_08.png</key>
            <key type="filename">gem_yellow_09.png</key>
            <key type="filename">gem_yellow_10.png</key>
            <key type="filename">gem_yellow_11.png</key>
            <key type="filename">gem_yellow_12.png</key>
            <key type="filename">gem_yellow_13.png</key>
            <key type="filename">gem_yellow_14.png</key>
            <key type="filename">gem_yellow_15.png</key>
            <key type="filename">gem_yellow_16.png</key>
            <key type="filename">gem_yellow_17.png</key>
            <key type="filename">gem_yellow_18.png</key>
            <key type="filename">gem_yellow_19.png</key>
            <key type="filename">gem_yellow_20.png</key>
            <key type="filename">gem_yellow_21.png</key>
            <key type="filename">gem_yellow_22.png</key>
            <key type="filename">gem_yellow_23.png</key>
            <key type="filename">gem_yellow_24.png</key>
            <key type="filename">gem_yellow_25.png</key>
            <key type="filename">gem_yellow_26.png</key>
            <key type="filename">gem_yellow_27.png</key>
            <key type="filename">gem_yellow_28.png</key>
            <key type="filename">gem_yellow_29.png</key>
            <key type="filename">gem_yellow_30.png</key>
            <key type="filename">gem_yellow_31.png</key>
            <key type="filename">gem_yellow_32.png</key>
            <key type="filename">gem_yellow_33.png</key>
            <key type="filename">gem_yellow_34.png</key>
            <key type="filename">gem_yellow_35.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
    </struct>
</data>
