<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.3.1</string>
        <key>fileName</key>
        <string>C:/PRG/Elementarium2/spritesheets_src/common2/common2.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.8</double>
                <key>extension</key>
                <string>x2</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.4</double>
                <key>extension</key>
                <string>x1</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>sparrow</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">atf</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../webclient/assets/textures/{v}/common2_{n}.xml</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">avatar/circle/female_dark.png</key>
            <key type="filename">avatar/circle/female_fire.png</key>
            <key type="filename">avatar/circle/female_light.png</key>
            <key type="filename">avatar/circle/female_nature.png</key>
            <key type="filename">avatar/circle/female_water.png</key>
            <key type="filename">avatar/circle/male_dark.png</key>
            <key type="filename">avatar/circle/male_fire.png</key>
            <key type="filename">avatar/circle/male_light.png</key>
            <key type="filename">avatar/circle/male_nature.png</key>
            <key type="filename">avatar/circle/male_water.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,54,109,109</rect>
                <key>scale9Paddings</key>
                <rect>54,54,109,109</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/arrow_left_down.png</key>
            <key type="filename">buttons/arrow_left_up.png</key>
            <key type="filename">buttons/arrow_right_down.png</key>
            <key type="filename">buttons/arrow_right_up.png</key>
            <key type="filename">inventory/alert/button/left_down.png</key>
            <key type="filename">inventory/alert/button/left_up.png</key>
            <key type="filename">inventory/alert/button/repair_down.png</key>
            <key type="filename">inventory/alert/button/repair_up.png</key>
            <key type="filename">inventory/alert/button/sell_down.png</key>
            <key type="filename">inventory/alert/button/sell_up.png</key>
            <key type="filename">inventory/alert/button/trash_down.png</key>
            <key type="filename">inventory/alert/button/trash_up.png</key>
            <key type="filename">inventory/alert/button/use_down.png</key>
            <key type="filename">inventory/alert/button/use_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,27,53,54</rect>
                <key>scale9Paddings</key>
                <rect>26,27,53,54</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/blue_down_bg.png</key>
            <key type="filename">buttons/blue_up_bg.png</key>
            <key type="filename">buttons/pink_down_bg.png</key>
            <key type="filename">buttons/pink_up_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,20</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,20</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/checkbox.png</key>
            <key type="filename">buttons/checkbox_selected.png</key>
            <key type="filename">buttons/icon_check_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,15,32,29</rect>
                <key>scale9Paddings</key>
                <rect>16,15,32,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/exit_down.png</key>
            <key type="filename">buttons/exit_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,24,48,48</rect>
                <key>scale9Paddings</key>
                <rect>24,24,48,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/icon_check_off.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,30,30</rect>
                <key>scale9Paddings</key>
                <rect>15,15,30,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/minus.png</key>
            <key type="filename">buttons/plus.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,36,73,73</rect>
                <key>scale9Paddings</key>
                <rect>36,36,73,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">fraction/border.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,33,33</rect>
                <key>scale9Paddings</key>
                <rect>16,16,33,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">fraction/border_light.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>94,119,188,238</rect>
                <key>scale9Paddings</key>
                <rect>94,119,188,238</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">fraction/fraction1_off.png</key>
            <key type="filename">fraction/fraction1_on.png</key>
            <key type="filename">fraction/fraction2_off.png</key>
            <key type="filename">fraction/fraction2_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>125,125,250,250</rect>
                <key>scale9Paddings</key>
                <rect>125,125,250,250</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/alert/background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>44,44,87,87</rect>
                <key>scale9Paddings</key>
                <rect>44,44,87,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/alert/button_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,30,56,59</rect>
                <key>scale9Paddings</key>
                <rect>28,30,56,59</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/alert/element/dark.png</key>
            <key type="filename">inventory/alert/element/fire.png</key>
            <key type="filename">inventory/alert/element/light.png</key>
            <key type="filename">inventory/alert/element/nature.png</key>
            <key type="filename">inventory/alert/element/water.png</key>
            <key type="filename">inventory/alert/ico_damage.png</key>
            <key type="filename">inventory/alert/ico_hp.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,14,28,28</rect>
                <key>scale9Paddings</key>
                <rect>14,14,28,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/alert/stat_line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>107,13,214,25</rect>
                <key>scale9Paddings</key>
                <rect>107,13,214,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/belt_slot.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>36,36,71,71</rect>
                <key>scale9Paddings</key>
                <rect>36,36,71,71</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/bg_left_female.png</key>
            <key type="filename">inventory/bg_left_male.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>234,304,467,608</rect>
                <key>scale9Paddings</key>
                <rect>234,304,467,608</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/bg_right.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>325,304,649,608</rect>
                <key>scale9Paddings</key>
                <rect>325,304,649,608</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/button/bag_left_down.png</key>
            <key type="filename">inventory/button/bag_left_up.png</key>
            <key type="filename">inventory/button/bag_right_down.png</key>
            <key type="filename">inventory/button/bag_right_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>17,16,34,32</rect>
                <key>scale9Paddings</key>
                <rect>17,16,34,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/button/beltdown_down.png</key>
            <key type="filename">inventory/button/beltdown_up.png</key>
            <key type="filename">inventory/button/beltup_down.png</key>
            <key type="filename">inventory/button/beltup_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,20,55,40</rect>
                <key>scale9Paddings</key>
                <rect>28,20,55,40</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/icon_belt_lock.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,26,43,51</rect>
                <key>scale9Paddings</key>
                <rect>21,26,43,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tab.png</key>
            <key type="filename">inventory/tab_selected.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>35,38,70,75</rect>
                <key>scale9Paddings</key>
                <rect>35,38,70,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tab_ico/craft.png</key>
            <key type="filename">inventory/tab_ico/craft_selected.png</key>
            <key type="filename">inventory/tab_ico/equipment.png</key>
            <key type="filename">inventory/tab_ico/equipment_selected.png</key>
            <key type="filename">inventory/tab_ico/potion.png</key>
            <key type="filename">inventory/tab_ico/potion_selected.png</key>
            <key type="filename">inventory/tab_ico/quest.png</key>
            <key type="filename">inventory/tab_ico/quest_selected.png</key>
            <key type="filename">inventory/tab_ico/recipie.png</key>
            <key type="filename">inventory/tab_ico/recipie_selected.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,32,64,64</rect>
                <key>scale9Paddings</key>
                <rect>32,32,64,64</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">inventory/tab_selected_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,44,103,87</rect>
                <key>scale9Paddings</key>
                <rect>51,44,103,87</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">list/button/scroll_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>95,20,190,39</rect>
                <key>scale9Paddings</key>
                <rect>95,20,190,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">list/button/scroll_icon_down.png</key>
            <key type="filename">list/button/scroll_icon_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,20,11</rect>
                <key>scale9Paddings</key>
                <rect>10,6,20,11</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">list/item_bg.png</key>
            <key type="filename">list/item_bg_selected.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>202,30,404,60</rect>
                <key>scale9Paddings</key>
                <rect>202,30,404,60</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/avatar_frame_volume.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,55,111,110</rect>
                <key>scale9Paddings</key>
                <rect>56,55,111,110</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/bg_level.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,21,41,42</rect>
                <key>scale9Paddings</key>
                <rect>20,21,41,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/bg_line.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,16,32,32</rect>
                <key>scale9Paddings</key>
                <rect>16,16,32,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/button_addmoney_down.png</key>
            <key type="filename">main/button_addmoney_up.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,22,56,44</rect>
                <key>scale9Paddings</key>
                <rect>28,22,56,44</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/crystal_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,19,38,38</rect>
                <key>scale9Paddings</key>
                <rect>19,19,38,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/exp_bar_bg.png</key>
            <key type="filename">main/exp_bar_track.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,11,111,22</rect>
                <key>scale9Paddings</key>
                <rect>56,11,111,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/goldcoin_icon.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,19,37,38</rect>
                <key>scale9Paddings</key>
                <rect>19,19,37,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/hpbar_dekor.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,19,54,39</rect>
                <key>scale9Paddings</key>
                <rect>27,19,54,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/hpbar_empty.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,15,63,30</rect>
                <key>scale9Paddings</key>
                <rect>32,15,63,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/hpbar_track.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>28,11,56,22</rect>
                <key>scale9Paddings</key>
                <rect>28,11,56,22</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">main/menu/achievment.png</key>
            <key type="filename">main/menu/achievment_selected.png</key>
            <key type="filename">main/menu/battle.png</key>
            <key type="filename">main/menu/battle_selected.png</key>
            <key type="filename">main/menu/help.png</key>
            <key type="filename">main/menu/help_selected.png</key>
            <key type="filename">main/menu/inventory.png</key>
            <key type="filename">main/menu/inventory_selected.png</key>
            <key type="filename">main/menu/news.png</key>
            <key type="filename">main/menu/news_selected.png</key>
            <key type="filename">main/menu/options.png</key>
            <key type="filename">main/menu/options_selected.png</key>
            <key type="filename">main/menu/quest.png</key>
            <key type="filename">main/menu/quest_selected.png</key>
            <key type="filename">main/menu/store.png</key>
            <key type="filename">main/menu/store_selected.png</key>
            <key type="filename">main/tab.png</key>
            <key type="filename">main/tab_selected.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>38,46,75,92</rect>
                <key>scale9Paddings</key>
                <rect>38,46,75,92</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/battle_field.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>278,278,557,557</rect>
                <key>scale9Paddings</key>
                <rect>278,278,557,557</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/buff_number.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,15,31,31</rect>
                <key>scale9Paddings</key>
                <rect>15,15,31,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/button_off.png</key>
            <key type="filename">match3/button_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,19,169,38</rect>
                <key>scale9Paddings</key>
                <rect>84,19,169,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/clock_off.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>69,62,139,124</rect>
                <key>scale9Paddings</key>
                <rect>69,62,139,124</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/clock_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,47,95,94</rect>
                <key>scale9Paddings</key>
                <rect>48,47,95,94</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/command_plate_off.png</key>
            <key type="filename">match3/command_plate_off2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>84,24,168,48</rect>
                <key>scale9Paddings</key>
                <rect>84,24,168,48</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/health_bar_mini_blue.png</key>
            <key type="filename">match3/health_bar_mini_green.png</key>
            <key type="filename">match3/health_bar_mini_purple.png</key>
            <key type="filename">match3/health_bar_mini_red.png</key>
            <key type="filename">match3/health_bar_mini_yellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,4,51,7</rect>
                <key>scale9Paddings</key>
                <rect>26,4,51,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/health_bar_off.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,16,51,32</rect>
                <key>scale9Paddings</key>
                <rect>26,16,51,32</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/health_bar_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,12,30,25</rect>
                <key>scale9Paddings</key>
                <rect>15,12,30,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/heart.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,19,41,37</rect>
                <key>scale9Paddings</key>
                <rect>21,19,41,37</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/magic_off.png</key>
            <key type="filename">match3/magic_on.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>50,51,100,102</rect>
                <key>scale9Paddings</key>
                <rect>50,51,100,102</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/mana_blue.png</key>
            <key type="filename">match3/mana_green.png</key>
            <key type="filename">match3/mana_purple.png</key>
            <key type="filename">match3/mana_red.png</key>
            <key type="filename">match3/mana_yellow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,25,50,50</rect>
                <key>scale9Paddings</key>
                <rect>25,25,50,50</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/plate.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>34,11,67,23</rect>
                <key>scale9Paddings</key>
                <rect>34,11,67,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">match3/plate2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>41,19,81,38</rect>
                <key>scale9Paddings</key>
                <rect>41,19,81,38</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">store/buy_item_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>78,90,155,180</rect>
                <key>scale9Paddings</key>
                <rect>78,90,155,180</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">store/item_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,117,164,234</rect>
                <key>scale9Paddings</key>
                <rect>82,117,164,234</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,15,15</rect>
                <key>scale9Paddings</key>
                <rect>8,8,15,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/background_blue.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,30,59,59</rect>
                <key>scale9Paddings</key>
                <rect>30,30,59,59</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/backgroung_with_title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,50,60,99</rect>
                <key>scale9Paddings</key>
                <rect>30,50,60,99</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/backgroung_without_title.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>30,31,60,63</rect>
                <key>scale9Paddings</key>
                <rect>30,31,60,63</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/header.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,20,48,39</rect>
                <key>scale9Paddings</key>
                <rect>24,20,48,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">window/main_background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>51,246,101,491</rect>
                <key>scale9Paddings</key>
                <rect>51,246,101,491</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>.</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
