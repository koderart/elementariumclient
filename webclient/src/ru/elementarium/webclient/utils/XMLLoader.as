package ru.elementarium.webclient.utils {

import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.utils.Dictionary;

import ru.elementarium.webclient.view.main.components.ELAlert;

public class XMLLoader {

    private static var _instance: XMLLoader;

    private var requestsDictionary:Dictionary = new Dictionary(true);
    
    public static function get instance():XMLLoader {
        if (_instance == null) {
            _instance = new XMLLoader();
        }
        return _instance;
    }

    public function XMLLoader() {
    }

    public function sendRequest(url:String, resultHandler:Function, method:String = URLRequestMethod.GET):void {
        var request:URLRequest = new URLRequest(url);
        request.method = method;
        var loader:URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, loaderResultHandler, false, 0, true);
        loader.addEventListener(IOErrorEvent.IO_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
        loader.load(request);

        if (resultHandler != null) {
            requestsDictionary[loader] = resultHandler;
        } else {
            requestsDictionary[loader] = true;
        }
    }

    private function loaderResultHandler(event:Event):void {
        var loader:URLLoader = event.target as URLLoader;
        loader.removeEventListener(Event.COMPLETE, loaderResultHandler);
        loader.removeEventListener(IOErrorEvent.IO_ERROR, httpRequestError);
        loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, httpRequestError);
        loader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);

        var data:Object;
        try {
            data = new XML(loader.data);
        } catch (e:Error) {
            ELAlert.show("Ожидался XML а пришел текст:"+loader.data, "Ошибка сервера");
            return;
        }

        if (requestsDictionary[loader] && requestsDictionary[loader] is Function) {
            requestsDictionary[loader].call(null, data);
        }

        delete requestsDictionary[loader];
    }

    private function httpRequestError(error:ErrorEvent):void {
        delete requestsDictionary[error.target];
    }

    private function httpStatusHandler(event:HTTPStatusEvent):void {
        if (event.status == 500) {
            delete requestsDictionary[event.target];
            ELAlert.show("Server error 500");
        }
    }
}
}
