package ru.elementarium.webclient.helper
{
    import feathers.controls.renderers.LayoutGroupListItemRenderer;
    import feathers.dragDrop.DragData;
    import feathers.dragDrop.DragDropManager;
    import feathers.dragDrop.IDragSource;

    import flash.geom.Point;

    import flash.utils.getDefinitionByName;

    import starling.display.DisplayObject;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;

    import starlingbuilder.engine.util.ParamUtil;

    public class DragToCanvasHelper
    {
        public static const ARTIKUL:String = "artikul";

        private var _renderer:LayoutGroupListItemRenderer;

        public function DragToCanvasHelper(renderer:LayoutGroupListItemRenderer):void
        {
            _renderer = renderer;

            _renderer.addEventListener(TouchEvent.TOUCH, onTouch);
        }

        private function onTouch(event:TouchEvent):void
        {
            if (DragDropManager.isDragging)
            {
                return;
            }

            var touch:Touch = event.getTouch(_renderer);
            if (touch && touch.phase == TouchPhase.MOVED)
            {
                var clone:DisplayObject = createRendererClone();

                var dragData:DragData = new DragData();
                dragData.setDataForFormat(ARTIKUL, _renderer.data);

                var localPoint:Point = new Point(touch.globalX, touch.globalY);
                _renderer.globalToLocal(localPoint, localPoint);

                DragDropManager.startDrag(_renderer as IDragSource, touch, dragData, clone as DisplayObject, -localPoint.x, -localPoint.y);
            }
        }

        private function createRendererClone():DisplayObject
        {
            var cls:Class = getDefinitionByName(ParamUtil.getClassName(_renderer)) as Class;
            var clone:Object = new cls();
            clone.width = _renderer.width;
            clone.height = _renderer.height;
            clone.styleName = _renderer.styleName;
            clone.data = _renderer.data;
            clone.owner = _renderer["owner"];
            //clone.alpha = 0.9;
            return clone as DisplayObject;
        }

    }
}
