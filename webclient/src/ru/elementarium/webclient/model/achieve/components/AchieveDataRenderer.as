package ru.elementarium.webclient.model.achieve.components {

import ru.muvs.admin.main.vo.AchieveVO;
import ru.muvs.elementarium.thrift.achieve.DataAchieve;

public class AchieveDataRenderer
    {
        public var dataStorage:AchieveVO;
        public var data:DataAchieve;
    
        public function AchieveDataRenderer(dataStorage:AchieveVO=null,data:DataAchieve=null)
        {
            this.dataStorage = dataStorage;
            this.data = data;
        }
    }
}
