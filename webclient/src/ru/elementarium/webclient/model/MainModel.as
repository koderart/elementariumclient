package ru.elementarium.webclient.model
{
import com.junkbyte.console.Cc;

import flash.events.Event;
import flash.geom.Point;

import flash.system.Capabilities;

import flash.utils.Dictionary;

import org.robotlegs.starling.mvcs.Actor;

import ru.mailru.MailruCall;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.battle.BuffVO;

import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import ru.muvs.elementarium.thrift.artikul.DataArtikulBelt;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlot;
import ru.muvs.elementarium.thrift.chat.action.DataChatAction;
import ru.muvs.elementarium.thrift.location.battle.DataLocationBattle;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.message.message.DataMessage;
import ru.muvs.elementarium.thrift.player.DataPlayer;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.quest.DataQuest;
import ru.muvs.elementarium.thrift.skill.DataSkill;
import ru.muvs.elementarium.thrift.skill.DataSkillType;

import starling.core.Starling;

    public class MainModel extends Actor
	{

        public static const assetsVersion:String = "v4";
        public static const serverVersion:int = 1002;
    
        public var player:DataPlayer = new DataPlayer();
        public var playerLevel:int;
        public var playerHpCurrent:int;
        public var playerHpMax:int;
        public var playerExpCurrent:int;
        public var playerExpMax:int;
        public var playerBagCapacity:int;
        public var playerBeltCapacity:int;

        public var isBattle:Boolean;

        public var inventoryArtikul:DataArtikul;
        public var durabilityArtikul:DataArtikul;
        public var inventoryArtikuls:Array = [];
        public var inventoryBelt:DataArtikulBelt;
        public var inventoryBelts:Array = [];
        public var inventorySlot:DataArtikulSlot;
        public var inventorySlots:Array = [];
        public static var artikulData:Array;
        public static var storeData:Array;
        public static var mobData:Array;
        public static var npcData:Array;
        public static var questData:Array;
        public static var levelData:Array;
        public static var levelExp:Array;
        public static var spellData:Dictionary;
        public static var paymentVkData:Array;
        public static var paymentMailData:Array;
        public static var paymentAppleData:Array;
        public static var skillTitleData:Array;
        public static var locationData:Array;
        public static var locationMemberData:Array;
        public static var challegeData:Array;
        public static var achieveData:Array;
        public static var localeData:Array;
        public static var magicData:Array;
        public static var potionData:Array;
        public static var buffData:Array;
    
        public var moneyGold:int;
        public var moneyCrystal:int;
        
        public var questList:Array;
        public var quest:DataQuest;

        public var currentLocation:int;
        public var locationMembers:Object = {};
        public var locationMembersBattle:Array = [];
        public var locationMembersPosition:Array = [];
        public var locationMemberStand:Point;
        private var _locationMember:DataLocationMember;
        public var locationBattle:DataLocationBattle;
        
        public var currentNpc:DataLocationMember;

        public var openArena:Boolean;
        public var openNpc:Boolean;
        public var openQuest:Boolean;
        public var currentStore:int;
        public var message:DataMessage;
        public var socialId:String;
        public var email:String;
        public var password:String;
        public var socialVkData:Object;
        public var socialMyMailData:Object;
        public var newsXML:XMLList;
        public var challengeProgress:Array;
        public var chatAction:DataChatAction;
        public var achieveList:Array;
        public var playerSkill:Array = [];

        public var storeKitPurchaseDeferred:Array = [];
        public var timerDisconnect:int;

        public static function get isMobile() : Boolean
        {
            var ios: Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;
            var android: Boolean = Capabilities.manufacturer.indexOf("Android") != -1;

            return ios || android;
        }

        public static function get scaleFactor() : int
        {
            var scaleFactor:Number = 1;
            BUILD::mobile {
                if (Starling.current.nativeOverlay.stage.stageWidth > 1280) {
                    scaleFactor = 2;
                }
            }
            return scaleFactor;
        }

        public static function getArtikulVO(id:int) : ArtikulVO
        {
            return artikulData[id];
        }

        public static function getLocale(token:String) : String
        {
            if (localeData[token]) {
                return localeData[token].ru;
            }
            return "???";
        }

        public static function getBuffVO(id:int) : BuffVO
        {
            return buffData[id];
        }
 
		public function MainModel()
		{

            if (Starling.current.nativeStage.loaderInfo.parameters.vk)
            {
                socialVkData = JSON.parse(Starling.current.nativeStage.loaderInfo.parameters.vk);
            } else if (Starling.current.nativeStage.loaderInfo.parameters.mailru)
            {
                MailruCall.addEventListener(Event.COMPLETE, mailruReadyHandler);
                MailruCall.init('webclient', "f789392211b330e31e109a27408ec12b");
            }

            spellData = new Dictionary();
            spellData[0] = new Dictionary();
            spellData[0][DataPlayerElement.DARK] = {needDark:3,title:"Точный удар", description:"Уничтожает все камни своей стихии, нанося урон в размере 30 единиц за каждый камень + физический урон.",reloading:"1 ход"};
            spellData[0][DataPlayerElement.FIRE] = {needFire:3,title:"Точный удар", description:"Уничтожает все камни своей стихии, нанося урон в размере 30 единиц за каждый камень + физический урон.",reloading:"1 ход"};
            spellData[0][DataPlayerElement.LIGHT] = {needLight:3,title:"Точный удар", description:"Уничтожает все камни своей стихии, нанося урон в размере 30 единиц за каждый камень + физический урон.",reloading:"1 ход"};
            spellData[0][DataPlayerElement.NATURE] = {needNature:3,title:"Точный удар", description:"Уничтожает все камни своей стихии, нанося урон в размере 30 единиц за каждый камень + физический урон.",reloading:"1 ход"};
            spellData[0][DataPlayerElement.WATER] = {needWater:3,title:"Точный удар", description:"Уничтожает все камни своей стихии, нанося урон в размере 30 единиц за каждый камень + физический урон.",reloading:"1 ход"};

            spellData[1] = new Dictionary();
            spellData[1][DataPlayerElement.DARK] = {needDark:7, needNature:7,title:"Исцеление", description:"Исцеляет игрока, за каждый камень своей стихии, в размере 100% от магического урона.",reloading:"1 ход"};
            spellData[1][DataPlayerElement.FIRE] = {needFire:7, needLight:7,title:"Исцеление", description:"Исцеляет игрока, за каждый камень своей стихии, в размере 100% от магического урона.",reloading:"1 ход"};
            spellData[1][DataPlayerElement.LIGHT] = {needLight:7, needDark:7,title:"Исцеление", description:"Исцеляет игрока, за каждый камень своей стихии, в размере 100% от магического урона.",reloading:"1 ход"};
            spellData[1][DataPlayerElement.NATURE] = {needNature:7, needWater:7,title:"Исцеление", description:"Исцеляет игрока, за каждый камень своей стихии, в размере 100% от магического урона.",reloading:"1 ход"};
            spellData[1][DataPlayerElement.WATER] = {needWater:7, needFire:7,title:"Исцеление", description:"Исцеляет игрока, за каждый камень своей стихии, в размере 100% от магического урона.",reloading:"1 ход"};

            spellData[2] = new Dictionary();
            spellData[2][DataPlayerElement.DARK] = {needDark:10, needNature:10,title:"Защита", description:"Следующим ходом поглощает весь наносимый противником урон.",reloading:"1 ход"};
            spellData[2][DataPlayerElement.FIRE] = {needFire:10, needLight:10,title:"Защита", description:"Следующим ходом поглощает весь наносимый противником урон.",reloading:"1 ход"};
            spellData[2][DataPlayerElement.LIGHT] = {needLight:10, needDark:10,title:"Защита", description:"Следующим ходом поглощает весь наносимый противником урон.",reloading:"1 ход"};
            spellData[2][DataPlayerElement.NATURE] = {needNature:10, needWater:10,title:"Защита", description:"Следующим ходом поглощает весь наносимый противником урон.",reloading:"1 ход"};
            spellData[2][DataPlayerElement.WATER] = {needWater:10, needFire:10,title:"Защита", description:"Следующим ходом поглощает весь наносимый противником урон.",reloading:"1 ход"};

            spellData[3] = new Dictionary();
            spellData[3][DataPlayerElement.DARK] = {needFire:15, needWater:15, needLight:15,title:"Взрыв", description:"Уничтожает все камни на поле и наносит 100% урона от своей стихии + 10 урона за остальные камни.",reloading:"1 ход"};
            spellData[3][DataPlayerElement.FIRE] = {needDark:15, needWater:15, needNature:15,title:"Взрыв", description:"Уничтожает все камни на поле и наносит 100% урона от своей стихии + 10 урона за остальные камни.",reloading:"1 ход"};
            spellData[3][DataPlayerElement.LIGHT] = {needNature:15, needFire:15, needWater:15,title:"Взрыв", description:"Уничтожает все камни на поле и наносит 100% урона от своей стихии + 10 урона за остальные камни.",reloading:"1 ход"};
            spellData[3][DataPlayerElement.NATURE] = {needFire:15, needLight:15, needDark:15,title:"Взрыв", description:"Уничтожает все камни на поле и наносит 100% урона от своей стихии + 10 урона за остальные камни.",reloading:"1 ход"};
            spellData[3][DataPlayerElement.WATER] = {needNature:15, needDark:15, needLight:15,title:"Взрыв", description:"Уничтожает все камни на поле и наносит 100% урона от своей стихии + 10 урона за остальные камни.",reloading:"1 ход"};

        }
    
        private function mailruReadyHandler(event:Object):void
        {
            socialId =   MailruCall.exec('mailru.session.vid');
            socialMyMailData = getMailruSessionData();
        }

        public function updatePlayerSkill(skills:Array)
        {
            for each (var skill:DataSkill in skills) {
                playerSkill[skill.type] = skill.base;
                switch (skill.type) {
                    case DataSkillType.HP:
                        playerHpCurrent = skill.base;
                        playerHpMax = skill.total;
                        break;
                    case DataSkillType.LEVEL:
                        playerLevel = skill.base;
                        playerExpMax = MainModel.levelExp[skill.base];
                        break;
                    case DataSkillType.EXP:
                        playerExpCurrent = skill.base;
                        break;
                    case DataSkillType.BAG_CAPACITY:
                        playerBagCapacity = skill.base;
                        break;
                    case DataSkillType.BELT_CAPACITY:
                        if (playerBeltCapacity != skill.base) {
                            playerBeltCapacity = skill.base;
                        }
                        break;
                }
            }
        }

        private function getMailruSessionData()
        {
            var session  = MailruCall.exec('mailru.session');
    
            var props:Array = [];
    
            for (var p:String in session) {
                if (p != "sig") {
                    props.push(p+"="+session[p]);
                }
            }
    
            props.sort();
            var str = props.join("");
    
            return {vid:session.vid, str:str, sig:session.sig, raw:session};
        }
    
        public function showMailruPayment (id:int, name:String, price:Number):void
        {
            MailruCall.exec("mailru.app.payments.showDialog", null, {service_id:id, service_name:name, mailiki_price:price});
        }

        public function get locationMember():DataLocationMember
        {
            return _locationMember;
        }
    
        public function set locationMember(value:DataLocationMember):void
        {
            _locationMember = value;
    
            if (locationMembers[_locationMember.id]) {
                locationMembers[_locationMember.id] = _locationMember;
            }
    
        }
    }
}
