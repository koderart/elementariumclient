package ru.elementarium.webclient.model
{
import org.robotlegs.starling.mvcs.Actor;

import ru.elementarium.webclient.view.match3.UserDataWrapper;
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffBegin;

import ru.muvs.elementarium.thrift.battle.DataClientBattleMagic;
import ru.muvs.elementarium.thrift.battle.DataClientBattlePotion;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMember;

public class Match3Model extends Actor
	{
        public var areaWidth:int;
        public var areaHeight:int;
        public var areaCellActions:Array;
        public var members:Array;
        public var membersWrapper:Object;
        public var currentMemberId:int;
        public var queueType:int;
        public var user:UserDataWrapper;
        public var userTarget:UserDataWrapper;
        public var memberJoin:DataBattleMember;
        public var destination:Array;
        public var currentMagic:DataClientBattleMagic;
        public var currentPotion:DataClientBattlePotion;
        public var currentBuff:Object;
        public var currentKickMemberId:int;
        public var leftTime:int;

        public function Match3Model()
		{
		}

        private var _areaCells:Array;

        public function set areaCells(areaCells:Array):void {
            _areaCells = areaCells;
        }

        public function get areaCells():Array {
            return _areaCells;
        }

        public function getUser(id:int):UserDataWrapper {
            return membersWrapper[id];
        }
    }
}
