package ru.elementarium.webclient.services {
import ru.muvs.elementarium.thrift.achieve.*;
import ru.muvs.elementarium.thrift.artikul.*;
import ru.muvs.elementarium.thrift.bag.*;
import ru.muvs.elementarium.thrift.battle.*;
import ru.muvs.elementarium.thrift.challenge.*;
import ru.muvs.elementarium.thrift.chat.*;
import ru.muvs.elementarium.thrift.doll.belt.*;
import ru.muvs.elementarium.thrift.doll.slot.*;
import ru.muvs.elementarium.thrift.location.*;
import ru.muvs.elementarium.thrift.login.*;
import ru.muvs.elementarium.thrift.message.DataClientMessage;
import ru.muvs.elementarium.thrift.money.*;
import ru.muvs.elementarium.thrift.payment.DataClientPaymentApple;
import ru.muvs.elementarium.thrift.payment.DataServerPaymentApple;
import ru.muvs.elementarium.thrift.ping.DataServerPing;
import ru.muvs.elementarium.thrift.player.*;
import ru.muvs.elementarium.thrift.quest.*;
import ru.muvs.elementarium.thrift.store.*;
import ru.muvs.elementarium.thrift.version.DataClientVesrion;
import ru.muvs.elementarium.thrift.version.DataServerVesrion;

public class Protocol {

    public static const LOGIN_VK_SERVER_COMMAND : int = 101;
    public static const LOGIN_VK_CLIENT_COMMAND : int = 102;
    public static const LOGIN_EMAIL_SERVER_COMMAND : int = 103;
    public static const LOGIN_EMAIL_CLIENT_COMMAND : int = 104;
    public static const LOGIN_MY_MAIL_SERVER_COMMAND : int = 105;
    public static const LOGIN_MY_MAIL_CLIENT_COMMAND : int = 106;
    public static const LOGIN_APPLE_SERVER_COMMAND : int = 107;
    public static const LOGIN_APPLE_CLIENT_COMMAND : int = 108;

    public static const BATTLE_QUEUE_JOIN_SERVER_COMMAND : int = 201;    // встать в очередь по бою
    public static const BATTLE_QUEUE_JOIN_CLIENT_COMMAND : int = 202; // приходит id очереди
    public static const BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND : int = 203;   // приходит список очереди
    
    public static const BATTLE_START_CLIENT_COMMAND : int = 301;        // приходит старт боя (выпилим)
    public static const BATTLE_FIN_CLIENT_COMMAND : int = 302;          // конец боя (на будущее)
    public static const BATTLE_JOIN_SERVER_COMMAND : int = 303;         // бой с мобом или вход в существующий бой
    public static const BATTLE_JOIN_CLIENT_COMMAND : int = 304;        // старт боя
    public static const BATTLE_UNJOIN_SERVER_COMMAND : int = 305;      // что-то на будущее
    public static const BATTLE_UNJOIN_CLIENT_COMMAND : int = 306;     // конец боя
    public static const BATTLE_JOIN_MEMBER_CLIENT_COMMAND : int = 307;       // 
    public static const BATTLE_UN_JOIN_MEMBER_CLIENT_COMMAND : int = 308;      //
    public static const BATTLE_MEMBER_JOIN_CLIENT_COMMAND : int = 309;
    public static const BATTLE_MEMBER_UNJOIN_CLIENT_COMMAND : int = 310;
    
    public static const BATTLE_MAGIC_SERVER_COMMAND : int = 401;
    public static const BATTLE_MAGIC_CLIENT_COMMAND : int = 402;
    public static const BATTLE_POTION_SERVER_COMMAND : int = 403;
    public static const BATTLE_POTION_CLIENT_COMMAND : int = 404;
    public static const BATTLE_BUFF_BEGIN_CLIENT_COMMAND : int = 405;
    public static const BATTLE_BUFF_END_CLIENT_COMMAND : int = 406;
    public static const BATTLE_BUFF_EFFECT_CLIENT_COMMAND : int = 407;
    public static const BATTLE_BUFF_COLLISION_CLIENT_COMMAND : int = 408;
    public static const BATTLE_BUFF_INTERRUPT_CLIENT_COMMAND : int = 409;
    public static const BATTLE_KICK_CLIENT_COMMAND : int = 410;
    public static const BATTLE_SWAP_CLIENT_COMMAND : int = 411;
    
    public static const BAG_LIST_SERVER_COMMAND : int = 501;
    public static const BAG_REMOVE_SERVER_COMMAND : int = 502;
    public static const BAG_ADD_CLIENT_COMMAND : int = 503;
    public static const BAG_REMOVE_CLIENT_COMMAND : int = 504;
    public static const BAG_CHANGE_CLIENT_COMMAND : int = 505;
    public static const BAG_LIST_CLIENT_COMMAND : int = 506;

    public static const SLOT_LIST_SERVER_COMMAND : int = 601;
    public static const SLOT_TAKE_OFF_SERVER_COMMAND : int = 602;
    public static const SLOT_TAKE_ON_SERVER_COMMAND : int = 603;
    public static const SLOT_LIST_CLIENT_COMMAND : int = 604;
    public static const SLOT_TAKE_OFF_CLIENT_COMMAND : int = 605;
    public static const SLOT_TAKE_ON_CLIENT_COMMAND : int = 606;

    public static const BELT_LIST_SERVER_COMMAND : int = 701;
    public static const BELT_TAKE_OFF_SERVER_COMMAND : int = 702;
    public static const BELT_TAKE_ON_SERVER_COMMAND : int = 703;
    public static const BELT_LIST_CLIENT_COMMAND : int = 704;
    public static const BELT_TAKE_OFF_CLIENT_COMMAND : int = 705;
    public static const BELT_TAKE_ON_CLIENT_COMMAND : int = 706;
    public static const BELT_CHANGE_CLIENT_COMMAND : int = 707;
    public static const BELT_REMOVE_CLIENT_COMMAND : int = 708;

    public static const MONEY_LIST_SERVER_COMMAND : int = 801;
    public static const MONEY_LIST_CLIENT_COMMAND : int = 802;
    public static const MONEY_CHANGE_CLIENT_COMMAND : int = 803;

    public static const STORE_BUY_SERVER_COMMAND : int = 901;

    public static const SKILL_LIST_SERVER_COMMAND : int = 1001;
    public static const SKILL_LIST_CLIENT_COMMAND : int = 1002;
    public static const SKILL_CHANGE_CLIENT_COMMAND : int = 1003;
    
    public static const ARTIKUL_ACTION_SERVER_COMMAND : int = 1101;
    public static const ARTIKUL_BREAK_CLIENT_COMMAND : int = 1102;
    public static const ARTIKUL_REPAIR_SERVER_COMMAND : int = 1103;
    public static const ARTIKUL_REPAIR_CLIENT_COMMAND : int = 1104;
    public static const ARTIKUL_SELL_SERVER_COMMAND : int = 1105;


    public static const QUEST_LIST_SERVER_COMMAND : int = 1201;
    public static const QUEST_LIST_CLIENT_COMMAND : int = 1202;
    public static const QUEST_REFUSE_SERVER_COMMAND : int = 1203;
    public static const QUEST_REFUSE_CLIENT_COMMAND : int = 1204;
    public static const QUEST_COMPLETE_CLIENT_COMMAND : int = 1205;

    public static const MESSAGE_CLIENT_COMMAND : int = 1301;

    public static const PING_SERVER_COMMAND : int = 1401;

    public static const LOCATION_LOAD_CLIENT_COMMAND : int = 1501;
    public static const LOCATION_ACTION_SERVER_COMMAND : int = 1502;
    public static const LOCATION_ACTION_CLIENT_COMMAND : int = 1503;

    public static const CHALLENGE_LIST_SERVER_COMMAND: int = 1601;
    public static const CHALLENGE_LIST_CLIENT_COMMAND: int = 1602;
    public static const CHALLENGE_PROGRESS_CLIENT_COMMAND: int = 1603;

    public static const CHAT_ACTION_SERVER_COMMAND: int = 1701;
    public static const CHAT_ACTION_CLIENT_COMMAND: int = 1702;

    public static const ACHIEVE_LIST_SERVER_COMMAND:int = 1801;
    public static const ACHIEVE_LIST_CLIENT_COMMAND:int = 1802;
    
    public static const PAYMENT_APPLE_SERVER_COMMAND:int = 1901;
    public static const PAYMENT_APPLE_CLIENT_COMMAND:int = 1902;
    
    public static const VERSION_SERVER_COMMAND:int = 2001;
    public static const VERSION_CLIENT_COMMAND:int = 2002;
    
    public function Protocol() {
    }

    static public function classFromCommand(id:int):Class {
        switch (id) {
            case LOGIN_VK_SERVER_COMMAND:
                return DataServerLoginVk;
                break;
            case LOGIN_VK_CLIENT_COMMAND:
                return DataClientLoginVk;
                break;
            case LOGIN_EMAIL_SERVER_COMMAND:
                return DataServerLoginEmail;
                break;
            case LOGIN_EMAIL_CLIENT_COMMAND:
                return DataClientLoginEmail;
                break;
            case LOGIN_MY_MAIL_SERVER_COMMAND:
                return DataServerLoginMyMail;
                break;
            case LOGIN_MY_MAIL_CLIENT_COMMAND:
                return DataClientLoginMyMail;
                break;
            case LOGIN_APPLE_SERVER_COMMAND:
                return DataServerLoginApple;
                break;
            case LOGIN_APPLE_CLIENT_COMMAND:
                return DataClientLoginApple;
                break;

            case BATTLE_QUEUE_JOIN_SERVER_COMMAND:
                return DataServerBattleQueueJoin;
                break;
            case BATTLE_QUEUE_JOIN_CLIENT_COMMAND:
                return DataClientBattleQueueJoin;
                break;
            case BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND:
                return DataServerBattleQueueUnjoin;
                break;
            case BATTLE_START_CLIENT_COMMAND:
                return DataClientBattleStart;
                break;
            case BATTLE_FIN_CLIENT_COMMAND:
                return DataClientBattleFin;
                break;
            case BATTLE_JOIN_SERVER_COMMAND:
                return DataServerBattleJoin;
                break;
            case BATTLE_JOIN_CLIENT_COMMAND:
                return DataClientBattleJoin;
                break;
            case BATTLE_UNJOIN_CLIENT_COMMAND:
                return DataClientBattleUnjoin;
                break;
            case BATTLE_MEMBER_JOIN_CLIENT_COMMAND:
                return DataClientBattleMemberJoin;
                break;
            case BATTLE_MEMBER_UNJOIN_CLIENT_COMMAND:
                return DataClientBattleMemberJoin;
                break;
            case BATTLE_MAGIC_SERVER_COMMAND:
                return DataServerBattleMagic;
                break;
            case BATTLE_MAGIC_CLIENT_COMMAND:
                return DataClientBattleMagic;
                break;
            case BATTLE_POTION_SERVER_COMMAND:
                return DataServerBattlePotion;
                break;
            case BATTLE_POTION_CLIENT_COMMAND:
                return DataClientBattlePotion;
                break;
            case BATTLE_BUFF_BEGIN_CLIENT_COMMAND:
                return DataClientBattleBuffBegin;
                break;
            case BATTLE_BUFF_END_CLIENT_COMMAND:
                return DataClientBattleBuffEnd;
                break;
            case BATTLE_BUFF_EFFECT_CLIENT_COMMAND:
                return DataClientBattleBuffEffect;
                break;
            case BATTLE_BUFF_COLLISION_CLIENT_COMMAND:
                return DataClientBattleBuffCollision;
                break;
            case BATTLE_BUFF_INTERRUPT_CLIENT_COMMAND:
                return DataClientBattleBuffInterrupt;
                break;
            case BATTLE_KICK_CLIENT_COMMAND:
                return DataClientBattleKick;
                break;
            case BATTLE_SWAP_CLIENT_COMMAND:
                return DataClientBattleSwap;
                break;
            case BAG_LIST_SERVER_COMMAND:
                return DataServerBagList;
                break;
            case BAG_REMOVE_SERVER_COMMAND:
                return DataServerBagRemove;
                break;
            case BAG_ADD_CLIENT_COMMAND:
                return DataClientBagAdd;
                break;
            case BAG_REMOVE_CLIENT_COMMAND:
                return DataClientBagRemove;
                break;
            case BAG_CHANGE_CLIENT_COMMAND:
                return DataClientBagChange;
                break;
            case BAG_LIST_CLIENT_COMMAND:
                return DataClientBagList;
                break;
            case SLOT_LIST_SERVER_COMMAND:
                return DataServerSlotList;
                break;
            case SLOT_TAKE_OFF_SERVER_COMMAND:
                return DataServerSlotTakeOff;
                break;
            case SLOT_TAKE_ON_SERVER_COMMAND:
                return DataServerSlotTakeOn;
                break;
            case SLOT_LIST_CLIENT_COMMAND:
                return DataClientSlotList;
                break;
            case SLOT_TAKE_OFF_CLIENT_COMMAND:
                return DataClientSlotTakeOff;
                break;
            case SLOT_TAKE_ON_CLIENT_COMMAND:
                return DataClientSlotTakeOn;
                break;

            case BELT_LIST_SERVER_COMMAND:
                return DataServerBeltList;
                break;
            case BELT_TAKE_OFF_SERVER_COMMAND:
                return DataServerBeltTakeOff;
                break;
            case BELT_TAKE_ON_SERVER_COMMAND:
                return DataServerBeltTakeOn;
                break;
            case BELT_LIST_CLIENT_COMMAND:
                return DataClientBeltList;
                break;
            case BELT_TAKE_OFF_CLIENT_COMMAND:
                return DataClientBeltTakeOff;
                break;
            case BELT_TAKE_ON_CLIENT_COMMAND:
                return DataClientBeltTakeOn;
                break;
            case BELT_CHANGE_CLIENT_COMMAND:
                return DataClientBeltChange;
                break;
            case BELT_REMOVE_CLIENT_COMMAND:
                return DataClientBeltRemove;
                break;

            case STORE_BUY_SERVER_COMMAND:
                return DataServerStoreBuy;
                break;

            case MONEY_LIST_SERVER_COMMAND:
                return DataServerMoneyList;
                break;
            case MONEY_LIST_CLIENT_COMMAND:
                return DataClientMoneyList;
                break;
            case MONEY_CHANGE_CLIENT_COMMAND:
                return DataClientMoneyChange;
                break;

            case SKILL_LIST_SERVER_COMMAND:
                return DataServerSkillList;
                break;
            case SKILL_LIST_CLIENT_COMMAND:
                return DataClientSkillList;
                break;
            case SKILL_CHANGE_CLIENT_COMMAND:
                return DataClientSkillChange;
                break;

            case ARTIKUL_ACTION_SERVER_COMMAND:
                return DataServerArtikulAction;
                break;
            case ARTIKUL_BREAK_CLIENT_COMMAND:
                return DataClientArtikulBreak;
                break;
            case ARTIKUL_REPAIR_SERVER_COMMAND:
                return DataServerArtikulRepair;
                break;
            case ARTIKUL_REPAIR_CLIENT_COMMAND:
                return DataClientArtikulRepair;
                break;
            case ARTIKUL_SELL_SERVER_COMMAND:
                return DataServerArtikulSell;
                break;

            case QUEST_LIST_SERVER_COMMAND:
                return DataServerQuestList;
                break;
            case QUEST_LIST_CLIENT_COMMAND:
                return DataClientQuestList;
                break;
            case QUEST_REFUSE_SERVER_COMMAND:
                return DataServerQuestRefuse;
                break;
            case QUEST_REFUSE_CLIENT_COMMAND:
                return DataClientQuestRefuse;
                break;
            case QUEST_COMPLETE_CLIENT_COMMAND:
                return DataClientQuestComplete;
                break;
            case MESSAGE_CLIENT_COMMAND:
                return DataClientMessage;
                break;
            case PING_SERVER_COMMAND:
                return DataServerPing;
                break;

            case LOCATION_LOAD_CLIENT_COMMAND:
                return DataClientLocationLoad;
                break;
            case LOCATION_ACTION_SERVER_COMMAND:
                return DataServerLocationAction;
                break;
            case LOCATION_ACTION_CLIENT_COMMAND:
                return DataClientLocationAction;
                break;
            case CHALLENGE_LIST_SERVER_COMMAND:
                return DataServerChallengeList;
                break;
            case CHALLENGE_LIST_CLIENT_COMMAND:
                return DataClientChallengeList;
                break;
            case CHALLENGE_PROGRESS_CLIENT_COMMAND:
                return DataClientChallengeProgress;
                break;
            case CHAT_ACTION_SERVER_COMMAND:
                return DataServerChatAction;
                break;
            case CHAT_ACTION_CLIENT_COMMAND:
                return DataClientChatAction;
                break;
            case ACHIEVE_LIST_SERVER_COMMAND:
                return DataServerAchieveList;
                break;
            case ACHIEVE_LIST_CLIENT_COMMAND:
                return DataClientAchieveList;
                break;
            case PAYMENT_APPLE_SERVER_COMMAND:
                return DataServerPaymentApple;
                break;
            case PAYMENT_APPLE_CLIENT_COMMAND:
                return DataClientPaymentApple;
                break;
            case VERSION_SERVER_COMMAND:
                return DataServerVesrion;
                break;
            case VERSION_CLIENT_COMMAND:
                return DataClientVesrion;
                break;

            default:
                return null;
        }
    }
}
}
