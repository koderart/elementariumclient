package ru.elementarium.webclient.services
{
import flash.events.Event;
import flash.utils.ByteArray;

public class SocketEvent extends Event
	{
		
		public static const ON_DATA:String = "SocketEvent_ON_DATA";
		
		public var idCommand : int;
		public var data : ByteArray;
		
		public function SocketEvent(type:String, idCommand:int, data:ByteArray)
		{
			super(type);
			
			this.idCommand = idCommand;
			this.data = data;
		}
	}
}