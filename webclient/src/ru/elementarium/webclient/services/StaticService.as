package ru.elementarium.webclient.services
{
import com.junkbyte.console.Cc;
import com.junkbyte.console.ConsoleChannel;

import deng.fzip.FZip;
import deng.fzip.FZipEvent;
import deng.fzip.FZipFile;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.utils.Dictionary;

import org.robotlegs.starling.mvcs.Actor;

import ru.elementarium.webclient.events.StaticDataEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.LocaleVO;
import ru.muvs.admin.main.vo.battle.BuffVO;
import ru.muvs.admin.main.vo.battle.MagicVO;
import ru.muvs.admin.main.vo.battle.PotionVO;

public class StaticService extends Actor
	{
        private var console:ConsoleChannel = new ConsoleChannel("StaticService");
    
		[Inject]
		public var mainModel:MainModel;

        private var _loadRevision:String;
        private var _currentRevision:String;
        private var _loader: FZip;

		public function StaticService():void
		{
        }
    
		public function load():void
		{
            var request:URLRequest = new URLRequest(ConnectionService.BASE_URL_HTTP+"/static/content/meta?"+Math.random());
            var loader:URLLoader = new URLLoader();
            loader.addEventListener(Event.COMPLETE, loaderResultHandler, false, 0, true);
            loader.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
            loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
            //loader.addEventListener(HTTP_RESPONSE_STATUS, onHttpResponseStatus);

            loader.load(request);
        }

        private function onIoError(event:IOErrorEvent):void
        {
            dispatch(new StaticDataEvent(StaticDataEvent.LOADED_ERROR));
        }

        function onSecurityError(event:SecurityErrorEvent):void
        {
            dispatch(new StaticDataEvent(StaticDataEvent.LOADED_ERROR));
        }

        private function loaderResultHandler(event:Object):void {
            var loader:URLLoader = event.target as URLLoader;
            loader.removeEventListener(Event.COMPLETE, loaderResultHandler);
            var lines:Array = loader.data.split("\n");
            for each (var str:String in lines) {
                if (str.indexOf("revision=") == 0) {
                    _loadRevision = str.substr(9);
                    break;
                }
            }

            if (_currentRevision == _loadRevision) {
                dispatch(new StaticDataEvent(StaticDataEvent.LOADED));
            } else {
                _loader = new FZip();
                _loader.addEventListener(ProgressEvent.PROGRESS, handleProgress);
                _loader.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
                _loader.addEventListener(FZipEvent.FILE_LOADED, handleFileLoaded);
                _loader.addEventListener(Event.COMPLETE, handleComplete);
                _loader.load(new URLRequest(ConnectionService.BASE_URL_HTTP+"/static/content/"+_loadRevision+"/content.zip"));
            }
        }
    private function handleProgress(e: ProgressEvent):void
    {
        var currentProgress:int = e.bytesLoaded/e.bytesTotal*100;
        dispatchWith(StaticDataEvent.PROGRESS, false, currentProgress);
    }

    private function handleFileLoaded(e: FZipEvent):void
    {
    }

    private function handleComplete(e: Event):void
    {
        var len:uint = _loader.getFileCount();

        for (var i:int=0; i < len; i++) {
            var file: FZipFile = _loader.getFileAt(i);
            if (file.sizeUncompressed > 0) {
                var jsonData:Object = JSON.parse(file.getContentAsString());
                switch (file.filename) {
                    case "ArtikulStorage.json":
                        MainModel.artikulData = [];
                        var artikul:ArtikulVO;
                        for each (var o:Object in jsonData) {
                            artikul = new ArtikulVO(o);
                            MainModel.artikulData[artikul.artikulId] = artikul;
                        }
                        break;
                    case "StoreStorage.json":
                        MainModel.storeData = [];
                        var store:StoreVO;
                        for each (o in jsonData) {
                            store = new StoreVO(o);
                            MainModel.storeData[store.storeId] = store;
                        }
                        break;
                    case "MobStorage.json":
                        MainModel.mobData = [];
                        var mob:MobVO;
                        for each (o in jsonData) {
                            mob = new MobVO(o);
                            MainModel.mobData[mob.mobId] = mob;
                        }
                        break;
                    case "NpcStorage.json":
                        MainModel.npcData = [];
                        var npc:NpcVO;
                        for each (o in jsonData) {
                            npc = new NpcVO(o);
                            MainModel.npcData[npc.npcId] = npc;
                        }
                        break;
                    case "QuestStorage.json":
                        MainModel.questData = [];
                        var quest:QuestVO;
                        for each (o in jsonData) {
                            quest = new QuestVO(o);
                            MainModel.questData[quest.questId] = quest;
                        }
                        break;
                    case "LevelStorage.json":
                        MainModel.levelExp = [];
                        MainModel.levelData = [];
                        var level:LevelVO;
                        for each (o in jsonData) {
                            level = new LevelVO(o);
                            if (!MainModel.levelData[level.element]) {
                                MainModel.levelData[level.element] = new Dictionary();
                            }
                            MainModel.levelData[level.element][level.number] = level;
                            MainModel.levelExp[level.number] = level.exp;
                        }
                        break;
                    case "BankVkItemStorage.json":
                        MainModel.paymentVkData = [];
                        for each (o in jsonData) {
                            MainModel.paymentVkData.push(new SocialPaymentVkVO(o));
                        }
                        break;
                    case "BankMyMailServiceStorage.json":
                        MainModel.paymentMailData = [];
                        for each (o in jsonData) {
                            MainModel.paymentMailData.push(new SocialPaymentMailVO(o));
                        }
                        break;
                    case "BankAppleInAppStorage.json":
                        MainModel.paymentAppleData = [];
                        for each (o in jsonData) {
                            MainModel.paymentAppleData.push(new SocialPaymentAppleVO(o));
                        }
                        break;
                    case "SkillStorage.json":
                        MainModel.skillTitleData = [];
                        for each (o in jsonData) {
                            MainModel.skillTitleData[o.skill] = o.title;
                        }
                        break;
                    case "LocationStorage.json":
                        MainModel.locationData = [];
                        MainModel.locationMemberData = [];
                        var location:LocationVO;
                        for each (o in jsonData) {
                            location = new LocationVO(o);
                            MainModel.locationData[location.locationId] = location;
                            for each (var member:LocationMemberVO in location.locationMember) {
                                MainModel.locationMemberData[member.locationMemberId] = member;
                            }
                        }
                        break;
                    case "AchieveStorage.json":
                        MainModel.achieveData = [];
                        var achieve:AchieveVO;
                        for each (o in jsonData) {
                            achieve = new AchieveVO(o);
                            MainModel.achieveData[achieve.achieveId] = achieve;
                        }
                        break;
                    case "LocalizationStorage.json":
                        MainModel.localeData = [];
                        var locale:LocaleVO;
                        for each (o in jsonData) {
                            locale = new LocaleVO(o);
                            if (locale.token) {
                                MainModel.localeData[locale.token] = locale;
                            }
                        }
                        break;
                    case "BattleMagicStorage.json":
                        MainModel.magicData = [];
                        var magic:MagicVO;
                        for each (o in jsonData) {
                            magic = new MagicVO(o);
                            MainModel.magicData[magic.battleMagicId] = magic;
                        }
                        break;
                    case "BattlePotionStorage.json":
                        MainModel.potionData = [];
                        var potion:PotionVO;
                        for each (o in jsonData) {
                            potion = new PotionVO(o);
                            MainModel.potionData[potion.battlePotionId] = potion;
                        }
                        break;
                    case "BattleBuffStorage.json":
                        MainModel.buffData = [];
                        var buff:BuffVO;
                        for each (o in jsonData) {
                            buff = new BuffVO(o);
                            MainModel.buffData[buff.battleBuffId] = buff;
                        }
                        break;
                }
            }
        }
        if (!MainModel.artikulData ||
            !MainModel.storeData ||
            !MainModel.mobData ||
            !MainModel.npcData ||
            !MainModel.levelData ||
            !MainModel.paymentVkData ||
            !MainModel.paymentMailData ||
            !MainModel.paymentAppleData ||
            !MainModel.skillTitleData ||
            !MainModel.locationData ||
            !MainModel.achieveData ||
            !MainModel.localeData ||
            !MainModel.magicData ||
            !MainModel.potionData ||
            !MainModel.buffData) {
            dispatch(new StaticDataEvent(StaticDataEvent.LOADED_ERROR));
        } else {
            _currentRevision = _loadRevision;
            dispatch(new StaticDataEvent(StaticDataEvent.LOADED));
        }
    }
}
}
