package ru.elementarium.webclient.services
{
import com.junkbyte.console.Cc;
import com.junkbyte.console.ConsoleChannel;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.Socket;
import flash.system.Security;
import flash.utils.ByteArray;
import flash.utils.clearTimeout;
import flash.utils.setTimeout;

import org.apache.thrift.FFUtil;
import org.apache.thrift.TBase;

public class SocketConnection extends EventDispatcher
	{
		private var ch:ConsoleChannel;	
		private var socket:Socket;

        private var _host:String;
        private var _globalBytes:uint = 0;

        public function get host():String
        {
         return _host;
        }

        private var _port:uint;

        public function get port():uint
        {
         return _port;
        }

		public function get connected():Boolean
		{
			return socket ? socket.connected : false;
		}
		
		public function SocketConnection(host:String, port:int)
		{
		    _host = host;
		    _port = port;
			ch = new ConsoleChannel ("net#"+host+":"+port);
		}
		
		
		private var maxReconnectCounts:int = 20;
		private var _reconnectCounts:int = 0;
		private var _connecting:Boolean;
		private var _connectingTimeout:int;

		public function connect():void
		{
			if (_connecting) return;
			
			if (_reconnectCounts == maxReconnectCounts) return;
			
			_reconnectCounts ++;
			if (socket == null)
			{
                Security.loadPolicyFile("xmlsocket://" + host + ":843");
				socket = new Socket();
				
				socket.addEventListener(Event.CONNECT, connectListener);
				socket.addEventListener(Event.CLOSE, closeListener);
				socket.addEventListener(ProgressEvent.SOCKET_DATA, socketDataListener);
				socket.addEventListener(IOErrorEvent.IO_ERROR, ioErrorListener);
				socket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorListener);
			}
			
			try {
				ch.log("Attempting connection...");  
				_connecting = true;
				socket.connect(host, port);
                //_connectingTimeout = setTimeout(checkConnection, 2000);

			} catch (e:Error) 
			{
				_connecting = false;
				ch.log("Connection problem!\n");
				ch.log(e.message);
			}
		}

        public function checkConnection():void
        {

            if (_connecting) {
                Cc.error("_connectingTimeout");
                socket.close();
                //Security.loadPolicyFile("xmlsocket://" + host + ":843");
                socket.connect(host, port);
                _connectingTimeout = setTimeout(checkConnection, 2000);
            }
        }

		public function disconnect():void
		{
			if (connected) 
			{
				socket.close();
			}
		}
		
		
		private var disposed:Boolean;
		
		public function dispose ():void
		{
			
			if (!disposed) 
			{
				ch.warn("connection disposed");
				
				disposed = true;
				
				socket.removeEventListener(Event.CONNECT, connectListener);
				socket.removeEventListener(Event.CLOSE, closeListener);
				socket.removeEventListener(ProgressEvent.SOCKET_DATA, socketDataListener);
				socket.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorListener);
				
				_connecting = false;
				
				disconnect();
				
				socket = null;
			}
		}
		
        public function sendToSocket(commandId:int, userId:int, data:TBase):void {
            Cc.warn("sendToSocket", commandId, _globalBytes);
            if (socket && socket.connected) {
                var BA:ByteArray = new ByteArray();
                var bufferOut:ByteArray = new ByteArray();
                FFUtil.EncodeMsg(data, BA);
                bufferOut.writeInt(BA.length);
                bufferOut.writeInt(commandId);
                bufferOut.writeInt(userId);
                bufferOut.writeBytes(BA);
				_globalBytes += bufferOut.length;
                //ch.log("#-RAW-DATA-OUT-# ", getTimer(), Hex.fromArray(bufferOut, true));
                socket.writeBytes(bufferOut);
                socket.flush();
            } else {
                ch.error("Not write to buffer. Socket not connected.");
            }
        }

        private var bufferIn:ByteArray = new ByteArray();
    
		protected function socketDataListener(event:ProgressEvent):void
		{
            var tmp:ByteArray = new ByteArray();
            socket.readBytes(tmp);
            tmp.readBytes(bufferIn, bufferIn.length);
            //ch.log("#-RAW-DATA-IN-# ", getTimer() , Hex.fromArray(bufferIn, true));
			_globalBytes += bufferIn.length;
            parseBufferIn();
		}

        private function parseBufferIn():void {
            bufferIn.position = 0;
    
            if (bufferIn.bytesAvailable >= 12) {
                var packetLength:int = bufferIn.readUnsignedInt();
                var idCommand:int = bufferIn.readUnsignedInt();
                var idUser:int = bufferIn.readUnsignedInt();
    
                if (bufferIn.bytesAvailable >= packetLength) {
                    var packetData:ByteArray = new ByteArray();
                    bufferIn.readBytes(packetData, 0, packetLength);
    
                    var tmp:ByteArray = new ByteArray();
                    bufferIn.readBytes(tmp);
                    bufferIn = tmp;

                    dispatchEvent (new SocketEvent(SocketEvent.ON_DATA, idCommand, packetData));
                    parseBufferIn();
                }
            }
        }
    
		protected function closeListener(event:Event):void
		{ 
			ch.log("closed");
			if (_connecting) {

                socket.connect(host, port);
                clearTimeout(_connectingTimeout);
                _connectingTimeout = setTimeout(checkConnection, 2000);
			}
			_connecting = false;
			dispatchEvent(event.clone());
		}
		
		protected function connectListener(event:Event):void
		{
            trace("connectListener")
			_reconnectCounts = 0;
			_connecting = false;
            clearTimeout(_connectingTimeout);
			ch.log("connected");
			dispatchEvent(event.clone());
		}
		
		protected function ioErrorListener(event:IOErrorEvent):void
		{
			ch.error("I/O Error: " + event.text);
			_connecting = false;
			this.dispatchEvent(event.clone());
		}
		
		protected function securityErrorListener(event:SecurityErrorEvent):void
		{
			ch.log("security error");
			_connecting = false;
			dispatchEvent(event.clone());
		}
		
	}
}