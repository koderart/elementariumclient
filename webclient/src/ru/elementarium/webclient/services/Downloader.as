package ru.elementarium.webclient.services {
import com.junkbyte.console.Cc;
import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;

import starling.events.EventDispatcher;

public class Downloader extends EventDispatcher{

    public static const LOADED:String = "Downloader.LOADED";
    public static const PROGRESS:String = "Downloader.PROGRESS";
    public static const LOADED_ERROR:String = "Downloader.LOADED_ERROR";

    private var _loader: URLLoader;
    private var _list: Array = [];
    private var _currentFile: String;
    
    private var _totalFile: int;
    private var _path: String;
    private var _url: String;
    
    public function Downloader(path:String, url:String)
    {
        _path = path;
        _url = url;
        
        _loader = new URLLoader();
        _loader.dataFormat = URLLoaderDataFormat.BINARY;
        _loader.addEventListener(Event.COMPLETE, handleComplete);
        _loader.addEventListener(ProgressEvent.PROGRESS, handleProgress);
        _loader.addEventListener(IOErrorEvent.IO_ERROR, handleError);
        _loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSecurityError);
    }

    private function handleProgress(e: ProgressEvent):void
    {
        var currentProgress:int = e.bytesLoaded/e.bytesTotal*100;
        dispatchEventWith(PROGRESS, false, "Скачивание: "+currentProgress+"% "+"["+(_totalFile-_list.length)+"/"+_totalFile+"]");
    }

    public function load():void
    {
        _totalFile = _list.length;
        next();
    }

    public function addFileToList(url: String):void
    {
        _list.push(url);
    }

    private function next():void
    {
        if (_list.length > 0) {
            loadAsset(_list.shift());
        } else {
            _loader.removeEventListener(Event.COMPLETE, handleComplete);
            _loader.removeEventListener(ProgressEvent.PROGRESS, handleProgress);
            _loader.removeEventListener(IOErrorEvent.IO_ERROR, handleError);
            _loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSecurityError);
            dispatchEventWith(LOADED);
        }
    }

    private function loadAsset(file: String):void
    {
        trace('Downloader', 'loadAsset', file);
        _currentFile = file;
        _loader.load(new URLRequest(_url + file));
    }

    private function handleComplete(e: Event):void
    {
        trace('Downloader', 'handleComplete');
        var loader:URLLoader = e.target as URLLoader;
        var file: File = File.applicationStorageDirectory.resolvePath(_path+"/"+_currentFile);
        file.preventBackup = true;
        if (!file.isDirectory) {
            var fs:FileStream = new FileStream();
            fs.open(file, FileMode.WRITE);
            fs.writeBytes(loader.data);
            fs.close();
        }
        next();
    }

    private function handleError(e: IOErrorEvent):void
    {
        dispatchEventWith(LOADED_ERROR);
        next();
    }

    private function handleSecurityError(e: SecurityErrorEvent):void
    {
        dispatchEventWith(LOADED_ERROR);
        next();
    }
}
}
