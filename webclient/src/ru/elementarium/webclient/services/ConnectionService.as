package ru.elementarium.webclient.services
{
import com.junkbyte.console.Cc;
import com.junkbyte.console.ConsoleChannel;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.utils.Dictionary;
import flash.utils.Timer;

import org.apache.thrift.FFUtil;
import org.apache.thrift.TBase;
import org.robotlegs.starling.mvcs.Actor;

import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.*;
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.admin.main.vo.LocationVO;
import ru.muvs.elementarium.thrift.achieve.DataClientAchieveList;
import ru.muvs.elementarium.thrift.achieve.DataServerAchieveList;
import ru.muvs.elementarium.thrift.artikul.*;
import ru.muvs.elementarium.thrift.bag.*;
import ru.muvs.elementarium.thrift.battle.*;
import ru.muvs.elementarium.thrift.battle.action.DataBattleAction;
import ru.muvs.elementarium.thrift.battle.action.DataBattleActionExtract;
import ru.muvs.elementarium.thrift.battle.action.DataBattleActionExtractSwapCell;
import ru.muvs.elementarium.thrift.battle.action.DataBattleActionExtractTakeBelt;
import ru.muvs.elementarium.thrift.battle.action.DataBattleActionType;
import ru.muvs.elementarium.thrift.battle.area.DataAreaCell;
import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMember;
import ru.muvs.elementarium.thrift.challenge.*;
import ru.muvs.elementarium.thrift.chat.*;
import ru.muvs.elementarium.thrift.chat.action.*;
import ru.muvs.elementarium.thrift.chat.channel.DataChatChannelType;
import ru.muvs.elementarium.thrift.chat.message.DataChatMessage;
import ru.muvs.elementarium.thrift.doll.belt.*;
import ru.muvs.elementarium.thrift.doll.slot.*;
import ru.muvs.elementarium.thrift.location.*;
import ru.muvs.elementarium.thrift.location.action.*;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.location.position.DataLocationPosition;
import ru.muvs.elementarium.thrift.login.*;
import ru.muvs.elementarium.thrift.message.DataClientMessage;
import ru.muvs.elementarium.thrift.money.*;
import ru.muvs.elementarium.thrift.payment.DataClientPaymentApple;
import ru.muvs.elementarium.thrift.payment.DataServerPaymentApple;
import ru.muvs.elementarium.thrift.ping.DataServerPing;
import ru.muvs.elementarium.thrift.player.*;
import ru.muvs.elementarium.thrift.quest.*;
import ru.muvs.elementarium.thrift.store.DataServerStoreBuy;
import ru.muvs.elementarium.thrift.version.DataClientVesrion;
import ru.muvs.elementarium.thrift.version.DataServerVesrion;

public class ConnectionService extends Actor
	{
		private static const BASE_PORT:int = 9090;

		private var _connection:SocketConnection;

        private var console:ConsoleChannel = new ConsoleChannel("ConnectionService");
    
        private var pingerTimer:Timer = new Timer(30000);
    
		[Inject]
		public var mainModel:MainModel;

		[Inject]
		public var gameModel:Match3Model;

        public static function get BASE_URL():String {
            BUILD::release { return "elemgame.com"; }
            BUILD::mobile { return "elemgame.com"; }
            BUILD::test { return "sandbox.elemgame.com"; }
        }

        public static function get BASE_URL_HTTP():String {
            BUILD::release { return ""; }
            BUILD::mobile { return "https://elemgame.com"; }
            BUILD::test { return ""; }
        }

        public static function get BASE_URL_ASSETS():String {
            BUILD::release { return "assets/"; }
            BUILD::mobile { return "https://elemgame.com/client/assets/"; }
            BUILD::test { return "assets/"; }
        }

		public function ConnectionService():void
		{
            pingerTimer.addEventListener(TimerEvent.TIMER, pingerTimer_timerHandler);
        }
    
		public function connect():void
		{
            console.log("connect");
            _connection = new SocketConnection(BASE_URL, BASE_PORT);
            _connection.connect();
            _connection.addEventListener(SocketEvent.ON_DATA, connection_SocketEvent_ON_DATAHandler);
            _connection.addEventListener(Event.CONNECT, connection_connectHandler);
            _connection.addEventListener(Event.CLOSE, connection_closeHandler);
            _connection.addEventListener(IOErrorEvent.IO_ERROR, connection_errorListener);
            _connection.addEventListener(SecurityErrorEvent.SECURITY_ERROR, connection_errorListener);
            pingerTimer.start();
        }

        public function disconnect():void
        {
            console.log("disconnect");
            _connection.disconnect();
            pingerTimer.stop();
            mainModel.player = new DataPlayer();
            dispatch(new ConnectionEvent(ConnectionEvent.DISCONNECTED));
        }
    
        public function connected():Boolean
        {
            return _connection && _connection.connected;
        }    
    
        private function pingerTimer_timerHandler(event:TimerEvent):void {
            if (_connection.connected) {
                _connection.sendToSocket(Protocol.PING_SERVER_COMMAND, mainModel.player.id, new DataServerPing());
            }
        }

        private function connection_connectHandler(event:Event):void {
            console.log("connection_connectHandler");
            pingerTimer.start();
            dispatch(new ConnectionEvent(ConnectionEvent.CONNECTED));
        }

        private function connection_closeHandler(event:Event):void {
            console.log("connection_closeHandler");
            pingerTimer.stop();
            mainModel.player = new DataPlayer();
            dispatch(new ConnectionEvent(ConnectionEvent.DISCONNECTED));
        }

        private function connection_errorListener(event:Event):void {
            console.log("connection_errorListener");
            dispatch(new ConnectionEvent(ConnectionEvent.CONNECT_FAILED));
        }

        private function connection_SocketEvent_ON_DATAHandler(event:SocketEvent):void {
            var cc:Class = Protocol.classFromCommand(event.idCommand);
            var dataClass:TBase = new cc();
            FFUtil.DecodeMsg(dataClass, event.data);
            parseCommand(event.idCommand, dataClass);
        }

        private function parseCommand(id:int, data:TBase):void {
            //console.warn("parseCommand", id);
            switch (id) {
                case Protocol.LOGIN_VK_CLIENT_COMMAND:
                    var vk:DataClientLoginVk = data as DataClientLoginVk;
                    console.log("vk.status", vk.status);
                    switch (vk.status) {
                        case DataClientLoginVkStatus.LOGIN_OK:
                            mainModel.player = vk.player;
                            dispatch(new LoginEvent(LoginEvent.LOGIN_SUCCESS));
                            break;
                        case DataClientLoginVkStatus.LOGIN_FAIL:
                            dispatch(new LoginEvent(LoginEvent.LOGIN_FAILED));
                            break;
                        case DataClientLoginVkStatus.PLAYER_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_UNDEFINED));
                            break;
                        case DataClientLoginVkStatus.PLAYER_FRACTION_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_FRACTION_UNDEFINED));
                            break;
                        case DataClientLoginVkStatus.PLAYER_NAME_BUSY:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_NAME_IS_BUSY));
                            break;
                    }
                    break;
                case Protocol.LOGIN_MY_MAIL_CLIENT_COMMAND:
                    var myMail:DataClientLoginMyMail = data as DataClientLoginMyMail;
                    console.log("myMail.status", myMail.status);
                    switch (myMail.status) {
                        case DataClientLoginMyMailStatus.LOGIN_OK:
                            mainModel.player = myMail.player;
                            dispatch(new LoginEvent(LoginEvent.LOGIN_SUCCESS));
                            break;
                        case DataClientLoginMyMailStatus.LOGIN_FAIL:
                            dispatch(new LoginEvent(LoginEvent.LOGIN_FAILED));
                            break;
                        case DataClientLoginMyMailStatus.PLAYER_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_UNDEFINED));
                            break;
                        case DataClientLoginMyMailStatus.PLAYER_FRACTION_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_FRACTION_UNDEFINED));
                            break;
                        case DataClientLoginMyMailStatus.PLAYER_NAME_BUSY:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_NAME_IS_BUSY));
                            break;
                    }
                    break;
                case Protocol.LOGIN_EMAIL_CLIENT_COMMAND:
                    var email:DataClientLoginEmail = data as DataClientLoginEmail;
                    console.log("email.status", email.status);
                    switch (email.status) {
                        case DataClientLoginEmailStatus.LOGIN_OK:
                            mainModel.player = email.player;
                            dispatch(new LoginEvent(LoginEvent.LOGIN_SUCCESS));
                            break;
                        case DataClientLoginEmailStatus.LOGIN_FAIL:
                            dispatch(new LoginEvent(LoginEvent.LOGIN_FAILED));
                            break;
                        case DataClientLoginEmailStatus.PLAYER_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_UNDEFINED));
                            break;
                        case DataClientLoginEmailStatus.PLAYER_FRACTION_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_FRACTION_UNDEFINED));
                            break;
                        case DataClientLoginEmailStatus.PLAYER_NAME_BUSY:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_NAME_IS_BUSY));
                            break;
                        case DataClientLoginEmailStatus.EMAIL_IS_BROKEN:
                            dispatch(new LoginEvent(LoginEvent.EMAIL_IS_BROKEN));
                            break;
                    }
                    break;
                case Protocol.LOGIN_APPLE_CLIENT_COMMAND:
                    var apple:DataClientLoginApple = data as DataClientLoginApple;
                    console.log("apple.status", apple.status);
                    switch (apple.status) {
                        case DataClientLoginAppleStatus.LOGIN_OK:
                            mainModel.player = apple.player;
                            dispatch(new LoginEvent(LoginEvent.LOGIN_SUCCESS));
                            break;
                        case DataClientLoginAppleStatus.LOGIN_FAIL:
                            dispatch(new LoginEvent(LoginEvent.LOGIN_FAILED));
                            break;
                        case DataClientLoginAppleStatus.PLAYER_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_UNDEFINED));
                            break;
                        case DataClientLoginAppleStatus.PLAYER_FRACTION_UNDEFINED:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_FRACTION_UNDEFINED));
                            break;
                        case DataClientLoginAppleStatus.PLAYER_NAME_BUSY:
                            dispatch(new LoginEvent(LoginEvent.PLAYER_NAME_IS_BUSY));
                            break;
                    }
                    break;

                case Protocol.BATTLE_QUEUE_JOIN_CLIENT_COMMAND:
                    var dd2:DataClientBattleQueueJoin = data as DataClientBattleQueueJoin;
                    console.log("Protocol.BATTLE_QUEUE_JOIN_CLIENT_COMMAND", dd2.queue);
                    gameModel.queueType = dd2.queue;
                    dispatchWith(BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_JOIN_CLIENT_COMMAND:
                    var bb:DataClientBattleJoin = data as DataClientBattleJoin;
                    console.log("Protocol.BATTLE_JOIN_CLIENT_COMMAND",bb.members, bb.areaWidth, bb.areaHeight);
                    mainModel.isBattle = true;
                    gameModel.currentMemberId = null;
                    gameModel.areaWidth = bb.areaWidth;
                    gameModel.areaHeight = bb.areaHeight;
                    gameModel.areaCells = bb.areaCells;
                    gameModel.members = bb.members;
                    dispatchWith(BattleEventType.BATTLE_JOIN_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_UNJOIN_CLIENT_COMMAND:
                    mainModel.isBattle = false;
                    console.log("Protocol.BATTLE_UN_JOIN_CLIENT_COMMAND");
                    dispatchWith(BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_FIN_CLIENT_COMMAND:
                    mainModel.isBattle = false;
                    console.log("Protocol.BATTLE_FIN_CLIENT_COMMAND");
                    break;
                case Protocol.BATTLE_SWAP_CLIENT_COMMAND:
                    var member:DataClientBattleSwap = data as DataClientBattleSwap;
                    console.log("Protocol.BATTLE_NEXT_IMPACT_MEMBER_CLIENT_COMMAND member-id", member.member.id, "time", member.left);
                    gameModel.leftTime = member.left;
                    gameModel.currentMemberId = member.member.id;
                    dispatchWith(BattleEventType.BATTLE_SWAP_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_MAGIC_CLIENT_COMMAND:
                    var magic:DataClientBattleMagic = data as DataClientBattleMagic;
                    console.log("Protocol.BATTLE_MAGIC_CLIENT_COMMAND", magic.magicId, magic.actions);
                    printActionText(magic.actions, "");
                    gameModel.currentMagic = magic;
                    dispatchWith(BattleEventType.BATTLE_MAGIC_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_MEMBER_JOIN_CLIENT_COMMAND:
                    var memberJoin:DataClientBattleMemberJoin = data as DataClientBattleMemberJoin;
                    console.log("Protocol.BATTLE_MEMBER_JOIN_CLIENT_COMMAND member", memberJoin.member);
                    gameModel.memberJoin = memberJoin.member;
                    dispatchWith(BattleEventType.BATTLE_MEMBER_JOIN_CLIENT_COMMAND);
                    break;
                case Protocol.BAG_LIST_CLIENT_COMMAND:
                    var bagList:DataClientBagList = data as DataClientBagList;
                    console.log("Protocol.BAG_LIST_CLIENT_COMMAND", bagList.artikuls);
                    mainModel.inventoryArtikuls = bagList.artikuls;
                    dispatch(new InventoryEvent(InventoryEvent.BAG_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.BAG_REMOVE_CLIENT_COMMAND:
                    var bagRemove:DataClientBagRemove = data as DataClientBagRemove;
                    console.log("Protocol.BAG_REMOVE_CLIENT_COMMAND", bagRemove.artikul);
                    mainModel.inventoryArtikul = bagRemove.artikul;
                    dispatch(new InventoryEvent(InventoryEvent.BAG_REMOVE_CLIENT_COMMAND));
                    break;
                case Protocol.BAG_ADD_CLIENT_COMMAND:
                    var bagAdd:DataClientBagAdd = data as DataClientBagAdd;
                    console.log("Protocol.BAG_ADD_CLIENT_COMMAND", bagAdd.artikul);
                    mainModel.inventoryArtikul = bagAdd.artikul;
                    dispatch(new InventoryEvent(InventoryEvent.BAG_ADD_CLIENT_COMMAND));
                    break;
                case Protocol.BAG_CHANGE_CLIENT_COMMAND:
                    var bagChange:DataClientBagChange = data as DataClientBagChange;
                    console.log("Protocol.BAG_CHANGE_CLIENT_COMMAND", bagChange.artikul);
                    mainModel.inventoryArtikul = bagChange.artikul;
                    dispatch(new InventoryEvent(InventoryEvent.BAG_CHANGE_CLIENT_COMMAND));
                    break;
                case Protocol.BELT_LIST_CLIENT_COMMAND:
                    var beltList:DataClientBeltList = data as DataClientBeltList;
                    console.log("Protocol.BELT_LIST_CLIENT_COMMAND", beltList.belts);
                    mainModel.inventoryBelts = beltList.belts;
                    dispatch(new InventoryEvent(InventoryEvent.BELT_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.BELT_TAKE_ON_CLIENT_COMMAND:
                    var beltOn:DataClientBeltTakeOn = data as DataClientBeltTakeOn;
                    console.log("Protocol.BELT_TAKE_ON_CLIENT_COMMAND", beltOn.belt);
                    mainModel.inventoryBelt = beltOn.belt;
                    mainModel.inventoryBelts[beltOn.belt.order-1] = beltOn.belt;
                    dispatch(new InventoryEvent(InventoryEvent.BELT_TAKE_ON_CLIENT_COMMAND));
                    break;
                case Protocol.BELT_TAKE_OFF_CLIENT_COMMAND:
                    var beltOff:DataClientBeltTakeOff = data as DataClientBeltTakeOff;
                    console.log("Protocol.BELT_TAKE_OFF_CLIENT_COMMAND", beltOff.belt);
                    mainModel.inventoryBelt = beltOff.belt;
                    delete mainModel.inventoryBelts[beltOff.belt.order-1];
                    dispatch(new InventoryEvent(InventoryEvent.BELT_TAKE_OFF_CLIENT_COMMAND));
                    break;
                case Protocol.BELT_CHANGE_CLIENT_COMMAND:
                    var beltChange:DataClientBeltChange = data as DataClientBeltChange;
                    console.log("Protocol.BELT_CHANGE_CLIENT_COMMAND", beltChange.belt);
                    break;
                case Protocol.BELT_REMOVE_CLIENT_COMMAND:
                    var beltRemove:DataClientBeltRemove = data as DataClientBeltRemove;
                    console.log("Protocol.BELT_REMOVE_CLIENT_COMMAND", beltRemove.belt);
                    break;
                case Protocol.SLOT_LIST_CLIENT_COMMAND:
                    var slotList:DataClientSlotList = data as DataClientSlotList;
                    console.log("Protocol.SLOT_LIST_CLIENT_COMMAND", slotList.slots);
                    mainModel.inventorySlots = slotList.slots;
                    dispatch(new InventoryEvent(InventoryEvent.SLOT_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.SLOT_TAKE_ON_CLIENT_COMMAND:
                    var slotOn:DataClientSlotTakeOn = data as DataClientSlotTakeOn;
                    console.log("Protocol.SLOT_TAKE_ON_CLIENT_COMMAND", slotOn.slot);
                    mainModel.inventorySlot = slotOn.slot;
                    dispatch(new InventoryEvent(InventoryEvent.SLOT_TAKE_ON_CLIENT_COMMAND));
                    break;
                case Protocol.SLOT_TAKE_OFF_CLIENT_COMMAND:
                    var slotOff:DataClientSlotTakeOff = data as DataClientSlotTakeOff;
                    console.log("Protocol.SLOT_TAKE_OFF_CLIENT_COMMAND", slotOff.slot);
                    mainModel.inventorySlot = slotOff.slot;
                    dispatch(new InventoryEvent(InventoryEvent.SLOT_TAKE_OFF_CLIENT_COMMAND));
                    break;
                case Protocol.MONEY_LIST_CLIENT_COMMAND:
                    var moneyList:DataClientMoneyList = data as DataClientMoneyList;
                    console.log("Protocol.MONEY_LIST_CLIENT_COMMAND", moneyList.moneys);
                    for each (var money:DataMoney in moneyList.moneys) {
                        switch (money.type) {
                            case DataMoneyType.GOLD: mainModel.moneyGold = money.money; break;
                            case DataMoneyType.CRYSTAL: mainModel.moneyCrystal = money.money; break;
                        }
                    }
                    dispatch(new UserEvent(UserEvent.MONEY_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.MONEY_CHANGE_CLIENT_COMMAND:
                    var moneyData:DataClientMoneyChange = data as DataClientMoneyChange;
                    console.log("Protocol.MONEY_CHANGE_CLIENT_COMMAND", moneyData.money);
                    switch (moneyData.money.type) {
                        case DataMoneyType.GOLD: mainModel.moneyGold = moneyData.money.money; break;
                        case DataMoneyType.CRYSTAL: mainModel.moneyCrystal = moneyData.money.money; break;
                    }
                    dispatch(new UserEvent(UserEvent.MONEY_CHANGE_CLIENT_COMMAND));
                    break;
                case Protocol.SKILL_LIST_CLIENT_COMMAND:
                    var skillList:DataClientSkillList = data as DataClientSkillList;
                    console.log("Protocol.SKILL_LIST_CLIENT_COMMAND", skillList.skills);
                    mainModel.updatePlayerSkill(skillList.skills);
                    dispatch(new UserEvent(UserEvent.SKILL_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.SKILL_CHANGE_CLIENT_COMMAND:
                    var skill:DataClientSkillChange = data as DataClientSkillChange;
                    console.log("Protocol.SKILL_CHANGE_CLIENT_COMMAND", skill.skills);
                    mainModel.updatePlayerSkill(skill.skills);
                    dispatch(new UserEvent(UserEvent.SKILL_CHANGE_CLIENT_COMMAND));
                    break;
                case Protocol.QUEST_REFUSE_CLIENT_COMMAND:
                    var questRefuse:DataClientQuestRefuse = data as DataClientQuestRefuse;
                    console.log("Protocol.QUEST_REFUSE_CLIENT_COMMAND", questRefuse.quest);
                    mainModel.quest = questRefuse.quest;
                    dispatch(new QuestEvent(QuestEvent.QUEST_REFUSE_CLIENT_COMMAND));
                    break;
                case Protocol.QUEST_LIST_CLIENT_COMMAND:
                    var questAllList:DataClientQuestList = data as DataClientQuestList;
                    console.log("Protocol.QUEST_LIST_NOT_DONE_CLIENT_COMMAND", questAllList.quests);
                    mainModel.currentNpc = null;
                    mainModel.questList = questAllList.quests;
                    dispatch(new QuestEvent(QuestEvent.QUEST_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.QUEST_COMPLETE_CLIENT_COMMAND:
                    var questComplete:DataClientQuestComplete = data as DataClientQuestComplete;
                    console.log("Protocol.QUEST_COMPLETE_CLIENT_COMMAND", questComplete.quest);
                    mainModel.quest = questComplete.quest;
                    dispatch(new QuestEvent(QuestEvent.QUEST_COMPLETE_CLIENT_COMMAND));
                    break;
                case Protocol.MESSAGE_CLIENT_COMMAND:
                    var message:DataClientMessage = data as DataClientMessage;
                    console.log("Protocol.MESSAGE_CLIENT_COMMAND", message.message);
                    mainModel.message = message.message;
                    dispatch(new GameEvent(GameEvent.MESSAGE_CLIENT_COMMAND));
                    break;
                case Protocol.ARTIKUL_REPAIR_CLIENT_COMMAND:
                    var repair:DataClientArtikulRepair = data as DataClientArtikulRepair;
                    console.log("Protocol.ARTIKUL_REPAIR_CLIENT_COMMAND", repair.artikul);
                    mainModel.durabilityArtikul = repair.artikul;
                    dispatch(new InventoryEvent(InventoryEvent.ARTIKUL_REPAIR_CLIENT_COMMAND));
                    break;

                case Protocol.LOCATION_LOAD_CLIENT_COMMAND:
                    mainModel.isBattle = false;
                    var locationEnter:DataClientLocationLoad = data as DataClientLocationLoad;
                    mainModel.currentLocation = locationEnter.locationId;
                    mainModel.locationMembers = {};
                    for each (var memberLoad:DataLocationMember in locationEnter.members) {
                        mainModel.locationMembers[memberLoad.id] = memberLoad;
                    }

                    console.log("LOCATION_LOAD_CLIENT_COMMAND", locationEnter.locationId);
                    dispatch(new LocationEvent(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND));
                    break;

                case Protocol.LOCATION_ACTION_CLIENT_COMMAND:
                    var locationAction:DataClientLocationAction = data as DataClientLocationAction;
                        //console.log("Protocol.LOCATION_ACTION_CLIENT_COMMAND", locationAction.action.type);
                        switch (locationAction.action.type) {
                            case DataLocationActionType.MEMBER_ADD:
                                console.log("    DataClientLocationActionType.MEMBER_ADD");
                                mainModel.locationMember = locationAction.action.memberAdd.member;
                                mainModel.locationMembers[mainModel.locationMember.id] = mainModel.locationMember;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD));
                                break;
                            case DataLocationActionType.MEMBER_REMOVE:
                                console.log("    DataClientLocationActionType.MEMBER_REMOVE");
                                mainModel.locationMember = locationAction.action.memberRemove.member;
                                delete mainModel.locationMembers[mainModel.locationMember.id];
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE));
                                break;
                            case DataLocationActionType.MEMBER_ENABLE:
                                console.log("    DataClientLocationActionType.MEMBER_ENABLE");
                                mainModel.locationMember = locationAction.action.memberEnable.member;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE));
                                break;
                            case DataLocationActionType.MEMBER_DISABLE:
                                console.log("    DataClientLocationActionType.MEMBER_DISABLE");
                                mainModel.locationMember = locationAction.action.memberDisable.member;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE));
                                break;
                            case DataLocationActionType.STAND:
                                mainModel.locationMemberStand = new Point(locationAction.action.stand.position.x, locationAction.action.stand.position.y);
                                console.log("    DataClientLocationActionType.STAND");
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_STAND));
                                break;
                            case DataLocationActionType.BATTLE_START:
                                console.log("    DataClientLocationActionType.BATTLE_START", locationAction.action.battleStart.members);
                                mainModel.locationMembersBattle = locationAction.action.battleStart.members;
                                for each (var battleStartMember:DataLocationMember in mainModel.locationMembersBattle) {
                                    if (mainModel.locationMembers[battleStartMember.id]) {
                                        mainModel.locationMembers[battleStartMember.id] = battleStartMember;
                                    }
                                }
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_START));
                                break;
                            case DataLocationActionType.BATTLE_STOP:
                                console.log("    DataClientLocationActionType.BATTLE_STOP", locationAction.action.battleStop.members);
                                mainModel.locationMembersBattle = locationAction.action.battleStop.members;
                                for each (var battleStopMember:DataLocationMember in mainModel.locationMembersBattle) {
                                    if (mainModel.locationMembers[battleStopMember.id]) {
                                        mainModel.locationMembers[battleStopMember.id] = battleStopMember;
                                    }
                                }
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_STOP));
                                break;
                            case DataLocationActionType.BATTLE_INFO:
                                console.log("    DataClientLocationActionType.BATTLE_INFO", locationAction.action.battleInfo);
                                mainModel.locationMember = locationAction.action.battleInfo.member;
                                mainModel.locationBattle = locationAction.action.battleInfo.battle;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_INFO));
                                break;
                            case DataLocationActionType.BATTLE_JOIN:
                                console.log("    DataClientLocationActionType.BATTLE_JOIN");
                                mainModel.locationMember = locationAction.action.battleJoin.member;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN));
                                break;
                            case DataLocationActionType.BATTLE_UNJOIN:
                                console.log("    DataClientLocationActionType.BATTLE_UNJOIN");
                                mainModel.locationMember = locationAction.action.battleUnjoin.member;
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN));
                                break;
                            case DataLocationActionType.QUEST_LIST:
                                console.log("    DataClientLocationActionType.QUEST_LIST");
                                mainModel.currentNpc = locationAction.action.questList.member;
                                mainModel.questList = locationAction.action.questList.quests;
                                dispatch(new QuestEvent(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST));
                                break;
                            case DataLocationActionType.QUEST_GIVE:
                                console.log("    DataClientLocationActionType.QUEST_GIVE");
                                mainModel.currentNpc = locationAction.action.questGive.member;
                                mainModel.quest = locationAction.action.questGive.quest;
                                dispatch(new QuestEvent(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_GIVE));
                                break;
                            case DataLocationActionType.QUEST_TAKE:
                                console.log("    DataClientLocationActionType.QUEST_TAKE");
                                mainModel.currentNpc = locationAction.action.questTake.member;
                                mainModel.quest = locationAction.action.questTake.quest;
                                dispatch(new QuestEvent(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_TAKE));
                                break;
                            case DataLocationActionType.TELEPORT:
                                console.log("    DataClientLocationActionType.TELEPORT_ENTER");
                                mainModel.currentLocation = locationAction.action.teleport.location;
                                mainModel.locationMembers = {};
                                for each (var memberTeleport:DataLocationMember in locationAction.action.teleport.members) {
                                    mainModel.locationMembers[memberTeleport.id] = memberTeleport;
                                }
                                dispatch(new LocationEvent(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND));
                                break;
                            case DataLocationActionType.CEMETERY:
                                console.log("    DataClientLocationActionType.CEMETERY");
                                mainModel.currentLocation = locationAction.action.cemetery.location;
                                mainModel.locationMembers = {};
                                for each (var memberCemetery:DataLocationMember in locationAction.action.cemetery.members) {
                                    mainModel.locationMembers[memberCemetery.id] = memberCemetery;
                                }
                                dispatch(new LocationEvent(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND));
                                break;
                            case DataLocationActionType.POSITION:
                                //console.log("    DataClientLocationActionType.POSITION", locationAction.action.position.members);
                                mainModel.locationMembersPosition = locationAction.action.position.members;
                                for each (var memberPos:DataLocationMember in mainModel.locationMembersPosition) {
                                    if (mainModel.locationMembers[memberPos.id]) {
                                        DataLocationMember(mainModel.locationMembers[memberPos.id]).position = memberPos.position;
                                    }
                                }
                                dispatch(new LocationEvent(LocationEvent.LOCATION_ACTION_POSITION));
                                break;
                        }
                break;
                
                case Protocol.CHALLENGE_LIST_CLIENT_COMMAND:
                    /*
                    var challengeList:DataClientChallengeList = data as DataClientChallengeList;
                    console.log("CHALLENGE_LIST_CLIENT_COMMAND", challengeList.challenges,challengeList.progresses);
                    MainModel.challegeData = [];
                    for each (var challenge:DataChallenge in challengeList.challenges) {
                        MainModel.challegeData[challenge.challengeId] = challenge;
                    }
                    mainModel.challengeProgress = challengeList.progresses;
                    dispatchWith(ChallengeEvent.CHALLENGE_LIST_CLIENT_COMMAND);
                    break;
                case Protocol.CHALLENGE_PROGRESS_CLIENT_COMMAND:
                    console.log("CHALLENGE_PROGRESS_CLIENT_COMMAND");
                    var challengeGoalProgressIn:DataClientChallengeProgress = data as DataClientChallengeProgress;
                    for each(var challengeProgress:DataChallengeProgress in mainModel.challengeProgress) {
                        for each(var challengeGoalProgress:DataChallengeGoalProgress in challengeProgress.progresses) {
                            if (challengeGoalProgress.challengeGoalId == challengeGoalProgressIn.progress.challengeGoalId) {
                                challengeGoalProgress.count = challengeGoalProgressIn.progress.count;
                            }
                        }
                    }
                    dispatchWith(ChallengeEvent.CHALLENGE_PROGRESS_CLIENT_COMMAND);
                    */
                    break;
                case Protocol.CHAT_ACTION_CLIENT_COMMAND:
                    var chatMessage:DataClientChatAction = data as DataClientChatAction;
                    console.log("Protocol.CHAT_ACTION_CLIENT_COMMAND", chatMessage.action);
                    mainModel.chatAction = chatMessage.action;
                    dispatch(new ChatEvent(ChatEvent.CHAT_MESSAGE_SEND_CLIENT_COMMAND));
                    break;
                case Protocol.ACHIEVE_LIST_CLIENT_COMMAND:
                    var achieveList:DataClientAchieveList = data as DataClientAchieveList;
                    console.log("Protocol.ACHIEVE_LIST_CLIENT_COMMAND", achieveList.achieves);
                    mainModel.achieveList = achieveList.achieves;
                    dispatch(new AchieveEvent(AchieveEvent.ACHIEVE_LIST_CLIENT_COMMAND));
                    break;
                case Protocol.BATTLE_BUFF_BEGIN_CLIENT_COMMAND:
                    var buffBegin:DataClientBattleBuffBegin = data as DataClientBattleBuffBegin;
                    console.log("Protocol.BATTLE_BUFF_BEGIN", buffBegin.buff, buffBegin.actions);
                    gameModel.currentBuff = buffBegin;
                    dispatchWith(BattleEventType.BATTLE_BUFF_BEGIN);
                    break;
                case Protocol.BATTLE_BUFF_END_CLIENT_COMMAND:
                    var buffEnd:DataClientBattleBuffEnd = data as DataClientBattleBuffEnd;
                    console.log("Protocol.BATTLE_BUFF_END", buffEnd.buff, buffEnd.actions);
                    gameModel.currentBuff = buffEnd;
                    dispatchWith(BattleEventType.BATTLE_BUFF_END);
                    break;
                case Protocol.BATTLE_BUFF_EFFECT_CLIENT_COMMAND:
                    var buffEffect:DataClientBattleBuffEffect = data as DataClientBattleBuffEffect;
                    console.log("Protocol.BATTLE_BUFF_EFFECT", buffEffect.buff, buffEffect.actions);
                    gameModel.currentBuff = buffEffect;
                    dispatchWith(BattleEventType.BATTLE_BUFF_EFFECT);
                    break;
                case Protocol.BATTLE_BUFF_COLLISION_CLIENT_COMMAND:
                    var buffCollision:DataClientBattleBuffCollision = data as DataClientBattleBuffCollision;
                    console.log("Protocol.BATTLE_BUFF_COLLISION", buffCollision.buff, buffCollision.actions);
                    gameModel.currentBuff = buffCollision;
                    dispatchWith(BattleEventType.BATTLE_BUFF_COLLISION);
                    break;
                case Protocol.BATTLE_BUFF_INTERRUPT_CLIENT_COMMAND:
                    var buffInterrupt:DataClientBattleBuffInterrupt = data as DataClientBattleBuffInterrupt;
                    console.log("Protocol.BATTLE_BUFF_INTERRUPT", buffInterrupt.buff, buffInterrupt.actions);
                    gameModel.currentBuff = buffInterrupt;
                    dispatchWith(BattleEventType.BATTLE_BUFF_INTERRUPT);
                    break;
                case Protocol.BATTLE_KICK_CLIENT_COMMAND:
                    var battleBuffKick:DataClientBattleKick = data as DataClientBattleKick;
                    console.log("Protocol.BATTLE_KICK_CLIENT_COMMAND", battleBuffKick.member.id);
                    gameModel.currentKickMemberId = battleBuffKick.member.id;
                    dispatchWith(BattleEventType.BATTLE_KICK_CLIENT_COMMAND);
                    break;
                case Protocol.BATTLE_POTION_CLIENT_COMMAND:
                    var battlePotion:DataClientBattlePotion = data as DataClientBattlePotion;
                    console.log("Protocol.BATTLE_POTION_CLIENT_COMMAND", battlePotion.actions, battlePotion.cools);
                    printActionText(battlePotion.actions, "");
                    gameModel.currentPotion = battlePotion;
                    dispatchWith(BattleEventType.BATTLE_POTION_CLIENT_COMMAND);
                    break;
                case Protocol.PAYMENT_APPLE_CLIENT_COMMAND:
                    var payment:DataClientPaymentApple = data as DataClientPaymentApple;
                    console.log("Protocol.PAYMENT_APPLE_CLIENT_COMMAND", payment.transactionId);
                    var paymentEvent:PaymentEvent = new PaymentEvent(PaymentEvent.PAYMENT_APPLE_CLIENT_COMMAND);
                    paymentEvent.transactionId = payment.transactionId;
                    dispatch(paymentEvent);
                    break;
                case Protocol.VERSION_CLIENT_COMMAND:
                    var version:DataClientVesrion = data as DataClientVesrion;
                    console.log("Protocol.VERSION_CLIENT_COMMAND", version.server);
                    var connectionEvent:ConnectionEvent = new ConnectionEvent(ConnectionEvent.VERSION_CLIENT_COMMAND);
                    connectionEvent.serverVersion = version.server;
                    dispatch(connectionEvent);
                    break;
                default:
                    console.error("command not found", id);
            }
        }

        private function printActionText(data:Array, level:String):void
        {
            if (data) {
                level += "    ";
                for each (var action:DataBattleAction in data) {
                    Cc.error(level + getActionText(action.type));
                    printActionText(action.children,level);
                }
            }
        }

        public static function getActionText(action:int):String
        {
            switch (action) {
                case DataBattleActionType.ADD_CELL:
                    return "ADD_CELL";
                    break;
                case DataBattleActionType.DAMAGE:
                    return "DAMAGE";
                    break;
                case DataBattleActionType.GIVE_BELT:
                    return "GIVE_BELT";
                    break;
                case DataBattleActionType.GIVE_BUFF:
                    return "GIVE_BUFF";
                    break;
                case DataBattleActionType.GIVE_POINT:
                    return "GIVE_POINT";
                    break;
                case DataBattleActionType.GIVE_STEP:
                    return "GIVE_STEP";
                    break;
                case DataBattleActionType.HEAL:
                    return "HEAL";
                    break;
                case DataBattleActionType.REMOVE_CELL:
                    return "REMOVE_CELL";
                    break;
                case DataBattleActionType.REMOVE_CELL_ALL:
                    return "REMOVE_CELL_ALL";
                    break;
                case DataBattleActionType.REMOVE_CELL_FORM:
                    return "REMOVE_CELL_FORM";
                    break;
                case DataBattleActionType.REMOVE_CELL_LINE:
                    return "REMOVE_CELL_LINE";
                    break;
                case DataBattleActionType.REMOVE_CELL_TYPE:
                    return "REMOVE_CELL_TYPE";
                    break;
                case DataBattleActionType.REPLACE_CELL:
                    return "REPLACE_CELL";
                    break;
                case DataBattleActionType.REPLACE_CELL_TYPE:
                    return "REPLACE_CELL_TYPE";
                    break;
                case DataBattleActionType.RUN_CELL:
                    return "RUN_CELL";
                    break;
                case DataBattleActionType.SHUFFLE_CELL:
                    return "SHUFFLE_CELL";
                    break;
                case DataBattleActionType.SWAP_CELL:
                    return "SWAP_CELL";
                    break;
                case DataBattleActionType.TAKE_BELT:
                    return "TAKE_BELT";
                    break;
                case DataBattleActionType.TAKE_NEGATIVE_BUFF:
                    return "TAKE_NEGATIVE_BUFF";
                    break;
                case DataBattleActionType.TAKE_NEUTRAL_BUFF:
                    return "TAKE_NEUTRAL_BUFF";
                    break;
                case DataBattleActionType.TAKE_POINT:
                    return "TAKE_POINT";
                    break;
                case DataBattleActionType.TAKE_POSITIVE_BUFF:
                    return "TAKE_POSITIVE_BUFF";
                    break;
                case DataBattleActionType.TAKE_STEP:
                    return "TAKE_STEP";
                    break;
                case DataBattleActionType.WRONG_CELL:
                    return "WRONG_CELL";
                    break;
            }
            return "~~~";
        }

        public function loginVK(vkid:String, vkkey:String, fraction:int):void
        {
            var vkLogin:DataServerLoginVk = new DataServerLoginVk();
            vkLogin.name = "";
            vkLogin.vkId = vkid;
            vkLogin.vkKey = vkkey;
            if (fraction) {vkLogin.fraction = fraction;}
            _connection.sendToSocket(Protocol.LOGIN_VK_SERVER_COMMAND, mainModel.player.id, vkLogin);
        }
    
        public function registerVK(vkid:String, vkkey:String, name:String, sex:int, element:int, fraction:int):void
        {
            var vkLogin:DataServerLoginVk = new DataServerLoginVk();
            vkLogin.name = name;
            vkLogin.vkId = vkid;
            vkLogin.vkKey = vkkey;
            vkLogin.sex = sex;
            vkLogin.element = element;
            vkLogin.fraction = fraction;
            _connection.sendToSocket(Protocol.LOGIN_VK_SERVER_COMMAND, mainModel.player.id, vkLogin);
        }

        public function loginMyMail(mailId:String, mailSource:String, mailSourceKey:String, fraction:int):void {
            var mailLogin:DataServerLoginMyMail = new DataServerLoginMyMail();
            mailLogin.name = "";
            mailLogin.myMailId = mailId;
            mailLogin.myMailSource = mailSource;
            mailLogin.myMailSourceKey = mailSourceKey;
            if (fraction) {mailLogin.fraction = fraction;}
            _connection.sendToSocket(Protocol.LOGIN_MY_MAIL_SERVER_COMMAND, mainModel.player.id, mailLogin);
        }

        public function registerMyMail(mailId:String, mailSource:String, mailSourceKey:String, name:String, sex:int, element:int, fraction:int):void
        {
            var mailLogin:DataServerLoginMyMail = new DataServerLoginMyMail();
            mailLogin.name = name;
            mailLogin.myMailId = mailId;
            mailLogin.myMailSource = mailSource;
            mailLogin.myMailSourceKey = mailSourceKey;
            mailLogin.sex = sex;
            mailLogin.element = element;
            mailLogin.fraction = fraction;
            _connection.sendToSocket(Protocol.LOGIN_MY_MAIL_SERVER_COMMAND, mainModel.player.id, mailLogin);
        }

        public function loginEmail(email:String, pass:String, fraction:int):void
        {
            var emailLogin:DataServerLoginEmail = new DataServerLoginEmail();
            emailLogin.name = "";
            emailLogin.email = email;
            emailLogin.password = pass;
            if (fraction) {emailLogin.fraction = fraction;}
            _connection.sendToSocket(Protocol.LOGIN_EMAIL_SERVER_COMMAND, mainModel.player.id, emailLogin);
        }
    
        public function registerEmail(email:String, pass:String, name:String, sex:int, element:int, fraction:int):void
        {
            var emailLogin:DataServerLoginEmail = new DataServerLoginEmail();
            emailLogin.name = name;
            emailLogin.email = email;
            emailLogin.password = pass;
            emailLogin.sex = sex;
            emailLogin.element = element;
            emailLogin.fraction = fraction;
            _connection.sendToSocket(Protocol.LOGIN_EMAIL_SERVER_COMMAND, mainModel.player.id, emailLogin);
        }
        
        public function loginApple(appleId:String, fraction:int):void
        {
            var appleLogin:DataServerLoginApple = new DataServerLoginApple();
            appleLogin.name = "";
            appleLogin.appleId = appleId;
            if (fraction) {appleLogin.fraction = fraction;}
            _connection.sendToSocket(Protocol.LOGIN_APPLE_SERVER_COMMAND, mainModel.player.id, appleLogin);
        }

        public function registerApple(appleId:String, name:String, sex:int, element:int, fraction:int):void
        {
            var appleLogin:DataServerLoginApple = new DataServerLoginApple();
            appleLogin.appleId = appleId;
            appleLogin.name = name;
            appleLogin.sex = sex;
            appleLogin.element = element;
            appleLogin.fraction = fraction;
            _connection.sendToSocket(Protocol.LOGIN_APPLE_SERVER_COMMAND, mainModel.player.id, appleLogin);
        }

        public function moveGems(swapFirstCell:DataAreaCell, swapSecondCell:DataAreaCell, source:DataBattleMember, destination:Array):void
        {
            var data:DataServerBattleMagic = new DataServerBattleMagic();
            data.magicId = 0;
            
            data.source = source;
            data.destination = destination.concat();

            var extractSwapCell:DataBattleActionExtractSwapCell = new DataBattleActionExtractSwapCell();
            extractSwapCell.first = swapFirstCell;
            extractSwapCell.second = swapSecondCell;

            var extract:DataBattleActionExtract = new DataBattleActionExtract();
            extract.swapCell = extractSwapCell;

            data.extracts = new Dictionary();
            data.extracts[DataBattleActionType.SWAP_CELL] = extract;
            _connection.sendToSocket(Protocol.BATTLE_MAGIC_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function useSpell(type:int, source:DataBattleMember, destination:Array):void
        {
            var data:DataServerBattleMagic = new DataServerBattleMagic();
            data.magicId = type;

            data.source = source;
            data.destination = destination;
            data.extracts = new Dictionary();
            _connection.sendToSocket(Protocol.BATTLE_MAGIC_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function usePotion(artikulBelt:DataBattleArtikulBelt, source:DataBattleMember, destination:Array):void
        {
            var data:DataServerBattlePotion = new DataServerBattlePotion();
            data.potionId = MainModel.getArtikulVO(artikulBelt.artikul.artikulId).potion.battlePotionId;

            data.source = source;
            data.destination = destination;

            var extractTakeBelt:DataBattleActionExtractTakeBelt = new DataBattleActionExtractTakeBelt();
            extractTakeBelt.belt = artikulBelt;

            var extract:DataBattleActionExtract = new DataBattleActionExtract();
            extract.takeBelt = extractTakeBelt;

            data.extracts = new Dictionary();
            data.extracts[DataBattleActionType.TAKE_BELT] = extract;

            _connection.sendToSocket(Protocol.BATTLE_POTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function enterRoom(id:int):void
        {
            var data:DataServerBattleQueueJoin = new DataServerBattleQueueJoin();
            data.queue = id;
            _connection.sendToSocket(Protocol.BATTLE_QUEUE_JOIN_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function exitRoom():void
        {
            var data:DataServerBattleQueueUnjoin = new DataServerBattleQueueUnjoin();
            _connection.sendToSocket(Protocol.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function storeBuy(store:int,section:int, artikulId:int, count:int, moneyType:int):void
        {
            var storeBuy:DataServerStoreBuy = new DataServerStoreBuy();
            storeBuy.store = store;
            storeBuy.section = section;
            storeBuy.artikulId = artikulId;
            storeBuy.count = count;
            storeBuy.money = new DataMoney();
            storeBuy.money.type = moneyType;
            _connection.sendToSocket(Protocol.STORE_BUY_SERVER_COMMAND, mainModel.player.id, storeBuy);
        }

        public function bagList():void
        {
            _connection.sendToSocket(Protocol.BAG_LIST_SERVER_COMMAND, mainModel.player.id, new DataServerBagList());
        }

        public function bagRemove(artikul:DataArtikul, count:int):void
        {
            var data:DataServerBagRemove = new DataServerBagRemove();
            data.artikul = artikul;
            data.count = count;
            _connection.sendToSocket(Protocol.BAG_REMOVE_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function artikulSell(artikul:DataArtikul, count:int):void
        {
            var data:DataServerArtikulSell = new DataServerArtikulSell();
            data.artikul = artikul;
            data.count = count;
            _connection.sendToSocket(Protocol.ARTIKUL_SELL_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function artikulUse(artikul:DataArtikul):void
        {
            var data:DataServerArtikulAction = new DataServerArtikulAction();
            data.artikul = artikul;
            _connection.sendToSocket(Protocol.ARTIKUL_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function artikulRepair(artikul:DataArtikul, moneyType:int):void
        {
            var data:DataServerArtikulRepair = new DataServerArtikulRepair();
            data.artikul = artikul;
            data.money = new DataMoney();
            data.money.type = moneyType;
            _connection.sendToSocket(Protocol.ARTIKUL_REPAIR_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function slotList():void
        {
            _connection.sendToSocket(Protocol.SLOT_LIST_SERVER_COMMAND, mainModel.player.id, new DataServerSlotList());
        }

        public function slotOn(slot:DataArtikulSlot):void
        {
            var data:DataServerSlotTakeOn = new DataServerSlotTakeOn();
            data.slot = slot;
            _connection.sendToSocket(Protocol.SLOT_TAKE_ON_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function slotOff(slot:DataArtikulSlot):void
        {
            var data:DataServerSlotTakeOff = new DataServerSlotTakeOff();
            data.slot = slot;
            _connection.sendToSocket(Protocol.SLOT_TAKE_OFF_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function beltList():void
        {
            _connection.sendToSocket(Protocol.BELT_LIST_SERVER_COMMAND, mainModel.player.id, new DataServerBeltList());
        }

        public function beltOn(belt:DataArtikulBelt):void
        {
            var data:DataServerBeltTakeOn = new DataServerBeltTakeOn();
            data.belt = belt;
            _connection.sendToSocket(Protocol.BELT_TAKE_ON_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function beltOff(belt:DataArtikulBelt):void
        {
            var data:DataServerBeltTakeOff = new DataServerBeltTakeOff();
            data.belt = belt;
            _connection.sendToSocket(Protocol.BELT_TAKE_OFF_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function startMob(id:int):void
        {
            var data:DataServerBattleJoin = new DataServerBattleJoin();
            var joinData:DataBattleJoin = new DataBattleJoin();
            joinData.type = DataBattleJoinType.MOB;
            joinData.mobId = id;
            data.joinData = joinData;
            _connection.sendToSocket(Protocol.BATTLE_JOIN_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function getMoney():void
        {
            var data:DataServerMoneyList = new DataServerMoneyList();
            _connection.sendToSocket(Protocol.MONEY_LIST_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function getSkill():void
        {
            var data:DataServerSkillList = new DataServerSkillList();
            _connection.sendToSocket(Protocol.SKILL_LIST_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function getQuestList(member:DataLocationMember):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.QUEST_LIST;
            data.extract = new DataLocationActionExtract();
            data.extract.questList = new DataLocationActionExtractQuestList();
            data.extract.questList.member = member;
            
            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function takeQuest(member:DataLocationMember, quest:DataQuest):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.QUEST_TAKE;
            data.extract = new DataLocationActionExtract();
            data.extract.questTake = new DataLocationActionExtractQuestTake();
            data.extract.questTake.member = member;
            data.extract.questTake.quest = quest;

            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function giveQuest(member:DataLocationMember, quest:DataQuest):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.QUEST_GIVE;
            data.extract = new DataLocationActionExtract();
            data.extract.questGive = new DataLocationActionExtractQuestGive();
            data.extract.questGive.member = member;
            data.extract.questGive.quest = quest;
            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function refuseQuest(quest:DataQuest):void
        {
            var data:DataServerQuestRefuse = new DataServerQuestRefuse();
            data.quest = quest;
            _connection.sendToSocket(Protocol.QUEST_REFUSE_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function getQuestAllList():void
        {
            _connection.sendToSocket(Protocol.QUEST_LIST_SERVER_COMMAND, mainModel.player.id, new DataServerQuestList());
        }
    
        public function enterLocation(member:DataLocationMember):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.TELEPORT;
            data.extract = new DataLocationActionExtract();
            data.extract.teleport = new DataLocationActionExtractTeleport();
            data.extract.teleport.member = member;

            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function enterPickup(member:DataLocationMember):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.PICKUP;
            data.extract = new DataLocationActionExtract();
            data.extract.pickup = new DataLocationActionExtractPickup();
            data.extract.pickup.member = member;

            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function joinBattleLocation(member:DataLocationMember):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.BATTLE_JOIN;
            data.extract = new DataLocationActionExtract();
            data.extract.battleJoin = new DataLocationActionExtractBattleJoin();
            data.extract.battleJoin.member = member;
            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function memberInfoLocation(member:DataLocationMember):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.BATTLE_INFO;
            data.extract = new DataLocationActionExtract();
            data.extract.battleInfo = new DataLocationActionExtractBattleInfo();
            data.extract.battleInfo.member = member;
            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function memberRunLocation(point:Point):void
        {
            var data:DataServerLocationAction = new DataServerLocationAction();
            data.action = new DataLocationAction();
            data.action.type = DataLocationActionType.RUN;
            data.extract = new DataLocationActionExtract();
            data.extract.run = new DataLocationActionExtractRun();
            data.extract.run.position = new DataLocationPosition();
            data.extract.run.position.x = point.x;
            data.extract.run.position.y = point.y;
            _connection.sendToSocket(Protocol.LOCATION_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }
    
        public function getChallengeList():void
        {
            //ignore
            //_connection.sendToSocket(Protocol.CHALLENGE_LIST_SERVER_COMMAND, mainModel.player.id, new DataServerChallengeList());
        }
    
        public function sendChat(message:DataChatMessage):void
        {
            var data:DataServerChatAction = new DataServerChatAction();
            data.action = new DataChatAction();
            data.action.type = DataChatActionType.MESSAGE_CHANNEL;
            data.extract = new DataChatActionExtract();
            data.extract.messageChannel = new DataChatActionExtractMessageChannel();
            data.extract.messageChannel.type = DataChatChannelType.GENERAL;
            data.extract.messageChannel.message = message;
            _connection.sendToSocket(Protocol.CHAT_ACTION_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function getAchieves():void
        {
            var data:DataServerAchieveList = new DataServerAchieveList();
            _connection.sendToSocket(Protocol.ACHIEVE_LIST_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function paymentApple(inApp:String, transactionId:String, receipt:String):void
        {
            var data:DataServerPaymentApple = new DataServerPaymentApple();
            data.inApp = inApp;
            data.transactionId = transactionId;
            data.receipt = receipt;
            _connection.sendToSocket(Protocol.PAYMENT_APPLE_SERVER_COMMAND, mainModel.player.id, data);
        }

        public function serverVersion():void
        {
            var data:DataServerVesrion = new DataServerVesrion();
            _connection.sendToSocket(Protocol.VERSION_SERVER_COMMAND, mainModel.player.id, data);
        }

}
}
