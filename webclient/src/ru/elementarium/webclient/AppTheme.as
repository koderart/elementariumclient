package ru.elementarium.webclient
{

import feathers.controls.Alert;
import feathers.controls.Button;
import feathers.controls.ButtonState;
import feathers.controls.Check;
import feathers.controls.List;
import feathers.controls.Panel;
import feathers.controls.ScrollBarDisplayMode;
import feathers.controls.ScrollContainer;
import feathers.controls.ScrollInteractionMode;
import feathers.controls.ScrollPolicy;
import feathers.controls.Scroller;
import feathers.controls.TabBar;
import feathers.controls.TextInput;
import feathers.controls.ToggleButton;
import feathers.controls.renderers.DefaultListItemRenderer;
import feathers.controls.text.BitmapFontTextRenderer;
import feathers.layout.HorizontalAlign;
import feathers.layout.HorizontalLayout;
import feathers.layout.TiledRowsLayout;
import feathers.skins.ImageSkin;
import feathers.text.BitmapFontTextFormat;
import feathers.themes.AeonDesktopThemeWithAssetManager;
import feathers.controls.Label;
import feathers.controls.text.TextFieldTextRenderer;
import feathers.core.ITextRenderer;

import flash.filesystem.File;
import flash.net.SharedObject;

import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import flash.utils.describeType;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.services.Downloader;
import ru.elementarium.webclient.view.EmbeddedAssets;
import ru.elementarium.webclient.view.EmbeddedLayouts;
import ru.elementarium.webclient.view.EmbeddedTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import starling.core.Starling;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.textures.Texture;
import starling.utils.AssetManager;
import starlingbuilder.engine.DefaultAssetMediator;
import starlingbuilder.engine.IUIBuilder;
import starlingbuilder.engine.UIBuilder;
import flash.geom.Rectangle;

public class AppTheme extends AeonDesktopThemeWithAssetManager
{

/*
        [Embed(source="/../assets/fonts/baldur.ttf",fontFamily="Baldur",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD:Class;
        [Embed(source="/../assets/fonts/hoog0555_cyr2.ttf",fontFamily="hooge 05_55 Cyr2",fontWeight="normal",mimeType="application/x-font",embedAsCFF="true")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD2:Class;
        [Embed(source="/../assets/fonts/myriadpro_bold.otf",fontFamily="Myriad Pro",fontWeight="bold",mimeType="application/x-font",embedAsCFF="false")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD3:Class;
        [Embed(source="/../assets/fonts/hobo_bold.otf",fontFamily="Hobo",fontWeight="bold",mimeType="application/x-font",embedAsCFF="true")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD4:Class;
        [Embed(source="/../assets/fonts/cambria.ttc",fontFamily="cambria",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD5:Class;
        [Embed(source="/../assets/fonts/heinrich.ttf",fontFamily="Heinrich123",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const SOURCE_SANS_PRO_SEMIBOLD6:Class;
*/

        [Embed(source="/../assets/fonts/arial.ttf",fontFamily="Arial",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FONT_ARIAL:Class;
        [Embed(source="/../assets/fonts/arial_bold.ttf",fontFamily="Arial",fontWeight="bold",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FONT_ARIAL_BOLD:Class;
        [Embed(source="/../assets/fonts/myriadpro.ttf",fontFamily="Myriad Pro",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const MYRIAD_PRO:Class;
        [Embed(source="/../assets/fonts/myriadpro_bold.ttf",fontFamily="Myriad Pro",fontWeight="bold",mimeType="application/x-font",embedAsCFF="false")]
        protected static const MYRIAD_PRO_BOLD:Class;
        [Embed(source="/../assets/fonts/franklin_gothic_medium.ttf",fontFamily="Franklin Gothic Medium",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FRANKLIN_GOTHIC_MEDIUM:Class;
        [Embed(source="/../assets/fonts/franklin_gothic_heavy.ttf",fontFamily="Franklin Gothic Heavy",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FRANKLIN_GOTHIC_HEAVY:Class;
        [Embed(source="/../assets/fonts/candara.ttf",fontFamily="Candara",fontWeight="normal",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FONT_CANDARA:Class;
        [Embed(source="/../assets/fonts/candara_bold.ttf",fontFamily="Candara",fontWeight="bold",mimeType="application/x-font",embedAsCFF="false")]
        protected static const FONT_CANDARA_BOLD:Class;

        private static var _instance: AppTheme;

        protected var assetAppManager : AssetManager;

        public static var uiBuilder:IUIBuilder;
        public static const linkers:Array = [TiledRowsLayout, HorizontalLayout];
        
        private var _assetsLoaderIoError:Boolean;

        private var _downloader: Downloader;

        private var _version: SharedObject;
        
        private var filesX1: Array = ["common_0", "common2_0", "gems_bang_0", "background"];
        private var filesX2: Array = ["common_0", "common_1", "common2_0", "common2_1", "gems_bang_0", "gems_bang_1", "gems_bang_2", "gems_bang_3", "background"];
	
    
		public function AppTheme()
		{
            _instance = this;

            assetAppManager = new AssetManager(MainModel.scaleFactor);
            assetAppManager.addEventListener(Event.IO_ERROR, assetAppManager_ioErrorHandler);
            assetAppManager.addEventListener(Event.SECURITY_ERROR, assetAppManager_ioErrorHandler);

            assetManager = new AssetManager(2);
            assetManager.addEventListener(Event.IO_ERROR, assetAppManager_ioErrorHandler);
            assetManager.addEventListener(Event.SECURITY_ERROR, assetAppManager_ioErrorHandler);
            BUILD::mobile {
                downloadAppThemeAssets()
            }
            BUILD::release {
                loadAppThemeAssets();
            }
            BUILD::test {
                loadAppThemeAssets();
            }
        }

        public function downloadAppThemeAssets():void
        {
            BUILD::mobile {
                _version = SharedObject.getLocal("version");

                if (_version.data.version != MainModel.assetsVersion) {
                    if (File.applicationStorageDirectory.resolvePath("gui").exists) {
                        File.applicationStorageDirectory.resolvePath("gui").deleteDirectory(true);
                    }
                    _version.data.version = MainModel.assetsVersion;
                    _version.flush();
                }

                if (_downloader) {
                    _downloader.removeEventListener(Downloader.LOADED, downloaderLoadedHandler);
                    _downloader.removeEventListener(Downloader.LOADED_ERROR, downloaderErrorHandler);
                    _downloader.removeEventListener(Downloader.PROGRESS, downloaderProgressHandler);
                }
                _downloader = new Downloader("gui", ConnectionService.BASE_URL_ASSETS + "textures/" + MainModel.assetsVersion + "/x"+MainModel.scaleFactor+"/");

                var urls:Array = getFilesArray();
                for each (var fileName:String in urls) {
                    var file:File = File.applicationStorageDirectory.resolvePath("gui/" + fileName + ".atf");
                    if (!file.exists) {
                        _downloader.addFileToList(fileName + ".atf");
                    }
                    file = File.applicationStorageDirectory.resolvePath("gui/" + fileName + ".xml");
                    if (!file.exists) {
                        _downloader.addFileToList(fileName + ".xml");
                    }
                }

                _downloader.addEventListener(Downloader.LOADED, downloaderLoadedHandler);
                _downloader.addEventListener(Downloader.LOADED_ERROR, downloaderErrorHandler);
                _downloader.addEventListener(Downloader.PROGRESS, downloaderProgressHandler);
                _assetsLoaderIoError = false;
                _downloader.load();
            }
        }
        
        private function loadAppThemeAssets():void
        {
            assetAppManager.enqueue(EmbeddedAssets);
            var urls:Array = getFilesArray();
            
            BUILD::mobile {
                assetAppManager.enqueue(File.applicationStorageDirectory.resolvePath("gui"));
            }

            BUILD::release {
                for each (var url:String in urls) {
                    assetAppManager.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/" + MainModel.assetsVersion + "/x"+MainModel.scaleFactor+"/" + url + ".atf",
                                            ConnectionService.BASE_URL_ASSETS + "textures/" + MainModel.assetsVersion + "/x"+MainModel.scaleFactor+"/" + url + ".xml");
                }
            }
            BUILD::test {
                for each (var url:String in urls) {
                    assetAppManager.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/" + MainModel.assetsVersion + "/x"+MainModel.scaleFactor+"/" + url + ".atf",
                                            ConnectionService.BASE_URL_ASSETS + "textures/" + MainModel.assetsVersion + "/x"+MainModel.scaleFactor+"/" + url + ".xml");
                }
            }

            assetAppManager.loadQueue(assetManager_onProgress1);

            if (uiBuilder == null) {
                uiBuilder = new UIBuilder(new DefaultAssetMediator(assetAppManager));
            }
            parseLayouts(EmbeddedLayouts, ParsedLayouts);
        }

        public function get assets():AssetManager
        {
            return assetAppManager;
        }

        public static function get instance():AppTheme {
            return _instance;
        }
    
        protected function getFilesArray():Array
        {
            switch (MainModel.scaleFactor) {
                case 1:
                    return filesX1;
                    break;
                case 2:
                    return filesX2;
                    break;
            }
            return [];
        }
    
        protected function assetManager_onProgress1(progress:Number):void
        {
            this.dispatchEventWith("progress", false, "Загрузка " + int(progress*100)+"%");
            if(progress !== 1) { return; }

            if (_assetsLoaderIoError) {
                this.dispatchEventWith("ioError");
                return;
            }
            assetManager.enqueue(EmbeddedTheme);
            /*
            assetManager.enqueue("assets/themes/aeon_desktop.png");
            assetManager.enqueue("assets/themes/aeon_desktop.xml");
            */
            assetManager.loadQueue(assetManager_onProgress2);
        }

        protected function assetManager_onProgress2(progress:Number):void
        {
            if(progress !== 1) { return; }
            if (_assetsLoaderIoError) {
                this.dispatchEventWith("ioError");
                return;
            }

            this.initialize();
            this.isComplete = true;
            this.dispatchEventWith(Event.COMPLETE, false, Starling.current);
        }

        override protected function setVerticalScrollBarIncrementButtonStyles(button:Button):void
        {
            var skin:ImageSkin = new ImageSkin( assetAppManager.getTexture("scrollbar/v_decrement") );
            button.defaultSkin = skin;
            button.hasLabelTextRenderer = false;
        }

        override protected function setVerticalScrollBarDecrementButtonStyles(button:Button):void
        {
            var skin:ImageSkin = new ImageSkin( assetAppManager.getTexture("scrollbar/v_increment") );
            button.defaultSkin = skin;
            button.hasLabelTextRenderer = false;
        }

        override protected function setVerticalScrollBarThumbStyles(button:Button):void
        {
            var skin:Image = new Image( assetAppManager.getTexture("scrollbar/v_tumb") );
            skin.scale9Grid = new Rectangle(0,7,7,1);
            button.defaultSkin = skin;
            button.hasLabelTextRenderer = false;
        }

        override protected function setVerticalScrollBarMinimumTrackStyles(track:Button):void
        {
            var defaultSkin:Image = new Image(assetAppManager.getTexture("scrollbar/v_track"));
            defaultSkin.scale9Grid = new Rectangle(0, 17, 15, 1);
            track.defaultSkin = defaultSkin;
            track.hasLabelTextRenderer = false;
        }

        override protected function setHorizontalScrollBarIncrementButtonStyles(button:Button):void
        {
            var skinSelector:ImageSkin = new ImageSkin(assetAppManager.getTexture("scrollbar/h_increment"));
            button.defaultSkin = skinSelector;
            button.hasLabelTextRenderer = false;
        }

        override protected function setHorizontalScrollBarDecrementButtonStyles(button:Button):void
        {
            var skinSelector:ImageSkin = new ImageSkin(assetAppManager.getTexture("scrollbar/h_decrement"));
            button.defaultSkin = skinSelector;
            button.hasLabelTextRenderer = false;
        }

        override protected function setHorizontalScrollBarThumbStyles(button:Button):void
        {
            var skinSelector:ImageSkin = new ImageSkin(assetAppManager.getTexture("scrollbar/h_tumb"));
            skinSelector.scale9Grid = new Rectangle(7,0,1,7);
            button.defaultSkin = skinSelector;
            button.hasLabelTextRenderer = false;
        }

        override protected function setHorizontalScrollBarMinimumTrackStyles(track:Button):void
        {
            var defaultSkin:Image = new Image(assetAppManager.getTexture("scrollbar/h_track"));
            defaultSkin.scale9Grid = new Rectangle(15, 1, 0, 17);
            track.defaultSkin = defaultSkin;
            track.hasLabelTextRenderer = false;

        }

        override protected function initializeStyleProviders():void
		{
			super.initializeStyleProviders(); // don't forget this!

            getStyleProviderForClass(List).setFunctionForStyleName("listTransparentBg", function(list:List):void {
                setScrollerStyles(list);
                list.verticalScrollPolicy = List.SCROLL_POLICY_AUTO;
                //list.backgroundSkin = new Scale9Image(simpleBorderBackgroundSkinTextures);
                list.padding = borderSize;
                list.interactionMode = ScrollInteractionMode.TOUCH_AND_SCROLL_BARS;
            });

            getStyleProviderForClass(List).setFunctionForStyleName("listTransparentNoScroll", function(list:List):void {
                setScrollerStyles(list);
                list.verticalScrollPolicy = List.SCROLL_POLICY_AUTO;
                list.horizontalScrollPolicy = List.SCROLL_POLICY_OFF;
                //list.backgroundSkin = new Scale9Image(simpleBorderBackgroundSkinTextures);
                list.padding = 0;
                list.scrollBarDisplayMode = ScrollBarDisplayMode.NONE;
                list.interactionMode = ScrollInteractionMode.TOUCH;
            });

            getStyleProviderForClass(List).setFunctionForStyleName("listTextBackBg", function(list:List):void {
                setScrollerStyles(list);
                list.verticalScrollPolicy = List.SCROLL_POLICY_AUTO;
                var skin:Image = new Image(assetAppManager.getTexture("window/text_back"));
                skin.scale9Grid = new Rectangle(7,7,1,1);
                list.backgroundSkin = skin;
                list.padding = borderSize;
            });

            getStyleProviderForClass(Panel).setFunctionForStyleName("panelTransparentBg", function(panel:Panel):void {
                setScrollerStyles(panel);
                //panel.backgroundSkin = new Scale9Image(panelBorderBackgroundSkinTextures);
                panel.paddingTop = 0;
                panel.paddingRight = gutterSize;
                panel.paddingBottom = gutterSize;
                panel.paddingLeft = gutterSize;
            });

            getStyleProviderForClass(Check).setFunctionForStyleName("artikulCheck", function(check:Check):void {

                var icon:ImageSkin = new ImageSkin(assetAppManager.getTexture("buttons/checkbox"));
                icon.selectedTexture = assetAppManager.getTexture("buttons/checkbox_selected");
                check.defaultIcon = icon;

                //bugfix default state
                check.customLabelStyleName = "artikul_checkbox";
                check.defaultLabelProperties.textFormat = new TextFormat( "Myriad Pro", 14, 0x45352D, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                check.defaultLabelProperties.embedFonts = true;
                check.gap = 4;
            });

            getStyleProviderForClass(ScrollContainer).setFunctionForStyleName("locationScroll", function(container:ScrollContainer):void {
                container.autoSizeMode = ScrollContainer.AUTO_SIZE_MODE_STAGE;
                container.interactionMode = ScrollContainer.INTERACTION_MODE_MOUSE;
                container.mouseWheelScrollDuration = 0;
                container.scrollBarDisplayMode = ScrollContainer.SCROLL_BAR_DISPLAY_MODE_NONE;
                container.hasElasticEdges = false;
                container.decelerationRate = Scroller.DECELERATION_RATE_FAST;
            });

            getStyleProviderForClass(ScrollContainer).setFunctionForStyleName("chatScroll", function(container:ScrollContainer):void {
                setScrollerStyles(container);
                container.verticalScrollPolicy = List.SCROLL_POLICY_AUTO;
                container.interactionMode = ScrollInteractionMode.TOUCH_AND_SCROLL_BARS;
            });

            getStyleProviderForClass( Button ).defaultStyleFunction = function(button:Button):void {
                if (button.label) {
                    var bWidth:int = button.label.length*9+40;
                } else {
                    var bWidth:int = 80;
                }
                var ss:Sprite = AppTheme.uiBuilder.create(ParsedLayouts.button_common) as Sprite;
                var borderImage:Image = ss.getChildByName("border") as Image;
                var bgImage:Image = ss.getChildByName("background") as Image;
                bgImage.texture = assets.getTexture("button/common/up_bg");
                borderImage.width = bWidth;
                bgImage.width = bWidth;
                // fix font
                bgImage.x = 2;
                borderImage.x = 2;
                button.defaultSkin = ss;

                ss = AppTheme.uiBuilder.create(ParsedLayouts.button_common) as Sprite;
                borderImage = ss.getChildByName("border") as Image;
                bgImage = ss.getChildByName("background") as Image;
                bgImage.texture = assets.getTexture("button/common/over_bg");
                borderImage.width = bWidth;
                bgImage.width = bWidth;
                // fix font
                bgImage.x = 2;
                borderImage.x = 2;
                button.hoverSkin = ss;

                ss = AppTheme.uiBuilder.create(ParsedLayouts.button_common) as Sprite;
                borderImage = ss.getChildByName("border") as Image;
                bgImage = ss.getChildByName("background") as Image;
                bgImage.texture = assets.getTexture("button/common/down_bg");
                borderImage.width = bWidth;
                bgImage.width = bWidth;
                // fix font
                bgImage.x = 2;
                borderImage.x = 2;
                button.downSkin = ss;
                
                ss = AppTheme.uiBuilder.create(ParsedLayouts.button_common) as Sprite;
                borderImage = ss.getChildByName("border") as Image;
                bgImage = ss.getChildByName("background") as Image;
                bgImage.texture = assets.getTexture("button/common/disabled_bg");
                borderImage.width = bWidth;
                bgImage.width = bWidth;
                // fix font
                bgImage.x = 2;
                borderImage.x = 2;
                button.disabledSkin = ss;
                button.defaultLabelProperties.textFormat = new TextFormat("Arial", 14, 0x989AA4, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                button.defaultLabelProperties.embedFonts = true;
                /*
                button.labelFactory = function():ITextRenderer
                {
                    var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
                    textRenderer.textFormat = new BitmapFontTextFormat("font_button");
                    return textRenderer;
                }
                */
            };

            getStyleProviderForClass(Button).setFunctionForStyleName("selectQuestButton", function(button:Button):void {

                var ss:Sprite = AppTheme.uiBuilder.create(ParsedLayouts.npc_item) as Sprite;
                var leftImage:Image = ss.getChildByName("leftImage") as Image;
                leftImage.texture = assets.getTexture("button/big/up_left");
                var rightImage:Image = ss.getChildByName("rightImage") as Image;
                rightImage.texture = assets.getTexture("button/big/up_right");
                var centerImage:Image = ss.getChildByName("centerImage") as Image;
                centerImage.texture = assets.getTexture("button/big/up_center");
                button.defaultSkin = ss;

                ss = AppTheme.uiBuilder.create(ParsedLayouts.npc_item) as Sprite;
                leftImage = ss.getChildByName("leftImage") as Image;
                leftImage.texture = assets.getTexture("button/big/over_left");
                rightImage = ss.getChildByName("rightImage") as Image;
                rightImage.texture = assets.getTexture("button/big/over_right");
                centerImage = ss.getChildByName("centerImage") as Image;
                centerImage.texture = assets.getTexture("button/big/over_center");
                button.hoverSkin = ss;

                button.defaultLabelProperties.textFormat = new TextFormat("Arial", 12, 0x989AA4, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                button.defaultLabelProperties.embedFonts = true;
                /*
                button.labelFactory = function():ITextRenderer
                {
                    var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
                    textRenderer.textFormat = new BitmapFontTextFormat("font_button");
                    return textRenderer;
                };
                */
                button.iconOffsetX = -10;
            });

            getStyleProviderForClass(Button).setFunctionForStyleName("common2", function(button:Button):void {
                var defaultSkin:Image = new Image(assetAppManager.getTexture("button/common2/up"));
                defaultSkin.scale9Grid = new Rectangle(9,0,8,28);
                button.defaultSkin = defaultSkin;
                var hoverSkin:Image = new Image(assetAppManager.getTexture("button/common2/over"));
                hoverSkin.scale9Grid = new Rectangle(9,0,8,28);
                button.hoverSkin = hoverSkin;
                button.defaultLabelProperties.textFormat = new TextFormat("Arial", 12, 0x989AA4, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                button.defaultLabelProperties.embedFonts = true;
                /*
                button.labelFactory = function():ITextRenderer
                {
                    var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
                    textRenderer.textFormat = new BitmapFontTextFormat("font_button");
                    return textRenderer;
                };
                */
                button.useHandCursor = true;
                button.horizontalAlign = Button.HORIZONTAL_ALIGN_LEFT;
                button.paddingLeft = 10;
                button.paddingRight = 10;
                button.paddingBottom = 2;
            });

            getStyleProviderForClass(Button).setFunctionForStyleName("storeCategoryUpButton", function(button:Button):void {
                var imageBg:Image = new Image(assetAppManager.getTexture("list/button/scroll_bg"));

                button.defaultSkin = imageBg;
                button.defaultIcon = new Image(assetAppManager.getTexture("list/button/scroll_icon_up"));
                button.downIcon  = new Image(assetAppManager.getTexture("list/button/scroll_icon_down"));
                button.pivotY = imageBg.height;
                button.scaleY = -1;

                button.useHandCursor = true;
            });
            
            getStyleProviderForClass(Button).setFunctionForStyleName("storeCategoryDownButton", function(button:Button):void {
                button.defaultSkin = new Image(assetAppManager.getTexture("list/button/scroll_bg"));
                button.defaultIcon = new Image(assetAppManager.getTexture("list/button/scroll_icon_up"));
                button.downIcon = new Image(assetAppManager.getTexture("list/button/scroll_icon_down"));

                button.useHandCursor = true;
            });

            getStyleProviderForClass(Button).setFunctionForStyleName("pinkOkButton", function(button:Button):void {
                var defaultSkin:Image = new Image(assetAppManager.getTexture("buttons/pink_up_bg"));
                defaultSkin.scale9Grid = new Rectangle(8,8,1,1);
                button.defaultSkin = defaultSkin;

                var downSkin:Image = new Image(assetAppManager.getTexture("buttons/pink_down_bg"));
                downSkin.scale9Grid = new Rectangle(8,8,1,1);
                button.downSkin = downSkin;

                button.defaultIcon = new Image(assetAppManager.getTexture("buttons/icon_check_on"));

                button.gap = 10;
                button.padding = 10;
                button.useHandCursor = true;

                //bugfix default state
                button.customLabelStyleName = "pink_button_label";
                button.defaultLabelProperties.textFormat = new TextFormat("Candara", 16, 0xD8B62F, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                button.defaultLabelProperties.embedFonts = true;
            });

            getStyleProviderForClass(Button).setFunctionForStyleName("blueCancelButton", function(button:Button):void {
                var defaultSkin:Image = new Image(assetAppManager.getTexture("buttons/blue_up_bg"));
                defaultSkin.scale9Grid = new Rectangle(8,8,1,1);
                button.defaultSkin = defaultSkin;

                var downSkin:Image = new Image(assetAppManager.getTexture("buttons/blue_down_bg"));
                downSkin.scale9Grid = new Rectangle(8,8,1,1);
                button.downSkin = downSkin;

                button.defaultIcon = new Image(assetAppManager.getTexture("buttons/icon_check_off"));
                
                button.gap = 10;
                button.padding = 10;
                button.useHandCursor = true;
                
                //bugfix default state
                button.customLabelStyleName = "pink_button_label";
                button.defaultLabelProperties.textFormat = new TextFormat("Candara", 16, 0xD8B62F, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                button.defaultLabelProperties.embedFonts = true;
            });

            getStyleProviderForClass(ToggleButton).setFunctionForStyleName("skillsInventoryButton", function(button:ToggleButton):void {
                button.defaultSkin = new Image(assetAppManager.getTexture("inventory/stats_button"));
                button.hoverSkin = new Image(assetAppManager.getTexture("inventory/stats_button"));
                button.downSkin = new Image(assetAppManager.getTexture("inventory/stats_button"));
                button.disabledSkin = new Image(assetAppManager.getTexture("inventory/stats_button"));

                button.defaultSelectedSkin = new Image(assetAppManager.getTexture("inventory/stats_selected_button"));
                button.selectedHoverSkin = new Image(assetAppManager.getTexture("inventory/stats_selected_button"));
                button.selectedDownSkin = new Image(assetAppManager.getTexture("inventory/stats_selected_button"));
                button.selectedDisabledSkin = new Image(assetAppManager.getTexture("inventory/stats_selected_button"));
                button.useHandCursor = true;
            });
            
            getStyleProviderForClass(ToggleButton).setFunctionForStyleName("mainMenuTabButton", function(tab:ToggleButton):void {
                var skin:ImageSkin = new ImageSkin(assetAppManager.getTexture("main/tab"));
                skin.selectedTexture = assetAppManager.getTexture("main/tab_selected");
                tab.defaultSkin = skin;
                tab.gap = 2;
            });

            getStyleProviderForClass(ToggleButton).setFunctionForStyleName("inventoryTabButton", function(tab:ToggleButton):void {
                var skin:ImageSkin = new ImageSkin(assetAppManager.getTexture("inventory/tab"));
                skin.selectedTexture = assetAppManager.getTexture("inventory/tab_selected");
                tab.defaultSkin = skin;
                tab.gap = 2;
            });

            getStyleProviderForClass(ToggleButton).setFunctionForStyleName("match3TabButton", function(tab:ToggleButton):void {
                var skin:ImageSkin = new ImageSkin(assetAppManager.getTexture("match3/button_off"));
                skin.selectedTexture = assetAppManager.getTexture("match3/button_on");
                tab.defaultSkin = skin;
                tab.gap = 1;
                tab.defaultLabelProperties.textFormat = new TextFormat("Arial", 18, 0xD7CF79, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                tab.defaultLabelProperties.embedFonts = true;
            });

            getStyleProviderForClass(ToggleButton).setFunctionForStyleName("transparentTabButton", function(tab:ToggleButton):void {
                tab.gap = 2;
                tab.useHandCursor = true;
            });

            getStyleProviderForClass(DefaultListItemRenderer).setFunctionForStyleName("storeListButton", function(itemRenderer:DefaultListItemRenderer):void {
                var skin:ImageSkin = new ImageSkin(assetAppManager.getTexture("list/item_bg"));
                skin.selectedTexture = assetAppManager.getTexture("list/item_bg_selected");
                itemRenderer.defaultLabelProperties.textFormat = new TextFormat("Candara", 18, 0xD8B62F, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
                itemRenderer.defaultLabelProperties.embedFonts = true;
                itemRenderer.horizontalAlign = HorizontalAlign.CENTER;
                itemRenderer.paddingRight = 30;
                itemRenderer.defaultSkin = skin;
            });

            
            getStyleProviderForClass(TextInput).setFunctionForStyleName("commonTextInput", function(textInput:TextInput):void {
                var skin:Image = new Image(assetAppManager.getTexture("textinput/input_field"));
                skin.scale9Grid = new Rectangle(6,13,13,13);
                textInput.backgroundSkin = skin;
                textInput.paddingLeft = 10;
                textInput.paddingRight = 10;
            });

            getStyleProviderForClass(TextInput).setFunctionForStyleName("chatTextInput", function(textInput:TextInput):void {// 13 15
                var skin:Image = new Image(assetAppManager.getTexture("chat/text_frame"));
                skin.scale9Grid = new Rectangle(7,9,4,4);
                textInput.backgroundSkin = skin;
                textInput.paddingLeft = 10;
                textInput.paddingRight = 10;
            });

            getStyleProviderForClass(TextInput).setFunctionForStyleName("storeTextInput", function(textInput:TextInput):void {
                textInput.backgroundSkin = new Quad(20,20, 0xBC8B42);

                textInput.customTextEditorStyleName = "fix";
                textInput.textEditorProperties.textFormat = new TextFormat("Franklin Gothic Medium", 24, 0x403426, false, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });

            getStyleProviderForClass(TextInput).setFunctionForStyleName("registrationTextInput", function(textInput:TextInput):void {
                var skin:Image = new Image(assetAppManager.getTexture("textinput/input_field"));
                skin.scale9Grid = new Rectangle(6,13,13,13);
                textInput.backgroundSkin = skin;
                textInput.paddingLeft = 10;
                textInput.paddingRight = 10;
                textInput.paddingBottom = 4;
                textInput.customTextEditorStyleName = "fix";
                textInput.textEditorProperties.textFormat = new TextFormat("Franklin Gothic Medium", 24, 0xD8B62F, false, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });

            getStyleProviderForClass(TextInput).defaultStyleFunction = function(textInput:TextInput):void
            {
                setBaseTextInputStyles(textInput);
                textInput.textEditorProperties.textFormat = new TextFormat(FONT_NAME, regularFontSize, 0x000000, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            };

            getStyleProviderForClass( Alert ).defaultStyleFunction = function(alert:Alert):void {
                alert.outerPaddingLeft  = 20;
                alert.outerPaddingRight  = 20;
                alert.outerPaddingTop  = 10;
                alert.outerPaddingBottom  = 10;
                alert.paddingBottom  = 5;
                var skin:Image = new Image(assetAppManager.getTexture("textinput/input_field"));
                skin.scale9Grid = new Rectangle(6,13,13,13);
                alert.backgroundSkin = skin;
            };

            /**
             * FONTS
             */
            getStyleProviderForClass(Label).setFunctionForStyleName("header-label", setHeaderLabelStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName("nickname-font", setHeaderLabelStyles);

			getStyleProviderForClass(Label).setFunctionForStyleName("match3_timer", function(label:Label):void {
                label.textRendererFactory = function():ITextRenderer
                {
                    var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
                    textRenderer.textFormat = new BitmapFontTextFormat("turn_font", NaN, 0xFFFFFF, "center");
                    return textRenderer;
                }
            });

            getStyleProviderForClass(Label).setFunctionForStyleName("loader_font", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("_sans", 14, 0xFFFFFF, false, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_turn_nick", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 11, 0xf7bc54, false, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_user_panel_nick", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 11, 0xe1ba76, true, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_user_panel_level", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 14, 0xe1ba76, true, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_user_panel_hp", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 11, 0xdcdcc6, false, false, false, null, null, TextFormatAlign.CENTER, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_skills", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 11, 0x311313, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_team_title", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 10, 0xa2a2a2, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            });
            getStyleProviderForClass(Label).setFunctionForStyleName("match3_team_user", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Arial", 12, 0xd7ab41, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            });

            getStyleProviderForClass(Label).setFunctionForStyleName("artikul_checkbox", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat( "Myriad Pro", 14, 0x45352D, true, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            });

            getStyleProviderForClass(Label).setFunctionForStyleName("pink_button_label", function(label:Label):void {
                label.textRendererProperties.textFormat = new TextFormat("Candara", 16, 0xD8B62F, false, false, false, null, null, TextFormatAlign.LEFT, 0, 0, 0, 0);
            });

            getStyleProviderForClass(Label).setFunctionForStyleName("match3LogText", function(label:Label):void {
                label.textRendererFactory = function():ITextRenderer
                {
                    var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
                    textRenderer.textFormat = new TextFormat( "Arial bold", 11, 0x80A1A9 );
                    //textRenderer.embedFonts = true;
                    textRenderer.isHTML = true;
                    return textRenderer;
                }
            });

            getStyleProviderForClass(Label).setFunctionForStyleName("chatTextStyle", function(label:Label):void {
                label.textRendererFactory = function():ITextRenderer
                {
                    var textRenderer:TextFieldTextRenderer = new TextFieldTextRenderer();
                    textRenderer.textFormat = new TextFormat( "Arial", 12, 0xfff798 );
                    //textRenderer.embedFonts = true;
                    textRenderer.isHTML = true;
                    textRenderer.wordWrap = true;
                    return textRenderer;
                }
            });
		}

		private function setHeaderLabelStyles( label:Label ):void
		{
            label.textRendererFactory = function():ITextRenderer
			{
                var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
                textRenderer.textFormat = new BitmapFontTextFormat("nickname_font");
                return textRenderer;
			}
		}

        public function getTexture(name:String):Texture
        {
            return assetAppManager.getTexture(name);
        }
    
        public function getTextures(prefix:String="", result:Vector.<Texture>=null):Vector.<Texture>
        {
            return assetAppManager.getTextures(prefix, result);
        }
    
        public function getExternalData(name:String):Object
        {
            return null;
        }
        
        private function parseLayouts(fromCls:Class, toCls:Class):void
        {
            var name:String;
            var description:XML = describeType(fromCls);
            var constants:XMLList = description..constant;
            for each(var constant:XML in constants)
            {
                name = constant.@name;
                toCls[name] = JSON.parse(new fromCls[name]());
            }
        }

        private function assetAppManager_ioErrorHandler(event:Object):void
        {
            _assetsLoaderIoError = true;
        }

        private function downloaderLoadedHandler(event:Event):void
        {
            if (_assetsLoaderIoError) {
                this.dispatchEventWith("ioError");
            } else {
                loadAppThemeAssets();
            }
        }

        private function downloaderErrorHandler(event:Event):void
        {
            _assetsLoaderIoError = true;
        }
    
        private function downloaderProgressHandler(event:Event):void
        {
            this.dispatchEventWith("progress", false, event.data);
        }
}
}