package ru.elementarium.webclient.events
{
import starling.events.Event;

public class PaymentEvent extends Event
	{
        public static const PAYMENT_APPLE_START:String = "PaymentEvent.PAYMENT_APPLE_START";
        public static const PAYMENT_APPLE_STOP:String = "PaymentEvent.PAYMENT_APPLE_STOP";
        public static const PAYMENT_APPLE_MAKE_PURCHASE:String = "PaymentEvent.PAYMENT_APPLE_MAKE_PURCHASE";
        public static const PAYMENT_APPLE_DEFERRED_PURCHASE:String = "PaymentEvent.PAYMENT_APPLE_DEFERRED_PURCHASE";
        public static const PAYMENT_APPLE_SERVER_COMMAND:String = "PaymentEvent.PAYMENT_APPLE_SERVER_COMMAND";
        public static const PAYMENT_APPLE_CLIENT_COMMAND:String = "PaymentEvent.PAYMENT_APPLE_CLIENT_COMMAND";

        public var inApp:String;
        public var receipt:String;
        public var transactionId:String;
    
        public function PaymentEvent(type:String)
        {
            super(type);
        }

    }
}
