package ru.elementarium.webclient.events
{
import starling.events.Event;

public class StaticDataEvent extends Event
	{
        public static const LOAD:String = "StaticDataEvent.LOAD";
        public static const PROGRESS:String = "StaticDataEvent.PROGRESS";
        public static const LOADED:String = "StaticDataEvent.LOADED";
        public static const LOADED_ERROR:String = "StaticDataEvent.LOADED_ERROR";

        public var progress:int;

        public function StaticDataEvent(type:String)
        {
            super(type);
        }
}
}
