package ru.elementarium.webclient.events
{
	public class BattleEventType
	{
        public static const BATTLE_QUEUE_LIST_OUT:String = "BattleEventType.BATTLE_QUEUE_LIST_OUT";
        public static const BATTLE_QUEUE_JOIN_CLIENT_COMMAND:String = "BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND";
        public static const BATTLE_JOIN_CLIENT_COMMAND:String = "BattleEventType.BATTLE_JOIN_CLIENT_COMMAND";
        public static const BATTLE_UNJOIN_CLIENT_COMMAND:String = "BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND";

        public static const BATTLE_MAGIC_CLIENT_COMMAND:String = "BattleEventType.BATTLE_MAGIC_CLIENT_COMMAND";
        public static const BATTLE_BUFF_BEGIN:String = "BattleEventType.BATTLE_BUFF_BEGIN";
        public static const BATTLE_BUFF_END:String = "BattleEventType.BATTLE_BUFF_END";
        public static const BATTLE_BUFF_INTERRUPT:String = "BattleEventType.BATTLE_BUFF_INTERRUPT";
        public static const BATTLE_BUFF_EFFECT:String = "BattleEventType.BATTLE_BUFF_EFFECT";
        public static const BATTLE_BUFF_COLLISION:String = "BattleEventType.BATTLE_BUFF_COLLISION";
        public static const BATTLE_KICK_CLIENT_COMMAND:String = "BattleEventType.BATTLE_BUFF_TIME";
        
        public static const BATTLE_POTION_CLIENT_COMMAND:String = "BattleEventType.BATTLE_POTION_CLIENT_COMMAND";

        public static const BATTLE_SWAP_CLIENT_COMMAND:String = "BattleEventType.BATTLE_SWAP_CLIENT_COMMAND";
        
        public static const BATTLE_MEMBER_JOIN_CLIENT_COMMAND:String = "BattleEventType.BATTLE_MEMBER_JOIN_CLIENT_COMMAND";
    }
}
