package ru.elementarium.webclient.events
{

import ru.muvs.elementarium.thrift.battle.area.DataAreaCell;
import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMember;

import starling.events.Event;

public class Match3Event extends Event
{
    public static const MOVE:String = "Match3Event.MOVE";
    public static const USE_SPELL:String = "Match3Event.USE_SPELL";
    public static const USE_POTION:String = "Match3Event.USE_POTION";

    public static const GAME_ENDED:String = "Match3Event.GAME_ENDED";
    
    public var swapFirstCell:DataAreaCell;
    public var swapSecondCell:DataAreaCell;
    public var source:DataBattleMember;
    public var destination:Array;

    public var idSpell:int;
    public var artikulBelt:DataBattleArtikulBelt;

    public function Match3Event(type:String)
    {
        super(type);
    }

}
}
