package ru.elementarium.webclient.events
{

import starling.events.Event;

public class StoreEvent extends Event
{
    public static const BUY:String = "StoreEvent.BUY";

    public var store:int;
    public var section:int;
    public var artikulId:int;
    public var count:int;
    public var moneyType:int;

    public function StoreEvent(type:String)
    {
        super(type);
    }

}
}
