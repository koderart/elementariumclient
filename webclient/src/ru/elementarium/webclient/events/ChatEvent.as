package ru.elementarium.webclient.events
{

import ru.muvs.elementarium.thrift.chat.message.DataChatMessage;
import starling.events.Event;

public class ChatEvent extends Event
{
    public static const CHAT_MESSAGE_SEND_SERVER_COMMAND : String = "ChatEvent.CHAT_MESSAGE_SEND_SERVER_COMMAND";
    public static const CHAT_MESSAGE_SEND_CLIENT_COMMAND : String = "ChatEvent.CHAT_MESSAGE_SEND_CLIENT_COMMAND";

    public var message:DataChatMessage;
    
    public function ChatEvent(type:String)
    {
        super(type);
    }

}
}
