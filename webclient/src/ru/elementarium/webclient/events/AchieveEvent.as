package ru.elementarium.webclient.events
{
import starling.events.Event;

public class AchieveEvent extends Event
{
    public static const ACHIEVE_LIST_SERVER_COMMAND : String = "AchieveEvent.ACHIEVE_LIST_SERVER_COMMAND";
    public static const ACHIEVE_LIST_CLIENT_COMMAND : String = "AchieveEvent.ACHIEVE_LIST_CLIENT_COMMAND";

    public function AchieveEvent(type:String)
    {
        super(type);
    }

}
}
