package ru.elementarium.webclient.events
{

import ru.muvs.elementarium.thrift.artikul.*;

import starling.events.Event;

public class InventoryEvent extends Event
{
    public static const BAG_LIST_SERVER_COMMAND : String = "InventoryEvent.BAG_LIST_SERVER_COMMAND";
    public static const BAG_REMOVE_SERVER_COMMAND : String = "InventoryEvent.BAG_REMOVE_SERVER_COMMAND";

    public static const SLOT_LIST_SERVER_COMMAND : String = "InventoryEvent.SLOT_LIST_SERVER_COMMAND";
    public static const SLOT_TAKE_OFF_SERVER_COMMAND : String = "InventoryEvent.SLOT_TAKE_OFF_SERVER_COMMAND";
    public static const SLOT_TAKE_ON_SERVER_COMMAND : String = "InventoryEvent.SLOT_TAKE_ON_SERVER_COMMAND";

    public static const BELT_LIST_SERVER_COMMAND : String = "InventoryEvent.BELT_LIST_SERVER_COMMAND";
    public static const BELT_TAKE_OFF_SERVER_COMMAND : String = "InventoryEvent.BELT_TAKE_OFF_SERVER_COMMAND";
    public static const BELT_TAKE_ON_SERVER_COMMAND : String = "InventoryEvent.BELT_TAKE_ON_SERVER_COMMAND";
    public static const ARTIKUL_ACTION_SERVER_COMMAND : String = "InventoryEvent.ARTIKUL_ACTION_SERVER_COMMAND";
    public static const ARTIKUL_REPAIR_SERVER_COMMAND : String = "InventoryEvent.ARTIKUL_REPAIR_SERVER_COMMAND";
    public static const ARTIKUL_SELL_SERVER_COMMAND : String = "InventoryEvent.ARTIKUL_SELL_SERVER_COMMAND";

    public static const ARTIKUL_REPAIR : String = "InventoryEvent.ARTIKUL_REPAIR";

    public static const BAG_ADD_CLIENT_COMMAND:String = "InventoryEventType.BAG_ADD_CLIENT_COMMAND";
    public static const BAG_REMOVE_CLIENT_COMMAND:String = "InventoryEventType.BAG_REMOVE_CLIENT_COMMAND";
    public static const BAG_CHANGE_CLIENT_COMMAND:String = "InventoryEventType.BAG_CHANGE_CLIENT_COMMAND";
    public static const BAG_LIST_CLIENT_COMMAND:String = "InventoryEventType.BAG_LIST_CLIENT_COMMAND";

    public static const SLOT_LIST_CLIENT_COMMAND:String = "InventoryEventType.SLOT_LIST_CLIENT_COMMAND";
    public static const SLOT_TAKE_OFF_CLIENT_COMMAND:String = "InventoryEventType.SLOT_TAKE_OFF_CLIENT_COMMAND";
    public static const SLOT_TAKE_ON_CLIENT_COMMAND:String = "InventoryEventType.SLOT_TAKE_ON_CLIENT_COMMAND";

    public static const BELT_LIST_CLIENT_COMMAND:String = "InventoryEventType.BELT_LIST_CLIENT_COMMAND";
    public static const BELT_TAKE_OFF_CLIENT_COMMAND:String = "InventoryEventType.BELT_TAKE_OFF_CLIENT_COMMAND";
    public static const BELT_TAKE_ON_CLIENT_COMMAND:String = "InventoryEventType.BELT_TAKE_ON_CLIENT_COMMAND";

    public static const ARTIKUL_REPAIR_CLIENT_COMMAND:String = "InventoryEventType.ARTIKUL_REPAIR_CLIENT_COMMAND";



    public var artikul:DataArtikul;
    public var count:int;
    public var artikulMoney:int;
    public var artikulBelt:DataArtikulBelt;
    public var artikulSlot:DataArtikulSlot;
    
    public function InventoryEvent(type:String)
    {
        super(type);
    }

}
}
