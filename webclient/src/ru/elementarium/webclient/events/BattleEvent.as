package ru.elementarium.webclient.events
{
import starling.events.Event;

public class BattleEvent extends Event
{
    public static const BATTLE_JOIN_SERVER_COMMAND : String = "BattleEvent.BATTLE_JOIN_SERVER_COMMAND";
    public static const BATTLE_QUEUE_JOIN_SERVER_COMMAND : String = "BattleEvent.BATTLE_QUEUE_JOIN_SERVER_COMMAND";
    public static const BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND : String = "BattleEvent.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND";

    public var id:int;

    public function BattleEvent(type:String)
    {
        super(type);
    }

}
}
