package ru.elementarium.webclient.events
{

import starling.events.Event;

public class GameEvent extends Event
{
    public static const ACTIVATE:String = "GameEvent.ACTIVATE";
    public static const DEACTIVATE:String = "GameEvent.DEACTIVATE";

    public static const MESSAGE_CLIENT_COMMAND:String = "GameEvent.MESSAGE_CLIENT_COMMAND";

    public static const RESET_MAIN_MENU:String = "GameEvent.RESET_MAIN_MENU";

    public function GameEvent(type:String)
    {
        super(type);
    }

}
}
