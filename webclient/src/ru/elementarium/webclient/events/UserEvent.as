package ru.elementarium.webclient.events
{
import starling.events.Event;

public class UserEvent extends Event
	{
        public static const MONEY_LIST_CLIENT_COMMAND:String = "MoneyEventType.MONEY_LIST_CLIENT_COMMAND";
        public static const MONEY_CHANGE_CLIENT_COMMAND:String = "MoneyEventType.MONEY_CHANGE_CLIENT_COMMAND";

        public static const MONEY_LIST_SERVER_COMMAND:String = "MoneyEventType.MONEY_LIST_SERVER_COMMAND";

        public static const SKILL_LIST_CLIENT_COMMAND:String = "SkillEventType.SKILL_LIST_CLIENT_COMMAND";
        public static const SKILL_CHANGE_CLIENT_COMMAND:String = "SkillEventType.SKILL_CHANGE_CLIENT_COMMAND";

        public static const SKILL_LIST_SERVER_COMMAND:String = "SkillEventType.SKILL_LIST_SERVER_COMMAND";

        public function UserEvent(type:String)
        {
            super(type);
        }

    }
}
