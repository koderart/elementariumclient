package ru.elementarium.webclient.events
{

import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.quest.DataQuest;
import starling.events.Event;

public class QuestEvent extends Event
{
    public static const LOCATION_ACTION_CLIENT_QUEST_TAKE : String = "QuestEvent.LOCATION_ACTION_CLIENT_QUEST_TAKE";
    public static const LOCATION_ACTION_CLIENT_QUEST_GIVE : String = "QuestEvent.LOCATION_ACTION_CLIENT_QUEST_GIVE";
    public static const LOCATION_ACTION_CLIENT_QUEST_LIST : String = "QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST";
    public static const QUEST_REFUSE_CLIENT_COMMAND : String = "QuestEvent.QUEST_REFUSE_CLIENT_COMMAND";
    public static const QUEST_LIST_CLIENT_COMMAND : String = "QuestEvent.QUEST_LIST_CLIENT_COMMAND";
    public static const QUEST_COMPLETE_CLIENT_COMMAND : String = "QuestEvent.QUEST_COMPLETE_CLIENT_COMMAND";

    public static const LOCATION_ACTION_SERVER_QUEST_LIST : String = "QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST";
    public static const LOCATION_ACTION_SERVER_QUEST_TAKE : String = "QuestEvent.LOCATION_ACTION_SERVER_QUEST_TAKE";
    public static const LOCATION_ACTION_SERVER_QUEST_GIVE : String = "QuestEvent.LOCATION_ACTION_SERVER_QUEST_GIVE";
    public static const QUEST_REFUSE_SERVER_COMMAND : String = "QuestEvent.QUEST_REFUSE_SERVER_COMMAND";
    public static const QUEST_LIST_SERVER_COMMAND : String = "QuestEvent.QUEST_LIST_SERVER_COMMAND";

    public var member:DataLocationMember;
    public var quest:DataQuest;
    
    public function QuestEvent(type:String)
    {
        super(type);
    }

}
}
