package ru.elementarium.webclient.events
{

import flash.geom.Point;

import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.events.Event;

public class LocationEvent extends Event
{


    public static const LOCATION_LOAD_CLIENT_COMMAND: String = "LocationEvent.LOCATION_LOAD_CLIENT_COMMAND";
    public static const LOCATION_ACTION_CLIENT_MEMBER_ADD: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD";
    public static const LOCATION_ACTION_CLIENT_MEMBER_REMOVE: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE";
    public static const LOCATION_ACTION_CLIENT_MEMBER_ENABLE: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE";
    public static const LOCATION_ACTION_CLIENT_MEMBER_DISABLE: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE";
    public static const LOCATION_ACTION_CLIENT_MEMBER_INFO: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_INFO";
    public static const LOCATION_ACTION_CLIENT_MEMBER_JOIN: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN";
    public static const LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN: String = "LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN";
    public static const LOCATION_ACTION_CLIENT_BATTLE_START: String = "LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_START";
    public static const LOCATION_ACTION_CLIENT_BATTLE_STOP: String = "LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_STOP";
    public static const LOCATION_ACTION_POSITION: String = "LocationEvent.LOCATION_ACTION_POSITION";

    public static const LOCATION_ACTION_SERVER_TELEPORT_ENTER: String = "LocationEvent.LOCATION_ACTION_SERVER_TELEPORT_ENTER";
    public static const LOCATION_ACTION_SERVER_PICKUP_ENTER: String = "LocationEvent.LOCATION_ACTION_SERVER_PICKUP_ENTER";
    public static const LOCATION_ACTION_SERVER_BATTLE_INFO: String = "LocationEvent.LOCATION_ACTION_SERVER_BATTLE_INFO";
    public static const LOCATION_ACTION_SERVER_BATTLE_JOIN: String = "LocationEvent.LOCATION_ACTION_SERVER_BATTLE_JOIN";

    public static const LOCATION_ACTION_SERVER_MEMBER_RUN: String = "LocationEvent.LOCATION_ACTION_SERVER_MEMBER_RUN";
    public static const LOCATION_ACTION_CLIENT_STAND: String = "LocationEvent.LOCATION_ACTION_CLIENT_STAND";

    public var member:DataLocationMember;
    public var runPoint:Point;
    
    public function LocationEvent(type:String)
    {
        super(type);
    }

}
}
