package ru.elementarium.webclient.events
{

import starling.events.Event;

public class ChallengeEvent extends Event
{
    public static const CHALLENGE_LIST_SERVER_COMMAND : String = "ChallengeEvent.CHALLENGE_LIST_SERVER_COMMAND";
    public static const CHALLENGE_LIST_CLIENT_COMMAND : String = "ChallengeEvent.CHALLENGE_LIST_CLIENT_COMMAND";
    public static const CHALLENGE_PROGRESS_CLIENT_COMMAND : String = "ChallengeEvent.CHALLENGE_PROGRESS_CLIENT_COMMAND";

    public var quest:Object;
    
    public function ChallengeEvent(type:String)
    {
        super(type);
    }

}
}
