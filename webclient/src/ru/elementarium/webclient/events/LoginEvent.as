package ru.elementarium.webclient.events
{
import starling.events.Event;

public class LoginEvent extends Event
{

    public static const REGISTER_LOADED:String = "LoginEvent.REGISTER_LOADED";
    
    public static const LOGIN_EMAIL_START:String = "RegisterEvent.LOGIN_EMAIL_START";
    public static const LOGIN_EMAIL:String = "RegisterEvent.LOGIN_EMAIL";
    public static const REGISTER_EMAIL:String = "RegisterEvent.REGISTER_EMAIL";
    public static const LOGIN_VK:String = "RegisterEvent.LOGIN_VK";
    public static const REGISTER_VK:String = "RegisterEvent.REGISTER_VK";
    public static const LOGIN_MY_MAIL:String = "RegisterEvent.LOGIN_MY_MAIL";
    public static const REGISTER_MY_MAIL:String = "RegisterEvent.REGISTER_MY_MAIL";
    public static const LOGIN_APPLE:String = "RegisterEvent.LOGIN_APPLE";
    public static const REGISTER_APPLE:String = "RegisterEvent.REGISTER_APPLE";

    public static const SOCIAL_ERROR:String = "RegisterEvent.SOCIAL_ERROR";

    public static const LOGIN_SUCCESS:String = "LoginEventType.LOGIN_SUCCESS";
    public static const LOGIN_FAILED:String = "LoginEventType.LOGIN_FAILED";
    public static const PLAYER_UNDEFINED:String = "LoginEventType.PLAYER_UNDEFINED";
    public static const PLAYER_NAME_IS_BUSY:String = "LoginEventType.PLAYER_NAME_IS_BUSY";
    public static const EMAIL_IS_BROKEN:String = "LoginEventType.EMAIL_IS_BROKEN";
    public static const PLAYER_FRACTION_UNDEFINED:String = "LoginEventType.PLAYER_FRACTION_UNDEFINED";

    public var email:String;
    public var pass:String;
    public var name:String;
    public var sex:int;
    public var element:int;
    public var fraction:int;

    public function LoginEvent(type:String)
    {
        super(type);
    }

}
}
