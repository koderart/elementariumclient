package ru.elementarium.webclient.events
{

import starling.events.Event;

public class HintEvent extends Event
{
    public static const SHOW:String = "HintEvent.SHOW";
    public static const HIDE:String = "HintEvent.HIDE";
    
    public var typeHint:int;
    public var spellId:int;
    public var userLevel:int;
    public var userElement:int;
    public var points:Array;
    public var x:int;
    public var y:int;
    public var artikulId:int;
    public var durabilityCurrent:int;
    public var durability:int;
    public var randomSkills:Array;

    public function HintEvent(type:String)
    {
        super(type);
    }

}
}
