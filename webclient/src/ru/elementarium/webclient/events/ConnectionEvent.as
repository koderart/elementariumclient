package ru.elementarium.webclient.events
{
import starling.events.Event;

public class ConnectionEvent extends Event
{
        public static const CONNECT:String = "ConnectionEvent.CONNECT";
        public static const CONNECTED:String = "ConnectionEvent.CONNECTED";
        public static const DISCONNECTED:String = "ConnectionEvent.DISCONNECTED";
        public static const RECONNECT:String = "ConnectionEvent.RECONNECT";
        public static const CONNECT_FAILED:String = "ConnectionEvent.CONNECT_FAILED";
        
        public static const VERSION_CLIENT_COMMAND:String = "ConnectionEvent.VERSION_CLIENT_COMMAND";
        public static const VERSION_CLIENT_ERROR:String = "ConnectionEvent.VERSION_CLIENT_ERROR";

        public var serverVersion:int;
        
        public function ConnectionEvent(type:String)
        {
            super(type);
        }
}
}
