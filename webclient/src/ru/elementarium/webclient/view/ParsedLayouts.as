package ru.elementarium.webclient.view {
    public class ParsedLayouts
    {
        public static var game:Object;
        public static var game_connection:Object;
        public static var game_option_panel:Object;
        public static var game_user_panel_left:Object;
        public static var game_money_panel:Object;
        public static var game_mob_panel:Object;
        
        public static var match3:Object;
        public static var match3_user_item:Object;
        public static var match3_user_panel_left:Object;
        public static var match3_user_panel_right:Object;
        public static var match3_magic_item:Object;
        public static var match3_belt_item:Object;
        public static var match3_buff_item:Object;
        public static var match3_magic_info:Object;
        public static var match3_buff_info:Object;
        public static var match3_potion_info:Object;
        public static var match3_message_damage:Object;
        
        public static var inventory:Object;
        public static var inventory_item:Object;
        public static var inventory_item_slot:Object;
        public static var inventory_item_belt:Object;
        public static var inventory_remove:Object;
        public static var inventory_sell:Object;
        public static var inventory_skills:Object;
        public static var inventory_artikul_alert:Object;
        public static var inventory_artikul_alert_stat:Object;
        public static var inventory_repair:Object;

        public static var store:Object;
        public static var store_item:Object;
        public static var store_item_buy:Object;

        public static var registration_token:Object;
        public static var registration_magic_item:Object;
        public static var registration_fraction:Object;
        
        public static var arena:Object;

        public static var hint_spell:Object;
        public static var hint_artikul:Object;
        public static var hint_artikul_money:Object;
        public static var hint_challenge:Object;
        public static var hint_challenge_item:Object;
        
        public static var alert:Object;
        
        public static var button_common:Object;

        public static var npc:Object;
        public static var npc_item:Object;

        public static var quest:Object;
        public static var quest_goal_item:Object;
        public static var quest_award_item:Object;
        public static var quest_money_exp_item:Object;

        public static var payment:Object;
        public static var payment_item:Object;


        public static var alert_level:Object;
        public static var alert_win:Object;
        public static var alert_defeat:Object;
        public static var alert_drop:Object;
        public static var alert_achieve:Object;
        public static var alert_daily_bonus:Object;
        public static var alert_take_give_component:Object;

        public static var quest_complete_alert:Object;

        public static var help:Object;
        public static var news:Object;
        
        public static var battle_info:Object;
        public static var location_user_item:Object;
        public static var location_info_user_item:Object;
        public static var chat:Object;

        public static var achieve:Object;
        public static var achieve_item:Object;
        
        public static var window:Object;
        public static var window_module:Object;

    }
}
