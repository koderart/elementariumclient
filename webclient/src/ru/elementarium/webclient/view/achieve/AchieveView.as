package ru.elementarium.webclient.view.achieve {

import feathers.controls.List;
import feathers.controls.renderers.IListItemRenderer;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.achieve.components.AchieveItemRenderer;
import ru.elementarium.webclient.view.main.ModuleView;

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;

    public class AchieveView extends ModuleView
    {
        public var _itemList:List;
        public var _pageLeftButton:Button;
        public var _pageRightButton:Button;
        public var _pageLabel:TextField;

        public function AchieveView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.achieve, false, this) as Sprite;

            _titleLabel.text = "ДОСТИЖЕНИЯ";
            
            var layout:VerticalLayout = new VerticalLayout();
            layout.gap = 0;
            layout.padding = 0;
            _itemList.layout = layout;
            _itemList.itemRendererFactory = function():IListItemRenderer { return new AchieveItemRenderer(); };
            
            validateAndAttach();
        }
    }
}
