package ru.elementarium.webclient.view.achieve
{
import com.junkbyte.console.Cc;

import feathers.data.ListCollection;
import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.achieve.components.AchieveDataRenderer;
import ru.muvs.elementarium.thrift.achieve.DataAchieve;
import starling.events.Event;

    public class AchieveViewMediator extends Mediator
	{
		[Inject]
		public var view:AchieveView;
        [Inject]
        public var mainModel:MainModel;
        
        private static const MAX_ITEM_TO_PAGE:int = 4;
        
        private var currentPage:int = 0;
        private var _achieveCollection:Array;

		override public function onRegister():void
		{
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._pageLeftButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._pageRightButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _achieveCollection = [];
            var data:AchieveDataRenderer;
            for each (var a:DataAchieve in mainModel.achieveList) {
                data = new AchieveDataRenderer(MainModel.achieveData[a.achieveId], a);
                _achieveCollection.push(data);
            }
            updatePageData();
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view._closeButton:
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                break;
                case view._pageLeftButton:
                    var newPage:int = currentPage-1;
                    if (newPage < 0) {newPage = 0;}
                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;
                case view._pageRightButton:

                    var dataLength:int = _achieveCollection.length;

                    newPage = currentPage+1;
                    if ((newPage*MAX_ITEM_TO_PAGE) >= dataLength ) {newPage = currentPage;}

                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;
            }
        }

        private function updatePageData():void
        {
            view._pageLabel.text = (currentPage+1) + "/" + Math.max(1, Math.ceil(_achieveCollection.length / MAX_ITEM_TO_PAGE));

            var startIndex:int = currentPage*MAX_ITEM_TO_PAGE;
            view._itemList.dataProvider = new ListCollection(_achieveCollection.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
        }

    }
}