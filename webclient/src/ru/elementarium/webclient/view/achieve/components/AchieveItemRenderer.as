package ru.elementarium.webclient.view.achieve.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;
import feathers.controls.ScrollContainer;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.achieve.components.AchieveDataRenderer;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.AchieveGoalVO;
import ru.muvs.admin.main.vo.AchieveSkillVO;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.BonusActionVO;
import ru.muvs.admin.main.vo.enum.AchieveGoalEnum;
import ru.muvs.admin.main.vo.enum.BonusActionEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.elementarium.thrift.achieve.DataAchieveGoal;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;

public class AchieveItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        private var initialized:Boolean = false;
        
        private var imageLoader:ImageLoader;
        private var titleLabel:TextField;
        private var goalLabel:TextField;
        private var rewardSkillLabel:TextField;
        private var rewardMoneyLabel:TextField;
        private var rewardCrystalIcon:Image;
        private var rewardGoldIcon:Image;
        private var doneGroup:LayoutGroup;
        private var artikulLoader:ImageLoader;
        private var artikulBorder:Image;
        private var artikulCountLabel:TextField;
        
        private var currentArtikul:ArtikulVO;

        public function AchieveItemRenderer()
        {
            super();
        }

        override protected function initialize():void
        {
            initialized = true;
            
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.achieve_item) as Sprite;
                addChild(_sprite);
                imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
                titleLabel = _sprite.getChildByName("titleLabel") as TextField;
                goalLabel = _sprite.getChildByName("goalLabel") as TextField;
                rewardSkillLabel = ScrollContainer(_sprite.getChildByName("scrollContainer")).getChildByName("rewardSkillLabel") as TextField;
                rewardMoneyLabel = _sprite.getChildByName("rewardMoneyLabel") as TextField;
                rewardCrystalIcon = _sprite.getChildByName("rewardCrystalIcon") as Image;
                rewardGoldIcon = _sprite.getChildByName("rewardGoldIcon") as Image;
                doneGroup = _sprite.getChildByName("doneGroup") as LayoutGroup;
                artikulLoader = _sprite.getChildByName("artikulLoader") as ImageLoader;
                artikulBorder = _sprite.getChildByName("artikulBorder") as Image;
                artikulBorder.addEventListener(TouchEvent.TOUCH, slotArtikulHintHandler);
                artikulCountLabel = _sprite.getChildByName("artikulCountLabel") as TextField;
                
                if (data) {
                    drawData();
                }
            }
        }

        private function slotArtikulHintHandler(event:TouchEvent):void {
            var touch:Touch = event.getTouch(event.currentTarget as DisplayObject);
            if (touch && touch.phase == TouchPhase.HOVER && currentArtikul)
            {
                dispatchEventWith("showHintEvent", true, {artikulId:currentArtikul.artikulId, x:touch.globalX, y:touch.globalY, durabilityCurrent:currentArtikul.durabilityCurrent, durability:currentArtikul.durability});
            } else {
                dispatchEventWith("hideHintEvent", true);
            }
        }

        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();

            }
        }

        private function drawData():void
        {
            var achieve:AchieveDataRenderer = data as AchieveDataRenderer;

            imageLoader.source = ConnectionService.BASE_URL_HTTP + achieve.dataStorage.image.url;
            titleLabel.text = achieve.dataStorage.title.ru;

            var goal : Array = [];
            var dataCountCurrent : Array = [];
            
            for each (var g:DataAchieveGoal in achieve.data.goals) {
                dataCountCurrent[g.goalId] = g.countCurrent;
            }
            
            for each (var achieveGoal:AchieveGoalVO in achieve.dataStorage.achieveGoal) {
                switch (achieveGoal.GOAL) {
                    case AchieveGoalEnum.KILL_MOB:
                        goal.push("Убийство моба " + achieveGoal.mob.title.ru + ": " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                    break;
                    case AchieveGoalEnum.KILL_PLAYER:
                        switch (achieveGoal.element) {
                            case ElementEnum.DARK:
                                goal.push("Убийство игроков тьмы: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                            break;
                            case ElementEnum.FIRE:
                                goal.push("Убийство игроков огня: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                            break;
                            case ElementEnum.LIGHT:
                                goal.push("Убийство игроков света: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                            break;
                            case ElementEnum.NATURE:
                                goal.push("Убийство игроков природы: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                            break;
                            case ElementEnum.WATER:
                                goal.push("Убийство игроков воды: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                            break;
                            default:
                                goal.push("Убийство игроков: " + dataCountCurrent[achieveGoal.achieveGoalId] + "/" + achieveGoal.count);
                        } 
                    break;
                }
            }           
            
            goalLabel.text = goal.join("\n");

            
            var skill : Array = [];
            for each (var achieveSkill:AchieveSkillVO in achieve.dataStorage.achieveSkill) {
                skill.push(achieveSkill.skill.title.ru + " +" + achieveSkill.value);
            }
            
            rewardSkillLabel.text = skill.join("\n");

            rewardMoneyLabel.text = "";
            rewardCrystalIcon.visible = false;
            rewardGoldIcon.visible = false;

            artikulLoader.source = null;
            artikulBorder.visible = false;
            artikulCountLabel.text = "";

            if (achieve.dataStorage.bonus.bonusId) {
                for each (var bonusAction:BonusActionVO in achieve.dataStorage.bonus.bonusAction) {
                    if (bonusAction.bonusAction == BonusActionEnum.ARTIKUL_GIVE || bonusAction.bonusAction == BonusActionEnum.ARTIKUL_TAKE) {
                        currentArtikul = MainModel.artikulData[bonusAction.artikul];
                        artikulLoader.source = ConnectionService.BASE_URL_HTTP + currentArtikul.image.url;
                        artikulBorder.visible = true;
                        artikulCountLabel.text = bonusAction.countFrom.toString();
                        break;
                    }
                    if (bonusAction.bonusAction == BonusActionEnum.MONEY_GIVE || bonusAction.bonusAction == BonusActionEnum.MONEY_TAKE) {
                        switch (bonusAction.moneyType) {
                            case MoneyEnum.CRYSTAL:
                                rewardCrystalIcon.visible = true;
                                rewardGoldIcon.visible = false;
                                break;
                            case MoneyEnum.GOLD:
                                rewardCrystalIcon.visible = false;
                                rewardGoldIcon.visible = true;
                                break;
                        }
                        rewardMoneyLabel.text = bonusAction.moneyFrom.toString();
                        break;
                    }
                }
            }
            
            doneGroup.visible = achieve.data.done;
        }
    }
}
