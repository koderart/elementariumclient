package ru.elementarium.webclient.view.login
{

import com.junkbyte.console.Cc;

import flash.net.SharedObject;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.LoginEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.muvs.elementarium.thrift.player.DataPlayerSex;

import starling.events.Event;

public class LoginViewMediator extends Mediator
	{
		[Inject]
		public var view:LoginView;
        [Inject]
        public var mainModel:MainModel;

        private var loginAction:Boolean = true;

        override public function onRegister():void
		{
            trace("LoginViewMediator onRegister");
            addContextListener(LoginEvent.EMAIL_IS_BROKEN, loginEventTypeHandler);
            addContextListener(LoginEvent.LOGIN_FAILED, loginEventTypeHandler);
            addContextListener(LoginEvent.PLAYER_NAME_IS_BUSY, loginEventTypeHandler);
            
            view.loginButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.regButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.regVKButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.toRegButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.toLoginButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            
            BUILD::test {
                view.toRegButton.visible = view.toRegButton.includeInLayout = true;
                var passSo:SharedObject = SharedObject.getLocal("pass");
                view.emailLoginInput.text = passSo.data.login ? passSo.data.login : "test@test.ru";
                view.passLoginInput.text = passSo.data.pass ? passSo.data.pass : "123";
            }
        }

        private function loginEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case LoginEvent.EMAIL_IS_BROKEN:
                    ELAlert.show("Неверный email", "Ошибка");
                    break;
                case LoginEvent.LOGIN_FAILED:
                    if (loginAction) {
                        ELAlert.show("Неверный email или пароль", "Ошибка");
                    } else {
                        ELAlert.show("Такой email уже зарегистрирован", "Ошибка");
                    }
                    break;
                case LoginEvent.PLAYER_NAME_IS_BUSY:
                    ELAlert.show("Такое имя уже занято", "Ошибка");
                    break;
            }
        }

        private function triggeredHandler(event:Event):void {
            switch (event.target) {
                case view.loginButton:
                    if (view.emailLoginInput.text.length == 0) {
                        ELAlert.show("Введите e-mail", "Ошибка");
                        return;
                    }
                    if (view.passLoginInput.text.length == 0) {
                        ELAlert.show("Введите пароль", "Ошибка");
                        return;
                    }

                    BUILD::test {
                        view.toRegButton.visible = view.toRegButton.includeInLayout = true;
                        var passSo:SharedObject = SharedObject.getLocal("pass");
                        passSo.data.login = view.emailLoginInput.text;
                        passSo.data.pass = view.passLoginInput.text;
                        passSo.flush();
                    }

                    var e:LoginEvent = new LoginEvent(LoginEvent.LOGIN_EMAIL);
                    mainModel.email = e.email = view.emailLoginInput.text;
                    mainModel.password = e.pass = view.passLoginInput.text;

                    dispatch(e);
                break;
                case view.regButton:
                    if (view.emailRegInput.text.length == 0) {
                        ELAlert.show("Введите e-mail", "Ошибка");
                        return;
                    }
                    if (view.passReg1Input.text.length == 0) {
                        ELAlert.show("Введите пароль", "Ошибка");
                        return;
                    }
                    if (view.passReg2Input.text.length == 0) {
                        ELAlert.show("Повторите пароль", "Ошибка");
                        return;
                    }
                    if (view.passReg1Input.text != view.passReg2Input.text) {
                        ELAlert.show("Пароли не совпадают", "Ошибка");
                        return;
                    }
                    if (view.nameRegInput.text.length == 0) {
                        ELAlert.show("Введите игровое имя", "Ошибка");
                        return;
                    }

                    var sex:int = DataPlayerSex.MALE;
                    if (view.sexFemale.isSelected) {
                        sex = DataPlayerSex.FEMALE;
                    }
                    e = new LoginEvent(LoginEvent.REGISTER_EMAIL);
                    e.email = view.emailRegInput.text;
                    e.pass = view.passReg1Input.text;
                    e.name = view.nameRegInput.text;
                    e.sex = sex;
                    e.element = view.elementList.selectedItem.data;
                    dispatch(e);
                break;
                case view.regVKButton:
                    if (view.nameVKInput.text.length == 0) {
                        ELAlert.show("Введите игровое имя", "Ошибка");
                        return;
                    }
                break;
                case view.toRegButton:
                    loginAction = false;
                    view.screenNavigator.pushScreen("regScreen");
                break;
                case view.toLoginButton:
                    loginAction = true;
                    view.screenNavigator.pushScreen("loginScreen");
                break;
            }
        }
    }
}
