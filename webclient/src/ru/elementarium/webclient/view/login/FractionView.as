package ru.elementarium.webclient.view.login {
import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;
import ru.muvs.elementarium.thrift.player.DataPlayerFraction;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class FractionView extends ModuleView
    {
        public var _okButton:feathers.controls.Button;
        
        public var _fraction1Group:LayoutGroup;
        public var _fraction1Icon:Image;
        public var _fraction1Border:Image;
        public var _fraction1Title:TextField;
        public var _fraction1Description:TextField;

        public var _fraction2Group:LayoutGroup;
        public var _fraction2Icon:Image;
        public var _fraction2Border:Image;
        public var _fraction2Title:TextField;
        public var _fraction2Description:TextField;
    
        public function FractionView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.registration_fraction, false, this) as Sprite;

            _titleLabel.text = "ВЫБЕРИТЕ ФРАКЦИЮ";
            _fraction1Title.text = MainModel.getLocale("fraction1.title");
            _fraction1Description.text = MainModel.getLocale("fraction1.description");
            _fraction2Title.text = MainModel.getLocale("fraction2.title");
            _fraction2Description.text = MainModel.getLocale("fraction2.description");
                    
            validateAndAttach();
        }
        
        public function setFraction(fraction:int):void
        {
            switch (fraction) {
                case DataPlayerFraction.FIRST:
                    _fraction1Icon.visible = _fraction1Border.visible = true;
                    _fraction2Icon.visible = _fraction2Border.visible = false;
                    break;
                case DataPlayerFraction.SECOND:
                    _fraction1Icon.visible = _fraction1Border.visible = false;
                    _fraction2Icon.visible = _fraction2Border.visible = true;
                    break;
            }
        }
        
}
}
