package ru.elementarium.webclient.view.login
{
import com.junkbyte.console.Cc;

import feathers.controls.List;
import feathers.data.ListCollection;

import mx.utils.StringUtil;

import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.LoginEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.elementarium.webclient.view.match3.MagicDataWrapper;
import ru.elementarium.webclient.view.match3.MagicInfo;
import ru.muvs.admin.main.vo.battle.MagicVO;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.player.DataPlayerSex;
import starling.display.Image;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class RegisterTokenViewMediator extends Mediator
	{
		[Inject]
		public var view:RegisterTokenView;

        [Inject]
        public var mainModel:MainModel;

        private var selectedSex:int;
        private var selectedElement:int;
        private var selectedElementVO:String;
        private var magicList:ListCollection = new ListCollection();

		override public function onRegister():void
		{
            addContextListener(LoginEvent.PLAYER_NAME_IS_BUSY, registerVkEventTypeHandler);

            view._maleAvaDisabled.addEventListener(TouchEvent.TOUCH, onTouch);
            view._maleAvaSelected.addEventListener(TouchEvent.TOUCH, onTouch);
            view._femaleAvaDisabled.addEventListener(TouchEvent.TOUCH, onTouch);
            view._femaleAvaSelected.addEventListener(TouchEvent.TOUCH, onTouch);

            view._startButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            setMale();

            view._magicList.dataProvider = magicList;
            view._magicList.addEventListener(Event.CHANGE, magicList_changeHandler);

            view._elementTabBar.addEventListener( Event.CHANGE, tab_changeHandler );
            view._elementTabBar.selectedIndex = Math.floor(Math.random()*5);
            tab_changeHandler(null);
        }

        private function tab_changeHandler( event:Event ):void
        {
            switch (view._elementTabBar.selectedIndex) {
                case 0:
                    selectedElement = DataPlayerElement.LIGHT;
                    selectedElementVO = ElementEnum.LIGHT;
                    break;
                case 1:
                    selectedElement = DataPlayerElement.DARK;
                    selectedElementVO = ElementEnum.DARK;
                    break;
                case 2:
                    selectedElement = DataPlayerElement.WATER;
                    selectedElementVO = ElementEnum.WATER;
                    break;
                case 3:
                    selectedElement = DataPlayerElement.NATURE;
                    selectedElementVO = ElementEnum.NATURE;
                    break;
                case 4:
                    selectedElement = DataPlayerElement.FIRE;
                    selectedElementVO = ElementEnum.FIRE;
                    break;
            }

            magicList.removeAll();

            for each (var magic:MagicVO in MainModel.magicData) {
                if (magic.visible && magic.element == selectedElementVO) {
                    magicList.addItem(new MagicDataWrapper(magic.battleMagicId, magic.cooldown));
                }
            }

            view.setAvatar(selectedElementVO);
            view.setDescription(selectedElementVO);
        }

        private function magicList_changeHandler(event:Event):void
        {
            var list:List = List( event.currentTarget );
            if (!list.selectedItem) {
                return;
            }

            var magicInfo:MagicInfo = new MagicInfo(MagicDataWrapper(list.selectedItem), selectedElementVO, true);
            view.stage.addChild(magicInfo);
            list.selectedItem = null;
        }

        private function registerVkEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case LoginEvent.PLAYER_NAME_IS_BUSY:
                    ELAlert.show("Такое имя уже занято", "Ошибка");
                    break;
            }
        }

        private function setMale():void
        {
            selectedSex = DataPlayerSex.MALE;
            view._maleAvaDisabled.visible = false;
            view._maleAvaSelected.visible = true;
            view._femaleAvaDisabled.visible = true;
            view._femaleAvaSelected.visible = false;
        }

        private function setFemale():void
        {
            selectedSex = DataPlayerSex.FEMALE;
            view._maleAvaDisabled.visible = true;
            view._maleAvaSelected.visible = false;
            view._femaleAvaDisabled.visible = false;
            view._femaleAvaSelected.visible = true;
        }

        private function onTouch(event:TouchEvent):void
        {
            var image:Image = event.target as Image;
            var touch: Touch = event.getTouch(image);
            if (touch && touch.phase == TouchPhase.ENDED)
            {
                switch (event.target) {
                    case view._maleAvaDisabled:
                        setMale();
                        break;
                    case view._femaleAvaDisabled:
                        setFemale();
                        break;
                }
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._startButton:
                    var name:String = StringUtil.trim(view._nicknameInput.text);
                    var rex:RegExp = /[\s\r\n]+/gim;
                    name = name.replace(rex,' ');

                    if(name.length > 3) {
                        if (mainModel.socialVkData) {
                            var e:LoginEvent = new LoginEvent(LoginEvent.REGISTER_VK);
                            e.name = name;
                            e.sex = selectedSex;
                            e.element = selectedElement;
                            dispatch(e);
                        } else  if (mainModel.socialMyMailData) {
                            e = new LoginEvent(LoginEvent.REGISTER_MY_MAIL);
                            e.name = name;
                            e.sex = selectedSex;
                            e.element = selectedElement;
                            dispatch(e);
                        } else if (MainModel.isMobile) {
                            e = new LoginEvent(LoginEvent.REGISTER_APPLE);
                            e.name = name;
                            e.sex = selectedSex;
                            e.element = selectedElement;
                            dispatch(e);
                        }
                    } else {
                        ELAlert.show("Введите имя своего героя", "Ошибка");
                    }
                break;
            }

        }
    }
}