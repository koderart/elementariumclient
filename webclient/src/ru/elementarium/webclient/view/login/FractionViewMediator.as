package ru.elementarium.webclient.view.login
{

import flash.sampler.pauseSampling;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.LoginEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.elementarium.thrift.player.DataPlayerFraction;

import starling.display.DisplayObject;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class FractionViewMediator extends Mediator
	{
		[Inject]
		public var view:FractionView;
        [Inject]
        public var mainModel:MainModel;

        private var currentFraction:int = 0;

		override public function onRegister():void
		{
            view._closeButton.visible = false;
            view._okButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._fraction1Group.addEventListener(TouchEvent.TOUCH, onTouch);
            view._fraction2Group.addEventListener(TouchEvent.TOUCH, onTouch);
            
            view._closeButton.visible = false;
            
            currentFraction = Math.random()<0.5 ? DataPlayerFraction.FIRST : DataPlayerFraction.SECOND;
            view.setFraction(currentFraction);
        }
    
        private function onTouch(event:TouchEvent):void
        {
            var target:DisplayObject = event.currentTarget as DisplayObject;
            var touch: Touch = event.getTouch(target);
            if (touch && touch.phase == TouchPhase.ENDED)
            {
                switch (target) {
                    case view._fraction1Group:
                        currentFraction = DataPlayerFraction.FIRST;
                        view.setFraction(currentFraction);
                        break;
                    case view._fraction2Group:
                        currentFraction = DataPlayerFraction.SECOND;
                        view.setFraction(currentFraction);
                        break;
                }
            }
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view._okButton:
                    var e:LoginEvent = new LoginEvent(LoginEvent.LOGIN_EMAIL);
                    e.email = mainModel.email;
                    e.pass = mainModel.password;
                    e.fraction = currentFraction;
                    dispatch(e);

                    break;
            }
        }
    }
}