package ru.elementarium.webclient.view.login.components {

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Sprite;

public class MagicItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;
        private var imageLoader:ImageLoader;

        private var _select:TapToSelect;

        public function MagicItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            super.initialize();
            
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.registration_magic_item) as Sprite;
                addChild(_sprite);

                imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
                imageLoader.width = 50;
                imageLoader.height = 50;
            }
        }

        override protected function commitData():void
        {
            imageLoader.source = ConnectionService.BASE_URL_HTTP + _data.staticVO.image.url;
        }
}
}
