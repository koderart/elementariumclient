package ru.elementarium.webclient.view.login{
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;
import feathers.controls.List;
import feathers.controls.TabBar;
import feathers.controls.TextInput;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.layout.Direction;
import feathers.layout.HorizontalLayout;

import flash.geom.Rectangle;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.login.components.MagicItemRenderer;
import ru.muvs.admin.main.vo.enum.ElementEnum;

import starling.display.BlendMode;

import starling.display.Button;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;
import starling.textures.TextureSmoothing;
import starling.utils.AssetManager;

import starlingbuilder.engine.DefaultAssetMediator;
import starlingbuilder.engine.UIBuilder;

public class RegisterTokenView extends Sprite
    {
        private var _sprite:Sprite;
        private var _assets : AssetManager;
    
        public var _mainLayout:LayoutGroup;
        public var _maleAva:ImageLoader;
        public var _femaleAva:ImageLoader;
        public var _maleAvaSelected:Image;
        public var _maleAvaDisabled:Image;
        public var _femaleAvaSelected:Image;
        public var _femaleAvaDisabled:Image;

        public var _setElementLabel:TextField;
        public var _setSexLabel:TextField;
        public var _setNameLabel:TextField;
        public var _startSkillLabel:TextField;
        public var _elementTitleLabel:TextField;
        public var _elementDescriptionLabel:TextField;
        public var _nicknameInput:TextInput;
        public var _startButton:Button;

        public var _elementTabBar:TabBar;
        public var _magicList : List;

        public function RegisterTokenView(assets:AssetManager)
        {
            super();
            _assets = assets;
            
            var uiBuilder:UIBuilder = new UIBuilder(new DefaultAssetMediator(_assets));
            
            _sprite = uiBuilder.create(ParsedLayouts.registration_token, true, this) as Sprite;
            addChild(_sprite);

            _setElementLabel.text = MainModel.getLocale("registration.set.element.title");
            _setSexLabel.text = MainModel.getLocale("registration.set.sex.title");
            _setNameLabel.text = MainModel.getLocale("registration.set.name.title");
            _startSkillLabel.text = MainModel.getLocale("registration.startskill.title");
            _magicList.itemRendererFactory = function():IListItemRenderer { return new MagicItemRenderer(); };
            var layoutH:HorizontalLayout = new HorizontalLayout();
            layoutH.gap = 5;
            layoutH.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            _magicList.layout = layoutH;
            _maleAvaSelected.useHandCursor = true;
            _maleAvaDisabled.useHandCursor = true;
            _femaleAvaSelected.useHandCursor = true;
            _femaleAvaDisabled.useHandCursor = true;

            _nicknameInput.maxChars = 12;
            _nicknameInput.restrict = "A-Za-zА-я0-9_\\- ";
            _elementTabBar.customTabStyleName = "transparentTabButton";
            _elementTabBar.gap = 10;
            _elementTabBar.direction = Direction.HORIZONTAL;

            _elementTabBar.dataProvider = new ListCollection(
                    [
                        { label: "",  defaultIcon:getImageElementTab("element/light", true),
                            defaultSelectedIcon:getImageElementTab("element/light")},
                        { label: "",  defaultIcon:getImageElementTab("element/dark", true),
                            defaultSelectedIcon:getImageElementTab("element/dark")},
                        { label: "",  defaultIcon:getImageElementTab("element/water", true),
                            defaultSelectedIcon:getImageElementTab("element/water")},
                        { label: "",  defaultIcon:getImageElementTab("element/nature", true),
                            defaultSelectedIcon:getImageElementTab("element/nature")},
                        { label: "",  defaultIcon:getImageElementTab("element/fire", true),
                            defaultSelectedIcon:getImageElementTab("element/fire")}
                    ]);

            _mainLayout.validate();
            addEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);
        }

        public function setAvatar(element:String):void
        {
            switch (element) {
                case ElementEnum.DARK:
                    _maleAva.source = _assets.getTexture("avatar/male_dark");
                    _femaleAva.source = _assets.getTexture("avatar/female_dark");
                    break;
                case ElementEnum.FIRE:
                    _maleAva.source = _assets.getTexture("avatar/male_fire");
                    _femaleAva.source = _assets.getTexture("avatar/female_fire");
                    break;
                case ElementEnum.LIGHT:
                    _maleAva.source = _assets.getTexture("avatar/male_light");
                    _femaleAva.source = _assets.getTexture("avatar/female_light");
                    break;
                case ElementEnum.NATURE:
                    _maleAva.source = _assets.getTexture("avatar/male_nature");
                    _femaleAva.source = _assets.getTexture("avatar/female_nature");
                    break;
                case ElementEnum.WATER:
                    _maleAva.source = _assets.getTexture("avatar/male_water");
                    _femaleAva.source = _assets.getTexture("avatar/female_water");
                    break;
            }
        }

        public function setDescription(element:String):void
        {
            switch (element) {
                case ElementEnum.DARK:
                    _elementTitleLabel.text = MainModel.getLocale("element.dark.title");
                    _elementDescriptionLabel.text = MainModel.getLocale("element.dark.description");
                    break;
                case ElementEnum.FIRE:
                    _elementTitleLabel.text = MainModel.getLocale("element.fire.title");
                    _elementDescriptionLabel.text = MainModel.getLocale("element.fire.description");
                    break;
                case ElementEnum.LIGHT:
                    _elementTitleLabel.text = MainModel.getLocale("element.light.title");
                    _elementDescriptionLabel.text = MainModel.getLocale("element.light.description");
                    break;
                case ElementEnum.NATURE:
                    _elementTitleLabel.text = MainModel.getLocale("element.nature.title");
                    _elementDescriptionLabel.text = MainModel.getLocale("element.nature.description");
                    break;
                case ElementEnum.WATER:
                    _elementTitleLabel.text = MainModel.getLocale("element.water.title");
                    _elementDescriptionLabel.text = MainModel.getLocale("element.water.description");
                    break;
            }
        }

        protected function getImageElementTab(textureName:String, small:Boolean=false):Image
        {
            var result:Image = new Image(_assets.getTexture(textureName));
            
            if (small) {
                result.textureSmoothing = TextureSmoothing.TRILINEAR;
                result.scaleX = 0.55;
                result.scaleY = 0.55;
            }
            return result;
        }
    
        protected function view_addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);
            
            var bgGradientImage:Image = new Image(AppTheme.instance.assets.getTexture("window/background_dark_gradient"));
            bgGradientImage.scale9Grid = new flash.geom.Rectangle(116, 113, 61, 61);

            bgGradientImage.width = stage.stageWidth;
            bgGradientImage.height = stage.stageHeight;
            bgGradientImage.blendMode = BlendMode.MULTIPLY;
            addChildAt(bgGradientImage,0);

            var bgImage:Image = new Image(AppTheme.instance.assets.getTexture("window/background2"));
            bgImage.tileGrid = new flash.geom.Rectangle(0, 0, bgImage.width, bgImage.height);
            
            bgImage.width = stage.stageWidth;
            bgImage.height = stage.stageHeight;
            addChildAt(bgImage,0);
            
            _sprite.x = int((stage.stageWidth-_sprite.width)/2);
            _sprite.y = int((stage.stageHeight-_sprite.height)/2);
        }
}
}
