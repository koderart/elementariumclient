package ru.elementarium.webclient.view
{

	public class EmbeddedAssets
	{
		//[Embed(source="/../assets/textures/common.atf")]
		//public static const common:Class;

		//[Embed(source="/../assets/textures/common.xml", mimeType="application/octet-stream")]
		//public static const common_xml:Class;

		//[Embed(source="/../assets/textures/gems_bang.png")]
		//public static const gems_bang:Class;
		
		//[Embed(source="/../assets/textures/gems_bang.xml", mimeType="application/octet-stream")]
		//public static const gems_bang_xml:Class;
		/*
		[Embed(source="/../assets/items/farm_bonus_point.png")]
		public static const farm_bonus_point:Class;
		
		[Embed(source="/../assets/items/farm_bonus_point.xml", mimeType="application/octet-stream")]
		public static const FarmBonusPointAtl:Class;
		
		[Embed(source="/../assets/items/farm_bang.png")]
		public static const farm_bang:Class;
		
		[Embed(source="/../assets/items/farm_bang.xml", mimeType="application/octet-stream")]
		public static const FarmBangAtl:Class;
        */
        //[Embed(source="/../assets/textures/nickname_font.png")]
        //public static const nickname_font:Class;
        //[Embed(source="/../assets/textures/nickname_font.fnt", mimeType="application/octet-stream")]
        //public static const nickname_font_xml:Class;

        //[Embed(source="/../assets/textures/turn_font.png")]
        //public static const turn_font:Class;
        //[Embed(source="/../assets/textures/turn_font.fnt", mimeType="application/octet-stream")]
        //public static const turn_font_xml:Class;

        //[Embed(source="/../assets/textures/font_button.png")]
        //public static const font_button:Class;
        //[Embed(source="/../assets/textures/font_button.fnt", mimeType="application/octet-stream")]
        //public static const font_button_xml:Class;

        //[Embed(source="/../assets/textures/battle_font.fnt", mimeType="application/octet-stream")]
        //public static const battle_font_xml:Class;
        
        //[Embed(source="/../assets/textures/numbers.fnt", mimeType="application/octet-stream")]
        //public static const numbers_xml:Class;

        //[Embed(source="/../assets/textures/big_numbers.fnt", mimeType="application/octet-stream")]
        //public static const big_numbers_xml:Class;

        //[Embed(source="/../assets/textures/crystal_shop_sm.fnt", mimeType="application/octet-stream")]
        //public static const crystal_shop_sm_xml:Class;

        //[Embed(source="/../assets/textures/green_font.fnt", mimeType="application/octet-stream")]
        //public static const green_font_xml:Class;

        /**
         * Particle
         */
        // png in spritesheet
        [Embed(source="/../assets/media/blue_star_1.pex", mimeType="application/octet-stream")]
        public static const blue_star_1_xml:Class;


        [Embed(source="/../assets/media/hint_accurate_blow/texture.png")]
        public static const hint_accurate_blow:Class;

        [Embed(source="/../assets/media/hint_accurate_blow/glow_water.pex", mimeType="application/octet-stream")]
        public static const glow_water_xml:Class;
        
        [Embed(source="/../assets/media/hint_accurate_blow/glow_nature.pex", mimeType="application/octet-stream")]
        public static const glow_nature_xml:Class;

        [Embed(source="/../assets/media/hint_accurate_blow/glow_dark.pex", mimeType="application/octet-stream")]
        public static const glow_dark_xml:Class;

        [Embed(source="/../assets/media/hint_accurate_blow/glow_fire.pex", mimeType="application/octet-stream")]
        public static const glow_fire_xml:Class;

        [Embed(source="/../assets/media/hint_accurate_blow/glow_light.pex", mimeType="application/octet-stream")]
        public static const glow_light_xml:Class;

        [Embed(source="/../assets/media/damage.pex", mimeType="application/octet-stream")]
        public static const damage_xml:Class;
        [Embed(source="/../assets/media/damage.png")]
        public static const damage:Class;


        public function EmbeddedAssets()
		{
		}
	}
}