package ru.elementarium.webclient.view.help {
import feathers.controls.Button;
import feathers.controls.ImageLoader;
import feathers.controls.List;
import feathers.controls.renderers.DefaultListItemRenderer;
import feathers.controls.renderers.IListItemRenderer;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;

import starling.display.Sprite;

public class HelpView extends ModuleView
    {
        public var _close2Button:Button;
        public var _actionButton:Button;
        public var _categoryUpButton:Button;
        public var _categoryDownButton:Button;

        public var _imageLoader:ImageLoader;
        public var _menuList:List;

        public function HelpView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.help, false, this) as Sprite;
            
            _titleLabel.text = "ПОМОЩЬ";
            _close2Button.label = "ЗАКРЫТЬ";
            _actionButton.label = "ДАЛЕЕ";
            
            
            _menuList.customItemRendererStyleName = "storeListButton";
            _menuList.itemRendererFactory = function():IListItemRenderer
            {
                var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
                renderer.labelField = "title";
                return renderer;
            };
            var layoutV:VerticalLayout = new VerticalLayout();
            layoutV.gap = 0;
            _menuList.layout = layoutV;

            validateAndAttach();
        }
}
}
