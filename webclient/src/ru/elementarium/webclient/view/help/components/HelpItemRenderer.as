package ru.elementarium.webclient.view.help.components {
import com.junkbyte.console.Cc;

import feathers.controls.Button;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import starling.events.Event;

public class HelpItemRenderer extends LayoutGroupListItemRenderer
    {
        private var selectQuestButton:Button;

        private var initialized:Boolean = false;
        private var _select:TapToSelect;
    
        public function HelpItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (!selectQuestButton) {
                selectQuestButton = new Button();
                selectQuestButton.styleName = "common2";
                selectQuestButton.width = 210;
                selectQuestButton.addEventListener(Event.TRIGGERED, triggeredHandler);
                addChild(selectQuestButton);
                if (data) {
                    drawData();
                }
            }
        }

        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();
            }
        }

        private function drawData():void
        {
            selectQuestButton.label = data.title;
        }
    
        private function triggeredHandler(event:Event):void {
            dispatchEventWith("openQuestEvent", true, data);
        }
}
}
