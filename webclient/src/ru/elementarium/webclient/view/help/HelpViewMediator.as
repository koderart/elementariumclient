package ru.elementarium.webclient.view.help
{
import com.junkbyte.console.Cc;
import feathers.data.ListCollection;
import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.utils.XMLLoader;

import starling.events.Event;

public class HelpViewMediator extends Mediator
	{
		[Inject]
		public var view:HelpView;
        [Inject]
        public var mainModel:MainModel;
        private var pages:Array;
        private var menuListCollection:ListCollection;
        private var currentPage:int = 0;

		override public function onRegister():void
		{
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._actionButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._categoryUpButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._categoryDownButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            view._menuList.addEventListener(Event.CHANGE, openHelpEventHandler);
            
            XMLLoader.instance.sendRequest(ConnectionService.BASE_URL_ASSETS+"help.xml", helpLoaderHandler);
        }

        private function helpLoaderHandler(data:XML):void {
            var pagesData:XMLList = new XMLList(data.page);
            
            pages = [];
            pagesData.(pages.push({title:@title, src:@src}));
            menuListCollection = new ListCollection(pages);
            view._menuList.dataProvider = menuListCollection;
            if (pages.length > 0) {
                view._menuList.selectedIndex = 0;
                openPage();
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._closeButton:
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                    break;
                case view._close2Button:
                    view.removeFromParent(true);
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                break;
                case view._categoryUpButton:
                    view._menuList.verticalScrollPosition -= 48;
                    if (view._menuList.verticalScrollPosition < 0) {
                        view._menuList.verticalScrollPosition = 0;
                    }
                    break;
                case view._categoryDownButton:
                    view._menuList.verticalScrollPosition += 48;
                    if (view._menuList.verticalScrollPosition > view._menuList.maxVerticalScrollPosition) {
                        view._menuList.verticalScrollPosition = view._menuList.maxVerticalScrollPosition;
                    }
                    break;
                case view._actionButton:
                    currentPage++;
                    if (currentPage >= pages.length) {
                        currentPage = pages.length - 1;
                    }
                    view._menuList.selectedIndex = currentPage;
                    openPage();
                break;
            }
        }

        private function openHelpEventHandler(event:Event):void {
            currentPage = menuListCollection.getItemIndex(view._menuList.selectedItem);
            openPage();
        }

        private function openPage():void {
            view._imageLoader.source = ConnectionService.BASE_URL_ASSETS + "help/" + pages[currentPage].src;
        }
}
}