package ru.elementarium.webclient.view.game
{

import com.junkbyte.console.Cc;

import feathers.controls.TabBar;
import feathers.events.FeathersEventType;
import flash.geom.Point;
import flash.net.SharedObject;

import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.Match3Model;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.utils.XMLLoader;
import ru.elementarium.webclient.view.achieve.AchieveView;
import ru.elementarium.webclient.view.arena.ArenaView;
import ru.elementarium.webclient.view.game.componets.*;
import ru.elementarium.webclient.view.game.componets.Location;
import ru.elementarium.webclient.view.help.HelpView;
import ru.elementarium.webclient.view.inventory.InventoryView;
import ru.elementarium.webclient.view.main.AppMediator;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.elementarium.webclient.view.news.NewsView;
import ru.elementarium.webclient.view.payment.PaymentView;
import ru.elementarium.webclient.view.quest.QuestView;
import ru.elementarium.webclient.view.inventory.RepairView;
import ru.elementarium.webclient.view.store.StoreView;
import ru.muvs.admin.main.vo.LocationVO;
import ru.muvs.admin.main.vo.MobVO;
import ru.muvs.admin.main.vo.NpcVO;
import ru.muvs.admin.main.vo.StoreVO;
import ru.muvs.elementarium.thrift.battle.DataBattleQueue;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.player.DataPlayerFraction;
import ru.muvs.elementarium.thrift.player.DataPlayerSex;
import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class GameViewMediator extends Mediator
	{
		[Inject]
		public var view:GameView;
        [Inject]
        public var mainModel:MainModel;
        [Inject]
        public var gameModel:Match3Model;

		override public function onRegister():void
		{
            addContextListener(UserEvent.MONEY_CHANGE_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(UserEvent.MONEY_LIST_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(UserEvent.SKILL_CHANGE_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(UserEvent.SKILL_LIST_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST, gameEventTypeHandler);
            addContextListener(QuestEvent.QUEST_LIST_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(InventoryEvent.ARTIKUL_REPAIR, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_START, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_STOP, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_INFO, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_POSITION, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_STAND, gameEventTypeHandler);
            addContextListener(ChallengeEvent.CHALLENGE_LIST_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(AchieveEvent.ACHIEVE_LIST_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(GameEvent.RESET_MAIN_MENU, gameEventTypeHandler);

            dispatch(new UserEvent(UserEvent.MONEY_LIST_SERVER_COMMAND));

            view.testBotButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.testStoreButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.paymentButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.moneyPanel.addEventListener(TouchEvent.TOUCH, onTouch);
            view.fullscreenButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.mainButtonTabBar.addEventListener( Event.CHANGE, mainMenu_changeHandler );

            view.addEventListener(Location.INFO_BATTLE_EVENT, infoDynamicBotEventHandler);
            view.addEventListener(Location.START_NPC_EVENT, startNpcEventHandler);
            view.addEventListener(Location.START_STORE_EVENT, startStoreEventHandler);
            view.addEventListener(Location.TELEPORT_LOCATION_EVENT, teleportLocationEventHandler);
            view.addEventListener(Location.PICKUP_LOCATION_EVENT, teleportLocationEventHandler);
            view.addEventListener(Location.MEMBER_RUN, memberRunEventHandler);

            view.userLeftNickLabel.text = mainModel.player.name;

            switch (mainModel.player.fraction) {
                case DataPlayerFraction.FIRST:
                    view.userLeftFraction.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_1");
                    break;
                case DataPlayerFraction.SECOND:
                    view.userLeftFraction.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_2");
                    break;
            }

            switch (mainModel.player.sex) {
                case DataPlayerSex.MALE:
                    switch (mainModel.player.element) {
                        case DataPlayerElement.DARK:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_dark");
                        break;
                        case DataPlayerElement.FIRE:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_fire");
                        break;
                        case DataPlayerElement.LIGHT:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_light");
                        break;
                        case DataPlayerElement.NATURE:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_nature");
                        break;
                        case DataPlayerElement.WATER:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_water");
                        break;
                    }
                    break;
                case DataPlayerSex.FEMALE:
                    switch (mainModel.player.element) {
                        case DataPlayerElement.DARK:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_dark");
                            break;
                        case DataPlayerElement.FIRE:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_fire");
                            break;
                        case DataPlayerElement.LIGHT:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_light");
                            break;
                        case DataPlayerElement.NATURE:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_nature");
                            break;
                        case DataPlayerElement.WATER:
                            view.userLeftAvatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_water");
                            break;
                    }
                    break;
            }

            BUILD::test {
                for each (var mob:MobVO in MainModel.mobData) {
                    var bb:MobButton = new MobButton();
                    bb.label = mob.title.ru;
                    bb.mobId = mob.mobId;
                    bb.addEventListener(Event.TRIGGERED, triggeredMobHandler);
                    view.mobPanel.addChild(bb);
                }

                for each (var store:StoreVO in MainModel.storeData) {
                    bb = new MobButton();
                    bb.label = store.title.ru;
                    bb.mobId = store.storeId;
                    bb.addEventListener(Event.TRIGGERED, triggeredStoreHandler);
                    view.storePanel.addChild(bb);
                }
            }
            
            if (mainModel.currentLocation) {
                view.createLocation(mainModel);
            }

            XMLLoader.instance.sendRequest(ConnectionService.BASE_URL_ASSETS+"news.xml", newsLoaderHandler);
        }
    
        private function newsLoaderHandler(data:XML):void {
            var pagesData:XMLList = new XMLList(data.page);
            mainModel.newsXML = pagesData;
            
            var len:int = pagesData.length();
            if (len > 0) {
                var newsSo:SharedObject = SharedObject.getLocal("news");
                if (newsSo.data.lastNewsId) {
                    for(var i:int=0; i < len; i++) {
                        if (int(pagesData[i].@id) == newsSo.data.lastNewsId) { break; } 
                    }
                    var count:int = len - (i+1);
                    view.newsCountLabel.text = count>0?count.toString():"";
                } else {
                    view.newsCountLabel.text = len.toString();
                }
            }
        }

        private function triggeredMobHandler(event:Event):void {
            var eBot:BattleEvent = new BattleEvent(BattleEvent.BATTLE_JOIN_SERVER_COMMAND);
            eBot.id = MobButton(event.target).mobId;
            dispatch(eBot);
        }

        private function triggeredStoreHandler(event:Event):void {
            mainModel.currentStore = MobButton(event.target).mobId;
            var store:StoreView = new StoreView();
            view.addChild(store);
            
        }

        private function gameEventTypeHandler(event:Event):void {
            switch (event.type) {
                case BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND:
                    if (!mainModel.openArena && gameModel.queueType != DataBattleQueue.NONE) {
                        var arena:ArenaView = new ArenaView();
                        view.addChild(arena);
                        mainModel.openArena = true;
                    }
                break;

                case UserEvent.MONEY_CHANGE_CLIENT_COMMAND:
                    view.moneyGoldLabel.text = mainModel.moneyGold.toString();
                    view.moneyCrystalLabel.text = mainModel.moneyCrystal.toString();
                break;
                case UserEvent.MONEY_LIST_CLIENT_COMMAND:
                    view.moneyGoldLabel.text = mainModel.moneyGold.toString();
                    view.moneyCrystalLabel.text = mainModel.moneyCrystal.toString();
                break;
                case UserEvent.SKILL_LIST_CLIENT_COMMAND:
                    updateUserPanel();
                    /*
                    var my_so:SharedObject = SharedObject.getLocal("comics");
                    if (mainModel.playerLevel == 1 && mainModel.playerExpCurrent == 0 && !my_so.data.comics) {
                        my_so.data.comics = true;
                        my_so.flush();
                        var comics:Comics = new Comics();
                        comics.addEventListener(Event.COMPLETE, comics_completeHandler);
                        view.addChild(comics);
                    }
                    */
                    break;
                case UserEvent.SKILL_CHANGE_CLIENT_COMMAND:
                    updateUserPanel();
                break;
                case QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST:
                    if (!mainModel.openNpc && mainModel.questList.length < 1) {
                        ELAlert.show(NpcVO(MainModel.locationMemberData[mainModel.currentNpc.npc.locationMemberId].npc).title.ru+" не хочет с вами разговаривать", "");
                        return;
                    }
                    if (!mainModel.openNpc) {
                        var npc:QuestView = new QuestView();
                        view.addChild(npc);
                        mainModel.openNpc = true;
                    }    
                break;
                case QuestEvent.QUEST_LIST_CLIENT_COMMAND:
                    if (!mainModel.openQuest && mainModel.questList.length < 1) {
                        ELAlert.show("У вас нет ни одного задания. Нажмите персонажа на карте, чтобы получить его.", "ЖУРНАЛ ЗАДАНИЙ");
                        view.mainButtonTabBar.selectedItem = null;
                        return;
                    }
                    if (!mainModel.openQuest) {
                        mainModel.currentNpc = null;
                        var quest:QuestView = new QuestView();
                        view.addChild(quest);
                        mainModel.openQuest = true;
                    }
                    break;
                case InventoryEvent.ARTIKUL_REPAIR:
                    var repair:RepairView = new RepairView();
                    mainModel.durabilityArtikul = InventoryEvent(event).artikul;
                    view.addChild(repair);
                break;
                case LocationEvent.LOCATION_LOAD_CLIENT_COMMAND:
                    view.createLocation(mainModel);
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE:
                    if (view.location)  {
                        view.location.updateClient(mainModel.locationMember);
                    }
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_STAND:
                    if (view.location)  {
                        view.location.userStand(mainModel.locationMemberStand);
                    }
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_START:
                case LocationEvent.LOCATION_ACTION_CLIENT_BATTLE_STOP:
                    if (view.location)  {
                        for each (var locationMember:DataLocationMember in mainModel.locationMembersBattle) {
                            view.location.updateClient(locationMember);
                        }
                    }
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_INFO:
                    var memberInfo:LocationMemberInfo = new LocationMemberInfo(mainModel.locationBattle, mainModel.locationMember);
                    memberInfo.addEventListener(Location.GOTO_BATTLE_EVENT, gotoMemberEventHandler);
                    view.addChild(memberInfo);
                break;
                case LocationEvent.LOCATION_ACTION_POSITION:
                    if (view.location)  {
                        view.location.updateClientPosition(mainModel.locationMembersPosition);
                    }
                    break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD:
                    if (view.location)  {
                        view.location.addMember(mainModel.locationMember);
                    }
                    break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE:
                    if (view.location)  {
                        view.location.removeMember(mainModel.locationMember);
                    }
                    break;
                case AchieveEvent.ACHIEVE_LIST_CLIENT_COMMAND:
                    var achieve:AchieveView = new AchieveView();
                    view.addChild(achieve);
                    break;
                case GameEvent.RESET_MAIN_MENU:
                        view.mainButtonTabBar.selectedItem = null;
                    break;
            }
        }

        private function updateUserPanel():void
        {
            view.userLeftHpPercentLabel.text = int(mainModel.playerHpCurrent / mainModel.playerHpMax * 100) + "%";
            view.userLeftHPBar.width = view.userLeftHPMaxWidth * mainModel.playerHpCurrent / mainModel.playerHpMax;

            view.userLeftExpPercentLabel.text = int(mainModel.playerExpCurrent / mainModel.playerExpMax * 100) + "%";
            var barWidth:int = view.userLeftExpMaxWidth * mainModel.playerExpCurrent / mainModel.playerExpMax;
            view.userLeftExpBar.mask = new Quad(barWidth+1,view.userLeftExpBar.height);

            view.userLeftLevelLabel.text = mainModel.playerLevel.toString();
        }

        private function onTouch(event:TouchEvent):void
        {
            var object:DisplayObject = event.currentTarget as DisplayObject;
            var touch:Touch = event.getTouch(object);
            if (touch && touch.phase == TouchPhase.ENDED) {
                switch (object) {
                    case view.moneyPanel:
                        var payment:PaymentView = new PaymentView();
                        view.addChild(payment);
                        break;
                }
            }
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view.testBotButton:
                    view.mobPanel.visible = !view.mobPanel.visible;
                break;
                case view.testStoreButton:
                    view.storePanel.visible = !view.storePanel.visible;
                break;
                case view.paymentButton:
                break;
                case view.fullscreenButton:
                    view.optionPanel.visible = false;
                    view.mainButtonTabBar.selectedItem = null;
                break;
            }
        }

        private function mainMenu_changeHandler( event:Event ):void
        {
            var tabs:TabBar = TabBar( event.currentTarget );

            view.optionPanel.visible = false;
            
            switch (tabs.selectedIndex) {
                case 0:
                    var arena:ArenaView = new ArenaView();
                    view.addChild(arena);
                    mainModel.openArena = true;
                    break;
                case 1:
                    var inventory:InventoryView = new InventoryView();
                    view.addChild(inventory);
                    break;
                case 2:
                    mainModel.currentStore = 3;
                    var store:StoreView = new StoreView();
                    view.addChild(store);
                    break;
                case 3:
                    dispatch(new QuestEvent(QuestEvent.QUEST_LIST_SERVER_COMMAND));
                    break;
                case 4:
                    dispatch(new AchieveEvent(AchieveEvent.ACHIEVE_LIST_SERVER_COMMAND));
                    break;
                case 5:
                    var help:HelpView = new HelpView();
                    view.addChild(help);
                    break;
                case 6:
                    view.newsCountLabel.text = "";
                    var news:NewsView = new NewsView();
                    view.addChild(news);
                    break;
                case 7:
                    view.optionPanel.visible = true;
                    break;
            }
        }

        private function startDynamicBotEventHandler(event:Event):void
        {
            var eLocation:LocationEvent = new LocationEvent(LocationEvent.LOCATION_ACTION_SERVER_BATTLE_JOIN);
            eLocation.member = event.data as DataLocationMember;
            dispatch(eLocation);
        }

        private function gotoMemberEventHandler(event:Event):void
        {
            if (view.location) {
                view.location.gotoMember(event.data as DataLocationMember);
            }
        }

        private function infoDynamicBotEventHandler(event:Event):void {
            var eLocation:LocationEvent = new LocationEvent(LocationEvent.LOCATION_ACTION_SERVER_BATTLE_INFO);
            eLocation.member = event.data as DataLocationMember;
            dispatch(eLocation);
        }

        private function startNpcEventHandler(event:Event):void {
            var eNpc:QuestEvent = new QuestEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST);
            eNpc.member = event.data as DataLocationMember;
            dispatch(eNpc);
        }

        private function startStoreEventHandler(event:Event):void {
            mainModel.currentStore = event.data as int;
            var store:StoreView = new StoreView();
            view.addChild(store);
        }

        private function memberRunEventHandler(event:Event):void {
            var eLocation:LocationEvent = new LocationEvent(LocationEvent.LOCATION_ACTION_SERVER_MEMBER_RUN);
            eLocation.runPoint = event.data as Point;
            dispatch(eLocation);
        }

        private function teleportLocationEventHandler(event:Event):void
        {
            switch(event.type) {
                case Location.TELEPORT_LOCATION_EVENT:
                    var eLocation:LocationEvent = new LocationEvent(LocationEvent.LOCATION_ACTION_SERVER_TELEPORT_ENTER);
                    eLocation.member = event.data as DataLocationMember;
                    dispatch(eLocation);
                    break;
                case Location.PICKUP_LOCATION_EVENT:
                    eLocation = new LocationEvent(LocationEvent.LOCATION_ACTION_SERVER_PICKUP_ENTER);
                    eLocation.member = event.data as DataLocationMember;
                    dispatch(eLocation);
                    break;
            }
        }

        private function comics_completeHandler(event:Event):void {
            var help:HelpView = new HelpView();
            view.addChild(help);
        }
}
}