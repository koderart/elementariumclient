package ru.elementarium.webclient.view.game {
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;
import feathers.controls.Panel;
import feathers.controls.ScrollContainer;
import feathers.controls.TabBar;
import feathers.data.ListCollection;
import feathers.events.FeathersEventType;
import feathers.layout.VerticalLayout;

import flash.display.Stage;
import flash.display.StageDisplayState;
import flash.events.MouseEvent;
import flash.geom.Point;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.game.componets.ExpHint;
import ru.elementarium.webclient.view.game.componets.Location;
import ru.muvs.admin.main.vo.LocationVO;

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.ButtonState;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.events.ResizeEvent;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;
import starling.text.TextFormat;
import starling.utils.Align;

public class GameView extends Sprite
    {
        private var _sprite:Sprite;

        public var userLeftNickLabel:TextField;
        public var userLeftLevelLabel:TextField;
        
        public var userLeftHpPercentLabel:TextField;
        public var userLeftHPBar:Image;
        public var userLeftHPBarBg:Image;
        public var userLeftHPMaxWidth:int;
        
        public var userLeftExpBar:Image;
        public var userLeftExpBarBg:Image;
        public var userLeftExpMaxWidth:int;
        public var userLeftExpPercentLabel:TextField;
        public var hintLabel:TextField;
    
        public var userLeftAvatar:Image;
        public var userLeftFraction:Image;

        public var testBotButton:starling.display.Button;
        public var testStoreButton:starling.display.Button;
        public var locationScroll:ScrollContainer;

        public var moneyGoldLabel:TextField;
        public var moneyCrystalLabel:TextField;

        public var hintCoords:ExpHint;
        
        public var paymentButton:starling.display.Button;
        public var newsCountLabel:TextField;
        public var loadingLabel:TextField;

        public var moneyPanel:Sprite;
    
        public var optionPanel:Sprite;
        public var fullscreenButton:starling.display.Button;
    
        public var mobPanel:Panel;
        public var storePanel:Panel;
        public var mainButtonTabBar:TabBar;
        public var bgLine1:Image;
        public var bgLine2:Image;
        public var userLeft:Sprite;
        public var hintLayoutGroup:LayoutGroup;

        private var _mainModel:MainModel;

        public var location:Location;
        private static const HELPER_POINT:Point = new Point();

        public function GameView(mainModel:MainModel)
        {
            super();
            _mainModel = mainModel;
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.game, false) as Sprite;

            bgLine1 = _sprite.getChildByName("bgLine1") as Image;
            bgLine2 = _sprite.getChildByName("bgLine2") as Image;

            testBotButton = _sprite.getChildByName("botButton") as starling.display.Button;
            testBotButton.visible = false;
            testStoreButton = _sprite.getChildByName("testStoreButton") as starling.display.Button;
            testStoreButton.visible = false;

            locationScroll = new ScrollContainer();
            locationScroll.styleName = "locationScroll";

            this.addChild(locationScroll);

            this.addChild(_sprite);

            userLeft = AppTheme.uiBuilder.create(ParsedLayouts.game_user_panel_left, false) as Sprite;
            
            var userPanelPosition:Sprite = _sprite.getChildByName("userPanelPosition") as Sprite;
            userPanelPosition.addChild(userLeft);
            
            userLeftNickLabel = userLeft.getChildByName("nameLabel") as TextField;
            userLeftNickLabel.text = "";
            userLeftLevelLabel = userLeft.getChildByName("levelLabel") as TextField;
            userLeftLevelLabel.text = "";
            
            userLeftHpPercentLabel = userLeft.getChildByName("hpPercentLabel") as TextField;
            userLeftHpPercentLabel.touchable = false;
            userLeftHPBar = userLeft.getChildByName("hpBar") as Image;
            userLeftHPBarBg = userLeft.getChildByName("hpBarBg") as Image;
            userLeftHPMaxWidth = userLeftHPBar.width;

            hintLayoutGroup = userLeft.getChildByName("hintLayoutGroup") as LayoutGroup;
            hintLayoutGroup.visible = false;
            hintLabel = hintLayoutGroup.getChildByName("hintLabel") as TextField;
            
            userLeftExpPercentLabel = userLeft.getChildByName("expPercentLabel") as TextField;
            userLeftExpPercentLabel.touchable = false;
            userLeftExpBar = userLeft.getChildByName("expBar") as Image;
            userLeftExpBar.touchable = false;
            userLeftExpBarBg = userLeft.getChildByName("expBarBg") as Image;
            userLeftExpMaxWidth = userLeftExpBar.width;
            
            userLeftAvatar = userLeft.getChildByName("avatar") as Image;
            userLeftFraction = userLeft.getChildByName("fraction") as Image;

            loadingLabel = _sprite.getChildByName("loadingLabel") as TextField;
            loadingLabel.touchable = false;

            mainButtonTabBar = new TabBar();
            mainButtonTabBar.customTabStyleName = "mainMenuTabButton";
            mainButtonTabBar.gap = 3;
            mainButtonTabBar.dataProvider = new ListCollection(
                    [
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/battle")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/battle_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/inventory")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/inventory_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/store")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/store_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/quest")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/quest_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/achievment")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/achievment_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/help")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/help_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/news")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/news_selected"))},
                    ]);
            if (!MainModel.isMobile) {
                mainButtonTabBar.dataProvider.addItem({ label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/options")),
                    defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("main/menu/options_selected"))});
            }
            mainButtonTabBar.validate();
            mainButtonTabBar.selectedItem = null;
            this.addChild( mainButtonTabBar );

            newsCountLabel = new TextField(33, 26, "", new TextFormat("Arial", 13, 0x2EB01F, Align.CENTER, Align.TOP));
            newsCountLabel.touchable = false;
            newsCountLabel.batchable = true;
            this.addChild(newsCountLabel);

            optionPanel = AppTheme.uiBuilder.create(ParsedLayouts.game_option_panel, false) as Sprite;
            fullscreenButton = optionPanel.getChildByName("fullscreenButton") as starling.display.Button;
            optionPanel.visible = false;
            this.addChild(optionPanel);

            moneyPanel = AppTheme.uiBuilder.create(ParsedLayouts.game_money_panel, false) as Sprite;
            this.addChild(moneyPanel);

            paymentButton = moneyPanel.getChildByName("paymentButton") as starling.display.Button;

            moneyGoldLabel = moneyPanel.getChildByName("moneyGoldLabel") as TextField;
            moneyGoldLabel.text = "";
            moneyCrystalLabel = moneyPanel.getChildByName("moneyCrystalLabel") as TextField;
            moneyCrystalLabel.text = "";

            BUILD::test {
                testBotButton.visible = true;
                
                mobPanel = new Panel();
                mobPanel.title = "Тест ботов";
                mobPanel.layout = new VerticalLayout();
                mobPanel.maxHeight = 800;
                mobPanel.visible = false;
                addChild(mobPanel);

                testStoreButton.visible = true;
                storePanel = new Panel();
                storePanel.title = "Тест магазинов";
                storePanel.layout = new VerticalLayout();
                storePanel.visible = false;
                addChild(storePanel);

                hintCoords = new ExpHint();
                addChild(hintCoords);
            }
            
            Starling.current.nativeStage.addEventListener(MouseEvent.CLICK, handleStageClick);

            addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            userLeftHPBarBg.addEventListener(TouchEvent.TOUCH, onTouch);
            userLeftExpBarBg.addEventListener(TouchEvent.TOUCH, onTouch);

        }

        protected function sprite_addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
            setPosition();
        }

        protected function sprite_removeFromStageHandler(event:Event):void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.removeEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
            if (stage.hasEventListener(TouchEvent.TOUCH, onTouchStage)) {
                stage.removeEventListener(TouchEvent.TOUCH, onTouchStage);
            }
        }

        protected function stage_resizeHandler(event:Event):void
        {
            setPosition();
        }

        protected function setPosition():void
        {
            moneyPanel.x = int(Starling.current.stage.stageWidth - moneyPanel.width) - 1;
            
            loadingLabel.x = int(Starling.current.stage.stageWidth/2);
            loadingLabel.y = int(Starling.current.stage.stageHeight/2);

            mainButtonTabBar.x = int((Starling.current.stage.stageWidth - mainButtonTabBar.width) / 2);

            newsCountLabel.x = mainButtonTabBar.x + 411;
            newsCountLabel.y = 34;

            optionPanel.x = mainButtonTabBar.x + 460;
            optionPanel.y = 73;

            bgLine1.width = mainButtonTabBar.x;
            var w:int = mainButtonTabBar.x + mainButtonTabBar.width;
            bgLine2.x = w;
            bgLine2.width = Starling.current.stage.stageWidth - w;

            if (hintCoords) {
                hintCoords.y = int(Starling.current.stage.stageHeight - hintCoords.height);
            }
            if (mobPanel) {
                mobPanel.x = int(Starling.current.stage.stageWidth - 215);
            }
            if (storePanel) {
                storePanel.x = int(Starling.current.stage.stageWidth - 215);
            }
        }
    
        private function handleStageClick(e:MouseEvent):void
        {
            if (fullscreenButton.stage != null) {
                if(fullscreenButton.state != ButtonState.UP)
                {
                    var flashStage:Stage = Starling.current.nativeStage as Stage;
                    var isFullScreen:Boolean = (flashStage.displayState == StageDisplayState.FULL_SCREEN) || (flashStage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE);
                    if (isFullScreen) {
                        flashStage.displayState = StageDisplayState.NORMAL;
                    } else {
                        flashStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
                    }
                }
            }
        }

        private function onTouch(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(event.target as DisplayObject);
            if (touch && touch.phase == TouchPhase.ENDED) {
                switch (event.target) {
                    case userLeftHPBarBg:
                        hintLabel.text = _mainModel.playerHpCurrent +" / "+ _mainModel.playerHpMax;
                        break;
                    case userLeftExpBarBg:
                        hintLabel.text = _mainModel.playerExpCurrent +" / "+ _mainModel.playerExpMax;
                        break;
                }

                if (!hintLayoutGroup.visible) {
                    stage.addEventListener(TouchEvent.TOUCH, onTouchStage);
                }
                hintLayoutGroup.visible = true;
            }
        }

        private function onTouchStage(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(event.target as DisplayObject);
            if (touch && touch.phase == TouchPhase.BEGAN) {
                stage.removeEventListener(TouchEvent.TOUCH, onTouchStage);
                hintLayoutGroup.visible = false;
            }
        }

        public function createLocation(mainModel:MainModel):void {
            if (location)  {
                location.removeEventListener(FeathersEventType.RESIZE, resizeLocationEventHandler);
                location.removeEventListener(TouchEvent.TOUCH, locationHandleTouch);
                location.removeEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
                location.removeFromParent(true);
            }

            location = new Location(mainModel);
            location.addEventListener(Event.COMPLETE, completeLocationEventHandler);
            location.addEventListener(FeathersEventType.RESIZE, resizeLocationEventHandler);
            location.addEventListener(TouchEvent.TOUCH, locationHandleTouch);
            location.addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
            locationScroll.alpha = 0;
            locationScroll.backgroundSkin = new Quad(100, 100, location.getBgColor());
            locationScroll.addChild(location);
            loadingLabel.visible = true;
        }

        private function completeLocationEventHandler(event:Event):void
        {
            event.target.removeEventListener(Event.COMPLETE, completeLocationEventHandler);

            var tween:Tween = new Tween(locationScroll, 0.3, Transitions.LINEAR);
            tween.animate('alpha', 1);
            Starling.current.juggler.add(tween);

            loadingLabel.visible = false;
        }

        private function resizeLocationEventHandler(event:Event):void
        {
            if (location.width < location.stage.stageWidth) {
                location.x = int((location.stage.stageWidth - location.width) / 2);
            }

            if (location.height < location.stage.stageHeight) {
                location.y = int((location.stage.stageHeight - location.height) / 2);
            }

            locationScroll.viewPort.width = location.width;
            locationScroll.viewPort.height = location.height;
            locationScroll.validate();
        }

        private function enterFrameHandler(event:EnterFrameEvent):void
        {
            var posX:int = location.user.x - int(location.stage.stageWidth / 2);
            if (posX < 0) { posX = 0;}
            if (posX > locationScroll.maxHorizontalScrollPosition) { posX = locationScroll.maxHorizontalScrollPosition;}

            var posY:int = location.user.y - int(location.stage.stageHeight / 2);
            if (posY < 0) { posY = 0;}
            if (posY > locationScroll.maxVerticalScrollPosition) { posY = locationScroll.maxVerticalScrollPosition;}

            locationScroll.horizontalScrollPosition = posX;
            locationScroll.verticalScrollPosition = posY;
        }

        private function locationHandleTouch(e: TouchEvent):void {
            var touch: Touch = e.getTouch(e.currentTarget as DisplayObject);
            if (hintCoords && location && touch && touch.phase == TouchPhase.ENDED) {
                HELPER_POINT.x = touch.globalX;
                HELPER_POINT.y = touch.globalY;
                location.globalToLocal(HELPER_POINT, HELPER_POINT);
                hintCoords.text.text = "["+LocationVO(MainModel.locationData[location.locationId]).locationId+"] " + HELPER_POINT.x + "/"+ HELPER_POINT.y;
            }
        }

        override public function dispose():void
        {
            Starling.current.nativeStage.removeEventListener(MouseEvent.CLICK, handleStageClick);
            userLeftHPBarBg.removeEventListener(TouchEvent.TOUCH, onTouch);
            userLeftExpBarBg.removeEventListener(TouchEvent.TOUCH, onTouch);

            if (location)  {
                location.removeEventListener(FeathersEventType.RESIZE, resizeLocationEventHandler);
                location.removeEventListener(TouchEvent.TOUCH, locationHandleTouch);
                location.removeEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
                location.removeFromParent(true);
                location = null;
            }
            super.dispose();
        }

}
}
