package ru.elementarium.webclient.view.game.componets {
import be.dauntless.astar.basic2d.BasicTile;
import be.dauntless.astar.core.IAstarTile;

import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.events.FeathersEventType;

import flash.filesystem.File;
import flash.geom.Point;
import flash.geom.Rectangle;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.services.Downloader;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.enum.*;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.location.member.DataLocationMemberType;

import starling.AnimSprite;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.extensions.PDParticleSystem;
import starling.utils.AssetManager;

public class Location extends Sprite
{
    public static const GOTO_BATTLE_EVENT:String = "GOTO_BATTLE_EVENT";
    public static const INFO_BATTLE_EVENT:String = "INFO_BATTLE_EVENT";
    public static const START_NPC_EVENT:String = "START_NPC_EVENT";
    public static const START_STORE_EVENT:String = "START_STORE_EVENT";
    public static const TELEPORT_LOCATION_EVENT:String = "TELEPORT_LOCATION_EVENT";
    public static const PICKUP_LOCATION_EVENT:String = "PICKUP_LOCATION_EVENT";
    public static const MEMBER_RUN:String = "MEMBER_RUN";

    private var _locationAssets:AssetManager = new AssetManager();

    private var _bgImagesCounter:int = 0;
    
    private var _bgImagesLayout:Sprite;
    private var _downLayer:Sprite;
    private var _middleLayer:Sprite;
    private var _topLayer:Sprite;
    private var _topMostLayer:Sprite;
    
    private var _user:CharacterWalk;
    private var _greed:Greed;
    
    private var _mainModel:MainModel;
    private var _locationId:int;
    private var _locationMembers:Object;
    private var _staticData:LocationVO;
    
    private var _dynamicMabObjects:Vector.<IMember>;
    private var _dynamicPlayerObjects:Vector.<CharacterWalk>;
    private var _userPath:Vector.<IAstarTile>;

    public function Location(model:MainModel)
    {
        super();

        _mainModel = model;
        _locationId = _mainModel.currentLocation;
        _locationMembers = _mainModel.locationMembers;

        _bgImagesLayout = new Sprite();
        _downLayer = new Sprite();
        _middleLayer = new Sprite();
        _topLayer = new Sprite();
        _topMostLayer = new Sprite();

        _staticData = MainModel.locationData[_locationId];
        //this.mask = new Quad(_staticData.hexWidth * Hexagon.WIDTH, _staticData.hexHeight * Hexagon.HEIGHT);

        // BG IMAGES

        BUILD::mobile {
            var downloader:Downloader = new Downloader(".", ConnectionService.BASE_URL_HTTP);

            for each (var img:LocationImageVO in _staticData.locationImage) {
                var file:File = File.applicationStorageDirectory.resolvePath("."+img.image.url);
                if (!file.exists) {
                    downloader.addFileToList(img.image.url);
                }
            }
        }

        BUILD::test {
            addBgImagesLayout(ConnectionService.BASE_URL_HTTP);
        }
        BUILD::release {
            addBgImagesLayout(ConnectionService.BASE_URL_HTTP);
        }
        // GREED

        _greed = new Greed(_staticData.hexWidth,_staticData.hexHeight, _staticData.locationPassableHex, _bgImagesLayout);
        _greed.addEventListener(Greed.PATH_FOUND, pathFoundHandler);
        
        // NPC AND MOBS

        _dynamicMabObjects = new Vector.<IMember>();
        _dynamicPlayerObjects = new Vector.<CharacterWalk>();

        var memberDisplay:Sprite;
        var memberStatic:LocationMemberVO;
        
        for each (var member:DataLocationMember in _locationMembers) {
            switch (member.type) {
                case DataLocationMemberType.PLAYER:
                    if (member.player.id == _mainModel.player.id) {
                        _user = new CharacterWalk(member, _greed, _mainModel.player);
                        _user.addEventListener(CharacterWalk.NEXT_MOVE, nextMoveHandler);
                        _middleLayer.addChild(_user);
                        _dynamicPlayerObjects.push(_user);
                        if (member.enable && !member.player.battle) {
                            ModelManager.instance.enqueue(_user.getModel());
                        }
                    } else {
                        var user:CharacterWalk = new CharacterWalk(member, _greed, _mainModel.player);
                        _middleLayer.addChild(user);
                        _dynamicPlayerObjects.push(user);
                        if (member.enable && !member.player.battle) {
                            ModelManager.instance.enqueue(user.getModel());
                        }
                        user.addEventListener(TouchEvent.TOUCH, onMemberTouch);
                    }
                    continue;
                    break;
                case DataLocationMemberType.MOB:
                    memberStatic = MainModel.locationMemberData[member.mob.locationMemberId];
                    if (member.enable && !member.mob.battle) {
                        ModelManager.instance.enqueue(memberStatic.model);
                    }
                    var characterMob:MapMemberMob = new MapMemberMob(member, memberStatic);
                    characterMob.addEventListener(TouchEvent.TOUCH, onMemberTouch);
                    memberDisplay = characterMob;
                    break;
                case DataLocationMemberType.NPC:
                    memberStatic = MainModel.locationMemberData[member.npc.locationMemberId];
                    if (member.enable) {
                        ModelManager.instance.enqueue(memberStatic.model);
                    }
                    var characterNpc:MapMemberNpc = new MapMemberNpc(member, memberStatic);
                    characterNpc.addEventListener(TouchEvent.TOUCH, onMemberTouch);
                    memberDisplay = characterNpc;
                    break;
                case DataLocationMemberType.TELEPORT:
                    memberStatic = MainModel.locationMemberData[member.teleport.locationMemberId];
                    var teleport : MapTeleport = new MapTeleport(member, memberStatic);
                    teleport.addEventListener(TouchEvent.TOUCH, onMemberTouch);
                    memberDisplay = teleport;
                    break;
                case DataLocationMemberType.PICKUP:
                    memberStatic = MainModel.locationMemberData[member.pickup.locationMemberId];
                    teleport = new MapTeleport(member, memberStatic);
                    teleport.addEventListener(TouchEvent.TOUCH, onMemberTouch);
                    memberDisplay = teleport;
                    break;
                default:
                    continue;
            }

            _dynamicMabObjects.push(memberDisplay);
            getLayerByType(memberStatic.layer).addChild(memberDisplay);

            var hexPoint:Point = Hexagon.getGlobalPoint(member.position.x, member.position.y);
            var positionX:int = hexPoint.x;
            var positionY:int = hexPoint.y;

            memberDisplay.x = positionX;
            memberDisplay.y = positionY;

            var tile:BasicTile = _greed.map.getTileAt(new Point(member.position.x, member.position.y)) as BasicTile;
            tile.setCost(10);
        }

        ModelManager.instance.loadQueue();

        // DECOR

        BUILD::mobile {
            for each (var decor:LocationDecorationVO in _staticData.locationDecoration) {
                switch (decor.locationDecoration) {
                    case LocationDecorationEnum.IMAGE:
                        file = File.applicationStorageDirectory.resolvePath("."+decor.image.url);
                        if (!file.exists) {
                            downloader.addFileToList(decor.image.url);
                        }
                        break;
                }
            }
            downloader.addEventListener(Downloader.LOADED, downloaderLoadedHandler);
            downloader.load();
        }

        BUILD::test {
            addDecorImagesLayout(ConnectionService.BASE_URL_HTTP);
        }
        BUILD::release {
            addDecorImagesLayout(ConnectionService.BASE_URL_HTTP);
        }        

        addChild(_bgImagesLayout);
        addChild(_downLayer);
        addChild(_greed);
        addChild(_middleLayer);
        addChild(_topLayer);
        addChild(_topMostLayer);
        
        this.addEventListener(TouchEvent.TOUCH, onLocationTouch);
        Starling.juggler.delayCall(enterFrameHandler, 1);
    }

    private function downloaderLoadedHandler(event:Event):void
    {
        event.target.removeEventListener(Downloader.LOADED, downloaderLoadedHandler);
        addBgImagesLayout("app-storage:");
        addDecorImagesLayout("app-storage:");
    }
    
    private function addBgImagesLayout(url:String):void
    {
        for each (var img:LocationImageVO in _staticData.locationImage) {
            var imgLoader:ImageLoader = new ImageLoader();
            imgLoader.addEventListener(Event.COMPLETE, handleImageLoaded);
            imgLoader.addEventListener(FeathersEventType.RESIZE, handleImageResize);
            imgLoader.source = url + img.image.url;
            imgLoader.x = img.positionX;
            imgLoader.y = img.positionY;
            _bgImagesLayout.addChild(imgLoader);
        }
    }

    private function addDecorImagesLayout(url:String):void
    {
        for each (var decor:LocationDecorationVO in _staticData.locationDecoration) {
            switch (decor.locationDecoration) {
                case LocationDecorationEnum.IMAGE:
                    var image:ImageLoader = new ImageLoader();
                    image.source = url + decor.image.url;
                    image.x = decor.positionX;
                    image.y = decor.positionY;
                    getLayerByType(decor.layer).addChild(image);
                    break;
                case LocationDecorationEnum.ANIMATION:
                    _locationAssets.enqueue(ConnectionService.BASE_URL_ASSETS+"objects/"+decor.source+".png");
                    _locationAssets.enqueue(ConnectionService.BASE_URL_ASSETS+"objects/"+decor.source+".xml");
                    break;
                case LocationDecorationEnum.PARTICLE:
                    _locationAssets.enqueue(ConnectionService.BASE_URL_ASSETS+"objects/"+decor.source+".png");
                    _locationAssets.enqueue(ConnectionService.BASE_URL_ASSETS+"objects/"+decor.source+".pex");
                    break;
            }
        }
        _locationAssets.loadQueue(assetManager_onProgress2);
    }

    public function getBgColor():int
    {
        switch (_staticData.background) {
            case LocationBackgroundEnum.WHITE:
                    return 0xFFFFFF;
                break;
            case LocationBackgroundEnum.BLACK:
                    return 0x000000;
                break;
        }
        return 0xFFFFFF;
    }
    
    private function enterFrameHandler():void
    {
        _middleLayer.sortChildren(sortChildrenCompare);
        Starling.juggler.delayCall(enterFrameHandler, 0.5);
    }

    public function sortChildrenCompare(object1:DisplayObject, object2:DisplayObject):Number
    {
        var bounds1:Rectangle = object1.bounds;
        var bounds2:Rectangle = object2.bounds;
        //if (bounds1.contains(bounds2.x, bounds2.y) || bounds1.contains(bounds2.x + bounds2.width, bounds2.y + bounds2.height)) {
            return bounds1.y + bounds1.height - bounds2.y - bounds2.height;
            //return object1.y + object1.height + bounds1.y - object2.y - object2.height - bounds2.y;
        //} else {
        //    return 0;
        //}
    }

    private function handleImageLoaded(e:Event):void
    {
        e.target.removeEventListener(Event.COMPLETE, handleImageLoaded);
        _bgImagesCounter++;
        if (_bgImagesCounter >= _staticData.locationImage.length) {
            dispatchEventWith(Event.COMPLETE);
        }
    }

    private function handleImageResize(e:Event):void
    {
        e.target.removeEventListener(FeathersEventType.RESIZE, handleImageResize);
        if (_bgImagesCounter >= _staticData.locationImage.length) {
            dispatchEventWith(FeathersEventType.RESIZE);
        }
    }
    
    protected function assetManager_onProgress2(progress:Number):void
    {
        if(progress !== 1) { return; }

        for each (var decor:LocationDecorationVO in _staticData.locationDecoration) {
            switch (decor.locationDecoration) {
                case LocationDecorationEnum.ANIMATION:
                    var animation:AnimSprite = new AnimSprite(_locationAssets.getTextureAtlas(decor.source));
                    animation.addSequence("anim", "idle_SE_");
                    Starling.juggler.add(animation);
                    animation.x = decor.positionX;
                    animation.y = decor.positionY;
                    animation.touchable = false;
                    animation.play();
                    getLayerByType(decor.layer).addChild(animation);
                    break;
                case LocationDecorationEnum.PARTICLE:
                    var _particles: PDParticleSystem;
                    _particles = new PDParticleSystem(_locationAssets.getXml(decor.source), _locationAssets.getTexture(decor.source));
                    _particles.touchable = false;
                    getLayerByType(decor.layer).addChild(_particles);
                    Starling.juggler.add(_particles);
                    _particles.start();

                    _particles.emitterX = decor.positionX;
                    _particles.emitterY = decor.positionY;
                    break;
            }
        }
    }

    private function getLayerByType(type:String):Sprite
    {
        switch (type) {
            case LocationLayerEnum.DOWN:
                return _downLayer;
                break;
            case LocationLayerEnum.MIDDLE:
                return _middleLayer;
                break;
            case LocationLayerEnum.TOP:
                return _topLayer;
                break;
            case LocationLayerEnum.TOPMOST:
                return _topMostLayer;
                break;
        }
        return _middleLayer;
    }
    
    private function onLocationTouch(event:TouchEvent):void
    {
        var location:Sprite = event.currentTarget as Sprite;
        var touch:Touch = event.getTouch(location);
        if (touch && touch.phase == TouchPhase.ENDED) {
            var localPoint:Point = new Point(touch.globalX, touch.globalY);
            globalToLocal(localPoint, localPoint);
            
            var toX:int = localPoint.x;
            var toY:int = localPoint.y;

            var battleFlag:Boolean = false;
            for each (var mapMember:IMember in _dynamicMabObjects) {
                if (mapMember.member.type == DataLocationMemberType.MOB && mapMember.member.enable) {
                    var memberStatic:LocationMemberVO = MainModel.locationMemberData[mapMember.member.mob.locationMemberId];
                    if (mapMember.member.type == DataLocationMemberType.MOB && mapMember.member.enable
                            && toX == memberStatic.positionHexX && toY == memberStatic.positionHexY) {
                        battleFlag = true;
                    }
                }
            }
            _greed.newPathToHexCoord(_user.currentPosition, Hexagon.getPXY(toX, toY), battleFlag);
        }
    }
    
    private function onMemberTouch(event:TouchEvent):void
    {
        var memberObject:DisplayObject = event.currentTarget as DisplayObject;
        var touch:Touch = event.getTouch(memberObject);
        if (touch && touch.phase == TouchPhase.ENDED)
        {
            event.stopImmediatePropagation();
            var memberStatic:LocationMemberVO;
            switch (IMember(memberObject).member.type) {
                case DataLocationMemberType.MOB:
                    memberStatic = MainModel.locationMemberData[IMember(memberObject).member.mob.locationMemberId];
                    break;
                case DataLocationMemberType.NPC:
                    memberStatic = MainModel.locationMemberData[IMember(memberObject).member.npc.locationMemberId];
                    break;
                case DataLocationMemberType.TELEPORT:
                    memberStatic = MainModel.locationMemberData[IMember(memberObject).member.teleport.locationMemberId];
                    break;
                case DataLocationMemberType.PICKUP:
                    memberStatic = MainModel.locationMemberData[IMember(memberObject).member.pickup.locationMemberId];
                    break;
            }

            if (IMember(memberObject).member.type == DataLocationMemberType.MOB && IMember(memberObject).member.mob.battle) {
                dispatchEventWith(INFO_BATTLE_EVENT, true, IMember(memberObject).member);
            } else if (IMember(memberObject).member.type == DataLocationMemberType.PLAYER && IMember(memberObject).member.player.battle) {
                dispatchEventWith(INFO_BATTLE_EVENT, true, IMember(memberObject).member);
            } else {
                _greed.newPathToHexCoord(_user.currentPosition, new Point(IMember(memberObject).member.position.x, IMember(memberObject).member.position.y),
                        IMember(memberObject).member.type == DataLocationMemberType.MOB);
            }
        }
    }
    
    public function get user():CharacterWalk
    {
        return _user;
    }
    
    public function userStand(position:Point):void
    {
        _user.stopUserSelf();
        _userPath.length = 0;
        _greed.clearPath();
        _user.currentPosition = position;
    }

    public function updateClient(member:DataLocationMember):void
    {
        for each (var mapMember:IMember in _dynamicMabObjects) {
            if (member.id == mapMember.member.id) {
                mapMember.member = member;
            }
        }

        for each (var mapPlayer:CharacterWalk in _dynamicPlayerObjects) {
            if (member.id == mapPlayer.id) {
                mapPlayer.member = member;
            }
        }
    }

    public function addMember(member:DataLocationMember):void
    {
        var user:CharacterWalk = new CharacterWalk(member, _greed, _mainModel.player);
        _middleLayer.addChild(user);
        _dynamicPlayerObjects.push(user);
        if (member.enable && !member.player.battle) {
            ModelManager.instance.enqueue(user.getModel());
            ModelManager.instance.loadQueue();
        }
    }

    public function gotoMember(member:DataLocationMember):void
    {
        _greed.newPathToHexCoord(_user.currentPosition, new Point(member.position.x, member.position.y), true);
    }

    public function removeMember(member:DataLocationMember):void
    {
        var i:int = _dynamicPlayerObjects.length;
        while(i-- > 0) {
            if (member.id == _dynamicPlayerObjects[i].id) {
                _middleLayer.removeChild(_dynamicPlayerObjects[i]);
                _dynamicPlayerObjects.removeAt(i);
            }
        }
    }

    public function updateClientPosition(members:Array):void
    {
        for each (var member:DataLocationMember in members) {
            if (_locationMembers[member.id].player.id != _mainModel.player.id) {
                for each (var mapPlayer:CharacterWalk in _dynamicPlayerObjects) {
                    if (member.id == mapPlayer.id) {
                        mapPlayer.addPathPoint(new Point(member.position.x, member.position.y));
                    }
                }
            }
        }
    }

    private function pathFoundHandler(event:Event) : void
    {
        _userPath = _greed.lastPathFound;
        if (!_user.moveAction) {
            nextMoveHandler(null);
        }
    }

    private function nextMoveHandler(event:Event):void
    {
        if (_userPath.length < 1) {
            _greed.clearPath();
            _user.stopUserSelf();
            return;
        }
        var tile:BasicTile = _userPath.shift() as BasicTile;
        var newPosition:Point = tile.getPosition();

        _greed.clearTile(newPosition);

        var memberStatic:LocationMemberVO;
        var stopFlag:Boolean;

        for each (var mapMember:IMember in _dynamicMabObjects) {
            
            switch (mapMember.member.type) {
                case DataLocationMemberType.MOB:
                    memberStatic = MainModel.locationMemberData[mapMember.member.mob.locationMemberId];
                    break;
                case DataLocationMemberType.NPC:
                    memberStatic = MainModel.locationMemberData[mapMember.member.npc.locationMemberId];
                    break;
                case DataLocationMemberType.TELEPORT:
                    memberStatic = MainModel.locationMemberData[mapMember.member.teleport.locationMemberId];
                    break;
                case DataLocationMemberType.PICKUP:
                    memberStatic = MainModel.locationMemberData[mapMember.member.pickup.locationMemberId];
                    break;
            }

            if (mapMember.member.enable && newPosition.x == memberStatic.positionHexX && newPosition.y == memberStatic.positionHexY) {
                switch (mapMember.member.type) {
                    case DataLocationMemberType.MOB:
                        continue;
                        /*
                        if (mapMember.member.mob.battle) {
                            dispatchEventWith(INFO_BATTLE_EVENT, false, mapMember.member);
                        } else {
                            _memberForBattle = mapMember.member;
                            ELAlert.show( "Напасть на "+memberStatic.mob.title.ru+"?", "НАПАДЕНИЕ",
                                    [
                                        { label: "В бой", triggered: function():void {
                                            dispatchEventWith(START_BATTLE_EVENT, false, _memberForBattle);
                                        } },
                                        { label: "Отмена" }
                                    ]);
                        }
                        stopFlag = true;
                        */
                        break;
                    case DataLocationMemberType.NPC:
                        if (mapMember.member.npc.locationMemberId == 1485) {
                            dispatchEventWith(START_STORE_EVENT, true, 5);
                        } else {
                            dispatchEventWith(START_NPC_EVENT, true, mapMember.member);
                        }
                        stopFlag = true;
                        break;
                    case DataLocationMemberType.TELEPORT:
                        if (mapMember.member.enable) {
                            stopFlag = true;
                            dispatchEventWith(TELEPORT_LOCATION_EVENT, true, mapMember.member);
                        }
                        break;
                    case DataLocationMemberType.PICKUP:
                        if (mapMember.member.enable) {
                            stopFlag = true;
                            dispatchEventWith(PICKUP_LOCATION_EVENT, true, mapMember.member);
                        }
                        break;
                }
            }
        }
        if (stopFlag) {
            _greed.clearPath();
            _user.stopUserSelf();
        } else {
            _user.addPathPoint(newPosition);
            dispatchEventWith(MEMBER_RUN, true, newPosition);
        }

    }

    private function disposeLayer(sprite:Sprite):void
    {
        while (sprite.numChildren > 0) {
            var obj:DisplayObject = sprite.getChildAt(0);
            if (obj is AnimSprite) {
                Starling.juggler.remove(obj as AnimSprite);
            }
            if (obj is PDParticleSystem) {
                Starling.juggler.remove(obj as PDParticleSystem);
            }
            if (obj is MapMember) {
                obj.removeEventListeners(TouchEvent.TOUCH);
            }
            obj.removeFromParent(true);
        }
        sprite.removeFromParent(true);
    }

    override public function dispose():void
    {
        while (_bgImagesLayout.numChildren > 0) {
            _bgImagesLayout.getChildAt(0).removeFromParent(true);
        }
        _bgImagesLayout.removeFromParent(true);
        
        disposeLayer(getLayerByType(LocationLayerEnum.DOWN));
        disposeLayer(getLayerByType(LocationLayerEnum.MIDDLE));
        disposeLayer(getLayerByType(LocationLayerEnum.TOP));
        disposeLayer(getLayerByType(LocationLayerEnum.TOPMOST));
        ModelManager.instance.dispose();
        if (_locationAssets) _locationAssets.purge();
        _locationAssets = null;
        _dynamicMabObjects.length = 0;
        this.removeEventListener(TouchEvent.TOUCH, onLocationTouch);
        super.dispose();
    }

    public function get locationId():int {
        return _locationId;
    }
}
}
