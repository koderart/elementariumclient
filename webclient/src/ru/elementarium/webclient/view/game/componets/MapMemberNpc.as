package ru.elementarium.webclient.view.game.componets
{
import ru.muvs.admin.main.vo.LocationMemberVO;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.filters.GlowFilter;

    public class MapMemberNpc extends MapMember
	{
		public function MapMemberNpc (member:DataLocationMember, memberStatic:LocationMemberVO)
		{
            nickNameColor = 0x63ff00;
            loaderImageColor = LoaderModelImage.GREEN;
            glowFilter = new GlowFilter(0x00ff00, 1, 6);
            title = memberStatic.npc.title.ru;
            super(member, memberStatic);
            _modelName = _memberStatic.model;
            if (member.enable) {
                _loadingImage = new LoaderModelImage(loaderImageColor);
                addChildAt(_loadingImage, 0);
                createName();
                ModelManager.instance.addEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler);
            }
        }
    }
}