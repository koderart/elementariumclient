﻿package ru.elementarium.webclient.view.game.componets {
import com.junkbyte.console.Cc;

import flash.filesystem.File;

import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.services.Downloader;

import starling.events.Event;
import starling.events.EventDispatcher;
import starling.textures.TextureAtlas;
import starling.utils.AssetManager;


public class ModelManager extends EventDispatcher{

	public static const COMPLETE: String = "ModelManager_COMPLETE";
	private static var _instance: ModelManager;

	private var _assets:AssetManager = new AssetManager();
	private var modelNames:Array = [];
	private var modelNamesWating:Array = [];

	private var _downloader: Downloader;
	
	private var _isLoading: Boolean;
	
	public static function get instance():ModelManager
	{
		if (_instance == null) {
			_instance = new ModelManager();
		}
		return _instance;
	}

	public function ModelManager()
	{
	}

	public function getModel(modelName:String):TextureAtlas
	{
		return _assets.getTextureAtlas(modelName);
	}

	public function enqueue(modelName:String):void
	{
		if (modelName && modelNames.indexOf(modelName) == -1 &&
		    modelNamesWating.indexOf(modelName) == -1 && getModel(modelName) == null) {
			if (_isLoading) {
				modelNamesWating.push(modelName);
			} else {
				modelNames.push(modelName);
			}
		}
	}
	
	public function loadQueue():void
	{
		if (!_isLoading) {
			_isLoading = true;
			
			BUILD::release {
				for each (var modelName:String in modelNames) {
					_assets.enqueue(ConnectionService.BASE_URL_ASSETS + "characters/" + modelName + ".png");
					_assets.enqueue(ConnectionService.BASE_URL_ASSETS + "characters/" + modelName + ".xml");
				}
				_assets.loadQueue(assetManager_onProgress2);
			}

			BUILD::test {
				for each (var modelName:String in modelNames) {
					_assets.enqueue(ConnectionService.BASE_URL_ASSETS + "characters/" + modelName + ".png");
					_assets.enqueue(ConnectionService.BASE_URL_ASSETS + "characters/" + modelName + ".xml");
				}
				_assets.loadQueue(assetManager_onProgress2);
			}

			BUILD::mobile {
				if (_downloader) {
					_downloader.removeEventListener(Downloader.LOADED, downloaderLoadedHandler);
					_downloader.removeEventListener(Downloader.LOADED_ERROR, downloaderErrorHandler);
					_downloader.removeEventListener(Downloader.PROGRESS, downloaderProgressHandler);
				}
				_downloader = new Downloader("characters", ConnectionService.BASE_URL_ASSETS + "characters/");

				for each (var fileName:String in modelNames) {
					var file:File = File.applicationStorageDirectory.resolvePath("characters/" + fileName + ".png");
					if (!file.exists) {
						_downloader.addFileToList(fileName + ".png");
					}
					file = File.applicationStorageDirectory.resolvePath("characters/" + fileName + ".xml");
					if (!file.exists) {
						_downloader.addFileToList(fileName + ".xml");
					}
				}

				_downloader.addEventListener(Downloader.LOADED, downloaderLoadedHandler);
				_downloader.addEventListener(Downloader.LOADED_ERROR, downloaderErrorHandler);
				_downloader.addEventListener(Downloader.PROGRESS, downloaderProgressHandler);

				_downloader.load();
			}
		}
	}

	public function dispose():void
	{
		modelNames = [];
		modelNamesWating = [];
		_isLoading = false;
		_assets.purge();
	}

	protected function assetManager_onProgress2(progress:Number):void
	{
		if (progress !== 1) { return; }
		_isLoading = false;
		modelNames = [];
		dispatchEventWith(COMPLETE);
		if (modelNamesWating.length > 0) {
			modelNames = modelNamesWating.concat();
			modelNamesWating = [];
			loadQueue();
		}
	}

	public function get assets():AssetManager
	{
		return _assets;
	}
	
	private function downloaderLoadedHandler(event:Event):void
	{
		for each (var modelName:String in modelNames) {
			_assets.enqueue(File.applicationStorageDirectory.resolvePath("characters/" + modelName + ".png"));
			_assets.enqueue(File.applicationStorageDirectory.resolvePath("characters/" + modelName + ".xml"));
		}
		_assets.loadQueue(assetManager_onProgress2);
	}

	private function downloaderErrorHandler(event:Event):void
	{
	}

	private function downloaderProgressHandler(event:Event):void
	{
	}

}
}