﻿package ru.elementarium.webclient.view.game.componets {

import be.dauntless.astar.basic2d.BasicTile;
import be.dauntless.astar.basic2d.IPositionTile;
import be.dauntless.astar.basic2d.Map;
import be.dauntless.astar.core.IAstarTile;

import com.junkbyte.console.Cc;

import flash.geom.Point;

import ru.elementarium.webclient.view.game.Directions;

public class MapHex extends Map {

	public function MapHex(width:Number, height:Number) {
		super(width, height);
	}

	override public function getNeighbours(tile:IAstarTile) : Vector.<IAstarTile> {

		var position:Point = (tile as IPositionTile).getPosition();
		
		var x:Number = position.x;
		var y:Number = position.y;

		var neighbours:Vector.<IAstarTile> = new Vector.<IAstarTile>();

		addNeighbour(new Point(x-1, y), neighbours);
		addNeighbour(new Point(x+1, y), neighbours);

		addNeighbour(new Point(x, y-1), neighbours);
		addNeighbour(new Point(x, y+1), neighbours);

		if(y % 2 == 0) {
			addNeighbour(new Point(x-1, y-1), neighbours);
			addNeighbour(new Point(x-1, y+1), neighbours);
		} else {
			addNeighbour(new Point(x+1, y-1), neighbours);
			addNeighbour(new Point(x+1, y+1), neighbours);
		}

		return neighbours;
	}

	override public function isDiagonal(from:IAstarTile, to:IAstarTile):Boolean {
		var fromPos:Point = (from as IPositionTile).getPosition();
		var toPos:Point = (to as IPositionTile).getPosition();
		return fromPos.y != toPos.y;
	}

	private function addNeighbour(neighbour:Point, neighbours:Vector.<IAstarTile>):void {
		var tile:IPositionTile = getTileAt(neighbour);
		if (tile != null) {
			neighbours.push(tile);
		}
	}

	public function getDirection (tile1:BasicTile, tile2:BasicTile):String
	{
		var v:Vector.<IAstarTile> = getNeighbours(tile1);

		var side:int = v.indexOf(tile2);

		if (side != -1) {

			if (tile1.getPosition().y % 2 == 0) {
				return Directions.EVEN_DIRECTIONS[side];
			} else {
				return Directions.ODD_DIRECTIONS[side];
			}

		} else {
			 var tile1Position:Point = tile1.getPosition();
			 var tile2Position:Point = tile2.getPosition();

			 if (tile2Position.y == tile1Position.y) {
				 if (tile2Position.x > tile1Position.x) {
					 return Directions.E;
				 } else {
					 return Directions.W;
				 }
			 } else if (tile2Position.y < tile1Position.y) {
				 if (tile2Position.x < tile1Position.x) {
					 return Directions.NW;
				 } else {
					 return Directions.NE;
				 }
			 } else {
				 if (tile2Position.x < tile1Position.x) {
					 return Directions.SW;
				 } else {
					 return Directions.SE;
				 }
			 }
		}
	}

}
}