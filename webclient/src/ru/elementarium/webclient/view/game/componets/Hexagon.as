package ru.elementarium.webclient.view.game.componets
{
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.geom.Point;
	
	public class Hexagon
	{
		public static var WIDTH:int = 68;
		public static var HEIGHT:int = 48;
		
		private static var W2:int = WIDTH/2;
		private static var H2:int = HEIGHT/4;
		private static var H3:int = H2*3;
		
		//-------------------------------------------------------------------
		
		public static function create(	px : Number = 0, 
										py : Number = 0,
										lineColor : Number = 0xffffff, 
										lineAlpha : Number = 0.1,
										fillColor : Number = 0xFFFFFF,
										fillAlpha : Number = 0					
										) : Shape {
			var res: Shape = new Shape();
			draw(res.graphics, 0, 0, lineColor, lineAlpha, fillColor, fillAlpha);

			var p: Point = getGlobalPoint(px,py);
			res.x = p.x;
			res.y = p.y;

			return res;
		}
		
		public static function draw(	graphics: Graphics, 
										px : Number = 0, 
										py : Number = 0, 
										lineColor : Number = 0xffffff, 
										lineAlpha : Number = 0.1,
										fillColor : Number = 0xFFFFFF,
										fillAlpha : Number = 0					
									) : void {
            W2 = WIDTH/2;
            H2 = HEIGHT/4;
            H3 = H2*3;

            var p: Point = getGlobalPoint(px,py);
			
			graphics.lineStyle(1, lineColor, lineAlpha);
			graphics.moveTo(p.x + W2, p.y);
			graphics.beginFill(fillColor, fillAlpha);
			graphics.lineTo(p.x + WIDTH, p.y + H2);
			graphics.lineTo(p.x + WIDTH, p.y + H3);
			graphics.lineTo(p.x + W2, p.y + HEIGHT);
			graphics.lineTo(p.x, p.y + H3);
			graphics.lineTo(p.x, p.y + H2);
			graphics.endFill();
		}
		
		public static function getGlobalPoint(px : Number, py : Number) : Point {
			var x : Number = (px * WIDTH) + ((py % 2) == 1 ? W2 : 0);
			var y : Number = (py * HEIGHT) - (py * H2);
			
			return new Point(x,y);
		}

		public static function getPXYByPoint(p: Point) : Point {
			return getPXY(p.x, p.y);
		}
		
		public static function getPXY(x : Number, y : Number) : Point {
			var py : Number = int(y / H3);
			if(py % 2 == 1){
				x -= W2;
			}
			var px : Number = int(x / WIDTH);
			
			var ym : Number = y % H3;
			var fxm : Number;
			
			if(ym < H2) {
				var xm : Number = x % WIDTH;
				if(xm < W2){
					fxm = -H2/W2*xm;
					if(ym-H2 < fxm){
						if(py % 2 == 0){
							px--;
						}
						py--;
					}
				}else{
					fxm = H2/W2*(xm - W2);
					if(ym < fxm){
						if(py % 2 == 1){
							px++;
						}
						py--;
					}
				}
			}
			
			return new Point(px, py);
		}
	}
}