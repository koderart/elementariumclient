package ru.elementarium.webclient.view.game.componets {
import com.junkbyte.console.Cc;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.MobVO;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.location.member.DataLocationMemberType;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class UserItemInfoRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;
        private var nameLabel:TextField;
        private var backgroundImage:Image;
        private var elementImage:Image;
        private var initialized:Boolean = false;

        public function UserItemInfoRenderer()
        {
            super();
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.location_info_user_item) as Sprite;
                addChild(_sprite);
                
                nameLabel = _sprite.getChildByName("nameLabel") as TextField;
                nameLabel.text = "";
                elementImage = _sprite.getChildByName("elementImage") as Image;
                backgroundImage = _sprite.getChildByName("backgroundImage") as Image;

                if (data) {
                    drawData();
                }
            }
        }
    
        override public function set data(value:Object):void
        {
            _data = value; //as DataLocationMember;
            if (!value) return;
            if (initialized) {
                drawData();
            }
        }

        private function drawData():void
        {
            if (!(data as DataLocationMember)) {
                backgroundImage.alpha = .5;
                elementImage.visible = false;
                return;
            }
            backgroundImage.alpha = 1;
            if (data.type == DataLocationMemberType.MOB) {
                var mobData:MobVO = MainModel.locationMemberData[data.mob.locationMemberId].mob;
                nameLabel.text = mobData.level>0?"["+mobData.level+"] "+mobData.title.ru:mobData.title.ru;
                elementImage.visible = false;
            }

            if (data.type == DataLocationMemberType.PLAYER) {
                nameLabel.text = "["+data.player.level+"] "+ data.player.name;
                elementImage.visible = true;
                switch (data.player.element) {
                    case DataPlayerElement.DARK:
                        elementImage.texture = AppTheme.instance.assets.getTexture("elements/small/dark");
                    break;
                    case DataPlayerElement.FIRE:
                        elementImage.texture = AppTheme.instance.assets.getTexture("elements/small/fire");
                    break;
                    case DataPlayerElement.LIGHT:
                        elementImage.texture = AppTheme.instance.assets.getTexture("elements/small/light");
                    break;
                    case DataPlayerElement.NATURE:
                        elementImage.texture = AppTheme.instance.assets.getTexture("elements/small/nature");
                    break;
                    case DataPlayerElement.WATER:
                        elementImage.texture = AppTheme.instance.assets.getTexture("elements/small/water");
                    break;
                }
            }
        }
}
}
