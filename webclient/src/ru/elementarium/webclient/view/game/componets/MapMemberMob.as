package ru.elementarium.webclient.view.game.componets
{
import feathers.controls.ImageLoader;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.LocationMemberVO;
import ru.muvs.admin.main.vo.MobVO;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.core.Starling;
import starling.display.Sprite;
import starling.filters.ColorMatrixFilter;
import starling.filters.GlowFilter;
import starling.text.TextField;

    public class MapMemberMob extends MapMember
    {
        private var _timeLabel:TextField;
        private var _mobPanel:Sprite;

        public function MapMemberMob (member:DataLocationMember, memberStatic:LocationMemberVO)
        {
            nickNameColor = 0xFF0000;
            loaderImageColor = LoaderModelImage.RED;
            glowFilter  = new GlowFilter(0xff0000, 1, 6);

            var mobData:MobVO = MainModel.mobData[memberStatic.mob.mobId];
            if (memberStatic.label) {
                title = mobData.level>0?mobData.title.ru+" ["+mobData.level+"]":mobData.title.ru;
            }

            super(member, memberStatic);

            if (_memberStatic.left) {
                _mobPanel = AppTheme.uiBuilder.create(ParsedLayouts.game_mob_panel, false) as Sprite;
                _mobPanel.pivotX = 10;
                _mobPanel.pivotY = 40;
                addChild(_mobPanel);
                
                _timeLabel = _mobPanel.getChildByName("timeLabel") as TextField;
                _timeLabel.text = member.mob.left.toString();
                
                var imageLoader:ImageLoader = _mobPanel.getChildByName("imageLoader") as ImageLoader;
                imageLoader.width = imageLoader.height = 87;
                //var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
                //colorFilter.adjustSaturation(-1);
                //imageLoader.filter = colorFilter;
                imageLoader.source = ConnectionService.BASE_URL_HTTP+MainModel.mobData[memberStatic.mob.mobId].imageAvatar.url;
            }

            _modelName = _memberStatic.model;

            battle = member.mob.battle;
            enable = member.enable;

            if (member.enable && !member.mob.battle) {
                _loadingImage = new LoaderModelImage(loaderImageColor);
                addChildAt(_loadingImage, 0);
                createName();
                ModelManager.instance.addEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler);
            }
        }

        override public function set member(member:DataLocationMember):void
        {
            _member = member;
            battle = member.mob.battle;
            enable = member.enable;

            if (member.enable && !battle && !_animation) {
                createModel();
            }
        }

        public function set enable(value:Boolean):void
        {
            if (_memberStatic.left) {
                if (_animation) {_animation.visible = value;}
                if (_nameTf) {_nameTf.visible = value;}
                //_loadingImage
                //_battleImage
                _mobPanel.visible = !value;
                if (!value) {
                    _timeLabel.text = _member.mob.left.toString();
                    Starling.juggler.delayCall(ticTimer, 1);
                }
            } else {
                visible = value;
            }
        }

        private function ticTimer():void
        {
            _member.mob.left--;
            if (_member.mob.left >= 0) {
                _timeLabel.text = _member.mob.left.toString();
                Starling.juggler.delayCall(ticTimer, 1);
            } else {
                _timeLabel.text = "";
            }
        }
    }
}