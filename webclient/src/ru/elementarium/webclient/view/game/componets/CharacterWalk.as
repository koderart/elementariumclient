package ru.elementarium.webclient.view.game.componets
{
import flash.geom.Point;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.game.Directions;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.player.DataPlayer;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.player.DataPlayerSex;

import starling.AnimSprite;
import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.MovieClip;
import starling.display.Quad;
import starling.textures.TextureAtlas;

public class CharacterWalk extends MapMember
	{
        public static var NEXT_MOVE:String = "CharacterWalk.NEXT_MOVE";

        [Inject]
        public var mainModel:MainModel;

        private var _speedWalk:Number;
        private var _speedIdle:Number;
		private var _animation2:MovieClip;
        private var _direction:String = Directions.SW;
        private var _action:String = CharActions.IDLE;
        private var _quad:Quad;
        private var _moveAction:Boolean;
        private var _currentPosition:Point;

        private var _path:Vector.<Point> = new <Point>[];

        private var _greed:Greed;
        private var _userSelf:DataPlayer;

        private var _delayCallText:int;
        private var _currentMove:Tween;

        public function CharacterWalk(member:DataLocationMember, greed:Greed, userSelf:DataPlayer)
		{
            _greed = greed;
            _userSelf = userSelf;
            if (_userSelf.fraction == member.player.fraction) {
                nickNameColor = 0xffc481;
            } else {
                nickNameColor = 0xFF0000;
            }

            loaderImageColor = LoaderModelImage.GREEN;

            super(member, null);

            title = "["+_member.player.level+"] "+_member.player.name;
            visible = _member.enable;
            battle = _member.player.battle;

            _currentPosition = new Point(_member.position.x, _member.position.y);

            _modelName = getModel();
            
            var globalPoint:Point = Hexagon.getGlobalPoint(_currentPosition.x, _currentPosition.y);
            x = globalPoint.x;
            y = globalPoint.y;

            if (member.enable && !member.player.battle) {
                _loadingImage = new LoaderModelImage(loaderImageColor);
                addChildAt(_loadingImage, 0);
                createName();
                ModelManager.instance.addEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler);
            }
        }

        public function getModel():String
        {
            switch (_member.player.sex) {
                case DataPlayerSex.MALE:
                    switch (_member.player.element) {
                        case DataPlayerElement.DARK:
                            return "male_dark";
                        case DataPlayerElement.FIRE:
                            return "male_fire";
                        case DataPlayerElement.LIGHT:
                            return "male_light";
                        case DataPlayerElement.NATURE:
                            return "male_nature";
                        case DataPlayerElement.WATER:
                            return "male_water";
                    }
                    break;
                case DataPlayerSex.FEMALE:
                    switch (_member.player.element) {
                        case DataPlayerElement.DARK:
                            return "female_dark";
                        case DataPlayerElement.FIRE:
                            return "female_fire";
                        case DataPlayerElement.LIGHT:
                            return "female_light";
                        case DataPlayerElement.NATURE:
                            return "female_nature";
                        case DataPlayerElement.WATER:
                            return "female_water";
                    }
                    break;
            }
            return "male_yellow";
        }
    
        override protected function createModel():void
        {
            var textureAtlas:TextureAtlas = ModelManager.instance.getModel(_modelName);
            if (!textureAtlas) {
                loadModel();
                return;
            }

            if (_loadingImage) {
                removeChild(_loadingImage, true);
                _loadingImage = null;
            }

            if (_animation) { return;}

            _animation = new AnimSprite(textureAtlas);
            _animation.touchable = false;

            switch (_modelName) {
                case "male_light":
                    _animation.pivotX = 55;
                    _animation.pivotY = 70;
                    _speedWalk = 14.4;//12;
                    _speedIdle = 12;
                    break;
                case "male_fire":
                    _animation.pivotX = 37;
                    _animation.pivotY = 71;
                    _speedWalk = 18;//15;
                    _speedIdle = 14;
                    break;
                case "male_nature":
                    _animation.pivotX = 41;
                    _animation.pivotY = 60;
                    _speedWalk = 13.2;//11;
                    _speedIdle = 10;
                    break;
                case "male_dark":
                    _animation.pivotX = 17;
                    _animation.pivotY = 70;
                    _speedWalk = 17;
                    _speedIdle = 16;
                    break;
                case "male_water":
                    _animation.pivotX = 59;
                    _animation.pivotY = 91;
                    _speedWalk = 12;
                    _speedIdle = 10;
                    break;
                case "female_light":
                    _animation.pivotX = 53;
                    _animation.pivotY = 63;
                    _speedWalk = 13;
                    _speedIdle = 10;
                    break;
                case "female_dark":
                    _animation.pivotX = 6;
                    _animation.pivotY = 63;
                    _speedWalk = 15;
                    _speedIdle = 10;
                    break;
                case "female_nature":
                    _animation.pivotX = 24;
                    _animation.pivotY = 60;
                    _speedWalk = 11;
                    _speedIdle = 10;
                    break;
                case "female_water":
                    _animation.pivotX = 31;
                    _animation.pivotY = 54;
                    _speedWalk = 12;
                    _speedIdle = 12;
                    break;
                case "female_fire":
                    _animation.pivotX = 28;
                    _animation.pivotY = 68;
                    _speedWalk = 20;
                    _speedIdle = 12;
                    break;
            }

            _animation.addSequence("idle_"+Directions.NE, "idle_"+Directions.NE+"_", true, _speedIdle);
            _animation.addSequence("idle_"+Directions.E, "idle_"+Directions.E+"_", true, _speedIdle);
            _animation.addSequence("idle_"+Directions.SE, "idle_"+Directions.SE+"_", true, _speedIdle);
            _animation.addSequence("idle_"+Directions.SW, "idle_"+Directions.SW+"_", true, _speedIdle);
            _animation.addSequence("idle_"+Directions.W, "idle_"+Directions.W+"_", true, _speedIdle);
            _animation.addSequence("idle_"+Directions.NW, "idle_"+Directions.NW+"_", true, _speedIdle);

            _animation.addSequence("walk_"+Directions.NE, "walk_"+Directions.NE+"_", true, _speedWalk);
            _animation.addSequence("walk_"+Directions.E, "walk_"+Directions.E+"_", true, _speedWalk);
            _animation.addSequence("walk_"+Directions.SE, "walk_"+Directions.SE+"_", true, _speedWalk);
            _animation.addSequence("walk_"+Directions.SW, "walk_"+Directions.SW+"_", true, _speedWalk);
            _animation.addSequence("walk_"+Directions.W, "walk_"+Directions.W+"_", true, _speedWalk);
            _animation.addSequence("walk_"+Directions.NW, "walk_"+Directions.NW+"_", true, _speedWalk);

            //_quad = new Quad(_animation.width, _animation.height, 0x00FF00);
            //_quad.alpha = .2;
            //addChild(_quad);

            Starling.juggler.add(_animation);
            _animation.play(_action + "_" +_direction);
            addChild(_animation);

            if (_modelName == "male_dark" || _modelName == "female_dark") {
                _animation2 = new MovieClip(ModelManager.instance.assets.getTextures("purple_glow_"), 14);
                _animation2.pivotX = 29;
                _animation2.pivotY = 37;

                _animation2.play();
                Starling.juggler.add(_animation2);
                addChild(_animation2);
            }

            createName();
        }

        override public function set member(member:DataLocationMember):void
        {
            _member = member;
            battle = member.player.battle;
            if (_animation2) {
                _animation2.visible = !member.player.battle;
            }
            
            visible = _member.enable;

            if (member.enable && !battle && !_animation) {
                createModel();
            }
        }

        private function setStatus(action:String, direction:String):void
        {
            if (_action != action || _direction != direction) {
                _action = action;
                _direction = direction;
                if (_animation) {
                    _animation.play(_action + "_" +_direction);
                }
            }
        }

        public function clearPath():void
        {
            _path.length = 0;
        }

        public function addPathPoint(newPoint:Point):void
        {
            _path.push(newPoint);
            if (!_moveAction) {
                nextMove();
            }
        }

        public function stopUserSelf():void
        {
            setStatus(CharActions.IDLE, _direction);
            _moveAction = false;
        }

        public function get currentPosition():Point
        {
            return _currentPosition;
        }

        public function set currentPosition(value:Point):void
        {
            _currentPosition = value;
            Starling.juggler.remove(_currentMove);
            var globalPoint:Point = Hexagon.getGlobalPoint(_currentPosition.x, _currentPosition.y);
            x = globalPoint.x;
            y = globalPoint.y;
        }

        private function nextMove():void
        {
            if (_delayCallText) {
                Starling.juggler.removeByID(_delayCallText);
                _delayCallText = null;
            }

            if (_path.length < 1) {
                _moveAction = false;
                if (_member.player.id != _userSelf.id) {
                    _delayCallText = Starling.juggler.delayCall(setStatus,0.2, CharActions.IDLE, _direction);
                }
                dispatchEventWith(NEXT_MOVE);
                return;
            }

            var newPosition:Point = _path.shift();

            if (newPosition.x == currentPosition.x && newPosition.y == currentPosition.y) {
                nextMove();
                return;
            }
            //DO move action
            _moveAction = true;

            setStatus(CharActions.WALK, _greed.getDirection(currentPosition, newPosition));

            _currentPosition = newPosition;

            var globalPoint:Point = Hexagon.getGlobalPoint(_currentPosition.x, _currentPosition.y);
            _currentMove = new Tween(this, 0.4, Transitions.LINEAR);
            _currentMove.roundToInt = true;
            _currentMove.onComplete = nextMove;
            _currentMove.moveTo(globalPoint.x, globalPoint.y);
            _currentMove.onComplete = nextMove;
            Starling.juggler.add(_currentMove);
        }

        public function get id():int {
            return _member.id;
        }

        public function get moveAction():Boolean
        {
            return _moveAction;
        }
}
}