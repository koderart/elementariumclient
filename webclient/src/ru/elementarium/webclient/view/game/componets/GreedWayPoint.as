package ru.elementarium.webclient.view.game.componets
{
import flash.geom.Point;

import ru.elementarium.webclient.AppTheme;

import starling.display.Image;


public class GreedWayPoint extends Image
{
    private var point:Point;

    public function GreedWayPoint(p:Point):void
    {
        super(AppTheme.instance.assets.getTexture("location/way_point"));
        this.point = p;
        var hexPoint:Point = Hexagon.getGlobalPoint(point.x, point.y);
        x = hexPoint.x;
        y = hexPoint.y;
        touchable = false;
    }

    public function check(p:Point):Boolean
    {
        return point.x == p.x && point.y == p.y;
    }
}
}
