package ru.elementarium.webclient.view.game.componets
{
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

public interface IMember
	{
		function get member ():DataLocationMember;
		function set member (value:DataLocationMember):void;
	}
}