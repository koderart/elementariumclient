package ru.elementarium.webclient.view.game.componets
{
import feathers.controls.ImageLoader;
import ru.elementarium.webclient.services.ConnectionService;
import ru.muvs.admin.main.vo.LocationMemberVO;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.display.DisplayObject;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.filters.GlowFilter;

    public class MapTeleport extends Sprite implements IMember
	{
        private var _member:DataLocationMember;
    
        private var glowGreen:GlowFilter = new GlowFilter(0x00FF00, 1, 6);

		public function MapTeleport (member:DataLocationMember, memberStatic:LocationMemberVO):void
		{
            _member = member;
            visible = member.enable;
            var image:ImageLoader = new ImageLoader();
            image.source = ConnectionService.BASE_URL_HTTP + memberStatic.image.url;
            image.pivotX = - memberStatic.positionX;
            image.pivotY = - memberStatic.positionY;

            addChild(image);
            this.addEventListener(TouchEvent.TOUCH, onCharTouch);
        }

        private function onCharTouch(event:TouchEvent):void {
            var image:DisplayObject = event.target as DisplayObject;
            var touch:Touch = event.getTouch(image);
            if (touch && touch.phase == TouchPhase.HOVER) {
                image.filter = glowGreen;
            } else {
                image.filter = null;
            }
        }

        override public function dispose():void {
            this.removeEventListener(TouchEvent.TOUCH, onCharTouch);
            while (this.numChildren > 0) {
                var obj:DisplayObject = getChildAt(0);
                obj.removeFromParent(true);
            }
            super.dispose();
        }

        public function get member():DataLocationMember {
            return _member;
        }
    
        public function set member(value:DataLocationMember):void {
            _member = value;
            visible = member.enable;
        }
   }
}