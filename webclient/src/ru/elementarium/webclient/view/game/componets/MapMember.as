package ru.elementarium.webclient.view.game.componets
{
import ru.elementarium.webclient.AppTheme;
import ru.muvs.admin.main.vo.LocationMemberVO;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.AnimSprite;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.filters.GlowFilter;
import starling.text.TextField;
import starling.text.TextFieldAutoSize;
import starling.text.TextFormat;
import starling.textures.TextureAtlas;
import starling.utils.Align;

    public class MapMember extends Sprite implements IMember
	{
        protected var nickNameColor:int;
        protected var loaderImageColor:String;
        protected var glowFilter:GlowFilter;
        protected var _nameTf:TextField;

        protected var title:String;
        protected var _animation:AnimSprite;

        protected var _member:DataLocationMember;

        protected var _loadingImage:Image;
        protected var _memberStatic:LocationMemberVO;   
        protected var _modelName:String;

        private var _battleImage:Image;
        private var _battleFlag:Boolean = false;

		public function MapMember (member:DataLocationMember, memberStatic:LocationMemberVO)
		{
            this._member = member;
            this._memberStatic = memberStatic;

            this.addEventListener(TouchEvent.TOUCH, onCharTouch);
        }

        protected function ModelManager_COMPLETEHandler(event:Event):void
        {
            if (ModelManager.instance.getModel(_modelName)) {
                ModelManager.instance.removeEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler);
                createModel();
            }
        }

        protected function loadModel():void
        {
            if (!ModelManager.instance.hasEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler)) {
                ModelManager.instance.addEventListener(ModelManager.COMPLETE, ModelManager_COMPLETEHandler);
            }
            if (!_loadingImage) {
                _loadingImage = new LoaderModelImage(loaderImageColor);
                addChildAt(_loadingImage, 0);

                createName();

                ModelManager.instance.enqueue(_modelName);
                ModelManager.instance.loadQueue();
            }
        }
    
        protected function createModel():void
        {
            var textureAtlas:TextureAtlas = ModelManager.instance.getModel(_modelName);
            if (!textureAtlas) {
                loadModel();
                return;
            }
            
            if (_loadingImage) {
                removeChild(_loadingImage, true);
                _loadingImage = null;
            }

            if (_animation) { return;}
            
            _animation = new AnimSprite(textureAtlas);
            _animation.pivotX = -_memberStatic.positionX;
            _animation.pivotY = -_memberStatic.positionY;

            _animation.addSequence("anim", "idle_SE_", true, _memberStatic.speed ? _memberStatic.speed : 24);
            _animation.currentFrame = Math.random() * textureAtlas.getTextures("idle_SE_").length;
            _animation.play();
            Starling.juggler.add(_animation);
            addChild(_animation);
            createName();
        }

        protected function createName():void
        {
            if (!title) {return;}
            
            if (!_nameTf) {
                _nameTf = new TextField(24, 24, title, new TextFormat("Franklin Gothic Medium", 14, nickNameColor, Align.CENTER, Align.TOP));
                //name_tf.format.bold = true;
                _nameTf.batchable = true;
                _nameTf.autoSize = TextFieldAutoSize.HORIZONTAL;
                addChild(_nameTf);
            }
            _nameTf.pivotX = int(_nameTf.width/2)-35;
            if (_animation) {
                _nameTf.pivotY = _animation.pivotY+20;
            } else if (_loadingImage) {
                _nameTf.pivotY = _loadingImage.pivotY + 20;
            }
        }

        private function onCharTouch(event:TouchEvent):void {
            var image:DisplayObject = event.target as DisplayObject;
            var touch:Touch = event.getTouch(image);
            if (touch && touch.phase == TouchPhase.HOVER) {
                this.filter = glowFilter;
            } else {
                this.filter = null;
            }
        }

        protected function set battle(value:Boolean):void
        {
            _battleFlag = value;

            if (_battleFlag && !_battleImage) {
                _battleImage  = new Image(AppTheme.instance.assets.getTexture("main/menu/battle_selected"));
                _battleImage.x = 5;
                _battleImage.y = -22;
                addChild(_battleImage);
            }

            if (!_battleFlag && _battleImage) {
                _battleImage.removeFromParent(true);
                _battleImage = null;
            }

            if (_nameTf) _nameTf.visible = !_battleFlag;
            if (_animation) {
                _animation.visible = !_battleFlag;
                _animation.touchable = !_battleFlag;
            }
        }

        protected function get battle():Boolean
        {
            return _battleFlag;
        }

        override public function dispose():void {
            this.removeEventListeners(TouchEvent.TOUCH);
            if (_animation) {
                Starling.juggler.remove(_animation);
                _animation.removeFromParent(true);
            }

            while (this.numChildren > 0) {
                getChildAt(0).removeFromParent(true);
            }
            super.dispose();
        }

        public function get member():DataLocationMember
        {
            return _member;
        }

        public function set member(value:DataLocationMember):void
        {
            _member = value;
            visible = member.enable;
            
            if (member.enable && !_animation) {
                createModel();
            }
        }
    }
}