package ru.elementarium.webclient.view.game.componets {
import com.junkbyte.console.Cc;

import feathers.controls.renderers.LayoutGroupListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.elementarium.thrift.battle.member.DataPlayerFraction;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.location.member.DataLocationMemberType;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

    public class UserItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        public var _nameLabel:TextField;
        public var _elementImage:Image;
        public var _battleImage:Image;
        public var _fractionImage:Image;

        private var initialized:Boolean = false;

        public function UserItemRenderer()
        {
            super();
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.location_user_item, false, this) as Sprite;
                addChild(_sprite);
            }
        }
    
        override protected function commitData():void
        {
            if (_data && _data is DataLocationMember) {
                var locationMember:DataLocationMember = _data as DataLocationMember;
                
                if (locationMember.type == DataLocationMemberType.PLAYER) {
                    _battleImage.visible = locationMember.player.battle;
                    _nameLabel.text = "[" + locationMember.player.level + "] " + locationMember.player.name;
                    switch (locationMember.player.element) {
                        case DataPlayerElement.DARK:
                            _elementImage.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/dark");
                            break;
                        case DataPlayerElement.FIRE:
                            _elementImage.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/fire");
                            break;
                        case DataPlayerElement.LIGHT:
                            _elementImage.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/light");
                            break;
                        case DataPlayerElement.NATURE:
                            _elementImage.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/nature");
                            break;
                        case DataPlayerElement.WATER:
                            _elementImage.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/water");
                            break;
                    }
                    switch (locationMember.player.fraction) {
                        case DataPlayerFraction.FIRST:
                            _fractionImage.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_1");
                            break;
                        case DataPlayerFraction.SECOND:
                            _fractionImage.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_2");
                            break;
                    }
                }
            }
        }

    }
}
