package ru.elementarium.webclient.view.game.componets
{
import ru.elementarium.webclient.AppTheme;

import starling.display.Image;

public class LoaderModelImage extends Image
{
    public static const RED:String = "red";
    public static const GREEN:String = "green";

    public function LoaderModelImage(value:String):void
    {
        super(AppTheme.instance.assets.getTexture("location/ghost_"+value));
        pivotX = 39 * 0.8;
        pivotY = 145 * 0.8;
        scaleX = 0.8;
        scaleY = 0.8;
    }
}
}
