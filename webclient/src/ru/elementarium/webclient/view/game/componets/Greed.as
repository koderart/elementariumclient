package ru.elementarium.webclient.view.game.componets {
import be.dauntless.astar.basic2d.BasicTile;
import be.dauntless.astar.basic2d.analyzers.WalkableAnalyzer;
import be.dauntless.astar.core.Astar;
import be.dauntless.astar.core.AstarEvent;
import be.dauntless.astar.core.IAstarTile;
import be.dauntless.astar.core.PathRequest;

import flash.display.BitmapData;
import flash.display.Shape;
import flash.geom.Point;
import ru.elementarium.webclient.AppTheme;
import ru.muvs.admin.main.vo.LocationPassableHexVO;

import starling.core.Starling;
import starling.display.Image;
import starling.display.MovieClip;
import starling.display.Sprite;
import starling.textures.Texture;

public class Greed extends Sprite
{

    public static const PATH_FOUND:String = "PATH_FOUND_Greed";

    private var _locationScrollBg:Sprite;

    private var _lastPathFound:Vector.<IAstarTile>;
    private var _wayPoint:Vector.<GreedWayPoint> = new <GreedWayPoint>[];

    private var _hexWidth:int;
    private var _hexHeight:int;
    private var _passableHex:Vector.<LocationPassableHexVO>;

    private var _map:MapHex;
    private var _mapZero:MapHex;
    private var _astar:Astar;
    private var _astarZero:Astar;

    private var _startTile:BasicTile;
    private var _endTile:BasicTile;

    private var _walkMarker:MovieClip;
    private var _redFlag:Boolean;

    public function Greed(hexWidth:int,hexHeight:int, passableHex:Vector.<LocationPassableHexVO>, locationScrollBg:Sprite)
    {
        super();
        _locationScrollBg = locationScrollBg;
        _hexWidth = hexWidth;
        _hexHeight = hexHeight;
        _passableHex = passableHex;

        createGrid();
        //drawGrid();

        _walkMarker = new MovieClip(AppTheme.instance.assets.getTextures("location/walk_marker_"), 24);
        _walkMarker.touchable = false;
        _walkMarker.play();
        Starling.juggler.add(_walkMarker);
    }

    public function newPathToHexCoord(currentPosition:Point, toHexCoord:Point, flag:Boolean=false):void {
        _redFlag = flag;
        _startTile = _map.getTileAt(currentPosition) as BasicTile;
        _endTile = _map.getTileAt(toHexCoord) as BasicTile;

        if (_endTile.getWalkable()) {

            _astar.getPath(new PathRequest(this._startTile, this._endTile, this._map));
        } else {
            var startTileZero:BasicTile = _mapZero.getTileAt(currentPosition) as BasicTile;
            var endTileZero:BasicTile = _mapZero.getTileAt(toHexCoord) as BasicTile;

            _astarZero.getPath(new PathRequest(startTileZero, endTileZero, _mapZero));
        }
    }
    
    private function createGrid():void
    {
        _map = new MapHex(_hexWidth, _hexHeight);
        _mapZero = new MapHex(_hexWidth, _hexHeight);
        
        for (var i:int = 0; i < _hexWidth; i++) {
            for (var j:int = 0; j < _hexHeight; j++) {
                var passable:Boolean = false;
                for (var k:int = 0; k < _passableHex.length; k++) {
                    if (i == _passableHex[k].x && j == _passableHex[k].y) {
                        passable = true;
                    }
                }
                var tile:BasicTile = new BasicTile(1,new Point(i,j),passable);
                this._map.setTile(tile);
            }
        }

        for (i = 0; i < _hexWidth; i++) {
            for (j = 0; j < _hexHeight; j++) {
                tile = new BasicTile(1,new Point(i,j),true);
                this._mapZero.setTile(tile);
            }
        }
        
        this._astar = new Astar();
        this._astar.addAnalyzer(new WalkableAnalyzer());
        this._astar.addEventListener(AstarEvent.PATH_FOUND,this.pathFoundHandler);
        this._astar.addEventListener(AstarEvent.PATH_NOT_FOUND,this.pathNotFoundHandler);

        this._astarZero = new Astar();
        this._astarZero.addAnalyzer(new WalkableAnalyzer());
        this._astarZero.addEventListener(AstarEvent.PATH_FOUND,this.zeroPathFoundHandler);
        this._astarZero.addEventListener(AstarEvent.PATH_NOT_FOUND,this.zeroPathNotFoundHandler);
    }

    private function drawGrid():void
    {
        var ss:Shape = new Shape();
        Hexagon.draw(ss.graphics, 0, 0, 0xCCCCCC, 0.6);
        ss.graphics.endFill();

        var hexBitmap:BitmapData = new BitmapData (Math.ceil(ss.width), Math.ceil(ss.height), true, 0);
        hexBitmap.draw(ss);
        var _hexTexture:Texture = Texture.fromBitmapData(hexBitmap);

        var hexImage:Image;
        var hexPoint:Point;

        for (var i:int = 0; i < _hexWidth; i++) {
            for (var j:int = 0; j < _hexHeight; j++) {
                hexImage = new Image(_hexTexture);
                hexImage.touchable = false;
                hexPoint = Hexagon.getGlobalPoint(i, j);
                hexImage.x = hexPoint.x;
                hexImage.y = hexPoint.y;
                _locationScrollBg.addChild(hexImage);
            }
        }
    }
    
    private function pathFoundHandler(event:AstarEvent) : void
    {
        clearPath();
        
        _lastPathFound = event.result.path;
        if (_lastPathFound.length < 2) {
            return;
        }

        _lastPathFound.splice(0,1);
        
        drawPath();

        var globalPoint:Point = Hexagon.getGlobalPoint(_endTile.getPosition().x, _endTile.getPosition().y);
        _walkMarker.x = globalPoint.x;
        _walkMarker.y = globalPoint.y;
        _walkMarker.color = _redFlag ? 0xFF0000 : 0xFFFFFF;
        if (!this.contains(_walkMarker)) {
            this.addChild(_walkMarker);
        }

        dispatchEventWith(PATH_FOUND);
    }

    private function pathNotFoundHandler(event:AstarEvent) : void
    {
    }

    private function zeroPathNotFoundHandler(event:AstarEvent) : void
    {
    }

    private function zeroPathFoundHandler(event:AstarEvent) : void
    {
        for (var i:int = event.result.path.length-1; i >= 0 ; i--)
        {
            var hex:BasicTile = _map.getTileAt(BasicTile(event.result.path[i]).getPosition()) as BasicTile;
            if (hex.getWalkable()) {
                _endTile = hex;
                _astar.getPath(new PathRequest(_startTile, _endTile, _map));
                break;
            }
        }
    }
    public function clearTile(point:Point):void
    {
        for (var i:int=0; i<_wayPoint.length; i++) {
            if (_wayPoint[i].check(point)) {
                this.removeChild(_wayPoint[i]);
                return;
            }
        }
    }

    public function clearPath ():void
    {
        _wayPoint.length = 0;
        this.removeChildren();
    }

    private function drawPath():void
    {
        for (var i:int = 0; i < _lastPathFound.length-1; i++)
        {
            var hex:BasicTile = _lastPathFound[i] as BasicTile;
            if (hex)
            {
                var wayPoint:GreedWayPoint = new GreedWayPoint(hex.getPosition());
                wayPoint.color = _redFlag ? 0xFF0000 : 0xFFFFFF;
                this.addChild(wayPoint);
                _wayPoint.push(wayPoint);
            }
        }
    }

    public function get map():MapHex
    {
        return _map;
    }

    public function get lastPathFound():Vector.<IAstarTile>
    {
        return _lastPathFound;
    }

    public function getDirection(currentPosition:Point, newPosition:Point):String
    {
        return _map.getDirection(this.map.getTileAt(currentPosition) as BasicTile, _map.getTileAt(newPosition) as BasicTile);
    }
}
}