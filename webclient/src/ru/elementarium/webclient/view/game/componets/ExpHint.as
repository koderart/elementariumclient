package ru.elementarium.webclient.view.game.componets {
import starling.display.Quad;
import starling.display.Sprite;
import starling.text.TextField;

public class ExpHint extends Sprite
{
    public var text:TextField;

    public function ExpHint()
    {
        super();
        var bg:Quad = new Quad(110, 20, 0xCCCCCC);
        addChild(bg);
        text = new TextField(110, 20,"");
        addChild(text);
    }
}
}
