package ru.elementarium.webclient.view.game {
import com.junkbyte.console.Cc;

import feathers.controls.Button;
import feathers.controls.LayoutGroup;
import feathers.controls.List;
import feathers.controls.Scroller;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.game.componets.Location;
import ru.elementarium.webclient.view.game.componets.UserItemInfoRenderer;
import ru.elementarium.webclient.view.main.WindowView;
import ru.muvs.elementarium.thrift.location.battle.DataLocationBattle;
import ru.muvs.elementarium.thrift.location.battle.DataLocationBattleMember;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;

import starling.display.Sprite;
import starling.events.Event;

public class LocationMemberInfo extends WindowView
    {
        public var _leftList:List;
        public var _rightList:List;
        public var _battleButton:Button;

        private var locationBattle:DataLocationBattle;
        private var locationMember:DataLocationMember;

        private var userListLeftCollection:ListCollection = new ListCollection();
        private var userListRightCollection:ListCollection = new ListCollection();

        public function LocationMemberInfo(locationBattle:DataLocationBattle, locationMember:DataLocationMember)
        {
            super();
            this.locationBattle = locationBattle;
            this.locationMember = locationMember;
            
            _content = AppTheme.uiBuilder.create(ParsedLayouts.battle_info, false, this) as Sprite;
            
            _titleLabel.text = "ИНФОРМАЦИЯ О БОЕ";
            
            var layoutV:VerticalLayout = new VerticalLayout();
            layoutV.gap = 1;
            _leftList.layout = layoutV;
            _leftList.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
            _leftList.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
            _leftList.itemRendererFactory = function():IListItemRenderer { return new UserItemInfoRenderer(); };
            _leftList.dataProvider = userListLeftCollection;

            layoutV = new VerticalLayout();
            layoutV.gap = 1;
            _rightList.layout = layoutV;
            _rightList.verticalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
            _rightList.horizontalScrollPolicy = Scroller.SCROLL_POLICY_OFF;
            _rightList.itemRendererFactory = function():IListItemRenderer { return new UserItemInfoRenderer(); };
            _rightList.dataProvider = userListRightCollection;
            
            _battleButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            
            for each (var member:DataLocationBattleMember in locationBattle.members) {
                if (member.side == 1) {
                    userListLeftCollection.addItem(member.member);
                }
                if (member.side == 2) {
                    userListRightCollection.addItem(member.member);
                }
            }
            validateAndAttach();
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _battleButton:
                    dispatchEventWith(Location.GOTO_BATTLE_EVENT, false, locationMember);
                    removeFromParent(true);
                break;
            }
        }

}
}
