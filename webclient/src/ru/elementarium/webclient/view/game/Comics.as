package ru.elementarium.webclient.view.game {
import com.junkbyte.console.Cc;
import feathers.controls.ImageLoader;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.utils.XMLLoader;
import ru.elementarium.webclient.view.main.AbstractView;

import starling.animation.DelayedCall;

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;

public class Comics extends Sprite
    {
        private var imageCanvas:Sprite;
        private var imageLoader1:ImageLoader;
        private var imageLoader2:ImageLoader;
        private var currentImageLoader:ImageLoader;
        private var pagesData:XMLList;
        private var currentPage:int = 0;
        private var textTimer:TextField;
        private var delayCallText:DelayedCall;

        public function Comics()
        {
            XMLLoader.instance.sendRequest(ConnectionService.BASE_URL_ASSETS+"comics.xml", comicsLoaderHandler);

            imageLoader1 = new ImageLoader();
            imageLoader2 = new ImageLoader();
            this.addEventListener(TouchEvent.TOUCH, onTouch);
            this.addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            imageLoader1.addEventListener(Event.IO_ERROR, imageLoader_ioErrorHandler);
            imageLoader1.addEventListener(Event.COMPLETE, imageLoader_completeHandler);
            imageLoader2.addEventListener(Event.IO_ERROR, imageLoader_ioErrorHandler);
            imageLoader2.addEventListener(Event.COMPLETE, imageLoader_completeHandler);
            currentImageLoader = imageLoader2;
            textTimer = new TextField(500, 100, "Кликните для продолжения");
            textTimer.format.color = 0x191715;
            textTimer.format.setTo("Verdana", 28);
            textTimer.alpha = 0;
        }

        protected function sprite_addedToStageHandler(event:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            var bgModal:Quad = new Quad(stage.stageWidth, stage.stageHeight);
            bgModal.alpha = 0;
            this.addChild(bgModal);
            imageCanvas = new Sprite();
            this.addChild(imageCanvas);
            imageCanvas.addChild(imageLoader1);
            imageCanvas.addChild(imageLoader2);
            this.addChild(textTimer);
        }

        private function comicsLoaderHandler(data:XML):void {
            pagesData = new XMLList(data.page);
            var len:int = pagesData.length();
            if (len > 0) {
                currentImageLoader.source = ConnectionService.BASE_URL_ASSETS + "comics/" + pagesData[currentPage].@src;
            } else {
                removeFromParent(true);
                dispatchEventWith(Event.COMPLETE);
            }
        }

        private function animationRemoveFromParent():void {
            currentImageLoader.visible = false;
            var tween:Tween = new Tween(this, 1, Transitions.LINEAR);
            tween.animate('alpha', 0);
            tween.onComplete = function():void { removeFromParent(true); };
            Starling.current.juggler.add(tween);
            dispatchEventWith(Event.COMPLETE);
        }

        private function showText():void {
            var tween:Tween = new Tween(textTimer, 1, Transitions.LINEAR);
            tween.animate('alpha', 1);
            Starling.current.juggler.add(tween);
        }

        private function onTouch(e: TouchEvent):void {
            var touch: Touch = e.getTouch(e.currentTarget as DisplayObject);
            if (touch && touch.phase == TouchPhase.ENDED) {
                textTimer.alpha = 0;
                currentPage++;
                if (currentPage >= pagesData.length()) {
                    animationRemoveFromParent();
                } else {
                    currentImageLoader.source = ConnectionService.BASE_URL_ASSETS + "comics/" + pagesData[currentPage].@src;
                }
            }
        }

        private function imageLoader_completeHandler(event:Event):void {
            Starling.juggler.remove(delayCallText);
            delayCallText = Starling.juggler.delayCall(showText, 10) as DelayedCall;

            var imageLoader:ImageLoader = event.target as ImageLoader;
            imageLoader.validate();
            imageLoader.x = int((stage.stageWidth-imageLoader.originalSourceWidth)/2);
            imageLoader.y = int((stage.stageHeight-imageLoader.originalSourceHeight)/2);
            imageCanvas.setChildIndex(imageLoader, imageCanvas.numChildren);
            if (currentImageLoader == imageLoader1) {
                currentImageLoader = imageLoader2;
            } else {
                currentImageLoader = imageLoader1;
            }
            imageLoader.alpha = 0;
            var tween:Tween = new Tween(imageLoader, 1, Transitions.LINEAR);
            tween.animate('alpha', 1);
            Starling.current.juggler.add(tween);

            textTimer.x = int((stage.stageWidth-textTimer.width)/2);
            textTimer.y = int((stage.stageHeight-textTimer.height)/2);
        }

        private function imageLoader_ioErrorHandler(event:Event):void {
            removeFromParent(true);
            dispatchEventWith(Event.COMPLETE);
        }

        override public function dispose():void {
            imageLoader1.source = null;
            imageLoader1.dispose();
            imageLoader2.source = null;
            imageLoader2.dispose();
            super.dispose();
        }
}
}
