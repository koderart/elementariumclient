﻿package ru.elementarium.webclient.view.game
{
	
	public class Directions
	{
		
		public static const N:String = "N";
		public static const E:String = "E";
		public static const W:String = "W";
		public static const S:String = "S";
		
		public static const NE:String = "NE";
		public static const NW:String = "NW";
		
		public static const SE:String = "SE";
		public static const SW:String = "SW";
		
		public static const ODD_DIRECTIONS:Array = [W, E, NW, SW, NE, SE];
		public static const EVEN_DIRECTIONS:Array =[W, E, NE, SE, NW, SW];

	}

}