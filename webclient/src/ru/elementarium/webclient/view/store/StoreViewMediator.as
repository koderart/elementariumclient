package ru.elementarium.webclient.view.store
{
import com.junkbyte.console.Cc;

import feathers.controls.List;
import feathers.data.ListCollection;
import flash.utils.Dictionary;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.events.StoreEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.store.components.BuyItem;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.ArtikulVO;

import starling.events.Event;

public class StoreViewMediator extends Mediator
	{
        private static const MAX_ITEM_TO_PAGE:int = 8;
		
        [Inject]
		public var view:StoreView;
        [Inject]
        public var mainModel:MainModel;

        private var menuListCollection:ListCollection;
        private var currentStore:StoreVO;
        private var currentSectionId:int;
        private var storeSection:Dictionary = new Dictionary();
    
        private var currentPage:int = 0;
    
		override public function onRegister():void
		{
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._categoryUpButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._categoryDownButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._pageLeftButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._pageRightButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            menuListCollection = new ListCollection();
            view._menuList.dataProvider = menuListCollection;
            view._menuList.addEventListener(Event.CHANGE, openMenuEventHandler);
            
            view._itemList.addEventListener(Event.CHANGE, openItemEventHandler);
            createStore();
        }

        private function createStore():void {
            currentStore = MainModel.storeData[mainModel.currentStore];

            currentStore.storeSection.sort(storeSectionSort);

            for each (var section:StoreSectionVO in currentStore.storeSection) {
                storeSection[section.storeSectionId] = section;
                menuListCollection.addItem(section);
            }
            
            view._menuList.selectedIndex = 0;
        }

        private function storeSectionSort(a:StoreSectionVO, b:StoreSectionVO):Number
        {
            if(a.order > b.order) {
                return 1;
            } else if(a.order < b.order) {
                return -1;
            } else  {
                return 0;
            }
        }

        private function openMenuEventHandler(event:Event):void
        {
            currentPage = 0;
            currentSectionId = StoreSectionVO(view._menuList.selectedItem).storeSectionId;
            updatePageData();
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._closeButton:
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                    break;
                case view._categoryUpButton:
                    view._menuList.verticalScrollPosition -= 48;
                    if (view._menuList.verticalScrollPosition < 0) {
                        view._menuList.verticalScrollPosition = 0;
                    }
                    break;
                case view._categoryDownButton:
                    view._menuList.verticalScrollPosition += 48;
                    if (view._menuList.verticalScrollPosition > view._menuList.maxVerticalScrollPosition) {
                        view._menuList.verticalScrollPosition = view._menuList.maxVerticalScrollPosition;
                    }
                    break;
                case view._pageLeftButton:
                    var newPage:int = currentPage-1;
                    if (newPage < 0) {newPage = 0;}
                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;
                case view._pageRightButton:
                    
                    var dataLength:int = storeSection[currentSectionId].storeSectionArtikul.length;

                    newPage = currentPage+1;
                    if ((newPage*MAX_ITEM_TO_PAGE) >= dataLength ) {newPage = currentPage;}

                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;

            }

        }

        private function updatePageData():void
        {
            view._pageLabel.text = (currentPage+1) + "/" + Math.max(1, Math.ceil(storeSection[currentSectionId].storeSectionArtikul.length / MAX_ITEM_TO_PAGE));
    
            var startIndex:int = currentPage*MAX_ITEM_TO_PAGE;
            view._itemList.dataProvider = new ListCollection(storeSection[currentSectionId].storeSectionArtikul.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
        }

        private function openItemEventHandler(event:Event):void
        {
            if (!List(event.target).selectedItem) {return}

            var buyItem:BuyItem = new BuyItem(List(event.target).selectedItem.artikul as ArtikulVO, mainModel);
            buyItem.addEventListener(BuyItem.BUY_EVENT, buyEventHandler);
            view.addChild(buyItem);

            List(event.target).selectedItem = null;
        }

        private function buyEventHandler(event:Event):void {
            var e:StoreEvent = new StoreEvent(StoreEvent.BUY);
            e.store = currentStore.storeId;
            e.section = currentSectionId;
            e.artikulId = event.data.artikulId;
            e.count = event.data.count;
            e.moneyType = event.data.moneyType;
            dispatch(e);
        }
}
}