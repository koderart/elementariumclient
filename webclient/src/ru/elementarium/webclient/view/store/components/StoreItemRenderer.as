package ru.elementarium.webclient.view.store.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.MoneyVO;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class StoreItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        private var imageLoader:ImageLoader;
        private var nameLabel:TextField;
        private var moneyLabel:TextField;
        private var moneyIcon:Image;

        private var initialized:Boolean = false;
        private var _select:TapToSelect;
    
        public function StoreItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.store_item) as Sprite;
                addChild(_sprite);

                imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
                nameLabel = _sprite.getChildByName("nameLabel") as TextField;
                moneyLabel = _sprite.getChildByName("moneyLabel") as TextField;
                moneyIcon = _sprite.getChildByName("moneyIcon") as Image;

            }
        }
    
        override protected function commitData():void
        {
            if(_data) {
                imageLoader.source = ConnectionService.BASE_URL_HTTP+_data.artikul.image.url;
                nameLabel.text = _data.artikul.title.ru;
                if (_data.artikul.artikulMoney.length > 0) {
                    var money:MoneyVO = _data.artikul.artikulMoney[0];
                    switch (money.moneyType) {
                        case MoneyEnum.GOLD:
                            moneyIcon.texture = AppTheme.instance.assets.getTexture("main/goldcoin_icon");
                        break;
                        case MoneyEnum.CRYSTAL:
                            moneyIcon.texture = AppTheme.instance.assets.getTexture("main/crystal_icon");
                        break;
                    }
                    moneyLabel.text = money.money.toString();
                }
            }
        }
    }
}
