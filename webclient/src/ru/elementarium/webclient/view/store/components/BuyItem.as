package ru.elementarium.webclient.view.store.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.TextInput;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.inventory.components.ArtikulAlert;
import ru.elementarium.webclient.view.main.WindowView;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.MoneyVO;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import ru.muvs.elementarium.thrift.money.DataMoneyType;

import starling.display.Button;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;

public class BuyItem extends WindowView
    {
    
        public static const BUY_EVENT:String = "BuyItem.BUY_EVENT";

        public var _imageLoader:ImageLoader;
        public var _artikulLabel:TextField;
        public var _moneyLabel:TextField;
        public var _messageLabel:TextField;
        public var _countInput:TextInput;
        public var _buyButton:feathers.controls.Button;
        public var _cancelButton:feathers.controls.Button;
        public var _minusButton:Button;
        public var _plusButton:Button;
        public var _moneyIcon:Image;
    
        private var currentCost:int;
        private var currentCount:int;
        private var _artikulVO:ArtikulVO;
        private var _mainModel:MainModel;

        public function BuyItem(artikulVO:ArtikulVO, mainModel:MainModel)
        {
            super();
            _artikulVO = artikulVO;
            _mainModel = mainModel;
            _content = AppTheme.uiBuilder.create(ParsedLayouts.store_item_buy, false, this) as Sprite;

            _titleLabel.text = "ПОКУПКА";
            _artikulLabel.text = _artikulVO.title.ru;
            _imageLoader.addEventListener(TouchEvent.TOUCH, touchArtikulHintHandler);
            _imageLoader.source = ConnectionService.BASE_URL_HTTP+_artikulVO.image.url;

            _countInput.text = "1";
            _countInput.restrict = "0-9";
            _countInput.maxChars = 4;
            _countInput.addEventListener(Event.CHANGE, countInput_changeHandler);
            
            _buyButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _cancelButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _minusButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _plusButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _messageLabel.text = "";

            _buyButton.visible = false;
            if (_artikulVO.artikulMoney.length > 0) {
                var money:MoneyVO = _artikulVO.artikulMoney[0];
                switch (money.moneyType) {
                    case MoneyEnum.GOLD:
                        _moneyIcon.texture = AppTheme.instance.assets.getTexture("main/goldcoin_icon");
                        break;
                    case MoneyEnum.CRYSTAL:
                        _moneyIcon.texture = AppTheme.instance.assets.getTexture("main/crystal_icon");
                        break;
                }
                _buyButton.visible = true;
                currentCost = money.money;
                currentCount = 1;
                updateMoney();
            }

            var levelMin:int = artikulVO.levelMin.number;
            var levelMax:int = artikulVO.levelMax.number;
            if (levelMin && (levelMin > _mainModel.playerLevel) ||
                    levelMax && (levelMax < _mainModel.playerLevel)) {
                _messageLabel.text = "ПРЕДМЕТ НЕ ПОДХОДИТ ВАМ ПО УРОВНЮ.";
            }

            validateAndAttach();
        }

        private function updateMoney():void {
            _countInput.text = currentCount.toString();
            _moneyLabel.text = String(currentCount*currentCost);
            _messageLabel.text = "";
            switch (_artikulVO.artikulMoney[0].moneyType) {
                case MoneyEnum.GOLD:
                    if (currentCount*currentCost > _mainModel.moneyGold) {
                        _messageLabel.text = "Не хватает золота.";
                    }
                    break;
                case MoneyEnum.CRYSTAL:
                    if (currentCount*currentCost > _mainModel.moneyCrystal) {
                        _messageLabel.text = "Не хватает кристаллов.";
                    }
                    break;
            }
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _cancelButton:
                    removeFromParent(true);
                break;
                case _minusButton:
                    currentCount--;
                    if (currentCount < 1) {
                        currentCount = 1;
                    }
                    updateMoney();
                    break;
                case _plusButton:
                    currentCount++;
                    if (currentCount > 9999) {
                        currentCount = 9999;
                    }
                    updateMoney();
                    break;
                case _buyButton:
                    if (_artikulVO.artikulMoney.length > 0) {
                        var e : Object = {};
                        e.artikulId = _artikulVO.artikulId;
                        e.count = _countInput.text;
                        switch (_artikulVO.artikulMoney[0].moneyType) {
                            case MoneyEnum.GOLD:
                                e.moneyType = DataMoneyType.GOLD;
                                if (currentCount*currentCost > _mainModel.moneyGold) {
                                    return;
                                }
                                break;
                            case MoneyEnum.CRYSTAL:
                                e.moneyType = DataMoneyType.CRYSTAL;
                                if (currentCount*currentCost > _mainModel.moneyCrystal) {
                                    return;
                                }
                                break;
                        }
                        dispatchEventWith(BUY_EVENT, false, e);
                        removeFromParent(true);
                    }
                    break;
            }
        }
        
        private function touchArtikulHintHandler(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(event.currentTarget as DisplayObject);
            if (touch && touch.phase == TouchPhase.ENDED) {
                var d:DataArtikul = new DataArtikul();
                d.artikulId = _artikulVO.artikulId;
                var artikulAlert:ArtikulAlert = new ArtikulAlert(d, null, _mainModel.player.element, true);

                stage.addChild(artikulAlert);
            }
        }

    private function countInput_changeHandler(event:Event):void
    {
        currentCount = int(TextInput(event.currentTarget).text);
        updateMoney();
    }
}
}
