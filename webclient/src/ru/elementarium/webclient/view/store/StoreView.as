package ru.elementarium.webclient.view.store {
import feathers.controls.List;
import feathers.controls.renderers.DefaultListItemRenderer;
import feathers.controls.renderers.IListItemRenderer;
import feathers.layout.FlowLayout;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;
import ru.elementarium.webclient.view.store.components.StoreItemRenderer;

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;

public class StoreView extends ModuleView
    {
        public var _menuList:List;
        public var _itemList:List;
        public var _categoryUpButton:feathers.controls.Button;
        public var _categoryDownButton:feathers.controls.Button;
        public var _pageLeftButton:Button;
        public var _pageRightButton:Button;
        public var _pageLabel:TextField;

        public function StoreView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.store, false, this) as Sprite;
            
            _titleLabel.text = "МАГАЗИН";
            
            _menuList.customItemRendererStyleName = "storeListButton";
            _menuList.itemRendererFactory = function():IListItemRenderer
            {
                var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
                renderer.labelFunction = function(data:Object):String {return data.title.ru;};
                return renderer;
            };
            var layoutV:VerticalLayout = new VerticalLayout();
            layoutV.gap = 0;
            _menuList.layout = layoutV;

            _itemList.itemRendererFactory = function():IListItemRenderer { return new StoreItemRenderer(); };
            var layoutF:FlowLayout = new FlowLayout();
            layoutF.gap = 5;
            _itemList.layout = layoutF;

            validateAndAttach();
        }
}
}
