package ru.elementarium.webclient.view.quest {
import feathers.controls.Button;
import feathers.controls.ImageLoader;
import feathers.controls.List;
import feathers.controls.renderers.DefaultListItemRenderer;
import feathers.controls.renderers.IListItemRenderer;
import feathers.layout.FlowLayout;
import feathers.layout.HorizontalLayout;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;
import ru.elementarium.webclient.view.quest.components.*;
import ru.muvs.admin.main.vo.QuestVO;

import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class QuestView extends ModuleView
    {
        public var _actionButton:Button;
        public var _categoryUpButton:Button;
        public var _categoryDownButton:Button;

        public var _avatarLoader:ImageLoader;
        public var _questList:List;
        public var _questSprite:Sprite;

        public var _targetList:List;
        public var _artikulList:List;
        public var _descriptionLabel:TextField;

        public function QuestView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.quest, false, this) as Sprite;

            _titleLabel.text = "ЖУРНАЛ ЗАДАНИЙ";

            _questList.customItemRendererStyleName = "storeListButton";
            _questList.itemRendererFactory = function():IListItemRenderer
            {
                var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
                renderer.gap = 10;

                renderer.labelFunction = function(data:Object):String {
                    return QuestVO(MainModel.questData[data.questId]).title.ru;
                };
                renderer.iconFunction = function(data:Object):DisplayObject {
                    if (data.complete) {
                        return new Image(AppTheme.instance.assets.getTexture("quest/ready_marker"));
                    }
                    if (data.goals) {
                        return new Image(AppTheme.instance.assets.getTexture("quest/taken_marker"));
                    }
                    return null;
                };
                return renderer;
            };
            var layoutV:VerticalLayout = new VerticalLayout();
            layoutV.gap = 0;
            _questList.layout = layoutV;

            _avatarLoader.width = _avatarLoader.height = 104;

            _targetList.itemRendererFactory = function():IListItemRenderer { return new TargetItemRenderer(); };
            layoutV = new VerticalLayout();
            layoutV.gap = 5;
            _targetList.layout = layoutV;


            _artikulList.itemRendererFactory = function():IListItemRenderer { return new AwardtemRenderer(); };
            var layoutF:FlowLayout = new FlowLayout();
            layoutF.horizontalGap = 0;
            layoutF.verticalGap = 5;
            layoutF.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            layoutF.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_MIDDLE;
            _artikulList.layout = layoutF;

            switchToQuest(false);

            validateAndAttach();
        }

        public function switchToQuest(quest:Boolean):void {
            _questSprite.visible = quest;
            _actionButton.visible = _actionButton.includeInLayout = quest;
        }
}
}
