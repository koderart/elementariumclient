package ru.elementarium.webclient.view.quest.components {

import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.enum.BonusActionEnum;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.admin.main.vo.enum.SkillEnum;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;


public class AwardtemRenderer extends LayoutGroupListItemRenderer
    {
        private var _initialized:Boolean = false;
        private var _select:TapToSelect;

        private var _sprite:Sprite;
        public var _countLabel:TextField;
        public var _moneyLabel:TextField;
        public var _moneyImage:Image;
        public var _imageLoader:ImageLoader;
        public var _artikulSprite:Sprite;
        public var _moneySprite:Sprite;
        
        public function AwardtemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            _initialized = true;

            if (!_sprite) {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.quest_award_item, false, this) as Sprite;
                addChild(_sprite);
            }
        }

        override protected function commitData():void
        {
            if(_data) {
                switch (_data.ACTION) {
                    case BonusActionEnum.ARTIKUL_GIVE:
                        _artikulSprite.visible = true;
                        _moneySprite.visible = false;
                        _imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.artikulData[_data.artikul].image.url;
                        _countLabel.text = _data.countFrom.toString();
                        if (_data.countFrom < _data.countTo) {
                            _countLabel.text = _data.countFrom.toString() + " - "+_data.countTo.toString();
                        }
                        if (_countLabel.text == "1") {_countLabel.text = "";}
                        break;
                    case BonusActionEnum.MONEY_GIVE:
                        _artikulSprite.visible = false;
                        _moneySprite.visible = true;
                        _moneyLabel.text = _data.moneyFrom.toString();
                        if (_data.moneyFrom < _data.moneyTo) {
                            _moneyLabel.text = _data.moneyFrom.toString() + " - "+_data.moneyTo.toString();
                        }
                        if (_data.moneyType == MoneyEnum.GOLD) {
                            _moneyImage.texture = AppTheme.instance.assets.getTexture("main/goldcoin_icon");
                        }
                        if (_data.moneyType == MoneyEnum.CRYSTAL) {
                            _moneyImage.texture = AppTheme.instance.assets.getTexture("main/crystal_icon");
                        }
                        break;
                    case BonusActionEnum.SKILL_MOD_PLUS:
                        _artikulSprite.visible = false;
                        _moneySprite.visible = true;
                        if (_data.skill.skill == SkillEnum.EXP) {
                            _moneyImage.texture = AppTheme.instance.assets.getTexture("currency/exp_icon_big");
                            _moneyLabel.text = _data.value;
                        }
                        break;
                }
            }
        }
    }
}
