package ru.elementarium.webclient.view.quest.components {

import feathers.controls.renderers.LayoutGroupListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.enum.*;
import ru.muvs.elementarium.thrift.quest.DataQuestGoal;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;


public class TargetItemRenderer extends LayoutGroupListItemRenderer
    {
        private var initialized:Boolean = false;
        private var _sprite:Sprite;
        public var _titleLabel:TextField;
        public var _valueLabel:TextField;
        public var _goalImage:Image;
    
        public function TargetItemRenderer()
        {
            super();
        }

        override protected function initialize():void
        {
            initialized = true;

            if (!_sprite) {

                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.quest_goal_item, false, this) as Sprite;
                addChild(_sprite);
            }
        }
    
        override protected function commitData():void
        {
            if (!_data) {
                return;
            }

            var current:int = 0;
            var count:int = 0;

            if (_data.goalStatic.GOAL == QuestGoalEnum.HAVE_MONEY) {
                if (_data.goal) {
                    current = DataQuestGoal(_data.goal).moneyCurrent;
                }
                count = _data.goalStatic.money;
            } else {
                if (_data.goal) {
                    current = DataQuestGoal(_data.goal).countCurrent;
                }
                count = _data.goalStatic.count
            }
            
            if (current >= count) {
                _goalImage.texture = AppTheme.instance.assets.getTexture("buttons/checkbox_selected");
            } else {
                _goalImage.texture = AppTheme.instance.assets.getTexture("buttons/checkbox");
            }
            
            switch (_data.goalStatic.GOAL) {
                case QuestGoalEnum.COLLECT_ARTIKUL:
                    _titleLabel.text = "Собрать " + _data.goalStatic.artikul.title.ru;
                    _valueLabel.text = current+"/"+ count.toString();
                break;
                case QuestGoalEnum.HAVE_ARTIKUL:
                    _titleLabel.text = "Принести " + _data.goalStatic.artikul.title.ru;
                    _valueLabel.text = current+"/"+ count.toString();
                break;
                case QuestGoalEnum.KILL_MOB:
                    _titleLabel.text = "Убить " + _data.goalStatic.mob.title.ru;
                    _valueLabel.text = current+"/"+ count.toString();
                break;
                case QuestGoalEnum.KILL_PLAYER:
                    _titleLabel.text = "Убить игроков";
                    switch (_data.goalStatic.element) {
                        case ElementEnum.DARK:
                            _titleLabel.text += " тьмы";
                            break;
                        case ElementEnum.FIRE:
                            _titleLabel.text += " огня";
                            break;
                        case ElementEnum.LIGHT:
                            _titleLabel.text += " света";
                            break;
                        case ElementEnum.NATURE:
                            _titleLabel.text += " природы";
                            break;
                        case ElementEnum.WATER:
                            _titleLabel.text += " воды";
                            break;
                    }
                    _valueLabel.text = current+"/"+ count.toString();
                break;
                case QuestGoalEnum.HAVE_MONEY:
                    _titleLabel.text = "Оплатить";
                    _valueLabel.text = count.toString();

                    switch (_data.goalStatic.moneyType) {
                        case MoneyEnum.GOLD:
                            _titleLabel.text += " монетами";
                            break;
                        case MoneyEnum.CRYSTAL:
                            _titleLabel.text += " кристаллами";
                            break;
                    }
                    break;
            }
        }
    }
}
