package ru.elementarium.webclient.view.quest
{
import com.junkbyte.console.Cc;

import feathers.data.ListCollection;
import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.events.HintEvent;
import ru.elementarium.webclient.events.QuestEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.inventory.components.ArtikulAlert;
import ru.elementarium.webclient.view.main.AppMediator;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.enum.*;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import ru.muvs.elementarium.thrift.quest.DataQuest;
import ru.muvs.elementarium.thrift.quest.DataQuestGoal;

import starling.events.Event;

public class QuestViewMediator extends Mediator
	{
		[Inject]
		public var view:QuestView;
        [Inject]
        public var mainModel:MainModel;

        private var currentQuestData:DataQuest;

		override public function onRegister():void
		{
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._actionButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._questList.addEventListener(Event.CHANGE, openQuestEventHandler);
            view._artikulList.addEventListener(Event.CHANGE, openArtikulEventHandler);
            view._categoryUpButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._categoryDownButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            view._artikulList.addEventListener("showHintEvent", showHintEventHandler);
            view._artikulList.addEventListener("hideHintEvent", hideHintEventHandler);
            addContextListener(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_TAKE, npcEventTypeHandler);
            addContextListener(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_GIVE, npcEventTypeHandler);
            addContextListener(QuestEvent.QUEST_REFUSE_CLIENT_COMMAND, npcEventTypeHandler);
            addContextListener(QuestEvent.QUEST_LIST_CLIENT_COMMAND, npcEventTypeHandler);
            addContextListener(QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST, npcEventTypeHandler);


            if (mainModel.currentNpc) {
                var npcVO:NpcVO = MainModel.locationMemberData[mainModel.currentNpc.npc.locationMemberId].npc;
                view._titleLabel.text = npcVO.title.ru;
                view._avatarLoader.source = ConnectionService.BASE_URL_HTTP + npcVO.imageAvatar.url;
            } else {
                view._titleLabel.text = "ЖУРНАЛ ЗАДАНИЙ";
            }

            updateQuestList();
        }

        private function npcEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case QuestEvent.LOCATION_ACTION_CLIENT_QUEST_TAKE:
                case QuestEvent.LOCATION_ACTION_CLIENT_QUEST_GIVE:
                case QuestEvent.QUEST_REFUSE_CLIENT_COMMAND:
                    view._questList.dataProvider.removeAll();
                    view.switchToQuest(false);
                    if (mainModel.currentNpc) {
                        var e:QuestEvent = new QuestEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST);
                        e.member = mainModel.currentNpc;
                        dispatch(e);
                    } else {
                        dispatch(new QuestEvent(QuestEvent.QUEST_LIST_SERVER_COMMAND));
                    }
                    break;
                case QuestEvent.QUEST_LIST_CLIENT_COMMAND:
                case QuestEvent.LOCATION_ACTION_CLIENT_QUEST_LIST:
                    view._questList.dataProvider.removeAll();
                    updateQuestList();
                    break;
            }
        }
    
        private function updateQuestList():void {
            view._questList.dataProvider = new ListCollection(mainModel.questList);
            if (view._questList.dataProvider.length > 0) {
                view._questList.selectedIndex = 0;
            } else {
                view._questList.selectedItem = null;
                mainModel.openQuest = false;
                mainModel.openNpc = false;
                view.removeFromParent(true);
                dispatchWith(GameEvent.RESET_MAIN_MENU);
            }
        }
    
        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._categoryUpButton:
                    view._questList.verticalScrollPosition -= 48;
                    if (view._questList.verticalScrollPosition < 0) {
                        view._questList.verticalScrollPosition = 0;
                    }
                    break;
                case view._categoryDownButton:
                    view._questList.verticalScrollPosition += 48;
                    if (view._questList.verticalScrollPosition > view._questList.maxVerticalScrollPosition) {
                        view._questList.verticalScrollPosition = view._questList.maxVerticalScrollPosition;
                    }
                    break;
                case view._closeButton:
                    mainModel.openQuest = false;
                    mainModel.openNpc = false;
                    //view.removeFromParent(true);
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                break;
                case view._actionButton:

                    var e:QuestEvent;

                    if (mainModel.currentNpc) {
                        if (!currentQuestData.goals) {
                            e = new QuestEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_TAKE);
                        }
                        if (currentQuestData.goals && currentQuestData.complete) {
                            e = new QuestEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_GIVE);
                        }
                        if (currentQuestData.goals && !currentQuestData.complete) {
                            ELAlert.show( "Отказаться от задания?", "Подтверждение",
                                    [
                                        { label: "ДА", triggered: refuseQuestHandler, style:"pinkOkButton"},
                                        { label: "Отмена", style:"blueCancelButton"}
                                    ]);
                            return;
                        }
                        e.member = mainModel.currentNpc;
                        e.quest = currentQuestData;
                        dispatch(e);
                    } else {
                        ELAlert.show( "Отказаться от задания?", "Подтверждение",
                                [
                                    { label: "ДА", triggered: refuseQuestHandler, style:"pinkOkButton"},
                                    { label: "Отмена", style:"blueCancelButton"}
                                ]);
                    }
                break;
            }
        }

        private function refuseQuestHandler(event:Event):void {
            var e:QuestEvent;
            e = new QuestEvent(QuestEvent.QUEST_REFUSE_SERVER_COMMAND);
            e.quest = currentQuestData;
            dispatch(e);

        }

        private function openQuest(questDataRender:DataQuest):void {
            currentQuestData = questDataRender;
            view.switchToQuest(true);

            //avatar
            if (!mainModel.currentNpc) {
                for each (var npc:NpcVO in MainModel.npcData) {
                    for each (var npcGive:NpcGiveVO in npc.npcGiveQuest) {
                        if (npcGive.quest.questId == currentQuestData.questId) {
                            view._avatarLoader.source = ConnectionService.BASE_URL_HTTP+npc.imageAvatar.url;
                            break;
                        }
                    }
                }
            }

            var quest:QuestVO = MainModel.questData[questDataRender.questId];
            view._descriptionLabel.text = quest.description.ru;

            if (!mainModel.currentNpc) {
                if (currentQuestData.goals && !currentQuestData.complete) {
                    view._actionButton.styleName = "blueCancelButton";
                    view._actionButton.label = "ОТКАЗАТСЯ";
                }
            } else {
                if (!currentQuestData.goals) {
                    view._actionButton.styleName = "pinkOkButton";
                    view._actionButton.label = "ВЗЯТЬ";
                }
                if (currentQuestData.goals && currentQuestData.complete) {
                    view._actionButton.styleName = "pinkOkButton";
                    view._actionButton.label = "СДАТЬ";
                }
                if (currentQuestData.goals && !currentQuestData.complete) {
                    view._actionButton.styleName = "blueCancelButton";
                    view._actionButton.label = "ОТКАЗАТСЯ";
                }
            }

            var targetListCollection:ListCollection = new ListCollection();

            if (!currentQuestData.goals) {
                for each (var goalStatic:QuestGoalVO in quest.questGoal) {
                    targetListCollection.addItem({goalStatic:goalStatic});
                }
            } else {
                for each (goalStatic in quest.questGoal) {
                    for each (var goal:DataQuestGoal in currentQuestData.goals) {
                        if (goal.goalId == goalStatic.questGoalId) {
                            targetListCollection.addItem({goal:goal, goalStatic:goalStatic});
                        }
                    }
                }
            }

            var artikulListCollection:ListCollection = new ListCollection();
            for each (var element:BonusActionVO in quest.bonus.bonusAction) {
                if (element.ACTION == BonusActionEnum.ARTIKUL_GIVE ||
                    element.ACTION == BonusActionEnum.MONEY_GIVE ||
                        (element.ACTION == BonusActionEnum.SKILL_MOD_PLUS && element.skill.skill == SkillEnum.EXP)) {
                    artikulListCollection.addItem(element);
                }
            }

            view._targetList.dataProvider = targetListCollection;
            view._artikulList.dataProvider = artikulListCollection;
        }

        private function openQuestEventHandler(event:Event):void
        {
            if (view._questList.selectedItem) {
                openQuest(view._questList.selectedItem as DataQuest);
            }
        }

        private function openArtikulEventHandler(event:Event):void
        {
            if (view._artikulList.selectedItem) {
                var bonusAction:BonusActionVO = view._artikulList.selectedItem as BonusActionVO;
                if (bonusAction.bonusAction == BonusActionEnum.ARTIKUL_GIVE) {
                    var d:DataArtikul = new DataArtikul();
                    d.artikulId = bonusAction.artikul;
                    var artikulAlert:ArtikulAlert = new ArtikulAlert(d, null, mainModel.player.element, true);
                    view.stage.addChild(artikulAlert);
                }
            }
            view._artikulList.selectedItem = null;
        }

        private function showHintEventHandler(event:Event):void {
            var e:HintEvent = new HintEvent(HintEvent.SHOW);
            e.typeHint = AppMediator.HINT_ARTIKUL;
            e.artikulId = event.data.artikulId;
            e.userLevel = mainModel.playerLevel;
            e.userElement = mainModel.player.element;
            e.durabilityCurrent = MainModel.artikulData[e.artikulId].durabilityCurrent;
            e.durability = MainModel.artikulData[e.artikulId].durability;
            e.x = event.data.x;
            e.y = event.data.y;
            dispatch(e);
        }

        private function hideHintEventHandler(event:Event):void {
            dispatch(new HintEvent(HintEvent.HIDE));
        }

}
}