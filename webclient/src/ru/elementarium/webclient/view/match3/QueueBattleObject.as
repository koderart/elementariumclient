/**
 * Created by Artem on 02.09.2016.
 */
package ru.elementarium.webclient.view.match3
{
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffBegin;
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffCollision;
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffEffect;
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffEnd;
import ru.muvs.elementarium.thrift.battle.DataClientBattleBuffInterrupt;
import ru.muvs.elementarium.thrift.battle.DataClientBattleMagic;
import ru.muvs.elementarium.thrift.battle.DataClientBattlePotion;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;

public class QueueBattleObject
{
    public static const TYPE_MAGIC:int = 0;
    public static const TYPE_POTION:int = 2;
    public static const TYPE_BUFF:int = 3;
    
    public var actions:Array;
    public var type:int;

    public var magic:DataClientBattleMagic;
    public var potion:DataClientBattlePotion;
    public var buff:DataBattleBuff;

    public function QueueBattleObject(obj:Object)
    {
        if (obj is DataClientBattleMagic) {
            type = TYPE_MAGIC;
            magic = obj as DataClientBattleMagic;
        } else if (obj is DataClientBattlePotion) {
            type = TYPE_POTION;
            potion = obj as DataClientBattlePotion;
        } else if (obj as DataClientBattleBuffBegin || obj as DataClientBattleBuffCollision || obj as DataClientBattleBuffEffect ||
                   obj as DataClientBattleBuffEnd || obj as DataClientBattleBuffInterrupt) {
            type = TYPE_BUFF;
            buff = obj.buff;
        }
        actions = obj.actions;
    }
}
}
