package ru.elementarium.webclient.view.match3.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.match3.PotionDataWrapper;

import starling.display.Sprite;
import starling.filters.ColorMatrixFilter;
import starling.text.TextField;

public class BeltItemRenderer extends LayoutGroupListItemRenderer
{
    private var _sprite:Sprite;

    private var imageLoader:ImageLoader;
    private var countLabel:TextField;
    private var cooldownLabel:TextField;

    private var _select:TapToSelect;
    private var initialized:Boolean = false;

    public function BeltItemRenderer()
    {
        super();
        this._select = new TapToSelect(this);
    }

    override protected function initialize():void
    {
        initialized = true;
        if (_sprite == null)
        {
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_belt_item) as Sprite;
            addChild(_sprite);

            imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
            imageLoader.pixelSnapping = true;
            imageLoader.width = 55;
            imageLoader.height = 55;
            countLabel = _sprite.getChildByName("countLabel") as TextField;
            countLabel.text = "";
            cooldownLabel = _sprite.getChildByName("cooldownLabel") as TextField;
            cooldownLabel.text = "";

            if (data) {
                drawData();
            }
        }
    }

    override public function set data(value:Object):void
    {
        _data = value;

        if (!value) return;

        if (initialized) {
            drawData();

        }
    }

    private function drawData():void
    {
        if (_data.artikulId) {
            _sprite.visible = true;
            imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.getArtikulVO(_data.artikulId).image.url;
            countLabel.text = _data.count.toString();
            if (PotionDataWrapper(_data).coolDown > 0) {
                cooldownLabel.text = PotionDataWrapper(_data).coolDown.toString();
                var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
                colorFilter.adjustSaturation(-1);
                this.filter = colorFilter;
            } else {
                cooldownLabel.text = "";
                this.filter = null;
            }

        } else {
            imageLoader.source = null;
            _sprite.visible = false;
        }
    }

}
}
