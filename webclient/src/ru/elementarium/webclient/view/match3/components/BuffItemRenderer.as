package ru.elementarium.webclient.view.match3.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Sprite;
import starling.text.TextField;

public class BuffItemRenderer extends LayoutGroupListItemRenderer
{
    private var _sprite:Sprite;

    private var imageLoader:ImageLoader;
    private var countLabel:TextField;

    private var _select:TapToSelect;
    private var initialized:Boolean = false;

    public function BuffItemRenderer()
    {
        super();
        this._select = new TapToSelect(this);
    }

    override protected function initialize():void
    {
        initialized = true;
        if (_sprite == null)
        {
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_buff_item) as Sprite;
            addChild(_sprite);

            imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
            imageLoader.width = 46;
            imageLoader.height = 46;

            countLabel = _sprite.getChildByName("countLabel") as TextField;
            countLabel.text = "";
            countLabel.touchable = false;

            if (data) {
                drawData();
            }
        }
    }

    override public function set data(value:Object):void
    {
        _data = value;

        if (!value) return;

        if (initialized) {
            drawData();

        }
    }

    private function drawData():void
    {
        if (_data.buffId) {
            _sprite.visible = true;
            imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.getBuffVO(_data.buffId).image.url;
            if (_data.duration > 0) {
                countLabel.text = _data.duration;
            } else {
                countLabel.text = "";
            }
        } else {
            imageLoader.source = null;
            _sprite.visible = false;
        }
    }

}
}
