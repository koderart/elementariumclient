package ru.elementarium.webclient.view.match3.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.match3.MagicDataWrapper;

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;

public class MagicItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;
        public var _imageLoader:ImageLoader;
        public var _cooldownLabel:TextField;
        public var _useButton:Button;


        private var _select:TapToSelect;
        private var initialized:Boolean = false;

        public function MagicItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_magic_item, false, this) as Sprite;
                addChild(_sprite);

                _imageLoader.width = 60;
                _imageLoader.height = 60;
            }
        }
    
        override protected function commitData():void
        {
            if (_data) {
                _imageLoader.source = ConnectionService.BASE_URL_HTTP + _data.staticVO.image.url;
                if (MagicDataWrapper(_data).coolDown > 0) {
                    _cooldownLabel.text = MagicDataWrapper(_data).coolDown.toString();
                } else {
                    _cooldownLabel.text = "";
                }


                if (MagicDataWrapper(_data).checkNeedPoint() && MagicDataWrapper(_data).coolDown == 0) {
                    _useButton.visible = true;
                } else {
                    _useButton.visible = false;
                }
            }
        }
    }
}
