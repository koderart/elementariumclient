package ru.elementarium.webclient.view.match3.components {
import com.junkbyte.console.Cc;

import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.battle.member.DataBattleMemberType;

import starling.display.Image;
import starling.display.Sprite;
import starling.filters.ColorMatrixFilter;
import starling.text.TextField;

    public class UserItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        public var _nameLabel:TextField;
        public var _hpBar:Image;
        public var _selectFrame:Image;
        public var _unselectFrame:Image;
        private var _hpBarMaxWidth:int;

        private var _select:TapToSelect;

        private var initialized:Boolean = false;

        public function UserItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_user_item, false, this) as Sprite;
                addChild(_sprite);
                _nameLabel.wordWrap = false;
                _hpBarMaxWidth = _hpBar.width;
            }
        }

        override protected function commitData():void
        {
            if(_data) {
                if(_data as String) {
                    _nameLabel.text = "";
                    _hpBar.visible = false;
                    _selectFrame.visible = false;
                    _unselectFrame.visible = true;
                } else {
                    _nameLabel.text = "["+data.level+"] " + data.name;
                    _hpBar.visible = true;
                    _hpBar.width = _hpBarMaxWidth * data.hp / data.hpTotal;

                    _selectFrame.visible = data.selected;
                    _unselectFrame.visible = !data.selected;

                    if (data.type == DataBattleMemberType.PLAYER) {
                        switch (data.elementEnum) {
                            case ElementEnum.DARK:
                                _hpBar.texture = AppTheme.instance.assets.getTexture("match3/health_bar_mini_purple");
                                break;
                            case ElementEnum.FIRE:
                                _hpBar.texture = AppTheme.instance.assets.getTexture("match3/health_bar_mini_red");
                                break;
                            case ElementEnum.LIGHT:
                                _hpBar.texture = AppTheme.instance.assets.getTexture("match3/health_bar_mini_yellow");
                                break;
                            case ElementEnum.NATURE:
                                _hpBar.texture = AppTheme.instance.assets.getTexture("match3/health_bar_mini_green");
                                break;
                            case ElementEnum.WATER:
                                _hpBar.texture = AppTheme.instance.assets.getTexture("match3/health_bar_mini_blue");
                                break;
                        }
                    } else {
                        var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
                        colorFilter.adjustSaturation(-1);
                        _hpBar.filter = colorFilter;

                    }
                }
            }
        }

    }
}
