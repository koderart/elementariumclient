package ru.elementarium.webclient.view.match3
{
import com.junkbyte.console.Cc;

import com.junkbyte.console.ConsoleChannel;

import feathers.controls.ImageLoader;
import feathers.controls.List;
import feathers.data.ListCollection;

import flash.events.TimerEvent;
import flash.utils.Timer;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.*;
import ru.muvs.admin.main.vo.battle.BuffVO;
import ru.muvs.admin.main.vo.battle.MagicVO;
import ru.muvs.admin.main.vo.battle.battleenum.BuffEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;

import ru.muvs.elementarium.thrift.battle.action.*;
import ru.muvs.elementarium.thrift.battle.area.DataAreaCell;
import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;
import ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool;
import ru.muvs.elementarium.thrift.battle.cool.DataBattlePotionCool;
import ru.muvs.elementarium.thrift.battle.member.*;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePointType;
import ru.muvs.elementarium.thrift.battle.skill.DataBattleSkillType;

import starling.animation.Transitions;

import starling.animation.Tween;
import starling.core.Starling;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.extensions.PDParticleSystem;
import starling.text.TextField;
import starling.utils.AssetManager;

public class Match3ViewMediator extends Mediator
	{
		[Inject]
		public var view:Match3View;

		[Inject]
		public var model:Match3Model;

        [Inject]
        public var mainModel:MainModel;

        private var _queueBattleAction:Vector.<QueueBattleActionObject> = new <QueueBattleActionObject>[];
        private var _queueBattleMagic:Vector.<QueueBattleObject> = new <QueueBattleObject>[];
        private var _currenBattleObject:QueueBattleObject;
        private var _turnBlocked:Boolean;

        private var console:ConsoleChannel = new ConsoleChannel("Match3ViewMediator");

        private var time:Timer = new Timer(1000, 30);
        
        private var _gameEndedFlag:Boolean;
        private var _queueBattleImpactFinish:Boolean = true;

        private var userListLeftCollection:ListCollection;
        private var userListRightCollection:ListCollection;

        private var assetAppManager : AssetManager;

        private var textLines:Vector.<String> = new Vector.<String>();

        private var magicInfo:MagicInfo;
        private var buffInfo:BuffInfo;
        private var potionInfo:PotionInfo;

        private var _delayCallDoAction:int;
    
        override public function onRemove():void
        {
            super.onRemove();
            trace("Match3 mediator onRemove");
            removeContextListener(BattleEventType.BATTLE_SWAP_CLIENT_COMMAND, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_MEMBER_JOIN_CLIENT_COMMAND, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_MAGIC_CLIENT_COMMAND, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_BUFF_BEGIN, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_BUFF_END, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_BUFF_INTERRUPT, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_BUFF_EFFECT, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_BUFF_COLLISION, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_KICK_CLIENT_COMMAND, battleEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_POTION_CLIENT_COMMAND, battleEventTypeHandler);
            
            if (_delayCallDoAction) {
                Starling.juggler.removeByID(_delayCallDoAction);
            }
        }

        override public function onRegister():void
		{
            super.onRegister();
            trace("Match3 mediator onRegister");

            addContextListener(BattleEventType.BATTLE_SWAP_CLIENT_COMMAND, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_MEMBER_JOIN_CLIENT_COMMAND, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_MAGIC_CLIENT_COMMAND, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_BUFF_BEGIN, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_BUFF_END, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_BUFF_INTERRUPT, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_BUFF_EFFECT, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_BUFF_COLLISION, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_KICK_CLIENT_COMMAND, battleEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_POTION_CLIENT_COMMAND, battleEventTypeHandler);

            time.addEventListener(TimerEvent.TIMER, onTimer);
            time.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);

            view.addEventListener(Match3View.MOVE_GEMS, viewHandler);
            view.addEventListener(Match3View.END_GAME, viewHandler);

            if (model.areaWidth && model.areaHeight) {
                view.drawGreed(model.areaWidth, model.areaHeight, model.areaCells);
            }

            userListLeftCollection = new ListCollection();
            view._userListLeft.dataProvider = userListLeftCollection;

            userListRightCollection = new ListCollection();
            view._userListRight.dataProvider = userListRightCollection;
            view._userListRight.addEventListener(Event.CHANGE, userListLeft_changeHandler);

            view._magicList.addEventListener(Event.CHANGE, magicList_changeHandler);

            view._beltList.addEventListener(Event.CHANGE, beltList_changeHandler);

            model.membersWrapper = {};
            for each (var member: DataBattleMember in model.members) {
                model.membersWrapper[member.id] = new UserDataWrapper(member);

                if (member.type == DataBattleMemberType.PLAYER && member.player.id == mainModel.player.id) {
                    model.user = model.membersWrapper[member.id];
                }
            }

            for each (member in model.members) {
                if (member.side == model.user.side) {
                    userListLeftCollection.addItem(model.membersWrapper[member.id]);
                } else {
                    userListRightCollection.addItem(model.membersWrapper[member.id]);
                }
            }

            while (userListLeftCollection.length < 10) {
                userListLeftCollection.addItem('empty'+Math.random());
            }
            if (userListRightCollection.length > 0) {
                model.userTarget = userListRightCollection.getItemAt(0) as UserDataWrapper;
            }
            while (userListRightCollection.length < 10) {
                userListRightCollection.addItem('empty'+Math.random());
            }

            //buff list
            view._leftTabCounterLabel.text = model.user.buffs.length.toString();
            view._userBuffLeft.dataProvider = new ListCollection(model.user.buffs);
            view._rightTabCounterLabel.text = model.userTarget.buffs.length.toString();
            view._userBuffRight.dataProvider = new ListCollection(model.userTarget.buffs);
            view._userBuffLeft.addEventListener(Event.CHANGE, buffList_changeHandler);
            view._userBuffRight.addEventListener(Event.CHANGE, buffList_changeHandler);

            //belt list
            for each (var itemBelt:DataBattleArtikulBelt in model.user.belts) {
                var cool:int = 0;
                for each (var pcool:DataBattlePotionCool in model.user.potionCools) {
                    if (pcool.potionId == itemBelt.potionId) {
                        cool = pcool.cool;
                    }
                }
                model.user.potions.push(new PotionDataWrapper(itemBelt,cool));
            }
            view._beltList.dataProvider = new ListCollection(model.user.potions);
            
            //magic list
            for each (var magic:MagicVO in MainModel.magicData) {
                if (magic.visible && model.user.elementEnum == magic.element && magic.level.number <= model.user.level) {
                    cool = 0;
                    for each (var mcool:DataBattleMagicCool in model.user.magicCools) {
                        if (mcool.magicId == magic.battleMagicId) {
                            cool = mcool.cool;
                        }
                    }
                    model.user.addMagic(magic.battleMagicId, cool);
                }
            }
            view._magicList.dataProvider = new ListCollection(model.user.magic);

            updateUserStatic(true);
            updateUserDynamic(true);
            updatePoints(true);
            updateUserStatic(false);
            updateUserDynamic(false);
            updatePoints(false);

            if (model.currentMemberId) {
                turnMember();
            }

            view.particleTestPanel.startParticle.addEventListener(Event.TRIGGERED, triggeredHandler);
        }

        private function triggeredHandler(event:Event):void {
            switch (event.target) {
                case view.particleTestPanel.startParticle:
                    assetAppManager = new AssetManager(1);
                    assetAppManager.enqueue("assets/media/"+view.particleTestPanel.filename.text+".png");
                    assetAppManager.enqueue("assets/media/"+view.particleTestPanel.filename.text+".pex");
                    assetAppManager.loadQueue(assetManager_onProgress);
                    break;
            }
        }

        protected function assetManager_onProgress(progress:Number):void
        {
            if(progress !== 1) { return; }
            trace(assetAppManager.getXml(view.particleTestPanel.filename.text));
            var _particles: PDParticleSystem;
            _particles = new PDParticleSystem(assetAppManager.getXml(view.particleTestPanel.filename.text), assetAppManager.getTexture(view.particleTestPanel.filename.text));
            _particles.touchable = false;
            view.particleTestPanel.particlesLayer.addChild(_particles);
            Starling.juggler.add(_particles);
            
            _particles.emitterX = int(view.particleTestPanel.fromX.text);
            _particles.emitterY = int(view.particleTestPanel.fromY.text);

            if (view.particleTestPanel.toX.text.length && view.particleTestPanel.toY.text.length) {
                var t:Tween = new Tween(_particles, Number(view.particleTestPanel.timeInput.text), view.particleTestPanel.tweenList.selectedItem.data);
                t.animate("emitterX", int(view.particleTestPanel.toX.text));
                t.animate("emitterY", int(view.particleTestPanel.toY.text));
                t.onComplete = function():void { _particles.stop(); Starling.juggler.remove(_particles); view.particleTestPanel.particlesLayer.removeChild(_particles);_particles.dispose() };
                Starling.juggler.add(t);
                _particles.start();
            } else {
                _particles.start(Number(view.particleTestPanel.timeInput.text));
            }
        }

        protected function onTimer(event:TimerEvent):void {
            view._timerLabel.text = String(model.leftTime - time.currentCount);
        }
    
        protected function onTimerComplete(event:TimerEvent):void
        {
            view._timerLabel.text = "0";
        }

        private function battleActionProcess():void
        {
            if (_queueBattleAction.length > 0)
            {
                _queueBattleImpactFinish = false;
                var item:QueueBattleActionObject = _queueBattleAction.shift();
                switch (item.action.type) {
                    case DataBattleActionType.ADD_CELL:
                    case DataBattleActionType.REMOVE_CELL:
                    case DataBattleActionType.SHUFFLE_CELL:
                    case DataBattleActionType.SWAP_CELL:
                    case DataBattleActionType.WRONG_CELL:
                    if (item.parent) {
                        _delayCallDoAction = Starling.juggler.delayCall(doAction, Match3View.GEM_CHANGE_TIME, item);
                    } else {
                        doAction(item);
                    }
                        break;
                    default:
                        doAction(item);
                }
            }
            else if (_queueBattleMagic.length > 0)
            {
                _currenBattleObject = _queueBattleMagic.shift();
                if (_currenBattleObject.actions.length > 0) {
                    switch (_currenBattleObject.type) {
                        case QueueBattleObject.TYPE_MAGIC:
                            if (_currenBattleObject.magic.magicId && _currenBattleObject.actions.length > 0) {
                                addMessageToLog(model.membersWrapper[_currenBattleObject.actions[0].source.id].name + " использовал " + MainModel.magicData[_currenBattleObject.magic.magicId].title.ru);
                                if (_currenBattleObject.actions[0].source.id == model.user.id) {
                                    model.user.magicUpdateCool(_currenBattleObject.magic);
                                    view._magicList.dataProvider.updateAll();
                                }
                            }
                            break;
                        case QueueBattleObject.TYPE_POTION:
                            if (_currenBattleObject.potion.potionId && _currenBattleObject.actions.length > 0) {
                                addMessageToLog(model.membersWrapper[_currenBattleObject.actions[0].source.id].name + " использовал " + MainModel.potionData[_currenBattleObject.potion.potionId].title.ru);
                                if (_currenBattleObject.actions[0].source.id == model.user.id) {
                                    model.user.potionUpdateCool(_currenBattleObject.potion);
                                    view._beltList.dataProvider.updateAll();
                                }
                            }
                            break;
                        case QueueBattleObject.TYPE_BUFF:
                            if (_currenBattleObject.buff.buffId && _currenBattleObject.actions.length > 0) {
                                
                                var destinationNames:Array = [];
                                for each (var uu:DataBattleMember in _currenBattleObject.actions[0].destination) {
                                    destinationNames.push(model.getUser(uu.id).name);
                                }
                                
                                var prefix:String = "У ";
                                if(BuffVO(MainModel.buffData[_currenBattleObject.buff.buffId]).battleBuff == BuffEnum.REFLECT_DAMAGE_TO_DAMAGE ||
                                BuffVO(MainModel.buffData[_currenBattleObject.buff.buffId]).battleBuff == BuffEnum.REFLECT_DAMAGE_TO_HEAL) {
                                    prefix = "На ";
                                }
                                
                                addMessageToLog(prefix + destinationNames.join(",") + 
                                        " сработал " + MainModel.buffData[_currenBattleObject.buff.buffId].title.ru);
                            }
                            break;
                    }
                }
                addActions(null, _currenBattleObject.actions);
                battleActionProcess();
            }
            else
            {
                _queueBattleImpactFinish = true;
                if (_gameEndedFlag) {
                    view.endGame(model.areaWidth, model.areaHeight);
                }
            }
        }

        private function battleEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case BattleEventType.BATTLE_MAGIC_CLIENT_COMMAND:
                    _queueBattleMagic.push(new QueueBattleObject(model.currentMagic));
                    if (_queueBattleImpactFinish) {
                        battleActionProcess();
                    }
                break;
                case BattleEventType.BATTLE_POTION_CLIENT_COMMAND:
                    _queueBattleMagic.push(new QueueBattleObject(model.currentPotion));
                    if (_queueBattleImpactFinish) {
                        battleActionProcess();
                    }
                break;
                case BattleEventType.BATTLE_SWAP_CLIENT_COMMAND:
                    turnMember();
                break;

                case BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND:
                    if (_queueBattleImpactFinish) {
                        view.endGame(model.areaWidth, model.areaHeight);
                    } else {
                        _gameEndedFlag = true;
                    }
                break;

                case BattleEventType.BATTLE_MEMBER_JOIN_CLIENT_COMMAND:
                    var m:UserDataWrapper = new UserDataWrapper(model.memberJoin);
                    model.membersWrapper[m.id] = m;
                    var targetCollection:ListCollection;
                    if (model.memberJoin.side == model.user.side) {
                        targetCollection = userListLeftCollection;
                    } else {
                        targetCollection = userListRightCollection;
                    }
                    var len:int = targetCollection.length;
                    for (var i:int=0; i < len; i++) {
                        var item:Object = targetCollection.getItemAt(i);
                        if (item is String && item.substr(0,5) == "empty") {
                            targetCollection.setItemAt(m, i);
                            break;
                        }
                    }
                    if (i >= 10) {
                        targetCollection.addItem(m);
                    }
                break;
                case BattleEventType.BATTLE_BUFF_EFFECT: //эффект
                case BattleEventType.BATTLE_BUFF_COLLISION: //взаимодействие с экшеном
                    _queueBattleMagic.push(new QueueBattleObject(model.currentBuff));
                    if (_queueBattleImpactFinish) {
                        battleActionProcess();
                    }
                break;
                case BattleEventType.BATTLE_BUFF_BEGIN: //начало
                    _queueBattleMagic.push(new QueueBattleObject(model.currentBuff));
                    if (_queueBattleImpactFinish) {
                        battleActionProcess();
                    }

                    var buff:DataBattleBuff = model.currentBuff.buff;
                    model.getUser(buff.destination).addBuff(buff);

                    if (buff.destination == model.user.id) {
                        view._leftTabCounterLabel.text = model.user.buffs.length.toString();
                        view._userBuffLeft.dataProvider = new ListCollection(model.user.buffs);
                        updateUserDynamic(true);
                        updateUserStatic(true);
                    }
                    if (buff.destination == model.userTarget.id) {
                        view._rightTabCounterLabel.text = model.userTarget.buffs.length.toString();
                        view._userBuffRight.dataProvider = new ListCollection(model.userTarget.buffs);
                        updateUserDynamic(false);
                        updateUserStatic(false);
                    }

                    break;
                case BattleEventType.BATTLE_BUFF_INTERRUPT: //прерывание
                case BattleEventType.BATTLE_BUFF_END:
                    // закончился баф
                    buff = model.currentBuff.buff;
                    model.getUser(buff.destination).deleteBuff(buff);
                    if (model.getUser(buff.destination).id == model.user.id) {
                        view._leftTabCounterLabel.text = model.user.buffs.length.toString();
                        view._userBuffLeft.dataProvider = new ListCollection(model.user.buffs);
                        updateUserDynamic(true);
                        updateUserStatic(true);
                    }
                    if (model.getUser(buff.destination).id == model.userTarget.id) {
                        view._rightTabCounterLabel.text = model.userTarget.buffs.length.toString();
                        view._userBuffRight.dataProvider = new ListCollection(model.userTarget.buffs);
                        updateUserDynamic(false);
                        updateUserStatic(false);
                    }

                    break;
                case BattleEventType.BATTLE_KICK_CLIENT_COMMAND:
                    model.getUser(model.currentKickMemberId).tickCooldown();

                    if (model.currentKickMemberId == model.user.id) {
                        view._magicList.dataProvider.updateAll();
                        view._beltList.dataProvider.updateAll();
                        view._userBuffLeft.dataProvider.updateAll();
                        view._userBuffLeft.dataProvider.updateAll();
                    }
                    if (model.currentKickMemberId == model.userTarget.id) {
                        view._userBuffRight.dataProvider.updateAll();
                    }
                    break;
            }
        }

        private function addActions(parent:DataBattleAction, data:Array):void
        {
            for each (var action:DataBattleAction in data) {
                _queueBattleAction.push(new QueueBattleActionObject(parent, action));
                if (action.children) {
                    addActions(action,action.children);
                }
            }
        }

        private function doAction(actionObject:QueueBattleActionObject):void
        {
            var currentCell:DataAreaCell;
            switch (actionObject.action.type) {
                case DataBattleActionType.TAKE_STEP:
                    //забрать ход
                    console.log("doAction.TAKE_STEP");
                    break;
                case DataBattleActionType.GIVE_STEP:
                    //дать ход
                    console.log("doAction.GIVE_STEP");
                    break;

                case DataBattleActionType.ADD_CELL:
                    console.log("doAction.ADD_CELL");
                    view.addCells(actionObject.action.addCell.cells);
                    break;
                case DataBattleActionType.REMOVE_CELL:
                    console.log("doAction.REMOVE_CELL");
                    currentCell = actionObject.action.removeCell.cells[0];
                    view.removeCells(actionObject.action.removeCell.cells);
                    break;
                case DataBattleActionType.SHUFFLE_CELL:
                    console.log("doAction.SHUFFLE_CELL");
                    model.areaCells = actionObject.action.shuffleCell.cells;
                    view.drawGreed(model.areaWidth, model.areaHeight, model.areaCells);
                    break;
                case DataBattleActionType.SWAP_CELL:
                    console.log("doAction.SWAP_CELL");
                    view.swapCell(actionObject.action.swapCell.cells);
                    break;
                case DataBattleActionType.WRONG_CELL:
                    console.log("doAction.WRONG_CELL");
                    view.swapWrongCell(actionObject.action.wrongCell.cells[0], actionObject.action.wrongCell.cells[1]);
                    break;
                case DataBattleActionType.REMOVE_CELL_ALL:
                    console.log("doAction.REMOVE_CELL_ALL");
                    break;
                case DataBattleActionType.REMOVE_CELL_FORM:
                    console.log("doAction.REMOVE_CELL_FORM");
                    break;
                case DataBattleActionType.REMOVE_CELL_LINE:
                    console.log("doAction.REMOVE_CELL_LINE");
                    break;
                case DataBattleActionType.REMOVE_CELL_TYPE:
                    console.log("doAction.REMOVE_CELL_TYPE");
                    break;
                case DataBattleActionType.REPLACE_CELL:
                    console.log("doAction.REPLACE_CELL");
                    view.removeCells(actionObject.action.replaceCell.cells);
                    view.addCells(actionObject.action.replaceCell.cells);
                    break;
                case DataBattleActionType.REPLACE_CELL_TYPE:
                    console.log("doAction.REPLACE_CELL_TYPE");
                    view.removeCells(actionObject.action.replaceCellType.cells);
                    view.addCells(actionObject.action.replaceCellType.cells);
                    break;
                case DataBattleActionType.RUN_CELL:
                    //проиграть камни
                    console.log("doAction.RUN_CELL");
                    break;

                case DataBattleActionType.GIVE_BELT:
                    // дать расходку
                    console.log("doAction.GIVE_BELT");
                    break;
                case DataBattleActionType.TAKE_BELT:
                    console.log("doAction.TAKE_BELT");
                    // забрать расходку
                    var removeFlag:Boolean = model.getUser(actionObject.action.source.id).deletePotion(actionObject.action.takeBelt.belt);
                    if (actionObject.action.source.id == model.user.id) {
                        if (removeFlag) {
                            view._beltList.dataProvider = new ListCollection(model.user.potions);
                        } else {
                            view._beltList.dataProvider.updateAll();
                        }
                    }

                    break;
                case DataBattleActionType.GIVE_BUFF:
                    // дать баф
                    console.log("doAction.GIVE_BUFF", actionObject.action.giveBuff.buff.id, actionObject.action.giveBuff.buff.buffId);
                    break;
                case DataBattleActionType.TAKE_NEGATIVE_BUFF:
                    //забрать баф
                    console.log("doAction.TAKE_NEGATIVE_BUFF");
                    break;
                case DataBattleActionType.TAKE_NEUTRAL_BUFF:
                    //забрать баф
                    console.log("doAction.TAKE_NEUTRAL_BUFF");
                    break;
                case DataBattleActionType.TAKE_POSITIVE_BUFF:
                    //забрать баф
                    console.log("doAction.TAKE_POSITIVE_BUFF");
                    break;
                case DataBattleActionType.GIVE_POINT:
                    // дать камней
                    console.log("doAction.GIVE_POINT", actionObject.action.givePoint.point, actionObject.action.source.id);
                    if (actionObject.action.givePoint.point.value == 0) break;
                    model.getUser(actionObject.action.source.id).givePoints(actionObject.action.givePoint.point);
                    if (actionObject.action.source.id == model.user.id) {
                        updatePoints(true);
                        if (actionObject.parent && actionObject.parent.type == DataBattleActionType.REMOVE_CELL) {
                            view.givePoint(actionObject.parent.removeCell.cells[0], actionObject.action.givePoint.point.value);
                        }
                    }
                    if (actionObject.action.source.id == model.userTarget.id) {
                        updatePoints(false);
                    }

                    var color:String;
                    if (_currenBattleObject.type == QueueBattleObject.TYPE_POTION) {
                        switch (actionObject.action.givePoint.point.type) {
                            case DataBattlePointType.DARK:
                                color = "<font color='#a350da'>тьмы</font> ";
                                break;
                            case DataBattlePointType.FIRE:
                                color = "<font color='#c13d40'>огня</font> ";
                                break;
                            case DataBattlePointType.LIGHT:
                                color = "<font color='#c7913f'>света</font> ";
                                break;
                            case DataBattlePointType.NATURE:
                                color = "<font color='#649f21'>природы</font> ";
                                break;
                            case DataBattlePointType.WATER:
                                color = "<font color='#1d8fd8'>воды</font> ";
                                break;
                        }

                        addMessageToLog(model.membersWrapper[actionObject.action.source.id].name + " получил " + actionObject.action.givePoint.point.value + " камней " + color);
                    }
                    break;
                case DataBattleActionType.TAKE_POINT:
                    console.log("doAction.TAKE_POINT", actionObject.action.takePoint.point, actionObject.action.source.id, model.user.id);
                    if (actionObject.action.takePoint.point.value == 0) break;
                    model.getUser(actionObject.action.source.id).takePoints(actionObject.action.takePoint.point);
                    if (actionObject.action.source.id == model.user.id) {
                        updatePoints(true);
                    }
                    if (actionObject.action.source.id == model.userTarget.id) {
                        updatePoints(false);
                    }
                    break;

                case DataBattleActionType.DAMAGE:
                case DataBattleActionType.DAMAGE_OVER_PROTECTION:
                    console.log("doAction.DAMAGE or DAMAGE_OVER_PROTECTION", actionObject.action.damage, actionObject.action.damageOverProtection);

                    if (actionObject.action.type == DataBattleActionType.DAMAGE) {
                        var damageType:int = actionObject.action.damage.damage;
                        var damage:int = actionObject.action.damage.value;
                    } else if (actionObject.action.type == DataBattleActionType.DAMAGE_OVER_PROTECTION) {
                        damageType = actionObject.action.damageOverProtection.damage;
                        damage = actionObject.action.damageOverProtection.value;
                    }

                    if (damage == 0 || actionObject.action.destination.length == 0) break;

                    for each (var destination:DataBattleMember in actionObject.action.destination) {
                        updateUserHp(model.getUser(destination.id), -damage);
                    }
                        
                    //view.flyingDamage(damageType, damage, getDestinationNames(actionObject.action.destination).join(","));
                        
                    var message:String;
                    var damageText:String = "";

                    switch (damageType) {
                        case DataBattleActionDamageType.DAMAGE:
                            damageText = " ";
                            break;
                        case DataBattleActionDamageType.DAMAGE_DARK:
                            damageText = "<font color='#a350da'>тьмой</font> ";
                            break;
                        case DataBattleActionDamageType.DAMAGE_FIRE:
                            damageText = "<font color='#c13d40'>огнем</font> ";
                            break;
                        case DataBattleActionDamageType.DAMAGE_LIGHT:
                            damageText = "<font color='#c7913f'>светом</font> ";
                            break;
                        case DataBattleActionDamageType.DAMAGE_NATURE:
                            damageText = "<font color='#649f21'>природой</font> ";
                            break;
                        case DataBattleActionDamageType.DAMAGE_WATER:
                            damageText = "<font color='#1d8fd8'>водой</font> ";
                            break;
                    }

                    message = model.getUser(actionObject.action.source.id).name + " нанес";
                    if (actionObject.action.type == DataBattleActionType.DAMAGE_OVER_PROTECTION) {
                        message += " через броню";
                    }
                    message +=  " <strong><font color='#FF0000'>" + damage + "</font></strong> урона ";
                    message += damageText + getDestinationNames(actionObject.action.destination).join(",");
                    addMessageToLog(message);
                    break;
                case DataBattleActionType.HEAL:
                    console.log("doAction.HEAL", actionObject.action.heal);

                    if (actionObject.action.heal.value == 0 || actionObject.action.destination.length == 0) break;

                    for each (destination in actionObject.action.destination) {
                        updateUserHp(model.getUser(destination.id), actionObject.action.heal.value);
                    }
                    var healText:String = "";
                    switch (actionObject.action.heal.heal) {
                        case DataBattleActionHealType.HEAL:
                            healText = " ";
                            break;
                        case DataBattleActionHealType.HEAL_DARK:
                            healText = "<font color='#a350da'>тьмой</font> ";
                            break;
                        case DataBattleActionHealType.HEAL_FIRE:
                            healText = "<font color='#c13d40'>огнем</font> ";
                            break;
                        case DataBattleActionHealType.HEAL_LIGHT:
                            healText = "<font color='#c7913f'>светом</font> ";
                            break;
                        case DataBattleActionHealType.HEAL_NATURE:
                            healText = "<font color='#649f21'>природой</font> ";
                            break;
                        case DataBattleActionHealType.HEAL_WATER:
                            healText = "<font color='#1d8fd8'>водой</font> ";
                            break;
                    }
                    message = model.getUser(actionObject.action.source.id).name + " восстановил <strong><font color='#00FF00'>" +actionObject.action.heal.value;
                    message += "</font></strong> здоровья " + healText + getDestinationNames(actionObject.action.destination).join(",");
                    addMessageToLog(message);
                    break;
                case DataBattleActionType.RESURRECTION:
                    for each (destination in actionObject.action.destination) {
                        message = model.getUser(actionObject.action.source.id).name + " восскресил " + model.getUser(destination.id).name;
                        updateUserHp(model.getUser(destination.id), actionObject.action.resurrection.health);
                    }

                    break;
            }

            battleActionProcess();
        }

        private function getDestinationNames(users:Array):Array
        {
            var destinationNames:Array = [];
            for each (var destination:DataBattleMember in users) {
                destinationNames.push(model.getUser(destination.id).name);
            }
            return destinationNames;
        }

        private function addMessageToLog(message:String):void
        {
            if (textLines.length > 5) {
                textLines.shift();
            }

            textLines.push(message);

            view._logLabel.text = "";
            for each (var m:String in textLines) {
                view._logLabel.text += m + "\n";
            }
        }

        private function updateUserHp(user:UserDataWrapper, value:int):void {
            user.updateHp(value);
            if (user.id == model.user.id) {
                updateUserDynamic(true);
            } else  if (user.id == model.userTarget.id) {
                updateUserDynamic(false);

                // если текущая цель умерла - попытаться выбрать другую
                if (user.hp == 0) {
                    for (var i:int=0; i < userListRightCollection.length; i++) {
                        var uu:UserDataWrapper = userListRightCollection.getItemAt(i) as UserDataWrapper;
                        if (uu && uu.hp > 0) {
                            model.userTarget = uu;
                            updateUserStatic(false);
                            updateUserDynamic(false);
                            updatePoints(false);
                            break;
                        }
                    }
                }
            }
            userListLeftCollection.updateAll();
            userListRightCollection.updateAll();
        }

        private function turnMember():void {

            //left user list
            for (var i:int=0; i < userListLeftCollection.length; i++) {
                var uu:UserDataWrapper = userListLeftCollection.getItemAt(i) as UserDataWrapper;
                if (!uu) continue;
                if (uu.selected && model.currentMemberId != uu.id) {
                    uu.selected = false;
                    userListLeftCollection.updateItemAt(i);
                } else if (model.currentMemberId == uu.id) {
                    uu.selected = true;
                    userListLeftCollection.updateItemAt(i);
                }
            }

            //right user list
            for (i=0; i < userListRightCollection.length; i++) {
                uu = userListRightCollection.getItemAt(i) as UserDataWrapper;
                if (!uu) continue;
                if (uu.selected && model.currentMemberId != uu.id) {
                    uu.selected = false;
                    userListRightCollection.updateItemAt(i);
                } else if (model.currentMemberId == uu.id) {
                    uu.selected = true;
                    userListRightCollection.updateItemAt(i);
                }
            }

            var currentMember:UserDataWrapper = model.membersWrapper[model.currentMemberId];
            view.turnLeft(currentMember.id == model.user.id, currentMember.side == model.user.side, currentMember.name);
            turnBlocked(model.currentMemberId != model.user.id);
            
            time.reset();
            time.repeatCount = model.leftTime;
            view._timerLabel.text = model.leftTime.toString();
            time.start();
        }

        private function turnBlocked(value:Boolean):void {
            _turnBlocked = value;
            view._fieldContainer.alpha = value ? 0.8 : 1;
            view.turnBlocked = value;
            view._magicList.alpha = value ? 0.6 : 1;
        }

        private function viewHandler(event:Event):void {
            switch (event.type) {
                case Match3View.MOVE_GEMS:
                    if (_turnBlocked) {
                        return;
                    }
                    turnBlocked(true);
                    view._fieldContainer.alpha = 1;
                    var e:Match3Event = new Match3Event(Match3Event.MOVE);
                    e.swapFirstCell = event.data.swapFirstCell;
                    e.swapSecondCell = event.data.swapSecondCell;
                    e.source = model.user.getSource();
                    e.destination = [model.userTarget.getSource()];
                    dispatch(e);
                break;
                case Match3View.END_GAME:
                    dispatch(new GameEvent(Match3Event.GAME_ENDED));
                break;
            }
        }

        //выбор пользователя в правом списке
        private function userListLeft_changeHandler(event:Event):void {
            
            var list:List = List( event.currentTarget );
            var uu:UserDataWrapper = list.selectedItem  as UserDataWrapper;
            // нельзя выбрать мертвого
            if (uu && uu.hp > 0) {
                model.userTarget = uu;
                updateUserStatic(false);
                updateUserDynamic(false);
                updatePoints(false);
            }
        }

        private function magicList_changeHandler(event:Event):void {

            var list:List = List( event.currentTarget );
            if (!list.selectedItem) {
                return;
            }
            if (_turnBlocked) {
                list.selectedItem = null;
                return;
            }

            magicInfo = new MagicInfo(MagicDataWrapper(list.selectedItem), model.user.elementEnum);
            magicInfo.addEventListener("useMagic", useMagicEventHandler);
            view.stage.addChild(magicInfo);

            list.selectedItem = null;
            //turnBlocked(true);
        }

        private function buffList_changeHandler(event:Event):void {

            var list:List = List( event.currentTarget );
            if (!list.selectedItem) {
                return;
            }

            buffInfo = new BuffInfo(list.selectedItem as DataBattleBuff);
            view.addChild(buffInfo);

            list.selectedItem = null;
            //turnBlocked(true);
        }
        
        private function useMagicEventHandler(event:Event):void
        {
            var e:Match3Event = new Match3Event(Match3Event.USE_SPELL);

            e.idSpell = event.data as int;
            e.destination = [model.userTarget.getSource()];
            e.source = model.user.getSource();
            dispatch(e);
        }

        private function beltList_changeHandler(event:Event):void {

            var list:List = List( event.currentTarget );
            if (!list.selectedItem) {
                return;
            }

            potionInfo = new PotionInfo(list.selectedItem as PotionDataWrapper);
            potionInfo.addEventListener("usePotion", usePotionEventHandler);
            view.addChild(potionInfo);
            
            //turnBlocked(true);
            list.selectedItem = null;
        }

        private function usePotionEventHandler(event:Event):void
        {
            var e:Match3Event = new Match3Event(Match3Event.USE_POTION);

            e.artikulBelt = event.data as DataBattleArtikulBelt;
            e.destination = [model.userTarget.getSource()];
            e.source = model.user.getSource();
            dispatch(e);
        }

        private function updateUserStatic(leftUser:Boolean):void {
            var user:UserDataWrapper;
            var userNickLabel:TextField;
            var userLeveLabel:TextField;
            var skill:Sprite;
            var avatar:Image;
            var fraction:Image;
            var avatarLoader:ImageLoader;
            if (leftUser) {
                user = model.user;
                userNickLabel = view._userLeftNickLabel;
                userLeveLabel = view._userLeftLeveLabel;
                avatar = view._userLeftAvatar;
                fraction = view._userLeftFraction;
                avatarLoader = view._userLeftAvatarLoader;
                skill = view._skillLeftPanel;
            } else {
                user = model.userTarget;
                userNickLabel = view._userRightNickLabel;
                userLeveLabel = view._userRightLeveLabel;
                avatar = view._userRightAvatar;
                fraction = view._userRightFraction;
                avatarLoader = view._userRightAvatarLoader;
                skill = view._skillRightPanel;
            }

            userNickLabel.text = user.name;

            if (user.type == DataBattleMemberType.PLAYER) {
                avatarLoader.source = null;
                avatar.visible = true;
                fraction.visible = true;

                switch (user.player.fraction) {
                    case DataPlayerFraction.FIRST:
                        fraction.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_1");
                        break;
                    case DataPlayerFraction.SECOND:
                        fraction.texture = AppTheme.instance.assets.getTexture("fraction/fraction_ico_2");
                        break;
                }
                switch (user.player.sex) {
                    case DataPlayerSex.MALE:
                        switch (user.elementEnum) {
                            case ElementEnum.DARK:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_dark");
                                break;
                            case ElementEnum.FIRE:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_fire");
                                break;
                            case ElementEnum.LIGHT:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_light");
                                break;
                            case ElementEnum.NATURE:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_nature");
                                break;
                            case ElementEnum.WATER:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/male_water");
                                break;
                        }
                        break;
                    case DataPlayerSex.FEMALE:
                        switch (user.elementEnum) {
                            case ElementEnum.DARK:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_dark");
                                break;
                            case ElementEnum.FIRE:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_fire");
                                break;
                            case ElementEnum.LIGHT:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_light");
                                break;
                            case ElementEnum.NATURE:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_nature");
                                break;
                            case ElementEnum.WATER:
                                avatar.texture = AppTheme.instance.assets.getTexture("avatar/circle/female_water");
                                break;
                        }
                        break;
                }
            }
            if (user.type == DataBattleMemberType.MOB) {
                avatar.visible = false;
                fraction.visible = false;
                avatarLoader.source = ConnectionService.BASE_URL_HTTP+MainModel.mobData[user.mob.id].imageAvatar.url;
            }

            userLeveLabel.text = user.level.toString();

            var currentSkill:TextField;
            currentSkill = skill.getChildByName("damage") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE).toString();
            currentSkill = skill.getChildByName("protection") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION).toString();
            currentSkill = skill.getChildByName("damage_dark") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE_DARK).toString();
            currentSkill = skill.getChildByName("protection_dark") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION_DARK).toString();
            currentSkill = skill.getChildByName("damage_fire") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE_FIRE).toString();
            currentSkill = skill.getChildByName("protection_fire") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION_FIRE).toString();
            currentSkill = skill.getChildByName("damage_light") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE_LIGHT).toString();
            currentSkill = skill.getChildByName("protection_light") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION_LIGHT).toString();
            currentSkill = skill.getChildByName("damage_water") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE_WATER).toString();
            currentSkill = skill.getChildByName("protection_water") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION_WATER).toString();
            currentSkill = skill.getChildByName("damage_nature") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.DAMAGE_NATURE).toString();
            currentSkill = skill.getChildByName("protection_nature") as TextField;
            currentSkill.text = user.getSkill(DataBattleSkillType.PROTECTION_NATURE).toString();
        }
    
        private function updateUserDynamic(leftUser:Boolean):void {
            var user:UserDataWrapper;
            var userHPLabel:TextField;
            var userHPPercentLabel:TextField;
            var userHPBar:Image;
            var userHPMaxWidth:int;
            var damage:PDParticleSystem;

            if (leftUser) {
                user = model.user;
                userHPLabel = view._userLeftHPLabel;
                userHPPercentLabel = view._userLeftHPPercentLabel;
                userHPBar = view._userLeftHPBar;
                userHPMaxWidth = view.userLeftHPMaxWidth;
                damage = view.leftDamage;
            } else {
                user = model.userTarget;
                userHPLabel = view._userRightHPLabel;
                userHPPercentLabel = view._userRightHPPercentLabel;
                userHPBar = view._userRightHPBar;
                userHPMaxWidth = view.userRightHPMaxWidth;
                damage = view.rightDamage;
            }

            userHPLabel.text = user.hp + "/" + user.hpTotal;
            userHPPercentLabel.text = int(user.hp / user.hpTotal * 100) + "%";

            var newWidth:int = userHPMaxWidth * user.hp / user.hpTotal;
            if (newWidth > userHPMaxWidth) newWidth = userHPMaxWidth;
            if (newWidth < userHPBar.width) {
                damage.start(0.1);
            }
            //userHPBar.width = newWidth;
            var tween:Tween = new Tween(userHPBar, 0.4, Transitions.EASE_OUT);
            tween.animate("width",newWidth);
            Starling.juggler.add(tween);
        }

        private function updatePoints(leftUser:Boolean):void
        {
            if (!view) {return;}

            var user:UserDataWrapper;
            var pointDark:TextField;
            var pointFire:TextField;
            var pointLight:TextField;
            var pointNature:TextField;
            var pointWater:TextField;

            if (leftUser) {
                user = model.user;
                pointDark = view._pointLeftDark;
                pointFire = view._pointLeftFire;
                pointLight = view._pointLeftLight;
                pointNature = view._pointLeftNature;
                pointWater = view._pointLeftWater;
            } else {
                user = model.userTarget;
                pointDark = view._pointRightDark;
                pointFire = view._pointRightFire;
                pointLight = view._pointRightLight;
                pointNature = view._pointRightNature;
                pointWater = view._pointRightWater;
            }

            pointDark.text = user.points[DataBattlePointType.DARK];
            pointFire.text = user.points[DataBattlePointType.FIRE];
            pointLight.text = user.points[DataBattlePointType.LIGHT];
            pointNature.text = user.points[DataBattlePointType.NATURE];
            pointWater.text = user.points[DataBattlePointType.WATER];

            if (leftUser && view._magicList) {
                model.user.magicUpdatePoint();
                view._magicList.dataProvider.updateAll();
             }
        }
}
}