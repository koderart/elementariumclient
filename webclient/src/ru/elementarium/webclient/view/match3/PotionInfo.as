package ru.elementarium.webclient.view.match3 {
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.PopupView;
import ru.muvs.admin.main.vo.battle.PotionVO;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.filters.ColorMatrixFilter;
import starling.text.TextField;

public class PotionInfo extends PopupView
{
    public var _mainLayout:LayoutGroup;
    public var _titleText:TextField;
    public var _descriptionText:TextField;
    public var _reloadingText:TextField;

    public var _buttonOk:Button;
    public var _buttonCancel:Button;

    private var _potion:PotionDataWrapper;

    public function PotionInfo(potion:PotionDataWrapper)
    {
        this._potion = potion;

        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_potion_info, false, this) as Sprite;
        super();
        
        _buttonOk.addEventListener(Event.TRIGGERED, triggeredHandler);
        _buttonCancel.addEventListener(Event.TRIGGERED, triggeredHandler);

        draw();
    }

    public function draw():void
    {
        var data :PotionVO = MainModel.potionData[_potion.potionId];
        _titleText.text = data.title.ru;
        _descriptionText.text = data.description.ru;
        _reloadingText.text = data.cooldown.toString();

        if (_potion.coolDown > 0) {
            var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
            colorFilter.adjustSaturation(-1);
            _buttonOk.filter = colorFilter;
            _buttonOk.enabled = false;
        } else {
            _buttonOk.filter = null;
            _buttonOk.enabled = true;
        }

        _mainLayout.validate();
    }

    private function triggeredHandler(event:Event):void
    {
        switch (event.target) {
            case _buttonCancel:
                removeFromParent(true);
                break;
            case _buttonOk:
                dispatchEventWith("usePotion", false, _potion.artikulBelt);
                removeFromParent(true);
                break;
        }
    }

}
}
