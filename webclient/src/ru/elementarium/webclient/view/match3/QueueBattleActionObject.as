/**
 * Created by Artem on 31.08.2016.
 */
package ru.elementarium.webclient.view.match3
{
import ru.muvs.elementarium.thrift.battle.action.DataBattleAction;

public class QueueBattleActionObject
{
    public var parent:DataBattleAction;
    public var action:DataBattleAction;
            
    public function QueueBattleActionObject(parent:DataBattleAction, action:DataBattleAction)
    {
        this.parent = parent;
        this.action = action;
    }
}
}
