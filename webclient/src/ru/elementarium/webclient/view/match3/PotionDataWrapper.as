package ru.elementarium.webclient.view.match3
{
import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;

public class PotionDataWrapper
{
    public var artikulBelt:DataBattleArtikulBelt;

    private var _coolDown:int = 0;

    public function PotionDataWrapper(data:DataBattleArtikulBelt, cooldown:int)
    {
        artikulBelt = data;
        _coolDown = cooldown;
    }

    public function get potionId():int
    {
        return artikulBelt.potionId;
    }

    public function get order():int
    {
        return artikulBelt.order;
    }

    public function get artikulId():int
    {
        return artikulBelt.artikul.artikulId;
    }

    public function get count():int
    {
        return artikulBelt.artikul.count;
    }

    public function set count(value:int):void
    {
        artikulBelt.artikul.count = value;
    }

    public function get coolDown():int
    {
        return _coolDown;
    }

    public function set coolDown(value:int):void
    {
        _coolDown = value;
    }
}
}
