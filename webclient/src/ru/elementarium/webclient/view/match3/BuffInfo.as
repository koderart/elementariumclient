package ru.elementarium.webclient.view.match3 {
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.PopupView;
import ru.muvs.admin.main.vo.battle.BuffVO;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;

import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

    public class BuffInfo extends PopupView
    {
        public var _mainLayout:LayoutGroup;
        public var _titleText:TextField;
        public var _descriptionText:TextField;
        public var _reloadingText:TextField;

        public var _buttonOk:Button;

        private var _buff:DataBattleBuff;

        public function BuffInfo(buff:DataBattleBuff)
        {
            this._buff = buff;

            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_buff_info, false, this) as Sprite;
            super();

            _buttonOk.addEventListener(Event.TRIGGERED, triggeredHandler);

            draw();
        }

        public function draw():void
        {
            var buff :BuffVO =  MainModel.buffData[_buff.buffId];
            _titleText.text = buff.title.ru;
            _descriptionText.text = buff.description.ru;
            if (_buff.duration == 0) {
                _reloadingText.text = "Постоянный";
            } else {
                _reloadingText.text = _buff.duration.toString();
            }
            _mainLayout.validate();
        }
    
        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _buttonOk:
                    removeFromParent(true);
                break;
            }
        }

    }
}
