package ru.elementarium.webclient.view.match3
{
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.battle.DataClientBattleMagic;
import ru.muvs.elementarium.thrift.battle.DataClientBattlePotion;
import ru.muvs.elementarium.thrift.battle.artikul.DataBattleArtikulBelt;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuff;
import ru.muvs.elementarium.thrift.battle.buff.DataBattleBuffType;
import ru.muvs.elementarium.thrift.battle.cool.DataBattleMagicCool;
import ru.muvs.elementarium.thrift.battle.cool.DataBattlePotionCool;
import ru.muvs.elementarium.thrift.battle.member.*;
import ru.muvs.elementarium.thrift.battle.point.*;
import ru.muvs.elementarium.thrift.battle.skill.*;

public class UserDataWrapper
{

    public var selected:Boolean;

    private var _dataBattle:DataBattleMember;
    private var _points:Array = [];
    private var _magic:Vector.<MagicDataWrapper> = new <MagicDataWrapper>[];
    private var _potions:Vector.<PotionDataWrapper> = new <PotionDataWrapper>[];

    private var _id:int;
    private var _elementEnum:String;
    private var _side:int;
    private var _name:String;
    private var _type:int;
    private var _player:DataBattleMemberPlayer;
    private var _mob:DataBattleMemberMob;

    private var _skills:Vector.<DataBattleSkill>;

    public function UserDataWrapper(data:DataBattleMember)
    {
        _dataBattle = data;

        _id = _dataBattle.id;

        _type = _dataBattle.type;
        _player = _dataBattle.player;
        _mob = _dataBattle.mob;

        if (_dataBattle.type == DataBattleMemberType.PLAYER) {
            _name = _dataBattle.player.name;
            switch (_dataBattle.player.element) {
                case DataPlayerElement.DARK:
                    _elementEnum = ElementEnum.DARK;
                    break;
                case DataPlayerElement.FIRE:
                    _elementEnum = ElementEnum.FIRE;
                    break;
                case DataPlayerElement.LIGHT:
                    _elementEnum = ElementEnum.LIGHT;
                    break;
                case DataPlayerElement.NATURE:
                    _elementEnum = ElementEnum.NATURE;
                    break;
                case DataPlayerElement.WATER:
                    _elementEnum = ElementEnum.WATER;
                    break;
            }
        }

        if (_dataBattle.type == DataBattleMemberType.MOB) {
            _name = MainModel.mobData[_dataBattle.mob.id].title.ru;
        }

        _skills = new Vector.<DataBattleSkill>(20, true);
        for each (var skillData:DataBattleSkill in _dataBattle.skills) {
            _skills[skillData.type] = skillData;
        }

        _side = _dataBattle.side;

        _points[DataBattlePointType.DARK] = 0;
        _points[DataBattlePointType.FIRE] = 0;
        _points[DataBattlePointType.LIGHT] = 0;
        _points[DataBattlePointType.NATURE] = 0;
        _points[DataBattlePointType.WATER] = 0;

        for each (var point:DataBattlePoint in _dataBattle.points) {
            _points[point.type] = point.value
        }
    }

    public function get id():int {
        return _id;
    }

    public function get elementEnum():String {
        return _elementEnum;
    }

    public function get level():int {
        return _skills[DataBattleSkillType.LEVEL].base;
    }

    public function get side():int {
        return _side;
    }

    public function getSource():DataBattleMember
    {
        var data:DataBattleMember = new DataBattleMember();
        data.id = id;
        return data;
    }

    public function get points():Array
    {
        return _points;
    }

    public function get belts():Array
    {
        return _dataBattle.belts;
    }

    public function get buffs():Array
    {
        return _dataBattle.buffs;
    }

    public function tickCooldown():void
    {
        for each (var buff:DataBattleBuff in _dataBattle.buffs) {
            if (buff.duration > 0) {
                buff.duration--;
            }
        }

        for each (var magic:MagicDataWrapper in _magic) {
            if (magic.coolDown > 0) {
                magic.coolDown--;
            }
        }

        for each (var potion:PotionDataWrapper in _potions) {
            if (potion.coolDown > 0) {
                potion.coolDown--;
            }
        }
    }

    public function addBuff(buff:DataBattleBuff):void
    {
        switch (buff.type) {
            case DataBattleBuffType.REDUCE_SKILL:
                _skills[buff.reduceSkill.skill.type] = buff.reduceSkill.skill;
                break;
            case DataBattleBuffType.REDUCE_SKILL_CURRENT:
                _skills[buff.reduceSkillCurrent.skill.type] = buff.reduceSkillCurrent.skill;
                break;
            case DataBattleBuffType.REDUCE_SKILL_TOTAL:
                _skills[buff.reduceSkillTotal.skill.type] = buff.reduceSkillTotal.skill;
                break;
            case DataBattleBuffType.INCREASE_SKILL:
                _skills[buff.increaseSkill.skill.type] = buff.increaseSkill.skill;
                break;
            case DataBattleBuffType.INCREASE_SKILL_CURRENT:
                _skills[buff.increaseSkillCurrent.skill.type] = buff.increaseSkillCurrent.skill;
                break;
            case DataBattleBuffType.INCREASE_SKILL_TOTAL:
                _skills[buff.increaseSkillTotal.skill.type] = buff.increaseSkillTotal.skill;
                break;
            case DataBattleBuffType.INVERT_IN_DAMAGE_TO_HEAL:
                break;
            case DataBattleBuffType.INVERT_OUT_DAMAGE_TO_HEAL:
                break;
            case DataBattleBuffType.STUN:
                break;
            case DataBattleBuffType.TICK:
                break;
        }
        _dataBattle.buffs.push(buff);
    }

    public function deleteBuff(buff:DataBattleBuff):void
    {
        var i:int = _dataBattle.buffs.length;
        while(i-- > 0) {
            if (_dataBattle.buffs[i].id == buff.id) {

                switch (buff.type) {
                    case DataBattleBuffType.REDUCE_SKILL:
                        _skills[buff.reduceSkill.skill.type] = buff.reduceSkill.skill;
                        break;
                    case DataBattleBuffType.REDUCE_SKILL_CURRENT:
                        _skills[buff.reduceSkillCurrent.skill.type] = buff.reduceSkillCurrent.skill;
                        break;
                    case DataBattleBuffType.REDUCE_SKILL_TOTAL:
                        _skills[buff.reduceSkillTotal.skill.type] = buff.reduceSkillTotal.skill;
                        break;
                    case DataBattleBuffType.INCREASE_SKILL:
                        _skills[buff.increaseSkill.skill.type] = buff.increaseSkill.skill;
                        break;
                    case DataBattleBuffType.INCREASE_SKILL_CURRENT:
                        _skills[buff.increaseSkillCurrent.skill.type] = buff.increaseSkillCurrent.skill;
                        break;
                    case DataBattleBuffType.INCREASE_SKILL_TOTAL:
                        _skills[buff.increaseSkillTotal.skill.type] = buff.increaseSkillTotal.skill;
                        break;
                    case DataBattleBuffType.INVERT_IN_DAMAGE_TO_HEAL:
                        break;
                    case DataBattleBuffType.INVERT_OUT_DAMAGE_TO_HEAL:
                        break;
                    case DataBattleBuffType.STUN:
                        break;
                    case DataBattleBuffType.TICK:
                        break;
                }

                _dataBattle.buffs.removeAt(i);
                return;
            }
        }
    }

    public function get magicCools():Array
    {
        return _dataBattle.magicCools;
    }

    public function get potionCools():Array
    {
        return _dataBattle.potionCools;
    }

    public function get name():String {
        return _name;
    }

    public function get hpTotal():int {
        return _skills[DataBattleSkillType.HP].total;
    }

    public function get hp():int {
        return _skills[DataBattleSkillType.HP].current;
    }

    public function get skills():Array {
        return _dataBattle.skills;
    }

    public function get type():int {
        return _type;
    }

    public function get player():DataBattleMemberPlayer {
        return _player;
    }

    public function get mob():DataBattleMemberMob {
        return _mob;
    }

    public function givePoints(point:DataBattlePoint):void
    {
        _points[point.type] += point.value;
    }

    public function takePoints(point:DataBattlePoint):void
    {
        _points[point.type] -= point.value;
        if (_points[point.type] < 0) {_points[point.type] = 0;}
    }

    public function updateHp(value:int):void {
        _skills[DataBattleSkillType.HP].current += value;
        if (_skills[DataBattleSkillType.HP].current <  0) {
            _skills[DataBattleSkillType.HP].current = 0;
        }
    }

    public function getSkill(type:int):int {
        return _skills[type].total;
    }

    public function get magic():Vector.<MagicDataWrapper>
    {
        return _magic;
    }

    public function addMagic(id:int, cool:int):void
    {
        _magic.push(new MagicDataWrapper(id, cool));
    }

    public function magicUpdateCool(dataMagic:DataClientBattleMagic):void
    {
        for each (var magic:MagicDataWrapper in _magic) {
            for each (var magicCool:DataBattleMagicCool in dataMagic.cools) {
                if (magic.magicId == magicCool.magicId) {
                    magic.coolDown = magicCool.cool;
                }
            }
        }
    }

    public function magicUpdatePoint():void
    {
        for each (var magic:MagicDataWrapper in _magic) {
            magic.userPoints = points;
        }
    }
    
    public function get potions():Vector.<PotionDataWrapper>
    {
        return _potions;
    }

    public function deletePotion(beltItem:DataBattleArtikulBelt):Boolean
    {
        var i:int = _potions.length;
        while(i-- > 0) {
            if (_potions[i].order == beltItem.order) {
                _potions[i].count--;
                if (_potions[i].count < 1) {
                    _potions.removeAt(i);
                    return true;
                }
            }
        }
        return false;
    }

    public function potionUpdateCool(dataPotion:DataClientBattlePotion):void
    {
        for each (var potion:PotionDataWrapper in _potions) {
            for each (var potionCool:DataBattlePotionCool in dataPotion.cools) {
                if (potion.potionId == potionCool.potionId) {
                    potion.coolDown = potionCool.cool;
                }
            }
        }
    }


}
}
