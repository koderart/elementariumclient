package ru.elementarium.webclient.view.match3
{
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.admin.main.vo.battle.ImpactConditionVO;
import ru.muvs.admin.main.vo.battle.MagicVO;
import ru.muvs.admin.main.vo.battle.battleenum.ImpactConditionEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePointType;

public class MagicDataWrapper
{
    public var magicId:int;

    private var _static:MagicVO;
    private var _userPoints:Array;
    private var _needPoints:Array = [];
    private var _coolDown:int = 0;

    public function MagicDataWrapper(id:int, cooldown:int)
    {
        magicId = id;
        _coolDown = cooldown;
        _static = MainModel.magicData[magicId];
        _needPoints[DataBattlePointType.DARK] = 0;
        _needPoints[DataBattlePointType.FIRE] = 0;
        _needPoints[DataBattlePointType.LIGHT] = 0;
        _needPoints[DataBattlePointType.NATURE] = 0;
        _needPoints[DataBattlePointType.WATER] = 0;

        if (_static.impact.impactExpression.impactCondition) {
            calcNeedPoint(_static.impact.impactExpression.impactCondition);
        }
    }

    private function calcNeedPoint(condition:ImpactConditionVO):void
    {
        if (condition.impactCondition == ImpactConditionEnum.HAVE_POINT) {
            switch (condition.point) {
                case ElementEnum.DARK:
                    _needPoints[DataBattlePointType.DARK] += condition.count;
                    break;
                case ElementEnum.FIRE:
                    _needPoints[DataBattlePointType.FIRE] += condition.count;
                    break;
                case ElementEnum.LIGHT:
                    _needPoints[DataBattlePointType.LIGHT] += condition.count;
                    break;
                case ElementEnum.NATURE:
                    _needPoints[DataBattlePointType.NATURE] += condition.count;
                    break;
                case ElementEnum.WATER:
                    _needPoints[DataBattlePointType.WATER] += condition.count;
                    break;
            }
        }
        if (condition.condition) {
            calcNeedPoint(condition.condition);
        }
        if (condition.next) {
            calcNeedPoint(condition.next);
        }
    }

    public function checkNeedPoint():Boolean
    {
        if (!_static.impact.impactExpression.impactCondition) {
            return false;
        }

        if (!_userPoints) {
            return false;
        }
        for (var type:String in _userPoints) {
            if (_userPoints[type] < _needPoints[type]) {
                return false;
            }
        }
        return true;
    }

    public function get userPoints():Array
    {
        return _userPoints;
    }

    public function set userPoints(value:Array):void
    {
        _userPoints = value;
    }

    public function get needPoints():Array
    {
        return _needPoints;
    }

    public function set needPoints(value:Array):void
    {
        _needPoints = value;
    }

    public function get coolDown():int
    {
        return _coolDown;
    }

    public function set coolDown(value:int):void
    {
        _coolDown = value;
    }

    public function get staticVO():MagicVO {
        return _static;
    }
}
}
