package ru.elementarium.webclient.view.match3 {
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.PopupView;
import ru.muvs.admin.main.vo.battle.MagicVO;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePointType;

import starling.display.Button;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.filters.ColorMatrixFilter;
import starling.text.TextField;

public class MagicInfo extends PopupView
    {
        public var _mainLayout:LayoutGroup;
        public var _titleText:TextField;
        public var _descriptionText:TextField;
        public var _reloadingText:TextField;
        public var _reloadingTitle:TextField;
        public var _needTitle:TextField;
    
        public var _needNatureText:TextField;
        public var _needDarkText:TextField;
        public var _needFireText:TextField;
        public var _needLightText:TextField;
        public var _needWaterText:TextField;

        public var _needNatureMarker:Image;
        public var _needDarkMarker:Image;
        public var _needFireMarker:Image;
        public var _needLightMarker:Image;
        public var _needWaterMarker:Image;
    
        public var _readyNatureMarker:Image;
        public var _readyDarkMarker:Image;
        public var _readyFireMarker:Image;
        public var _readyLightMarker:Image;
        public var _readyWaterMarker:Image;

        public var _buttonGroup:LayoutGroup;
        public var _buttonOk:Button;
        public var _buttonCancel:Button;

        private var _magic:MagicDataWrapper;
        private var _userElement:String;
        private var _hintMode:Boolean;

        public function MagicInfo(magic:MagicDataWrapper, userElement:String, hintMode:Boolean=false)
        {
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_magic_info, false, this) as Sprite;
            super();
            this._magic = magic;
            this._userElement = userElement;
            this._hintMode = hintMode;


            _buttonOk.addEventListener(Event.TRIGGERED, triggeredHandler);
            _buttonCancel.addEventListener(Event.TRIGGERED, triggeredHandler);

            draw();
        }

        public function draw():void
        {
            var data :MagicVO = MainModel.magicData[_magic.magicId];
            _titleText.text = data.title.ru;
            _descriptionText.text = data.description.ru;
            _reloadingText.text = data.cooldown.toString();
    
            _needNatureText.text = "";
            _needDarkText.text = "";
            _needFireText.text = "";
            _needLightText.text = "";
            _needWaterText.text = "";
    
            _needNatureMarker.visible = false;
            _needDarkMarker.visible = false;
            _needFireMarker.visible = false;
            _needLightMarker.visible = false;
            _needWaterMarker.visible = false;
    
            _readyNatureMarker.visible = false;
            _readyDarkMarker.visible = false;
            _readyFireMarker.visible = false;
            _readyLightMarker.visible = false;
            _readyWaterMarker.visible = false;
    
            if (_magic.needPoints[DataBattlePointType.NATURE]) {
                _needNatureMarker.visible = true;
                _needNatureText.text = _magic.needPoints[DataBattlePointType.NATURE];
                if (_magic.userPoints && _magic.userPoints[DataBattlePointType.NATURE] >= _magic.needPoints[DataBattlePointType.NATURE]) { _readyNatureMarker.visible = true; }
            }
            if (_magic.needPoints[DataBattlePointType.DARK]) {
                _needDarkMarker.visible = true;
                _needDarkText.text = _magic.needPoints[DataBattlePointType.DARK];
                if (_magic.userPoints && _magic.userPoints[DataBattlePointType.DARK] >= _magic.needPoints[DataBattlePointType.DARK]) { _readyDarkMarker.visible = true; }
            }
            if (_magic.needPoints[DataBattlePointType.FIRE]) {
                _needFireMarker.visible = true;
                _needFireText.text = _magic.needPoints[DataBattlePointType.FIRE];
                if (_magic.userPoints && _magic.userPoints[DataBattlePointType.FIRE] >= _magic.needPoints[DataBattlePointType.FIRE]) { _readyFireMarker.visible = true; }
            }
            if (_magic.needPoints[DataBattlePointType.LIGHT]) {
                _needLightMarker.visible = true;
                _needLightText.text = _magic.needPoints[DataBattlePointType.LIGHT];
                if (_magic.userPoints && _magic.userPoints[DataBattlePointType.LIGHT] >= _magic.needPoints[DataBattlePointType.LIGHT]) { _readyLightMarker.visible = true; }
            }
            if (_magic.needPoints[DataBattlePointType.WATER]) {
                _needWaterMarker.visible = true;
                _needWaterText.text = _magic.needPoints[DataBattlePointType.WATER];
                if (_magic.userPoints && _magic.userPoints[DataBattlePointType.WATER] >= _magic.needPoints[DataBattlePointType.WATER]) { _readyWaterMarker.visible = true; }
            }
    
            switch (_userElement) {
                case ElementEnum.DARK:
                    _titleText.format.color = 0x9D38E3;
                    break;
                case ElementEnum.FIRE:
                    _titleText.format.color = 0xDE212A;
                    break;
                case ElementEnum.LIGHT:
                    _titleText.format.color = 0xE4A21F;
                    break;
                case ElementEnum.NATURE:
                    _titleText.format.color = 0x5D9F16;
                    break;
                case ElementEnum.WATER:
                    _titleText.format.color = 0x1B7CAF;
                    break;
            }
            if (_hintMode) {
                _buttonGroup.removeChild(_buttonCancel);
            } else {
                if (_magic.userPoints == null || _magic.checkNeedPoint()) {
                    _buttonOk.filter = null;
                    _buttonOk.enabled = true;
                } else {
                    var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
                    colorFilter.adjustSaturation(-1);
                    _buttonOk.filter = colorFilter;
                    _buttonOk.enabled = false;
                }
            }
            _mainLayout.validate();
        }
    
        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _buttonCancel:
                    removeFromParent(true);
                break;
                case _buttonOk:
                    if (!_hintMode) {
                        dispatchEventWith("useMagic", false, _magic.magicId);
                    }
                    removeFromParent(true);
                break;
            }
        }

}
}
