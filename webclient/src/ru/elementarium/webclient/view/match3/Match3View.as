package ru.elementarium.webclient.view.match3 {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.Label;
import feathers.controls.LayoutGroup;
import feathers.controls.List;
import feathers.controls.TabBar;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.layout.FlowLayout;
import feathers.layout.HorizontalLayout;
import feathers.layout.HorizontalLayout;
import feathers.layout.VerticalLayout;

import flash.geom.Point;
import flash.geom.Rectangle;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.match3.components.BeltItemRenderer;
import ru.elementarium.webclient.view.match3.components.BuffItemRenderer;
import ru.elementarium.webclient.view.match3.components.GreedCell;
import ru.elementarium.webclient.view.match3.components.MagicItemRenderer;
import ru.elementarium.webclient.view.match3.components.UserItemRenderer;
import ru.muvs.elementarium.thrift.battle.action.DataBattleActionDamageType;
import ru.muvs.elementarium.thrift.battle.area.DataAreaCell;
import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.MovieClip;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.ResizeEvent;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.extensions.PDParticleSystem;
import starling.text.TextField;
import starling.text.TextFormat;
import starling.utils.Align;

    public class Match3View extends Sprite
    {
        public static var MOVE_GEMS:String = "Match3View.MOVE_GEMS";
        public static var END_GAME:String = "Match3View.END_GAME";
        public static var GEM_CHANGE_TIME:Number = 0.4;
    
        private var _sprite:Sprite;

        private static var GEM_SIZE:int = 70;
        private static var gems:Array = ['match3/gems/gem_0', 'match3/gems/gem_1', 'match3/gems/gem_2', 'match3/gems/gem_3', 'match3/gems/gem_4', 'match3/gems/gem_5'];
        
        private var selectedGreed:GreedCell;
        private var currentCell:GreedCell;
        private var _currentLineEndGame:int = 0;

        public var particleContainer:Sprite;
        public var _background:Image;
        public var _backgroundGradient:Image;
        public var _bgLine:Image;

        public var _userLeftNickLabel:TextField;
        public var _userLeftLeveLabel:TextField;
        public var _userLeftHPLabel:TextField;
        public var _userLeftHPPercentLabel:TextField;
        public var _userLeftHPBar:Image;
        public var userLeftHPMaxWidth:int;
        public var _userLeftAvatar:Image;
        public var _userLeftFraction:Image;
        public var _userLeftAvatarLoader:ImageLoader;
        public var _statsLeftGroup:LayoutGroup;

        public var _userRightNickLabel:TextField;
        public var _userRightLeveLabel:TextField;
        public var _userRightHPLabel:TextField;
        public var _userRightHPPercentLabel:TextField;
        public var _userRightHPBar:Image;
        public var userRightHPMaxWidth:int;
        public var _userRightAvatar:Image;
        public var _userRightFraction:Image;
        public var _userRightAvatarLoader:ImageLoader;
        public var _statsRightGroup:LayoutGroup;

        public var _userListLeft:List;
        public var _userListRight:List;
        public var _userBuffLeft:List;
        public var _userBuffRight:List;
        public var _skillLeftPanel:Sprite;
        public var _skillRightPanel:Sprite;
        
        public var _leftTabBar:TabBar;
        public var _rightTabBar:TabBar;
        public var _leftTabCounterLabel:TextField;
        public var _rightTabCounterLabel:TextField;
    
        public var _timerLabel:TextField;
        public var _turnMessageLabel:TextField;
        public var _fieldContainer:Sprite;
        public var _battleField:Sprite;

        public var _timerSprite:Sprite;
        public var _timerGreen:Image;

        public var _pointLeftDark:TextField;
        public var _pointLeftFire:TextField;
        public var _pointLeftLight:TextField;
        public var _pointLeftNature:TextField;
        public var _pointLeftWater:TextField;

        public var _pointRightDark:TextField;
        public var _pointRightFire:TextField;
        public var _pointRightLight:TextField;
        public var _pointRightNature:TextField;
        public var _pointRightWater:TextField;

        public var _magicList : List;
        public var _beltList : List;

        public var _logLabel:Label;

        public var particleTestPanel:Match3ParticleTest;

        private var _turnBlocked:Boolean;
        private var _selectedAnimation:MovieClip;
    
        public var leftDamage:PDParticleSystem;
        public var rightDamage:PDParticleSystem;

        public function Match3View()
        {
            super();

            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3, false, this) as Sprite;
            addChild(_sprite);

            _logLabel.width = 334;
            _logLabel.height = 90;

            _magicList.itemRendererFactory = function():IListItemRenderer { return new MagicItemRenderer(); };
            var layoutH:HorizontalLayout = new HorizontalLayout();
            layoutH.gap = 5;
            layoutH.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            _magicList.layout = layoutH;

            _beltList.itemRendererFactory = function():IListItemRenderer { return new BeltItemRenderer(); };
            layoutH = new HorizontalLayout();
            layoutH.gap = 10;
            layoutH.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            _beltList.layout = layoutH;

            _userLeftNickLabel.text = "";
            _userLeftLeveLabel.text = "";
            _userLeftHPLabel.text = "";
            _userLeftHPPercentLabel.text = "";
            userLeftHPMaxWidth = _userLeftHPBar.width;
            _userLeftAvatarLoader.width = _userLeftAvatarLoader.height = 87;
            
            _userRightNickLabel.text = "";
            _userRightLeveLabel.text = "";
            _userRightHPLabel.text = "";
            _userRightHPPercentLabel.text = "";
            userRightHPMaxWidth = _userRightHPBar.width;
            _userRightAvatarLoader.width = _userRightAvatarLoader.height = 87;

            _leftTabBar.customTabStyleName = "match3TabButton";
            _leftTabBar.dataProvider = new ListCollection(
                    [
                        { label: "Параметры"},
                        { label: "Эффекты"}
                    ]);
            _leftTabBar.addEventListener( Event.CHANGE, tab_changeHandler );
            
            _rightTabBar.customTabStyleName = "match3TabButton";
            _rightTabBar.dataProvider = new ListCollection(
                    [
                        { label: "Параметры"},
                        { label: "Эффекты"}
                    ]);
            _rightTabBar.addEventListener( Event.CHANGE, tab_changeHandler );
            
            _userListLeft.itemRendererFactory = function():IListItemRenderer { return new UserItemRenderer(); };
            var layoutF : FlowLayout = new FlowLayout();
            layoutF.gap = 3;
            _userListLeft.layout = layoutF;
            _userListLeft.maxHeight = 370;

            _userBuffLeft.itemRendererFactory = function():IListItemRenderer { return new BuffItemRenderer(); };
            _userBuffLeft.visible = false;
            layoutF = new FlowLayout();
            layoutF.horizontalGap = 3;
            layoutF.verticalGap = 2;
            layoutF.paddingTop = 5;
            layoutF.paddingLeft = 7;
            _userBuffLeft.layout = layoutF;

            _userListRight.itemRendererFactory = function():IListItemRenderer { return new UserItemRenderer(); };
            layoutF = new FlowLayout();
            layoutF.gap = 2;
            _userListRight.layout = layoutF;
            _userListRight.maxHeight = 370;

            _userBuffRight.itemRendererFactory = function():IListItemRenderer { return new BuffItemRenderer(); };
            _userBuffRight.visible = false;
            layoutF = new FlowLayout();
            layoutF.horizontalGap = 3;
            layoutF.verticalGap = 2;
            layoutF.paddingTop = 5;
            layoutF.paddingLeft = 7;
            _userBuffRight.layout = layoutF;

            _timerLabel.text = "";
            _turnMessageLabel.text = "";
            _turnMessageLabel.touchable = false;
            _fieldContainer.mask = new Quad(425,425);

            _selectedAnimation = new MovieClip(AppTheme.instance.assets.getTextures("gem_light/"), 30);
            _selectedAnimation.x = 200;
            _selectedAnimation.y = 200;
            _selectedAnimation.addEventListener(TouchEvent.TOUCH, itemSelected_clickHandler);
            Starling.juggler.add(_selectedAnimation);
            _selectedAnimation.play();
            _selectedAnimation.visible = false;
            _sprite.addChild(_selectedAnimation);


            leftDamage = new PDParticleSystem(AppTheme.instance.assets.getXml("damage_xml"), AppTheme.instance.assets.getTexture("damage"));
            leftDamage.touchable = false;
            _sprite.addChild(leftDamage);
            Starling.juggler.add(leftDamage);
            leftDamage.emitterX = 54;
            leftDamage.emitterY = 64;

            rightDamage = new PDParticleSystem(AppTheme.instance.assets.getXml("damage_xml"), AppTheme.instance.assets.getTexture("damage"));
            rightDamage.touchable = false;
            _sprite.addChild(rightDamage);
            Starling.juggler.add(rightDamage);
            rightDamage.emitterX = 971;
            rightDamage.emitterY = 64;

            particleContainer = new Sprite();
            _sprite.addChild(particleContainer);

            particleTestPanel = new Match3ParticleTest();
            //addChild(particleTestPanel);

            addEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);
        }
    
        public function turnLeft(self:Boolean, left:Boolean, name:String):void {
            _timerSprite.x = left ? 213 : 700;
            _turnMessageLabel.alpha = 1;
            if (self) {
                _turnMessageLabel.text = "Ваш ход";
                _timerGreen.visible = true;
                var t:Tween = new Tween(_turnMessageLabel, 1);
                t.animate("alpha", 0);
                Starling.juggler.add(t);
            } else {
                _turnMessageLabel.text = "Ходит " + name;
                _timerGreen.visible = false;
            }
        }

        public function drawGreed(areaWidth:int, areaHeight:int,areaCells:Array):void {
            _fieldContainer.removeChildren();
            var cells:Array = [];
            for (var i:int = 0; i < areaWidth; i++) {
                for (var j:int = 0; j < areaHeight; j++) {
                    var d:DataAreaCell = getAreaCellStatic(areaCells, i, j);
                    cells.push(d);
                }
            }
            addCells(cells);
        }

        private function tab_changeHandler( event:Event ):void
        {
            var stats:Boolean = TabBar(event.target).selectedIndex;
            switch (event.target) {
                case _leftTabBar:
                    _statsLeftGroup.visible = !stats;
                    _userBuffLeft.visible = stats; 
                    break;
                case _rightTabBar:
                    _statsRightGroup.visible = !stats;
                    _userBuffRight.visible = stats;
                    break;
            }
        }

        private function getAreaCellStatic(data:Array, x:int, y:int):DataAreaCell {
            for each (var i:DataAreaCell in data) {
                if (i.positionX == x && i.positionY == y) {
                    return i;
                }
            }
            return null;
        }

        public function addCells(cells:Array):void {
            // поиск самого нижнего камня для выравнивания по верхнему краю поля
            var lowerGemIndex:int = 0;
            for each (var data:DataAreaCell in cells) {
                lowerGemIndex = Math.max(lowerGemIndex,data.positionY);
            }
    
            lowerGemIndex += 1;
    
            for each (data in cells) {
                var item:GreedCell = new GreedCell();
                item.data = data;
                item.image.source = AppTheme.instance.assets.getTexture(gems[data.type]);
                item.imageHover.source = AppTheme.instance.assets.getTexture(gems[data.type]);
                item.addEventListener(TouchEvent.TOUCH, item_clickHandler);
    
                item.x = data.positionX * GEM_SIZE;
                item.y = GEM_SIZE * (data.positionY - lowerGemIndex);
                moveTo(item, item.x, data.positionY * GEM_SIZE);
                _fieldContainer.addChild(item);
            }
        }

        public function removeCells(cells:Array):void {
            var i:DataAreaCell;
            for each (i in cells) {
                var cell:GreedCell = getCellGreed(i.positionX, i.positionY);
                if (cell) {
                    removeGemWithAnim(cell);
                    _fieldContainer.removeChild(cell);
                }
            }
        }
    
        private function moveTo(gem:GreedCell, _x:Number, _y:Number, onComplete:Function=null, onCompleteArgs:Array=null):void
        {
            var tween:Tween = new Tween(gem, GEM_CHANGE_TIME, Transitions.EASE_OUT);
            tween.moveTo(_x,_y);
            if (onComplete != null) {
                tween.onComplete = onComplete;
                tween.onCompleteArgs = onCompleteArgs;
            }
            Starling.juggler.add(tween);
        }

        public function getCellGreed(x:int, y:int):GreedCell {
            for (var i:int = 0; i < _fieldContainer.numChildren; i++) {
                var item:GreedCell = _fieldContainer.getChildAt(i) as GreedCell;
                if (item.data.positionX == x && item.data.positionY == y) {
                    return item;
                }
            }
            return null;
        }
    
        public function swapCell(cells:Array):void {

            for (var i:int=0; i<cells.length; i+=2) {
                var data1:DataAreaCell = cells[i];
                var data2:DataAreaCell = cells[i + 1];

                var cell1:GreedCell = getCellGreed(data1.positionX, data1.positionY);
                var cell2:GreedCell = getCellGreed(data2.positionX, data2.positionY);

                if (cell1) {
                    cell1.data.positionX = data2.positionX;
                    cell1.data.positionY = data2.positionY;

                    moveTo(cell1, data2.positionX * GEM_SIZE, data2.positionY * GEM_SIZE);
                }
                if (cell2) {
                    cell2.data.positionX = data1.positionX;
                    cell2.data.positionY = data1.positionY;

                    moveTo(cell2, data1.positionX * GEM_SIZE, data1.positionY * GEM_SIZE);
                }
            }
        }
    
        public function swapWrongCell(data1:DataAreaCell, data2:DataAreaCell):void {
    
            var cell1:GreedCell = getCellGreed(data1.positionX, data1.positionY);
            var cell2:GreedCell = getCellGreed(data2.positionX, data2.positionY);
    
            if (cell1) {
                moveTo(cell1, data2.positionX * GEM_SIZE, data2.positionY * GEM_SIZE, moveTo, [cell1, data1.positionX * GEM_SIZE, data1.positionY * GEM_SIZE]);
            }
            if (cell2) {
                moveTo(cell2, data1.positionX * GEM_SIZE, data1.positionY * GEM_SIZE, moveTo, [cell2, data2.positionX * GEM_SIZE, data2.positionY * GEM_SIZE]);
            }

            _turnMessageLabel.alpha = 1;
            _turnMessageLabel.text = "Неверный ход";
            var t:Tween = new Tween(_turnMessageLabel, 1);
            t.animate("alpha", 0);
            Starling.juggler.add(t);

        }

        public function givePoint(data1:DataAreaCell, point:int):void {
            var particle:TextField = new TextField(100, 100, "+"+point, new TextFormat("Arial", 25, 0xF4BC40, Align.CENTER, Align.CENTER));
            particle.pivotX = 50;
            particle.pivotY = 50;
            particle.x = _battleField.x + _fieldContainer.x + data1.positionX * GEM_SIZE;
            particle.y = _battleField.y + _fieldContainer.y + data1.positionY * GEM_SIZE;
            particle.touchable = false;
            particleContainer.addChild(particle);

            var t:Tween = new Tween(particle, 3);
            t.onComplete = function():void { TextField(this.target).removeFromParent(true) };
            t.animate("alpha", 0);
            Starling.juggler.add(t);
        }

        public function flyingDamage(type:int, value:int, name:String):void
        {
            var damage:Sprite = AppTheme.uiBuilder.create(ParsedLayouts.match3_message_damage) as Sprite;
            damage.touchable = false;
            var layoutGroup:LayoutGroup = damage.getChildByName("layoutGroup2") as LayoutGroup;
            
            var nameText:TextField = layoutGroup.getChildByName("name") as TextField;
            nameText.text = name;
            var valueText:TextField = layoutGroup.getChildByName("value") as TextField;
            valueText.text = "+" + value.toString();
            layoutGroup.validate();
            
            var icon:Image = layoutGroup.getChildByName("icon") as Image;
            
            switch (type) {
                case DataBattleActionDamageType.DAMAGE:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/ico_damage");
                    break;
                case DataBattleActionDamageType.DAMAGE_DARK:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/dark");
                    break;
                case DataBattleActionDamageType.DAMAGE_FIRE:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/fire");
                    break;
                case DataBattleActionDamageType.DAMAGE_LIGHT:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/light");
                    break;
                case DataBattleActionDamageType.DAMAGE_NATURE:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/nature");
                    break;
                case DataBattleActionDamageType.DAMAGE_WATER:
                    icon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/water");
                    break;
            }

            flyingObjects.push(damage);
            if (!flyingFlag) {
                doFlyingMessage();
            }
        }
        
        private var flyingObjects:Array = [];
        private var flyingFlag:Boolean;
        
        private function doFlyingMessage():void
        {
            flyingFlag = false;
            if (flyingObjects.length == 0) {return;}
            flyingFlag = true;
            
            var object:DisplayObject = flyingObjects.shift();
            object.x = _battleField.x + _fieldContainer.x +100;
            object.y = _battleField.y + _fieldContainer.y+150;
            particleContainer.addChild(object);

            var t:Tween = new Tween(object, 5);
            t.onComplete = function():void { Sprite(this.target).removeFromParent(true) };
            t.animate("alpha",.9);
            t.moveTo(object.x, object.y-150);
            Starling.juggler.add(t);
            
            Starling.juggler.delayCall(doFlyingMessage, 0.5);
        }
        
        private function removeGemWithAnim(gem:GreedCell):Boolean
        {
            var deathTime:int = 30;
            var badaBoom:MovieClip;
            switch(gem.data.type)
            {
                case 1:
                    badaBoom = new MovieClip(AppTheme.instance.assets.getTextures("gem_purple_"),deathTime);
                    break;
                case 2:
                    badaBoom = new MovieClip(AppTheme.instance.assets.getTextures("gem_red_"),deathTime);
                    break;
                case 3:
                    badaBoom = new MovieClip(AppTheme.instance.assets.getTextures("gem_yellow_"),deathTime);
                    break;
                case 4:
                    badaBoom = new MovieClip(AppTheme.instance.assets.getTextures("gem_blue_"),deathTime);
                    break;
                case 5:
                    badaBoom = new MovieClip(AppTheme.instance.assets.getTextures("gem_green_"),deathTime);
            }
    
            if(badaBoom)
            {
                var gap:int = 29;
                badaBoom.x = _battleField.x + _fieldContainer.x + gem.x - gap;
                badaBoom.y = _battleField.y + _fieldContainer.y + gem.y - gap;
                particleContainer.addChild(badaBoom);
                Starling.juggler.add(badaBoom);
                badaBoom.loop = false;
                badaBoom.addEventListener("complete", onDeathEffectLoopComplete);
                //removeGemFromRender(gem);
                
                //startParticle("blue_star_1",new Point(badaBoom.x, badaBoom.y), new Point(210, 160));
            }

            return true;
        }

        protected function onDeathEffectLoopComplete(event:Event):void
        {
            var mc:MovieClip = event.target as MovieClip;
            mc.removeEventListener("complete", onDeathEffectLoopComplete);
            if (mc && particleContainer.contains(mc)) particleContainer.removeChild(mc,true);
        }

        protected function startParticle(name:String, from:Point, to:Point):void
        {
            var particles: PDParticleSystem;
            particles = new PDParticleSystem(AppTheme.instance.assets.getXml(name+"_xml"), AppTheme.instance.assets.getTexture("particle/"+name));
            particles.touchable = false;
            particleContainer.addChild(particles);
            Starling.juggler.add(particles);
            particles.start();
    
            particles.emitterX = from.x;
            particles.emitterY = from.y;
    
            var t:Tween = new Tween(particles, 1, Transitions.EASE_IN_OUT);
            t.animate("emitterX", to.x);
            t.animate("emitterY", to.y);
            t.onComplete = function():void { particles.stop(); Starling.juggler.remove(particles); particleContainer.removeChild(particles);particles.dispose() };
            Starling.juggler.add(t);
        }

        private function itemSelected_clickHandler(event:TouchEvent):void {
            var touch: Touch = event.getTouch(event.currentTarget as DisplayObject);
            if (touch) {
                if (touch.phase == TouchPhase.ENDED) {
                    if (selectedGreed) {
                        _selectedAnimation.visible = false;
                        selectedGreed = null;
                    }
                }
            }
        }
    
        private function item_clickHandler(event:TouchEvent):void {
            if (_turnBlocked) {return;}
            currentCell = event.currentTarget as GreedCell;
            var touch: Touch = event.getTouch(currentCell);
            if (touch) {
                if (touch.phase == TouchPhase.HOVER) {
                    currentCell.imageHover.visible = true;
                }
                if (touch.phase == TouchPhase.ENDED) {
                    if (selectedGreed) {
                        if (selectedGreed == currentCell) {
                            _selectedAnimation.visible = false;
                            selectedGreed = null;
                            return;
                        }
                        _selectedAnimation.visible = false;
                        dispatchEventWith(MOVE_GEMS, false, {swapFirstCell:selectedGreed.data,swapSecondCell:currentCell.data});
                    } else {
                        selectedGreed = currentCell;
                        _selectedAnimation.visible = true;
                        _selectedAnimation.x = _battleField.x + _fieldContainer.x + selectedGreed.x - 11;
                        _selectedAnimation.y = _battleField.y + _fieldContainer.y + selectedGreed.y - 12;
                    }
                }
            } else {
                currentCell.imageHover.visible = false;
            }
        }
    
        public function endGame(areaWidth:int, areaHeight:int):void {
            if (_currentLineEndGame < areaHeight) {
                for (var i:int=0; i < areaWidth; i++) {
                    var cell:GreedCell = getCellGreed(i, _currentLineEndGame);
                    if (cell) {
                        removeGemWithAnim(cell);
                        _fieldContainer.removeChild(cell);
                    }
                }
                _currentLineEndGame++;
                Starling.juggler.delayCall(endGame, GEM_CHANGE_TIME, areaWidth, areaHeight);
            } else {
                dispatchEventWith(END_GAME);
            }
        }

        public function set turnBlocked(value:Boolean):void {
            _turnBlocked = value;
            
            _beltList.isEnabled = !_turnBlocked;
            _magicList.isEnabled = !_turnBlocked;
            
            if (_turnBlocked) {
                if (currentCell) {
                    currentCell.imageHover.visible = false;
                }
                if (selectedGreed) {
                    selectedGreed.imageHover.visible = false;
                    selectedGreed = null;
                }
                _selectedAnimation.visible = false;
            }
        }

        protected function sprite_removeFromStageHandler(event:Event):void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.removeEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
        }

        protected function stage_resizeHandler(event:Event):void
        {
            _sprite.x = int((stage.stageWidth - _sprite.width)/2);
            _sprite.y = int((stage.stageHeight - 800)/2);
        }
        
        protected function view_addedToStageHandler(event:Event):void {
            removeEventListener(Event.ADDED_TO_STAGE, view_addedToStageHandler);

            _bgLine.width = _sprite.width;
            
            if (MainModel.isMobile) {
                _background.width = stage.stageWidth;
                _background.height = stage.stageHeight;
                _backgroundGradient.width = stage.stageWidth;
                _backgroundGradient.height = stage.stageHeight;
            } else {
                addEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
                stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);

                _sprite.x = int((stage.stageWidth - _sprite.width) / 2);
                _sprite.y = int((stage.stageHeight - 800) / 2);
            }
        }
    }
}
