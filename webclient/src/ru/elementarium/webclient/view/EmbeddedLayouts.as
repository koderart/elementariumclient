/**
 *  Starling Builder
 *  Copyright 2015 SGN Inc. All Rights Reserved.
 *
 *  This program is free software. You can redistribute and/or modify it in
 *  accordance with the terms of the accompanying license agreement.
 */
package ru.elementarium.webclient.view{
    public class EmbeddedLayouts
    {
        [Embed(source="/../assets/layouts/game/game.json", mimeType="application/octet-stream")]
        public static const game:Class;
        [Embed(source="/../assets/layouts/game_connection.json", mimeType="application/octet-stream")]
        public static const game_connection:Class;
        [Embed(source="/../assets/layouts/game/option_panel.json", mimeType="application/octet-stream")]
        public static const game_option_panel:Class;
        [Embed(source="/../assets/layouts/game/user_panel_left.json", mimeType="application/octet-stream")]
        public static const game_user_panel_left:Class;
        [Embed(source="/../assets/layouts/game/money_panel.json", mimeType="application/octet-stream")]
        public static const game_money_panel:Class;
        [Embed(source="/../assets/layouts/game/mob_panel.json", mimeType="application/octet-stream")]
        public static const game_mob_panel:Class;

        [Embed(source="/../assets/layouts/match3/match3.json", mimeType="application/octet-stream")]
        public static const match3:Class;
        [Embed(source="/../assets/layouts/match3/user_item.json", mimeType="application/octet-stream")]
        public static const match3_user_item:Class;
        [Embed(source="/../assets/layouts/match3/user_panel_left.json", mimeType="application/octet-stream")]
        public static const match3_user_panel_left:Class;
        [Embed(source="/../assets/layouts/match3/user_panel_right.json", mimeType="application/octet-stream")]
        public static const match3_user_panel_right:Class;
        [Embed(source="/../assets/layouts/match3/magic_item.json", mimeType="application/octet-stream")]
        public static const match3_magic_item:Class;
        [Embed(source="/../assets/layouts/match3/belt_item.json", mimeType="application/octet-stream")]
        public static const match3_belt_item:Class;
        [Embed(source="/../assets/layouts/match3/buff_item.json", mimeType="application/octet-stream")]
        public static const match3_buff_item:Class;
        [Embed(source="/../assets/layouts/match3/magic_info.json", mimeType="application/octet-stream")]
        public static const match3_magic_info:Class;
        [Embed(source="/../assets/layouts/match3/buff_info.json", mimeType="application/octet-stream")]
        public static const match3_buff_info:Class;
        [Embed(source="/../assets/layouts/match3/potion_info.json", mimeType="application/octet-stream")]
        public static const match3_potion_info:Class;
        [Embed(source="/../assets/layouts/match3/message_damage.json", mimeType="application/octet-stream")]
        public static const match3_message_damage:Class;

        [Embed(source="/../assets/layouts/inventory/inventory.json", mimeType="application/octet-stream")]
        public static const inventory:Class;
        [Embed(source="/../assets/layouts/inventory/item.json", mimeType="application/octet-stream")]
        public static const inventory_item:Class;
        [Embed(source="/../assets/layouts/inventory/item_slot.json", mimeType="application/octet-stream")]
        public static const inventory_item_slot:Class;
        [Embed(source="/../assets/layouts/inventory/item_belt.json", mimeType="application/octet-stream")]
        public static const inventory_item_belt:Class;
        [Embed(source="/../assets/layouts/inventory/remove.json", mimeType="application/octet-stream")]
        public static const inventory_remove:Class;
        [Embed(source="/../assets/layouts/inventory/skills.json", mimeType="application/octet-stream")]
        public static const inventory_skills:Class;
        [Embed(source="/../assets/layouts/inventory/sell.json", mimeType="application/octet-stream")]
        public static const inventory_sell:Class;
        [Embed(source="/../assets/layouts/inventory/artikul_alert.json", mimeType="application/octet-stream")]
        public static const inventory_artikul_alert:Class;
        [Embed(source="/../assets/layouts/inventory/artikul_alert_stat.json", mimeType="application/octet-stream")]
        public static const inventory_artikul_alert_stat:Class;
        [Embed(source="/../assets/layouts/inventory/repair.json", mimeType="application/octet-stream")]
        public static const inventory_repair:Class;


        [Embed(source="/../assets/layouts/store/store.json", mimeType="application/octet-stream")]
        public static const store:Class;
        [Embed(source="/../assets/layouts/store/item.json", mimeType="application/octet-stream")]
        public static const store_item:Class;
        [Embed(source="/../assets/layouts/store/item_buy.json", mimeType="application/octet-stream")]
        public static const store_item_buy:Class;

        [Embed(source="/../assets/layouts/registration/token.json", mimeType="application/octet-stream")]
        public static const registration_token:Class;
        [Embed(source="/../assets/layouts/registration/magic_item.json", mimeType="application/octet-stream")]
        public static const registration_magic_item:Class;
        [Embed(source="/../assets/layouts/registration/fraction.json", mimeType="application/octet-stream")]
        public static const registration_fraction:Class;

        [Embed(source="/../assets/layouts/arena.json", mimeType="application/octet-stream")]
        public static const arena:Class;

        [Embed(source="/../assets/layouts/hint/spell.json", mimeType="application/octet-stream")]
        public static const hint_spell:Class;
        [Embed(source="/../assets/layouts/hint/artikul.json", mimeType="application/octet-stream")]
        public static const hint_artikul:Class;
        [Embed(source="/../assets/layouts/hint/artikul_money.json", mimeType="application/octet-stream")]
        public static const hint_artikul_money:Class;
        [Embed(source="/../assets/layouts/hint/challenge.json", mimeType="application/octet-stream")]
        public static const hint_challenge:Class;
        [Embed(source="/../assets/layouts/hint/challenge_item.json", mimeType="application/octet-stream")]
        public static const hint_challenge_item:Class;

        [Embed(source="/../assets/layouts/alert/alert.json", mimeType="application/octet-stream")]
        public static const alert:Class;

        [Embed(source="/../assets/layouts/button_common.json", mimeType="application/octet-stream")]
        public static const button_common:Class;

        [Embed(source="/../assets/layouts/quest/quest.json", mimeType="application/octet-stream")]
        public static const quest:Class;
        [Embed(source="/../assets/layouts/quest/goal_item.json", mimeType="application/octet-stream")]
        public static const quest_goal_item:Class;
        [Embed(source="/../assets/layouts/quest/award_item.json", mimeType="application/octet-stream")]
        public static const quest_award_item:Class;

        [Embed(source="/../assets/layouts/payment/payment.json", mimeType="application/octet-stream")]
        public static const payment:Class;
        [Embed(source="/../assets/layouts/payment/item.json", mimeType="application/octet-stream")]
        public static const payment_item:Class;


        [Embed(source="/../assets/layouts/alert/level.json", mimeType="application/octet-stream")]
        public static const alert_level:Class;
        [Embed(source="/../assets/layouts/alert/win.json", mimeType="application/octet-stream")]
        public static const alert_win:Class;
        [Embed(source="/../assets/layouts/alert/defeat.json", mimeType="application/octet-stream")]
        public static const alert_defeat:Class;
        [Embed(source="/../assets/layouts/alert/drop.json", mimeType="application/octet-stream")]
        public static const alert_drop:Class;
        [Embed(source="/../assets/layouts/alert/achieve.json", mimeType="application/octet-stream")]
        public static const alert_achieve:Class;
        [Embed(source="/../assets/layouts/alert/daily_bonus.json", mimeType="application/octet-stream")]
        public static const alert_daily_bonus:Class;
        [Embed(source="/../assets/layouts/alert/take_give_component.json", mimeType="application/octet-stream")]
        public static const alert_take_give_component:Class;

        [Embed(source="/../assets/layouts/help.json", mimeType="application/octet-stream")]
        public static const help:Class;
        [Embed(source="/../assets/layouts/news.json", mimeType="application/octet-stream")]
        public static const news:Class;

        [Embed(source="/../assets/layouts/quest/complete_alert.json", mimeType="application/octet-stream")]
        public static const quest_complete_alert:Class;

        [Embed(source="/../assets/layouts/game/battle_info.json", mimeType="application/octet-stream")]
        public static const battle_info:Class;

        [Embed(source="/../assets/layouts/game/user_item.json", mimeType="application/octet-stream")]
        public static const location_user_item:Class;
        [Embed(source="/../assets/layouts/game/info_user_item.json", mimeType="application/octet-stream")]
        public static const location_info_user_item:Class;
        [Embed(source="/../assets/layouts/chat.json", mimeType="application/octet-stream")]
        public static const chat:Class;

        [Embed(source="/../assets/layouts/achieve/achieve.json", mimeType="application/octet-stream")]
        public static const achieve:Class;
        [Embed(source="/../assets/layouts/achieve/item.json", mimeType="application/octet-stream")]
        public static const achieve_item:Class;
        
        [Embed(source="/../assets/layouts/window.json", mimeType="application/octet-stream")]
        public static const window:Class;
        [Embed(source="/../assets/layouts/window_module.json", mimeType="application/octet-stream")]
        public static const window_module:Class;

    }
}
