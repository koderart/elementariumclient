package ru.elementarium.webclient.view.alert {

import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;
import feathers.controls.List;
import feathers.controls.renderers.IListItemRenderer;
import feathers.layout.FlowLayout;
import feathers.layout.HorizontalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.alert.components.*;
import ru.elementarium.webclient.view.main.WindowView;
import ru.muvs.elementarium.thrift.message.DataMessageType;

import starling.display.Sprite;
import starling.text.TextField;

public class AlertView extends WindowView
    {
        public var _close2Button:feathers.controls.Button;

        public var _bodyLabel:TextField;
        public var _imageLoader:ImageLoader;

        public var _statLayout:LayoutGroup;
        public var _statLabel:TextField;
        public var _giveTitleGroup:LayoutGroup;
        public var _giveArtikulList:List;
        public var _takeTitleGroup:LayoutGroup;
        public var _takeArtikulList:List;


        public var _takeGiveComponent:Sprite;
        public var _takeGiveLayout:LayoutGroup;

        public function AlertView(type:int)
        {
            super();
            switch (type) {
                case DataMessageType.GOT:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_drop, false, this) as Sprite;
                    _titleLabel.text = "ПОЛУЧЕНО";
                break;
                case DataMessageType.VICTORY:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_win, false, this) as Sprite;
                    _titleLabel.text = "ПОБЕДА!";
                break;
                case DataMessageType.DEFEAT:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_defeat, false, this) as Sprite;
                    _titleLabel.text = "ПОРАЖЕНИЕ";
                break;
                case DataMessageType.LEVEL_UP:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_level, false, this) as Sprite;
                    _titleLabel.text = "НОВЫЙ УРОВЕНЬ!";
                break;
                case DataMessageType.ACHIEVE:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_achieve, false, this) as Sprite;
                    _titleLabel.text = "НОВОЕ ДОСТИЖЕНИЕ!";
                break;
                case DataMessageType.DAILY_BONUS:
                    _content = AppTheme.uiBuilder.create(ParsedLayouts.alert_daily_bonus, false, this) as Sprite;
                    _titleLabel.text = "ЕЖЕДНЕВНЫЙ БОНУС";
                    break;
                default:
                    return;
            }
                        
            var takeGiveComponent = AppTheme.uiBuilder.create(ParsedLayouts.alert_take_give_component, false, this) as Sprite;
            _takeGiveComponent.addChild(takeGiveComponent);

            _giveArtikulList.itemRendererFactory = function():IListItemRenderer { return new InventoryItemRenderer(); };
            var layoutF:FlowLayout = new FlowLayout();
            layoutF.padding = 10;
            layoutF.horizontalGap = 0;
            layoutF.verticalGap = 5;
            layoutF.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            _giveArtikulList.layout = layoutF;

            _takeArtikulList.itemRendererFactory = function():IListItemRenderer { return new InventoryItemRenderer(); };
            layoutF = new FlowLayout();
            layoutF.padding = 10;
            layoutF.horizontalGap = 0;
            layoutF.verticalGap = 5;
            layoutF.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
            _takeArtikulList.layout = layoutF;

            _statLabel.text = "";
            _statLayout.visible = _statLayout.includeInLayout = false;
        }

        public function updateHeight():void
        {
            _takeGiveLayout.validate();
            _contentLayout.validate();
            validateAndAttach();
        }
}
}
