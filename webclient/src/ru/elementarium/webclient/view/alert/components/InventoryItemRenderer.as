package ru.elementarium.webclient.view.alert.components {

import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.elementarium.thrift.money.DataMoneyType;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;


public class InventoryItemRenderer extends LayoutGroupListItemRenderer
{
    private var _initialized:Boolean = false;
    private var _select:TapToSelect;

    private var _sprite:Sprite;
    public var _countLabel:TextField;
    public var _moneyLabel:TextField;
    public var _moneyImage:Image;
    public var _imageLoader:ImageLoader;
    public var _artikulSprite:Sprite;
    public var _moneySprite:Sprite;

    public function InventoryItemRenderer()
    {
        super();
        this._select = new TapToSelect(this);
    }

    override protected function initialize():void
    {
        _initialized = true;

        if (!_sprite) {
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.quest_award_item, false, this) as Sprite;
            addChild(_sprite);
        }
    }

    override protected function commitData():void
    {
        if(_data) {
            if(_data.artikul) {

                _artikulSprite.visible = true;
                _moneySprite.visible = false;
                _imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.artikulData[_data.artikul].image.url;
                _countLabel.text = _data.count > 1 ? _data.count.toString() : "";
            
            } else if (_data.moneyType) {

                _artikulSprite.visible = false;
                _moneySprite.visible = true;
                _moneyLabel.text = _data.count.toString();
                if (_data.moneyType == DataMoneyType.GOLD) {
                    _moneyImage.texture = AppTheme.instance.assets.getTexture("main/goldcoin_icon");
                }
                if (_data.moneyType == DataMoneyType.CRYSTAL) {
                    _moneyImage.texture = AppTheme.instance.assets.getTexture("main/crystal_icon");
                }

            } else if (_data.experience) {

                _artikulSprite.visible = false;
                _moneySprite.visible = true;
                _moneyImage.texture = AppTheme.instance.assets.getTexture("currency/exp_icon_big");
                _moneyLabel.text = _data.experience;
            }
        }
    }
}
}
