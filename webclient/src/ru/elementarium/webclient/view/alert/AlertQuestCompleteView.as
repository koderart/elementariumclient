package ru.elementarium.webclient.view.alert {

import com.junkbyte.console.Cc;
import feathers.controls.ImageLoader;
import feathers.controls.Button;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.WindowView;

import starling.display.Sprite;
import starling.text.TextField;

public class AlertQuestCompleteView extends WindowView
    {
        public var _close2Button:Button;

        public var _messageLabel:TextField;
        public var _imageLoader:ImageLoader;

        public function AlertQuestCompleteView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.quest_complete_alert, false, this) as Sprite;
            _titleLabel.text = "ЗАДАНИЕ ВЫПОЛНЕНО!";
            
            _imageLoader.width = _imageLoader.height = 88;
            
            validateAndAttach();
        }
}
}
