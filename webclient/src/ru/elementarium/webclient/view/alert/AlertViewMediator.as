package ru.elementarium.webclient.view.alert
{
import com.junkbyte.console.Cc;

import feathers.controls.List;
import feathers.data.ListCollection;
import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.inventory.components.ArtikulAlert;
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import ru.muvs.elementarium.thrift.message.*;
import ru.muvs.elementarium.thrift.message.message.got.DataGot;
import ru.muvs.elementarium.thrift.message.message.got.DataGotType;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.skill.DataSkillType;
import starling.events.Event;

public class AlertViewMediator extends Mediator
	{
		[Inject]
		public var view:AlertView;
        [Inject]
        public var mainModel:MainModel;

		override public function onRegister():void
		{
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);

            if (view._giveArtikulList) {
                view._giveArtikulList.addEventListener(Event.CHANGE, openArtikulEventHandler);
            }

            if (view._takeArtikulList) {
                view._takeArtikulList.addEventListener(Event.CHANGE, openArtikulEventHandler);
            }

            var gotArray:Array = [];
            switch (mainModel.message.type) {
                case DataMessageType.ACHIEVE:
                    gotArray = mainModel.message.achieve.got;
                    break;
                case DataMessageType.DAILY_BONUS:
                    gotArray = mainModel.message.dailyBonus.got;
                    break;
                case DataMessageType.DEFEAT:
                    gotArray = mainModel.message.defeat.got;
                    break;
                case DataMessageType.GOT:
                    gotArray = mainModel.message.got.got;
                    break;
                case DataMessageType.VICTORY:
                    gotArray = mainModel.message.victory.got;
                    break;
            }

            var obj:Object;
            var experience: int = 0;
            
            var artikulGive:Array = [];
            var artikulTake:Array = [];

            var moneyGive:Array = [];
            var moneyTake:Array = [];
            
            for each (var got:DataGot in gotArray) {
                switch (got.type) {
                    case DataGotType.ARTIKUL:
                        obj = {artikul:got.artikul.artikul.artikulId,count:got.artikul.artikul.count};
                        if (obj.count > 0) {
                            if (artikulGive[obj.artikul]) {
                                artikulGive[obj.artikul].count += obj.count;
                            } else {
                                artikulGive[obj.artikul] = obj;
                            }
                        } else {
                            obj.count *= -1;
                            if (artikulTake[obj.artikul]) {
                                artikulTake[obj.artikul].count += obj.count;
                            } else {
                                artikulTake[obj.artikul] = obj;
                            }
                        }
                        break;
                    case DataGotType.MONEY:
                        obj = {moneyType:got.money.money.type,count:got.money.money.money};
                        if (obj.count > 0) {
                            if (moneyGive[obj.moneyType]) {
                                moneyGive[obj.moneyType].count += obj.count;
                            } else {
                                moneyGive[obj.moneyType] = obj;
                            }
                        } else {
                            obj.count *= -1;
                            if (moneyTake[obj.moneyType]) {
                                moneyTake[obj.moneyType].count += obj.count;
                            } else {
                                moneyTake[obj.moneyType] = obj;
                            }
                        }
                        break;
                    case DataGotType.SKILL:
                        if (got.skill.value.type == DataSkillType.EXP) {
                            experience += got.skill.value.value;
                        }
                        break;
                }
            }
            view._giveArtikulList.dataProvider = new ListCollection();
            for each (obj in moneyGive) {
                view._giveArtikulList.dataProvider.addItem(obj);
            }
            for each (obj in artikulGive) {
                view._giveArtikulList.dataProvider.addItem(obj);
            }
            view._takeArtikulList.dataProvider = new ListCollection();
            for each (obj in moneyTake) {
                view._takeArtikulList.dataProvider.addItem(obj);
            }
            for each (obj in artikulTake) {
                view._takeArtikulList.dataProvider.addItem(obj);
            }
            if (experience > 0) {
                view._giveArtikulList.dataProvider.addItem({experience:experience});
            }

            if (mainModel.message.type != DataMessageType.GOT) {
                view._giveTitleGroup.visible = view._giveTitleGroup.includeInLayout = view._giveArtikulList.dataProvider.length > 0;
            } else {
                view._giveTitleGroup.visible = view._giveTitleGroup.includeInLayout = false;
            }
            view._giveArtikulList.visible = view._giveArtikulList.includeInLayout = view._giveArtikulList.dataProvider.length > 0;
            view._takeTitleGroup.visible = view._takeTitleGroup.includeInLayout = view._takeArtikulList.dataProvider.length > 0;
            view._takeArtikulList.visible = view._takeArtikulList.includeInLayout = view._takeArtikulList.dataProvider.length > 0;

            if (mainModel.message.type == DataMessageType.LEVEL_UP) {
                view._bodyLabel.text = "ВЫ ПОЛУЧИЛИ " + mainModel.message.levelUp.level.toString() + " УРОВЕНЬ";
                var element:String;
                switch (mainModel.player.element) {
                    case DataPlayerElement.DARK:
                        element = ElementEnum.DARK;
                        break;
                    case DataPlayerElement.FIRE:
                        element = ElementEnum.FIRE;
                        break;
                    case DataPlayerElement.LIGHT:
                        element = ElementEnum.LIGHT;
                        break;
                    case DataPlayerElement.NATURE:
                        element = ElementEnum.NATURE;
                        break;
                    case DataPlayerElement.WATER:
                        element = ElementEnum.WATER;
                        break;
                }
                for each (var skill:LevelSkillVO in LevelVO(MainModel.levelData[element][mainModel.message.levelUp.level]).levelSkillBase) {
                    view._statLabel.text += skill.skill.title.ru + ": " + skill.value + "\n";
                }
            }
            if (mainModel.message.type == DataMessageType.ACHIEVE) {
                var achieve:AchieveVO = MainModel.achieveData[mainModel.message.achieve.achieve];
                view._imageLoader.source = ConnectionService.BASE_URL_HTTP + achieve.image.url;
                view._imageLoader.width = view._imageLoader.height = 64;
                view._bodyLabel.text = achieve.title.ru;
                
                for each (var achieveSkill:AchieveSkillVO in achieve.achieveSkill) {
                    view._statLabel.text += achieveSkill.skill.title.ru + ": +" + achieveSkill.value + "\n";
                }
            }

            view._statLayout.visible = view._statLayout.includeInLayout = view._statLabel.text.length > 0;
            view.updateHeight();
        }
    
        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view._close2Button:
                    view.removeFromParent(true);
                break;
            }
        }
    
        private function openArtikulEventHandler(event:Event):void
        {
            if (List(event.target).selectedItem && List(event.target).selectedItem.artikul) {
                var d:DataArtikul = new DataArtikul();
                d.artikulId = List(event.target).selectedItem.artikul;
                var artikulAlert:ArtikulAlert = new ArtikulAlert(d, null, mainModel.player.element, true);
                view.stage.addChild(artikulAlert);
            }
            List(event.target).selectedItem = null;
        }

}
}