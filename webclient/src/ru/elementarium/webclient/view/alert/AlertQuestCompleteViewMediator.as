package ru.elementarium.webclient.view.alert
{
import com.junkbyte.console.Cc;
import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.muvs.admin.main.vo.NpcTakeVO;
import ru.muvs.admin.main.vo.NpcVO;
import ru.muvs.admin.main.vo.QuestVO;

import starling.events.Event;

public class AlertQuestCompleteViewMediator extends Mediator
	{
		[Inject]
		public var view:AlertQuestCompleteView;
        [Inject]
        public var mainModel:MainModel;

		override public function onRegister():void
		{
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);

            var quest:QuestVO = MainModel.questData[mainModel.quest.questId];
            var npc:NpcVO;
            for each (var npcData:NpcVO in MainModel.npcData) {
                for each (var npcTake:NpcTakeVO in npcData.npcTakeQuest) {
                    if (npcTake.quest.questId == mainModel.quest.questId) {
                        npc = npcData;
                        break;
                    }
                }
            }
            view._messageLabel.text = "Задание "+quest.title.ru+" выполнено.\n"+npc.title.ru+" примет его у вас.";
            view._imageLoader.source = ConnectionService.BASE_URL_HTTP+npc.imageAvatar.url;
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._close2Button:
                    view.removeFromParent(true);
                break;
            }
        }
}
}