package ru.elementarium.webclient.view.main
{
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;

import flash.desktop.NativeApplication;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.AppContext;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.Match3Model;
import ru.elementarium.webclient.view.alert.AlertQuestCompleteView;
import ru.elementarium.webclient.view.alert.AlertView;
import ru.elementarium.webclient.view.game.GameView;
import ru.elementarium.webclient.view.login.FractionView;
import ru.elementarium.webclient.view.login.LoginView;
import ru.elementarium.webclient.view.login.RegisterTokenView;
import ru.elementarium.webclient.view.main.components.ArtikulHint;
import ru.elementarium.webclient.view.main.components.ChallengeHint;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.elementarium.webclient.view.match3.Match3View;
import ru.elementarium.webclient.view.main.components.SpellHint;
import ru.muvs.elementarium.thrift.message.DataMessageType;
import ru.muvs.elementarium.thrift.message.message.text.DataTextType;

import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.events.Event;
import starling.utils.AssetManager;

public class AppMediator extends Mediator
	{
        public static const HINT_SPELL:int = 1;
        public static const HINT_ARTIKUL:int = 2;
        public static const HINT_CHALLENGE:int = 3;
    
		[Inject]
		public var view:AppView;
		[Inject]
		public var gameModel:Match3Model;
        [Inject]
        public var mainModel:MainModel;

        private var spellHint:SpellHint;
        private var artikulHint:ArtikulHint;
        private var challengeHint:ChallengeHint;
        
        private var backgroundImage:Image;
        private var _loginState:Boolean;
        private var _deactivatedState:Boolean;

        override public function onRegister():void
		{
            addContextListener(HintEvent.SHOW, appEventTypeHandler);
            addContextListener(HintEvent.HIDE, appEventTypeHandler);
            addContextListener(Match3Event.GAME_ENDED, appEventTypeHandler);
            addContextListener(ConnectionEvent.DISCONNECTED, appEventTypeHandler);
			addContextListener(LoginEvent.LOGIN_SUCCESS, appEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_JOIN_CLIENT_COMMAND, appEventTypeHandler);
            addContextListener(GameEvent.MESSAGE_CLIENT_COMMAND, appEventTypeHandler);
            addContextListener(QuestEvent.QUEST_COMPLETE_CLIENT_COMMAND, appEventTypeHandler);
            addContextListener(LoginEvent.REGISTER_LOADED, appEventTypeHandler);
            addContextListener(LoginEvent.LOGIN_EMAIL_START, appEventTypeHandler);
            addContextListener(GameEvent.ACTIVATE, appEventTypeHandler);
            addContextListener(GameEvent.DEACTIVATE, appEventTypeHandler);
            addContextListener(ConnectionEvent.VERSION_CLIENT_ERROR, appEventTypeHandler);
            addContextListener(LoginEvent.PLAYER_FRACTION_UNDEFINED, appEventTypeHandler);

            view.layoutGroup.addChild(new StaticView());

            spellHint = new SpellHint();
            spellHint.visible = false;
            artikulHint = new ArtikulHint();
            artikulHint.visible = false;
            challengeHint = new ChallengeHint();
            challengeHint.visible = false;


            backgroundImage = new Image(AppTheme.instance.assets.getTexture("background"));
            backgroundImage.x = int((view.stage.stageWidth-backgroundImage.width)/2);
            backgroundImage.y = int((view.stage.stageHeight-backgroundImage.height)/2);

            view.addChildAt(backgroundImage, 0);
            view.stage.addEventListener(Event.RESIZE, view_resizeHandler);

            BUILD::test {
                Starling.current.showStats = true;
            }
            BUILD::mobile {
                NativeApplication.nativeApplication.addEventListener(
                        "activate", function (e:*):void {
                            dispatch(new GameEvent(GameEvent.ACTIVATE));
                        }
                );

                NativeApplication.nativeApplication.addEventListener(
                        "deactivate", function (e:*):void {
                            dispatch(new GameEvent(GameEvent.DEACTIVATE));
                        }
                );
            }
            dispatch(new PaymentEvent(PaymentEvent.PAYMENT_APPLE_START));
        }
    
        private function deleteView(layoutGroup:LayoutGroup):void
        {
            while (layoutGroup.numChildren > 0) {
                var d:DisplayObject = layoutGroup.getChildAt(0);
                trace("deleteView", d);
                d.removeFromParent(true);
                AppContext.instance.getMediatorMap.removeMediatorByView(d);
            }
            spellHint.visible = false;
            artikulHint.visible = false;
            challengeHint.visible = false;
        }

        private function appEventTypeHandler(event:Event):void
		{
            switch (event.type) {
                case LoginEvent.REGISTER_LOADED:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.messageLabel.text = "";
                    view.layoutGroup.addChild(new RegisterTokenView(event.data as AssetManager));
                    break;
                case LoginEvent.LOGIN_EMAIL_START:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new LoginView());
                break;
                case ConnectionEvent.DISCONNECTED:
                    _loginState = false;
                    if(_deactivatedState) { return; }
                    deleteView(view.layoutGroup);
                    deleteView(view.chatGroup);
                    view.layoutGroup.addChild(new StaticView(true));
                    break;
                case GameEvent.ACTIVATE:
                    trace("case GameEvent.ACTIVATE");
                    NativeApplication.nativeApplication.executeInBackground = false;
                    Starling.current.start();
                    _deactivatedState = false;
                    if (_loginState) {
                        if (mainModel.isBattle) {
                            view.layoutGroup.addChild(new Match3View());
                        } else {
                            view.layoutGroup.addChild(new GameView(mainModel));
                        }
                        view.chatGroup.addChild(new ChatView());
                    } else {
                        view.layoutGroup.addChild(new StaticView(true));
                    }
                    break;
                case GameEvent.DEACTIVATE:
                    trace("case GameEvent.DEACTIVATE");
                    _deactivatedState = true;
                    deleteView(view.layoutGroup);
                    deleteView(view.chatGroup);
                    NativeApplication.nativeApplication.executeInBackground = true;
                    Starling.current.stop(true);
                    break;
                case LoginEvent.LOGIN_SUCCESS:
                    _loginState = true;
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new GameView(mainModel));
                    view.chatGroup.addChild(new ChatView());
                    break;
                case BattleEventType.BATTLE_JOIN_CLIENT_COMMAND:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new Match3View());
                    break;
                case Match3Event.GAME_ENDED:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new GameView(mainModel));
                break;
                case HintEvent.SHOW:
                    switch (HintEvent(event).typeHint) {
                        case HINT_SPELL:
                            if (!spellHint.visible) {
                                spellHint.visible = true;
                                view.hintGroup.addChild(spellHint);
                                spellHint.draw(HintEvent(event).spellId, HintEvent(event).userElement, HintEvent(event).points);
                            }
                        break;
                        case HINT_ARTIKUL:
                            if (!artikulHint.visible) {
                                artikulHint.visible = true;
                                view.hintGroup.addChild(artikulHint);
                                artikulHint.draw(HintEvent(event).artikulId,HintEvent(event).userLevel,HintEvent(event).userElement,HintEvent(event).durabilityCurrent,HintEvent(event).durability, HintEvent(event).randomSkills);
                            }
                        break;
                        case HINT_CHALLENGE:
                            if (!challengeHint.visible) {
                                challengeHint.visible = true;
                                view.hintGroup.addChild(challengeHint);
                                challengeHint.draw(mainModel.challengeProgress);
                            }
                        break;
                    }
                    view.hintGroup.x = HintEvent(event).x + 10;
                    view.hintGroup.y = HintEvent(event).y + 10;
                        
                    if ((view.hintGroup.x + view.hintGroup.width) > view.stage.stageWidth) {
                        view.hintGroup.x = HintEvent(event).x - view.hintGroup.width;
                    }
                    if ((view.hintGroup.y + view.hintGroup.height) > view.stage.stageHeight) {
                        view.hintGroup.y = HintEvent(event).y - view.hintGroup.height;
                    }
                    break;
                case HintEvent.HIDE:
                    spellHint.visible = false;
                    artikulHint.visible = false;
                    challengeHint.visible = false;
                    view.hintGroup.removeChildren();
                break;
                case QuestEvent.QUEST_COMPLETE_CLIENT_COMMAND:
                    if (_deactivatedState) {return;}
                    var alertQuestView:AlertQuestCompleteView = new AlertQuestCompleteView();
                    view.layoutAlert.addChild(alertQuestView);
                break;
                case GameEvent.MESSAGE_CLIENT_COMMAND:
                    if (_deactivatedState) {return;}
                    switch (mainModel.message.type) {
                        case DataMessageType.GOT:
                            if (mainModel.message.got && mainModel.message.got.got.length > 0) {
                                var alertViewGot:AlertView = new AlertView(mainModel.message.type);
                                view.layoutAlert.addChild(alertViewGot);
                            }
                        break;
                        case DataMessageType.DAILY_BONUS:
                        case DataMessageType.VICTORY:
                        case DataMessageType.DEFEAT:
                        case DataMessageType.LEVEL_UP:
                        case DataMessageType.ACHIEVE:
                            var alertView:AlertView = new AlertView(mainModel.message.type);
                            view.layoutAlert.addChild(alertView);
                        break;
                        case DataMessageType.TEXT:
                            switch (mainModel.message.text.text.type) {
                                case DataTextType.INFO:
                                    ELAlert.show(MainModel.getLocale(mainModel.message.text.text.info.text), "СООБЩЕНИЕ");
                                break;
                                case DataTextType.ERROR:
                                    ELAlert.show(MainModel.getLocale(mainModel.message.text.text.error.text), "ВНИМАНИЕ");
                                break;
                            }
                        break;
                    }
                break;
                case ConnectionEvent.VERSION_CLIENT_ERROR:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new ServerVersionView());
                break;
                case LoginEvent.PLAYER_FRACTION_UNDEFINED:
                    if (_deactivatedState) {return;}
                    deleteView(view.layoutGroup);
                    view.layoutGroup.addChild(new FractionView());
                break;
            }
		}
    
        private function view_resizeHandler(event:Event):void
        {
            backgroundImage.x = int((view.stage.stageWidth-backgroundImage.width)/2);
            backgroundImage.y = int((view.stage.stageHeight-backgroundImage.height)/2);
        }
}
}
