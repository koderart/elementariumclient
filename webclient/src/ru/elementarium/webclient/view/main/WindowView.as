package ru.elementarium.webclient.view.main {

import feathers.controls.LayoutGroup;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class WindowView extends Sprite
    {
        protected var _sprite:Sprite;
        public var _titleLabel:TextField;
        public var _mainLayout:LayoutGroup;
        public var _contentSprite:Sprite;
        public var _closeButton:starling.display.Button;

        public var _contentLayout:LayoutGroup;
    
        protected var _content:Sprite;

        public function WindowView()
        {
            super();
            
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.window, false, this) as Sprite;
                        
            VerticalLayout(_mainLayout.layout).paddingLeft = 10;
            VerticalLayout(_mainLayout.layout).paddingRight = 10;
            VerticalLayout(_mainLayout.layout).paddingBottom = 10;
                
            _closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        }

        protected function sprite_addedToStageHandler(event:Event):void
        {
            _sprite.removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            var bgModal:Quad = new Quad(stage.stageWidth, stage.stageHeight, 0);
            bgModal.alpha = 0.6;
            addChildAt(bgModal, 0);
            _sprite.x = int((stage.stageWidth-_sprite.width)/2);
            //Transitions.EASE_OUT
            var tween:Tween = new Tween(_sprite, 0.5, Transitions.EASE_OUT);
            tween.animate('y', int((stage.stageHeight-_sprite.height)/2));
            tween.roundToInt = true;//roundProps for fix the blink when collapsing item
            //tween.onComplete = _onTweenViewportComplete;

            Starling.current.juggler.add(tween);
        }
        
        protected function validateAndAttach():void
        {
            _contentSprite.addChild(_content);
            _contentLayout.validate();
            _mainLayout.validate();
            _closeButton.x = _mainLayout.width;

            _sprite.addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            _sprite.y = -_sprite.height;
            addChild(_sprite);
        }
    
        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _closeButton:
                    removeFromParent(true);
                    break;
            }
        }
    
        public function get sprite():Sprite {
            return _sprite;
        }
}
}
