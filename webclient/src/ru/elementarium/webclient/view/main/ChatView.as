package ru.elementarium.webclient.view.main {
import com.junkbyte.console.Cc;

import feathers.controls.Label;
import feathers.controls.List;
import feathers.controls.ScrollContainer;
import feathers.controls.TextInput;
import feathers.controls.renderers.IListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.game.componets.UserItemRenderer;
import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.ResizeEvent;
import starling.text.TextField;

public class ChatView extends Sprite
    {
        private var _sprite:Sprite;

        public var _userList:List;
    
        public var _chatSprite:Sprite;
        public var _chatInput:TextInput;
        public var _sendChatButton:Button;
        public var _scrollChat:ScrollContainer;
        public var _textChat:Label;
        public var _minimizeChatButton:Button;
        public var _newMessageButton:Button;

        public var _userListSprite:Sprite;
        public var _minimizeUserListButton:Button;

        public var _userListLabel:TextField;
        public var _chatLabel:TextField;

        private var _minimizedChat:Boolean = false;
        private var _minimizedUserList:Boolean = false;
    
        public function ChatView()
        {
            super();
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.chat, false, this) as Sprite;

            _textChat.width = 285;
            _textChat.maxWidth = 285;
            _textChat.wordWrap = true;

            _chatInput.maxChars = 255;

            _newMessageButton.visible = false;
            
            _scrollChat.styleName = "chatScroll";
            
            _userList.itemRendererFactory = function():IListItemRenderer { return new UserItemRenderer(); };

            addChild(_sprite);

            addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
        }
    
        protected function stage_resizeHandler(event:Event):void
        {
            setPosition();
        }
    
        protected function sprite_addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
            setPosition();
        }

        protected function sprite_removeFromStageHandler(event:Event):void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.removeEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
        }

        protected function setPosition():void
        {
            _chatSprite.x = 2;
            if (_minimizedChat) {
                _chatSprite.y = stage.stageHeight - 21;
            } else {
                _chatSprite.y = int(stage.stageHeight - _chatSprite.height) - 1;
            }

            _userListSprite.x = int(stage.stageWidth - _userListSprite.width) - 1;
            if (_minimizedUserList) {
                _userListSprite.y = stage.stageHeight - 21;
            } else {
                _userListSprite.y = int(stage.stageHeight - _userListSprite.height) - 1;
            }
        }
    
        public function newMessage():void
        {
            if (_minimizedChat) {
                _newMessageButton.visible = true;
            }
        }

        public function minimizeAll():void
        {
            if (!_minimizedChat) {
                turnChat();
            }
            if (!_minimizedUserList) {
                turnUserList();
            }
        }
    
        public function maximizeAll():void
        {
            if (_minimizedChat) {
                turnChat();
            }
            if (_minimizedUserList) {
                turnUserList();
            }
        }
    
        public function turnChat():void
        {
            _newMessageButton.visible = false;
            var tween:Tween = new Tween(_chatSprite, 0.5, Transitions.EASE_OUT);
            if (_minimizedChat) {
                tween.animate('y', int(stage.height - _chatSprite.height) - 1);
            } else {
                tween.animate('y', stage.height - 21);
            }
            
            tween.roundToInt = true;//roundProps for fix the blink when collapsing item
            Starling.current.juggler.add(tween);
            
            _minimizedChat = !_minimizedChat;
            _minimizeChatButton.scaleY = _minimizedChat ? -1 : 1;
        }

        public function turnUserList():void {
            var tween:Tween = new Tween(_userListSprite, 0.5, Transitions.EASE_OUT);
            if (_minimizedUserList) {
                tween.animate('y', int(stage.height - _userListSprite.height) - 1);
            } else {
                tween.animate('y', stage.height - 21);
            }
            
            tween.roundToInt = true;//roundProps for fix the blink when collapsing item
            Starling.current.juggler.add(tween);

            _minimizedUserList = !_minimizedUserList;
            _minimizeUserListButton.scaleY = _minimizedUserList ? -1 : 1;
        }

    public function get minimizedChat():Boolean
    {
        return _minimizedChat;
    }

    public function get minimizedUserList():Boolean
    {
        return _minimizedUserList;
    }
}
}
