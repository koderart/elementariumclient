package ru.elementarium.webclient.view.main
{
import com.junkbyte.console.Cc;

import feathers.data.ListCollection;
import feathers.events.FeathersEventType;
import flash.utils.getTimer;
import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.Match3Model;
import ru.muvs.elementarium.thrift.chat.message.DataChatMessage;
import ru.muvs.elementarium.thrift.location.member.DataLocationMember;
import ru.muvs.elementarium.thrift.location.member.DataLocationMemberType;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;

import starling.display.DisplayObject;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.utils.StringUtil;

public class ChatViewMediator extends Mediator
	{
		[Inject]
		public var view:ChatView;
        [Inject]
        public var mainModel:MainModel;
        [Inject]
        public var gameModel:Match3Model;

        private var userListCollection:ListCollection;
        private var lastSendTime:uint;

        override public function onRemove():void
        {
            super.onRemove();
            trace("Chat mediator onRemove");
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE, gameEventTypeHandler);
            removeContextListener(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND, gameEventTypeHandler);
            removeContextListener(ChatEvent.CHAT_MESSAGE_SEND_CLIENT_COMMAND, gameEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_JOIN_CLIENT_COMMAND, gameEventTypeHandler);
            removeContextListener(BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND, gameEventTypeHandler);
        }

		override public function onRegister():void
		{
            super.onRegister();
            trace("Chat mediator onRegister");
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE, gameEventTypeHandler);
            addContextListener(LocationEvent.LOCATION_LOAD_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(ChatEvent.CHAT_MESSAGE_SEND_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_JOIN_CLIENT_COMMAND, gameEventTypeHandler);
            addContextListener(BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND, gameEventTypeHandler);

            view._sendChatButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._minimizeChatButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._newMessageButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._minimizeUserListButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            view._userListLabel.addEventListener(TouchEvent.TOUCH, onTouch);
            view._chatLabel.addEventListener(TouchEvent.TOUCH, onTouch);

            view._chatInput.addEventListener( FeathersEventType.ENTER, input_enterHandler );
            
            userListCollection = new ListCollection();
            view._userList.dataProvider = userListCollection;

        }

        private function input_enterHandler(event:Event):void {
            sendMessage(view._chatInput.text);
        }
    
        private function gameEventTypeHandler(event:Event):void {
            switch (event.type) {
                case BattleEventType.BATTLE_JOIN_CLIENT_COMMAND:
                    view.minimizeAll();
                break;
                case BattleEventType.BATTLE_UNJOIN_CLIENT_COMMAND:
                    view.maximizeAll();
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_JOIN:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_UN_JOIN:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ENABLE:
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_DISABLE:
                    userListCollection.setItemAt(mainModel.locationMember, getUserIndex());
                break;
                case LocationEvent.LOCATION_LOAD_CLIENT_COMMAND:
                    if (mainModel.locationMembers) {
                        userListCollection.removeAll();
                        for each (var client:DataLocationMember in mainModel.locationMembers) {
                            if (client.type == DataLocationMemberType.PLAYER) {
                                userListCollection.addItem(client);
                            }
                        }
                    }
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_ADD:
                    if (getUserIndex() == -1) {
                        userListCollection.addItem(mainModel.locationMember);
                    }
                break;
                case LocationEvent.LOCATION_ACTION_CLIENT_MEMBER_REMOVE:
                    userListCollection.removeItemAt(getUserIndex());
                break;
                case ChatEvent.CHAT_MESSAGE_SEND_CLIENT_COMMAND:

                    var chatCounter:Array = view._textChat.text.split("\n");
                    if (chatCounter.length > 50) {
                        chatCounter = chatCounter.slice(chatCounter.length-50);
                        view._textChat.text = chatCounter.join("\n");
                    }

                    var msg:String;
                    var date:Date = new Date(mainModel.chatAction.messageChannel.message.time*1000);
                    var color:String;
                    switch (mainModel.chatAction.messageChannel.source.player.element) {
                        case DataPlayerElement.DARK:
                            color = "#a350da";
                        break;
                        case DataPlayerElement.FIRE:
                            color = "#c13d40";
                        break;
                        case DataPlayerElement.LIGHT:
                            color = "#c7913f";
                        break;
                        case DataPlayerElement.NATURE:
                            color = "#649f21";
                        break;
                        case DataPlayerElement.WATER:
                            color = "#1d8fd8";
                        break;
                    }
                    msg = "[";
                    msg += date.hours<10 ? "0"+date.hours : date.hours.toString();
                    msg += ":";
                    msg += date.minutes<10 ? "0"+date.minutes : date.minutes.toString();
                    msg += "] ";
                    msg += "<font color='"+color+"'>"+ mainModel.chatAction.messageChannel.source.player.name+"</font>";
                    msg += " : ";
                    msg += mainModel.chatAction.messageChannel.message.text+"\n";
                    view._textChat.text += msg;
                    view._textChat.validate();
                    view._scrollChat.validate();
                    view._scrollChat.verticalScrollPosition = view._scrollChat.maxVerticalScrollPosition;

                    view.newMessage();
                break;
            }
        }

        private function getUserIndex():int {
            for (var i:int=userListCollection.length-1; i >= 0 ; i--) {
                var member:DataLocationMember = userListCollection.getItemAt(i) as DataLocationMember;
                if (member.id == mainModel.locationMember.id) {
                    return i;
                }
            }
            return -1;
        }

        private function onTouch(event:TouchEvent):void
        {
            var object:DisplayObject = event.currentTarget as DisplayObject;
            var touch:Touch = event.getTouch(object);
            if (touch && touch.phase == TouchPhase.ENDED) {
                switch (object) {
                    case view._chatLabel:
                        view.turnChat();
                        break;
                    case view._userListLabel:
                        view.turnUserList();
                        break;
                }
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._sendChatButton:
                    sendMessage(view._chatInput.text);
                break;
                case view._minimizeChatButton:
                case view._newMessageButton:
                case view._chatLabel:
                    view.turnChat();
                break;
                case view._minimizeUserListButton:
                case view._userListLabel:
                    view.turnUserList();
                break;
            }
        }

        private function sendMessage(message:String):void {
            var now:uint = getTimer();
            if ((now - lastSendTime) < 1500){
                return;
            }

            var e:ChatEvent = new ChatEvent(ChatEvent.CHAT_MESSAGE_SEND_SERVER_COMMAND);
            e.message = new DataChatMessage();
            e.message.text = StringUtil.trim(message);

            if (e.message.text.length > 0) {
                dispatch(e);
            }
            lastSendTime = now;
            view._chatInput.text = "";
        }
}
}