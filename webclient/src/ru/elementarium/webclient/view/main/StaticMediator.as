package ru.elementarium.webclient.view.main
{
import com.junkbyte.console.Cc;

import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.MainModel;
import starling.events.Event;

public class StaticMediator extends Mediator
	{
		[Inject]
		public var view:StaticView;
		[Inject]
        public var mainModel:MainModel;

        override public function onRemove():void
        {
            super.onRemove();
            trace("Static mediator onRemove");
            removeContextListener(StaticDataEvent.PROGRESS, appEventTypeHandler);
            removeContextListener(StaticDataEvent.LOADED, appEventTypeHandler);
            removeContextListener(StaticDataEvent.LOADED_ERROR, appEventTypeHandler);
            removeContextListener(ConnectionEvent.CONNECTED, appEventTypeHandler);
            removeContextListener(LoginEvent.SOCIAL_ERROR, appEventTypeHandler);
            removeContextListener(ConnectionEvent.DISCONNECTED, appEventTypeHandler);
            removeContextListener(ConnectionEvent.CONNECT_FAILED, appEventTypeHandler);
            removeContextListener(LoginEvent.LOGIN_FAILED, appEventTypeHandler);
        }

        override public function onRegister():void
		{
		    super.onRegister();
            addContextListener(StaticDataEvent.PROGRESS, appEventTypeHandler);
            addContextListener(StaticDataEvent.LOADED, appEventTypeHandler);
            addContextListener(StaticDataEvent.LOADED_ERROR, appEventTypeHandler);
            addContextListener(ConnectionEvent.CONNECTED, appEventTypeHandler);
            addContextListener(LoginEvent.SOCIAL_ERROR, appEventTypeHandler);
            addContextListener(ConnectionEvent.DISCONNECTED, appEventTypeHandler);
            addContextListener(ConnectionEvent.CONNECT_FAILED, appEventTypeHandler);
            addContextListener(LoginEvent.LOGIN_FAILED, appEventTypeHandler);

            if (view.disconnectedFlag) {
                view._messageLabel.text = "Подключение завершено.";
                view._connectButton.label = "ПОДКЛЮЧИТЬ";
                view.buttonState(true);
            } else {
                dispatch(new StaticDataEvent(StaticDataEvent.LOAD));
                view.buttonState(false);
                view._messageLabel.text = "Загрузка данных...";
            }
            view._connectButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view._connectButton:
                    dispatch(new StaticDataEvent(StaticDataEvent.LOAD));
                    view._connectButton.visible = view._connectButton.includeInLayout = false;
                    break;
            }
        }

        private function appEventTypeHandler(event:Event):void
		{
            switch (event.type) {
                case StaticDataEvent.PROGRESS:
                    view._messageLabel.text = "Загрузка данных... "+event.data+"%";
                    break;
                case StaticDataEvent.LOADED:
                    view._messageLabel.text = "Подключение...";
                    view.buttonState(false);
                    break;
                case StaticDataEvent.LOADED_ERROR:
                    view._messageLabel.text = "Ошибка загрузки данных. Проверьте подключение к интернету.";
                    view._connectButton.label = "ПОВТОРИТЬ";
                    view.buttonState(true);
                    break;
                case ConnectionEvent.CONNECTED:
                    view._messageLabel.text = "Авторизация...";
                    break;
                case LoginEvent.SOCIAL_ERROR:
                    view._messageLabel.text = "Ошибка подключения к социальной сети.\nПожалуйста, отключите блокоровщик рекламы (AdBlock, Ghostery и т.д.)\nи обновите страницу.";
                    break;
                case ConnectionEvent.DISCONNECTED:
                    view._messageLabel.text = "Подключение завершено.";
                    view._connectButton.label = "ПОДКЛЮЧИТЬ";
                    view.buttonState(true);
                    break;
                case ConnectionEvent.CONNECT_FAILED:
                    view._messageLabel.text = "Ошибка подключения.";
                    view._connectButton.label = "ПОДКЛЮЧИТЬ";
                    view.buttonState(true);
                    break;
                case LoginEvent.LOGIN_FAILED:
                    if (mainModel.socialVkData || mainModel.socialMyMailData) {
                        view._messageLabel.text = "Ошибка входа через соц. сеть.";
                    } else {
                        view._messageLabel.text = "Ошибка авторизации.";
                    }
                    break;
            }
		}
}
}
