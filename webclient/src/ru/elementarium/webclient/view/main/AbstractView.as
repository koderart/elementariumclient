package ru.elementarium.webclient.view.main {

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;

public class AbstractView extends Sprite
    {
        protected var _sprite:Sprite;

        public function AbstractView()
        {
            super();
            if (_sprite) {
                _sprite.addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
                _sprite.y = -_sprite.height;
                addChild(_sprite);
            }
        }

        protected function sprite_addedToStageHandler(event:Event):void {
            _sprite.removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            var bgModal:Quad = new Quad(stage.stageWidth, stage.stageHeight, 0);
            bgModal.alpha = 0.6;
            addChildAt(bgModal, 0);
            _sprite.x = int((stage.stageWidth-_sprite.width)/2);
            //Transitions.EASE_OUT
            var tween:Tween = new Tween(_sprite, 0.5, Transitions.EASE_OUT);
            tween.animate('y', int((stage.stageHeight-_sprite.height)/2));
            tween.roundToInt = true;//roundProps for fix the blink when collapsing item
            //tween.onComplete = _onTweenViewportComplete;

            Starling.current.juggler.add(tween);
        }

        public function get sprite():Sprite {
            return _sprite;
        }
}
}
