package ru.elementarium.webclient.view.main {
import com.junkbyte.console.Cc;

import feathers.controls.Button;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.ResizeEvent;
import starling.text.TextField;

    public class ServerVersionView extends Sprite
    {
        private var _sprite:Sprite;

        public var _background:Image;
        public var _titleLabel:TextField;
        public var _messageLabel:TextField;
        public var _connectButton:Button;

        public function ServerVersionView()
        {
            super();
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.game_connection, false, this) as Sprite;

            _titleLabel.text = "ОБНОВЛЕНИЕ";

            if (MainModel.isMobile) {
                _messageLabel.text = "Ваша версия игы устарела. Пожалуйста, обновите приложение через App Store.";
                // https://itunes.apple.com/ru/app/elementarium/id1148548108?mt=8
            } else {
                _messageLabel.text = "Ваша версия игы устарела. Пожалуйста, очистите кеш браузера и обновите страницу.";
            }

            _connectButton.visible = false;
            _background.height = 126;

            this.addChild(_sprite);

            addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
        }

        protected function sprite_addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
            _sprite.x = int((stage.stageWidth - _sprite.width)/2);
            _sprite.y = int((stage.stageHeight - _sprite.height)/2);
        }

        protected function sprite_removeFromStageHandler(event:Event):void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.removeEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
        }

        protected function stage_resizeHandler(event:Event):void
        {
            _sprite.x = int((stage.stageWidth - _sprite.width)/2);
            _sprite.y = int((stage.stageHeight - _sprite.height)/2);
        }
    }
}
