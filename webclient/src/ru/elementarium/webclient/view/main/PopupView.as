package ru.elementarium.webclient.view.main {

import starling.animation.Transitions;
import starling.animation.Tween;
import starling.core.Starling;
import starling.display.DisplayObject;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class PopupView extends Sprite
    {
        protected var _sprite:Sprite;

        public function PopupView()
        {
            super();
            if (_sprite) {
                _sprite.addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
                addChild(_sprite);
            }
        }

        protected function sprite_addedToStageHandler(event:Event):void {
            _sprite.removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            var bgModal:Quad = new Quad(parent.width, parent.height, 0);
            bgModal.alpha = 0.4;
            bgModal.addEventListener(TouchEvent.TOUCH, onBgModalTouch);
            addChildAt(bgModal, 0);
    
            _sprite.x = int((stage.stageWidth-_sprite.width)/2);
            _sprite.y = int((stage.stageHeight-_sprite.height)/2);
    
            _sprite.alpha = 0;
            var tween:Tween = new Tween(_sprite, 0.5, Transitions.EASE_OUT);
            tween.animate('alpha', 1);
    
            Starling.current.juggler.add(tween);
        }
    
        private function onBgModalTouch(event:TouchEvent):void
        {
            var touch:Touch = event.getTouch(event.target as DisplayObject);
            if (touch && touch.phase == TouchPhase.BEGAN) {
                event.target.removeEventListener(TouchEvent.TOUCH, onBgModalTouch);
                this.removeFromParent(true);
            }
        }
}
}
