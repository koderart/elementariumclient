package ru.elementarium.webclient.view.main.components {
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;
import feathers.controls.List;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.layout.HorizontalLayout;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.elementarium.thrift.challenge.*;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class ChallengeHint extends Sprite
{
    private var _sprite:Sprite;
    private var layoutGroup:LayoutGroup;
    private var background:Image;
    private var headerSeparator:Image;
    
    private var titleText:TextField;
    private var descriptionText:TextField;
    private var targetText:TextField;
    private var awardText:TextField;
    private var listProgress:List;
    private var listProgressData:ListCollection = new ListCollection();

    public function ChallengeHint()
    {
        super();
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_challenge) as Sprite;
        addChild(_sprite);

        layoutGroup = _sprite.getChildByName("layoutGroup") as LayoutGroup;

        titleText = layoutGroup.getChildByName("titleText") as TextField;
        titleText.text = "В поисках душ";

        headerSeparator = layoutGroup.getChildByName("headerSeparator") as Image;
        headerSeparator.width = titleText.width + 20;

        descriptionText = layoutGroup.getChildByName("descriptionText") as TextField;
        descriptionText.text = "Верховные лидеры объявили всем, что при достаточном количестве душ, они проведут обряд, который откроет новые земли.\nУбивая монстров на локациях, с некоторой вероятностью вы сможете получить \“Душу монстра\”.\n“Чистую душу” можно получить только при победе в колизее.";
        targetText = layoutGroup.getChildByName("targetText") as TextField;
        
        awardText = layoutGroup.getChildByName("awardText") as TextField;
        awardText.text = "Новые земли, с редкими ресурсами и амуницией. А так же вы встретитесь с другими стихийными народами.";

        listProgress = layoutGroup.getChildByName("listProgress") as List;
        listProgress.itemRendererFactory = function():IListItemRenderer { return new ChallengeArtikulItemRenderer(); };
        var layoutH:HorizontalLayout = new HorizontalLayout();
        layoutH.gap = 20;
        layoutH.horizontalAlign = HorizontalLayout.HORIZONTAL_ALIGN_CENTER;
        listProgress.layout = layoutH;
        listProgress.dataProvider = listProgressData;
        
        background = _sprite.getChildByName("background") as Image;
    }

    public function draw(challengeProgressList:Array):void
    {
        targetText.text = "";
        listProgressData.removeAll();
        /*
        for each (var challengeProgress:DataChallengeProgress in challengeProgressList) {
            var challenge : DataChallenge = MainModel.challegeData[challengeProgress.challengeId];
            for each(var challengeGoal:DataChallengeGoal in challenge.goals) {
                var currentCount:int = 0;
                for each(var challengeGoalProgress:DataChallengeGoalProgress in challengeProgress.progresses) {
                    if (challengeGoal.challengeGoalId == challengeGoalProgress.challengeGoalId) {
                        currentCount = challengeGoalProgress.count;
                    }
                }
                targetText.text += "Собрать: " + ArtikulVO(MainModel.artikulData[challengeGoal.artikulId]).title.ru +" x "+challengeGoal.count+"\n";
                listProgressData.addItem({artikulId:challengeGoal.artikulId, currentCount:currentCount, count:challengeGoal.count});
            }
        }
        */
        layoutGroup.validate();
        background.height = layoutGroup.height;
    }
}
}
