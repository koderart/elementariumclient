package ru.elementarium.webclient.view.main.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;

import flash.utils.Dictionary;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.ArtikulSkillBaseVO;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.MoneyVO;
import ru.muvs.admin.main.vo.enum.ColorEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.admin.main.vo.enum.SkillEnum;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.skill.DataArtikulRandomSkill;
import ru.muvs.elementarium.thrift.skill.DataSkillType;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class ArtikulHint extends Sprite
{
    private var _sprite:Sprite;
    private var backgroundImage:Image;
    private var borderImage:Image;
    private var itemRarity:Image;

    private var titleLabel:TextField;
    private var repairTitle:TextField;
    private var weightyTitle:TextField;
    private var skillsLabel:TextField;
    private var imageLoader:ImageLoader;
    
    private var layoutGroup:LayoutGroup;
    private var moneyPosition:Sprite;
    private var moneyGroup:LayoutGroup;


    public function ArtikulHint()
    {
        super();
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_artikul) as Sprite;
        addChild(_sprite);

        layoutGroup = _sprite.getChildByName("layoutGroup") as LayoutGroup;
        titleLabel = _sprite.getChildByName("titleLabel") as TextField;
        repairTitle = layoutGroup.getChildByName("repairTitle") as TextField;
        weightyTitle = layoutGroup.getChildByName("weightyTitle") as TextField;
        skillsLabel = layoutGroup.getChildByName("skillsLabel") as TextField;

        backgroundImage = _sprite.getChildByName("backgroundImage") as Image;
        borderImage = _sprite.getChildByName("borderImage") as Image;
        itemRarity = _sprite.getChildByName("itemRarity") as Image;

        imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
        moneyGroup = layoutGroup.getChildByName("moneyGroup") as LayoutGroup;
        moneyPosition = _sprite.getChildByName("moneyPosition") as Sprite;
    }

    public function draw(artikulId:int,userLevel:int,userElement:int,durabilityCurrent:int, durability:int, randomSkills:Array):void
    {
        var randomSkillsDic:Dictionary = new Dictionary();
        for each (var skillRandom:DataArtikulRandomSkill in randomSkills) {
            randomSkillsDic[skillRandom.value.type] = skillRandom.value.value;
        }
        
        var currentElement:String;
        switch(userElement) {
            case DataPlayerElement.DARK: currentElement = ElementEnum.DARK; break;
            case DataPlayerElement.FIRE: currentElement = ElementEnum.FIRE; break;
            case DataPlayerElement.LIGHT: currentElement = ElementEnum.LIGHT; break;
            case DataPlayerElement.NATURE: currentElement = ElementEnum.NATURE; break;
            case DataPlayerElement.WATER: currentElement = ElementEnum.WATER; break;
        }

        var data :ArtikulVO = MainModel.artikulData[artikulId];

        titleLabel.text = data.title.ru;
        imageLoader.source = ConnectionService.BASE_URL_HTTP+data.image.url;
        switch (data.artikulColor) {
            case ColorEnum.GREY:
                itemRarity.color = 0x585858;
            break;
            case ColorEnum.GREEN:
                itemRarity.color = 0x0b750b;
            break;
            case ColorEnum.BLUE:
                itemRarity.color = 0x0048c0;
            break;
            case ColorEnum.ORANGE:
                itemRarity.color = 0xf0820b;
                break;
            case ColorEnum.PURPLE:
                itemRarity.color = 0x691988;
            break;
            case ColorEnum.RED:
                itemRarity.color = 0xad010f;
            break;
        }
        moneyGroup.removeChildren(1,-1,true);
        moneyGroup.visible = moneyGroup.includeInLayout = data.artikulMoney.length > 0;
        for each (var money:MoneyVO in data.artikulMoney) {
            var moneySprite:Sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_artikul_money) as Sprite;

            var moneyLabel:TextField = moneySprite.getChildByName("moneyLabel") as TextField;
            moneyLabel.text = money.money.toString();
            var goldIcon:Image = moneySprite.getChildByName("goldIcon") as Image;
            goldIcon.visible = false;

            var crystalIcon:Image = moneySprite.getChildByName("crystalIcon") as Image;
            crystalIcon.visible = false;

            if (money.moneyType == MoneyEnum.GOLD) {
                goldIcon.visible = true;
            }
            if (money.moneyType == MoneyEnum.CRYSTAL) {
                crystalIcon.visible = true;
            }
            moneyGroup.addChild(moneySprite);
        }

        skillsLabel.text = "";
        if (data.levelMin.levelId) {
            
            skillsLabel.text += MainModel.getLocale("artikul.hint.level") + " " + data.levelMin.number;
        }
        if (data.levelMax.levelId) {
            skillsLabel.text += " - " + data.levelMax.number;
        }
        if (data.levelMin.levelId || data.levelMax.levelId) {
            skillsLabel.text += "\n";
        }
        if (data.durability > 0 || data.durabilityCurrent > 0) {
            skillsLabel.text += MainModel.getLocale("artikul.hint.durability") + " "+durabilityCurrent+"/"+durability+"\n";
            if (durabilityCurrent==0) {
                repairTitle.text = durability==0 ? MainModel.getLocale("artikul.hint.durabilityzero") : MainModel.getLocale("artikul.hint.needrepair");
            } else {
                repairTitle.text = "";
            }
        } else {
            repairTitle.text = "";
        }
        if (data.weighty) {
            weightyTitle.text = MainModel.getLocale("artikul.hint.weighty");
        } else {
            weightyTitle.text = "";
        }
        
        for each (var artikulSkill:ArtikulSkillBaseVO in data.artikulSkillBase) {
            if (artikulSkill.element == currentElement) {
                var randomValue:int;
                switch (artikulSkill.skill.skill) {
                    case SkillEnum.HP:
                        randomValue = randomSkillsDic[DataSkillType.HP];
                        delete randomSkillsDic[DataSkillType.HP];
                        break;
                    case SkillEnum.DAMAGE:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE];
                        delete randomSkillsDic[DataSkillType.DAMAGE];
                        break;
                    case SkillEnum.DAMAGE_DARK:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE_DARK];
                        delete randomSkillsDic[DataSkillType.DAMAGE_DARK];
                        break;
                    case SkillEnum.DAMAGE_FIRE:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE_FIRE];
                        delete randomSkillsDic[DataSkillType.DAMAGE_FIRE];
                        break;
                    case SkillEnum.DAMAGE_LIGHT:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE_LIGHT];
                        delete randomSkillsDic[DataSkillType.DAMAGE_LIGHT];
                        break;
                    case SkillEnum.DAMAGE_NATURE:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE_NATURE];
                        delete randomSkillsDic[DataSkillType.DAMAGE_NATURE];
                        break;
                    case SkillEnum.DAMAGE_WATER:
                        randomValue = randomSkillsDic[DataSkillType.DAMAGE_WATER];
                        delete randomSkillsDic[DataSkillType.DAMAGE_WATER];
                        break;
                    case SkillEnum.PROTECTION:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION];
                        delete randomSkillsDic[DataSkillType.PROTECTION]; 
                        break;
                    case SkillEnum.PROTECTION_DARK:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION_DARK];
                        delete randomSkillsDic[DataSkillType.PROTECTION_DARK];
                        break;
                    case SkillEnum.PROTECTION_FIRE:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION_FIRE];
                        delete randomSkillsDic[DataSkillType.PROTECTION_FIRE];
                        break;
                    case SkillEnum.PROTECTION_LIGHT:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION_LIGHT];
                        delete randomSkillsDic[DataSkillType.PROTECTION_LIGHT];
                        break;
                    case SkillEnum.PROTECTION_NATURE:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION_NATURE];
                        delete randomSkillsDic[DataSkillType.PROTECTION_NATURE];
                        break;
                    case SkillEnum.PROTECTION_WATER:
                        randomValue = randomSkillsDic[DataSkillType.PROTECTION_WATER];
                        delete randomSkillsDic[DataSkillType.PROTECTION_WATER];
                        break;
                }
                skillsLabel.text += artikulSkill.skill.title.ru + ": ";
                if (randomValue) {
                    skillsLabel.text += (artikulSkill.value+randomValue)+ " ("+randomValue+")";
                } else {
                    skillsLabel.text += artikulSkill.value;
                }
                skillsLabel.text += "\n";
            }
        }

        for (var key:Object in randomSkillsDic) {
            var skillTitle:String;
            switch (key) {
                case DataSkillType.HP:
                    skillTitle = MainModel.skillTitleData[SkillEnum.HP].ru;
                    break;
                case DataSkillType.DAMAGE:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE].ru;
                    break;
                case DataSkillType.PROTECTION:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION].ru;
                    break;
                case DataSkillType.DAMAGE_DARK:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE_DARK].ru;
                    break;
                case DataSkillType.PROTECTION_DARK:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION_DARK].ru;
                    break;
                case DataSkillType.DAMAGE_FIRE:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE_FIRE].ru;
                    break;
                case DataSkillType.PROTECTION_FIRE:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION_FIRE].ru;
                    break;
                case DataSkillType.DAMAGE_LIGHT:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE_LIGHT].ru;
                    break;
                case DataSkillType.PROTECTION_LIGHT:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION_LIGHT].ru;
                    break;
                case DataSkillType.DAMAGE_WATER:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE_WATER].ru;
                    break;
                case DataSkillType.PROTECTION_WATER:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION_WATER].ru;
                    break;
                case DataSkillType.DAMAGE_NATURE:
                    skillTitle = MainModel.skillTitleData[SkillEnum.DAMAGE_NATURE].ru;
                    break;
                case DataSkillType.PROTECTION_NATURE:
                    skillTitle = MainModel.skillTitleData[SkillEnum.PROTECTION_NATURE].ru;
                    break;
            }
            
            skillsLabel.text += skillTitle + ": " + randomSkillsDic[key] +"\n";
        }
        
        if (data.description.ru) {
            skillsLabel.text += data.description.ru;
        }
        layoutGroup.invalidate();
        layoutGroup.validate();

        borderImage.height = layoutGroup.y + layoutGroup.height+10;
        backgroundImage.height = layoutGroup.y + layoutGroup.height-15;
    }
}
}
