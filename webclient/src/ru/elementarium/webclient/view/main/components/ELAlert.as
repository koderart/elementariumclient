package ru.elementarium.webclient.view.main.components {

import feathers.controls.Button;
import feathers.controls.LayoutGroup;
import feathers.core.PopUpManager;
import feathers.layout.HorizontalLayout;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class ELAlert extends Sprite
{
    private var _sprite:Sprite;
    private var titleLabel:TextField;
    private var messageLabel:TextField;
    public var buttonGroup:LayoutGroup;
    private var closeButton:starling.display.Button;


    public function ELAlert()
    {
        super();
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.alert) as Sprite;
        addChild(_sprite);

        titleLabel = _sprite.getChildByName("titleLabel") as TextField;
        messageLabel = _sprite.getChildByName("messageLabel") as TextField;

        buttonGroup = _sprite.getChildByName("buttonGroup") as LayoutGroup;
        var hLayout:HorizontalLayout = new HorizontalLayout();
        hLayout.gap = 15;
        hLayout.horizontalAlign = "center";
        buttonGroup.layout = hLayout;

        closeButton = _sprite.getChildByName("closeButton") as starling.display.Button;
        closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
    }

    public function triggeredHandler(event:Event):void {
        PopUpManager.removePopUp(this);
    }

    public static function show(message:String, title:String = null, buttons:Array = null,
                                isModal:Boolean = true, isCentered:Boolean = true):ELAlert
    {
        var alert:ELAlert = new ELAlert();
        alert.titleLabel.text = title;
        alert.messageLabel.text = message;
        
        if (!buttons) {
            buttons = [{ label: "OK", style:"pinkOkButton"}];
        }

        for each (var obj:Object in buttons) {
            var button:Button = new Button();
            button.label = obj.label;
            if (obj.triggered) {
                button.addEventListener(Event.TRIGGERED, obj.triggered);
            }
            if (obj.style) {
                button.styleName = obj.style;
            }
            button.addEventListener(Event.TRIGGERED, alert.triggeredHandler);

            alert.buttonGroup.addChild(button)
        }

        PopUpManager.addPopUp(alert, isModal, isCentered);
        return alert;
    }
}
}
