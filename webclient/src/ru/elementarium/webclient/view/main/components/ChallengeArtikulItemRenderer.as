package ru.elementarium.webclient.view.main.components {

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.enum.ColorEnum;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;


public class ChallengeArtikulItemRenderer extends LayoutGroupListItemRenderer
    {
        private var initialized:Boolean = false;
        private var _sprite:Sprite;
        private var titleLabel:TextField;
        private var imageLoader:ImageLoader;
        private var itemRarity:Image;
    
        public function ChallengeArtikulItemRenderer()
        {
            super();
        }

        override protected function initialize():void
        {
            initialized = true;

            if (!_sprite) {

                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_challenge_item) as Sprite;
                addChild(_sprite);

                titleLabel = _sprite.getChildByName("titleLabel") as TextField;
                imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
                imageLoader.width = 32;
                imageLoader.height = 32;
                itemRarity = _sprite.getChildByName("itemRarity") as Image;
                itemRarity.color = 0x585858;
                if (data) {
                    drawData();
                }
            }
        }

        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();
            }
        }

        private function drawData():void
        {
            var dataArtikul:ArtikulVO = MainModel.artikulData[data.artikulId];
            titleLabel.text = data.currentCount+"/"+data.count;
            imageLoader.source = ConnectionService.BASE_URL_HTTP+dataArtikul.image.url;
            switch (dataArtikul.artikulColor) {
                case ColorEnum.GREY:
                    itemRarity.color = 0x585858;
                    break;
                case ColorEnum.GREEN:
                    itemRarity.color = 0x0b750b;
                    break;
                case ColorEnum.BLUE:
                    itemRarity.color = 0x0048c0;
                    break;
                case ColorEnum.ORANGE:
                    itemRarity.color = 0xf0820b;
                    break;
                case ColorEnum.PURPLE:
                    itemRarity.color = 0x691988;
                    break;
                case ColorEnum.RED:
                    itemRarity.color = 0xad010f;
                    break;
            }
        }
    }
}
