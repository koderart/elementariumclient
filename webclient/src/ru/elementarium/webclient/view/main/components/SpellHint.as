package ru.elementarium.webclient.view.main.components {

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePoint;
import ru.muvs.elementarium.thrift.battle.point.DataBattlePointType;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class SpellHint extends Sprite
{
    private var _sprite:Sprite;
    private var needPanel:Sprite;
    private var background:Image;

    private var titleText:TextField;
    private var descriptionText:TextField;
    private var reloadingText:TextField;

    private var needNatureText:TextField;
    private var needDarkText:TextField;
    private var needFireText:TextField;
    private var needLightText:TextField;
    private var needWaterText:TextField;

    private var needNatureMarker:Image;
    private var needDarkMarker:Image;
    private var needFireMarker:Image;
    private var needLightMarker:Image;
    private var needWaterMarker:Image;

    private var readyNatureMarker:Image;
    private var readyDarkMarker:Image;
    private var readyFireMarker:Image;
    private var readyLightMarker:Image;
    private var readyWaterMarker:Image;

    public function SpellHint()
    {
        super();
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_spell) as Sprite;
        addChild(_sprite);

        needPanel = _sprite.getChildByName("needPanel") as Sprite;
        
        titleText = _sprite.getChildByName("titleText") as TextField;

        descriptionText = _sprite.getChildByName("descriptionText") as TextField;
        background = _sprite.getChildByName("background") as Image;
        
        reloadingText = needPanel.getChildByName("reloadingText") as TextField;
        needNatureText = needPanel.getChildByName("needNatureText") as TextField;
        needDarkText = needPanel.getChildByName("needDarkText") as TextField;
        needFireText = needPanel.getChildByName("needFireText") as TextField;
        needLightText = needPanel.getChildByName("needLightText") as TextField;
        needWaterText = needPanel.getChildByName("needWaterText") as TextField;

        needNatureMarker = needPanel.getChildByName("needNatureMarker") as Image;
        needDarkMarker = needPanel.getChildByName("needDarkMarker") as Image;
        needFireMarker = needPanel.getChildByName("needFireMarker") as Image;
        needLightMarker = needPanel.getChildByName("needLightMarker") as Image;
        needWaterMarker = needPanel.getChildByName("needWaterMarker") as Image;

        readyNatureMarker = needPanel.getChildByName("readyNatureMarker") as Image;
        readyDarkMarker = needPanel.getChildByName("readyDarkMarker") as Image;
        readyFireMarker = needPanel.getChildByName("readyFireMarker") as Image;
        readyLightMarker = needPanel.getChildByName("readyLightMarker") as Image;
        readyWaterMarker = needPanel.getChildByName("readyWaterMarker") as Image;
    }

    public function draw(spellId:int, userElement:int, points:Array=null):void
    {
        var data :Object = MainModel.spellData[spellId][userElement];
        titleText.text = data.title;
        descriptionText.text = data.description;
        reloadingText.text = data.reloading;

        needNatureText.text = "";
        needDarkText.text = "";
        needFireText.text = "";
        needLightText.text = "";
        needWaterText.text = "";

        needNatureMarker.visible = false;
        needDarkMarker.visible = false;
        needFireMarker.visible = false;
        needLightMarker.visible = false;
        needWaterMarker.visible = false;

        readyNatureMarker.visible = false;
        readyDarkMarker.visible = false;
        readyFireMarker.visible = false;
        readyLightMarker.visible = false;
        readyWaterMarker.visible = false;

        var pointDark:int = 0;
        var pointFire:int = 0;
        var pointLight:int = 0;
        var pointNature:int = 0;
        var pointWater:int = 0;

        if (points)
        for each (var point:DataBattlePoint in points) {
            switch (point.type) {
                case DataBattlePointType.DARK:
                    pointDark = point.value;
                    break;
                case DataBattlePointType.FIRE:
                    pointFire = point.value;
                    break;
                case DataBattlePointType.LIGHT:
                    pointLight = point.value;
                    break;
                case DataBattlePointType.NATURE:
                    pointNature = point.value;
                    break;
                case DataBattlePointType.WATER:
                    pointWater = point.value;
                    break;
            }
        }

        if (data.needNature) {
            needNatureMarker.visible = true;
            needNatureText.text = data.needNature;
            if (pointNature >= data.needNature) { readyNatureMarker.visible = true; }
        }
        if (data.needDark) {
            needDarkMarker.visible = true;
            needDarkText.text = data.needDark;
            if (pointDark >= data.needDark) { readyDarkMarker.visible = true; }
        }
        if (data.needFire) {
            needFireMarker.visible = true;
            needFireText.text = data.needFire;
            if (pointFire >= data.needFire) { readyFireMarker.visible = true; }
        }
        if (data.needLight) {
            needLightMarker.visible = true;
            needLightText.text = data.needLight;
            if (pointLight >= data.needLight) { readyLightMarker.visible = true; }
        }
        if (data.needWater) {
            needWaterMarker.visible = true;
            needWaterText.text = data.needWater;
            if (pointWater >= data.needWater) { readyWaterMarker.visible = true; }
        }

        switch (userElement) {
            case DataPlayerElement.DARK:
                titleText.format.color = 0x9D38E3;
            break;
            case DataPlayerElement.FIRE:
                titleText.format.color = 0xDE212A;
            break;
            case DataPlayerElement.LIGHT:
                titleText.format.color = 0xE4A21F;
            break;
            case DataPlayerElement.NATURE:
                titleText.format.color = 0x5D9F16;
            break;
            case DataPlayerElement.WATER:
                titleText.format.color = 0x1B7CAF;
            break;
        }
        
        background.height = 76 + descriptionText.height;
        needPanel.y = 26 + descriptionText.height;
    }
}
}
