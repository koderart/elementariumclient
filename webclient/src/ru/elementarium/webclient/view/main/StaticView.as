package ru.elementarium.webclient.view.main {
import com.junkbyte.console.Cc;

import feathers.controls.Button;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.ResizeEvent;
import starling.text.TextField;

    public class StaticView extends Sprite
    {
        private var _sprite:Sprite;

        public var _background:Image;
        public var _titleLabel:TextField;
        public var _messageLabel:TextField;
        public var _connectButton:Button;

        public var disconnectedFlag:Boolean;

        public function StaticView(disconnected:Boolean=false)
        {
            super();
            disconnectedFlag = disconnected;
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.game_connection, false, this) as Sprite;

            _titleLabel.text = "ПОДКЛЮЧЕНИЕ";
            _connectButton.label = "ПОДКЛЮЧИТСЯ";

            this.addChild(_sprite);

            addEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
        }

        protected function sprite_addedToStageHandler(event:Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, sprite_addedToStageHandler);
            addEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.addEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
            _sprite.x = int((stage.stageWidth - _sprite.width)/2);
            _sprite.y = int((stage.stageHeight - _sprite.height)/2);
        }

        protected function sprite_removeFromStageHandler(event:Event):void
        {
            removeEventListener(Event.REMOVED_FROM_STAGE, sprite_removeFromStageHandler);
            stage.removeEventListener(ResizeEvent.RESIZE, stage_resizeHandler);
        }

        protected function stage_resizeHandler(event:Event):void
        {
            _sprite.x = int((stage.stageWidth - _sprite.width)/2);
            _sprite.y = int((stage.stageHeight - _sprite.height)/2);
        }

        public function buttonState(button:Boolean):void
        {
            _connectButton.visible = _connectButton.includeInLayout = button;
            _background.height = button ? 180 : 126;
        }
    }
}
