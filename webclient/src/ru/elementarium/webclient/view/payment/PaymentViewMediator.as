package ru.elementarium.webclient.view.payment
{

import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;

import flash.external.ExternalInterface;

import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.PaymentEvent;
import ru.elementarium.webclient.view.payment.components.PaymentAppleItemRenderer;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.payment.components.PaymentMailItemRenderer;
import ru.elementarium.webclient.view.payment.components.PaymentVkItemRenderer;

import starling.events.Event;

public class PaymentViewMediator extends Mediator
	{
		[Inject]
		public var view:PaymentView;
        [Inject]
        public var mainModel:MainModel;

        public static const SOURCE_EXTERNAL:String= "external";
        public static const SOURCE_API:String= "api";
        public static const SOURCE_EVENT:String= "eventCallback";

        public static const API_IS_APP_USER:String= "users.isAppUser";
        public static const API_USERS_GET:String= "users.get";
        public static const API_FRIENDS_GET:String= "friends.get";
        public static const API_GROUP_MEMBER:String= "groups.isMember";
        public static const API_WALL_POST:String= "wall.post";

        public static const EXT_INSTALL_BOX:String= "showInstallBox";
        public static const EXT_ORDER_BOX:String= "showOrderBox";
        public static const EXT_SETTINGS_BOX:String= "showSettingsBox";
        public static const EXT_INVITE_BOX:String= "showInviteBox";


		override public function onRegister():void
		{
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._itemList.addEventListener(Event.CHANGE, paymentEventHandler);

            if (mainModel.socialVkData) {
                view._itemList.itemRendererFactory = function():IListItemRenderer { return new PaymentVkItemRenderer(); };
                view._itemList.dataProvider = new ListCollection(MainModel.paymentVkData);
            } else if (mainModel.socialMyMailData) {
                view._itemList.itemRendererFactory = function():IListItemRenderer { return new PaymentMailItemRenderer(); };
                view._itemList.dataProvider = new ListCollection(MainModel.paymentMailData);
            } else if (MainModel.isMobile) {
                view._itemList.itemRendererFactory = function():IListItemRenderer { return new PaymentAppleItemRenderer(); };
                view._itemList.dataProvider = new ListCollection(MainModel.paymentAppleData);
            }
        }

        private function paymentEventHandler(event:Event):void {

            if (view._itemList.selectedItem) {
                if (mainModel.socialVkData) {
                    showOrderBox({type:"item", item:view._itemList.selectedItem.itemId});
                } else  if (mainModel.socialMyMailData) {
                    mainModel.showMailruPayment(view._itemList.selectedItem.serviceId, view._itemList.selectedItem.serviceName.ru, view._itemList.selectedItem.price);
                } else if (MainModel.isMobile) {
                    var eventPayment:PaymentEvent = new PaymentEvent(PaymentEvent.PAYMENT_APPLE_MAKE_PURCHASE);
                    eventPayment.inApp = view._itemList.selectedItem.inApp;
                    dispatch(eventPayment)
                }
                view._itemList.selectedItem = null;
            }
            //view.removeFromParent(true);
        }

        protected function sendToNetwork(obj:Object):Boolean
        {
            if (ExternalInterface.available)
            {
                return ExternalInterface.call ("getFromFlash", obj);
            }
            return false;
        }

        public function showOrderBox (args:Object):void
        {
            sendToNetwork({source:SOURCE_EXTERNAL, method:EXT_ORDER_BOX, args:args});
        }

        private function gameEventTypeHandler(event:Event):void
        {
            switch (event.type) {
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._close2Button:
                    view.removeFromParent(true);
                break;
            }

        }

}
}