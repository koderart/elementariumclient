package ru.elementarium.webclient.view.payment {

import feathers.controls.Button;
import feathers.controls.List;
import feathers.layout.FlowLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;

import starling.display.Sprite;

public class PaymentView extends ModuleView
    {
        public var _close2Button:Button;
        public var _itemList:List;

        public function PaymentView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.payment, false, this) as Sprite;

            _titleLabel.text = "ПОПОЛНЕНИЕ СЧЕТА";
            _close2Button.label = "ЗАКРЫТЬ";
            var layout:FlowLayout = new FlowLayout();
            layout.gap = 20;
            
            layout.horizontalAlign = FlowLayout.HORIZONTAL_ALIGN_CENTER;
            layout.verticalAlign = FlowLayout.VERTICAL_ALIGN_MIDDLE;
            _itemList.layout = layout;
            
            validateAndAttach();
        }
}
}
