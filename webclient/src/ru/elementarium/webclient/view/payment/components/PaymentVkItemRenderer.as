package ru.elementarium.webclient.view.payment.components {
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class PaymentVkItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        private var bonusPercentLabel:TextField;
        private var bonusLabel:TextField;
        private var valueLabel:TextField;
        private var currencyLabel:TextField;
        private var background:Image;
        private var bonusFlag:Image;
        private var initialized:Boolean = false;

        private var _select:TapToSelect;
    
        public function PaymentVkItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.payment_item) as Sprite;
                _sprite.useHandCursor = true;
                addChild(_sprite);

                bonusPercentLabel = _sprite.getChildByName("bonusPercentLabel") as TextField;
                bonusPercentLabel.text = "";
                bonusLabel = _sprite.getChildByName("bonusLabel") as TextField;
                bonusLabel.text = "";
                valueLabel = _sprite.getChildByName("valueLabel") as TextField;
                valueLabel.text = "";
                currencyLabel = _sprite.getChildByName("currencyLabel") as TextField;
                currencyLabel.text = "";

                background = _sprite.getChildByName("background") as Image;
                bonusFlag = _sprite.getChildByName("bonusFlag") as Image;
                bonusFlag.visible = false;
                
                if (data) {
                    drawData();
                }
            }
        }

        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();

            }
        }

        private function drawData():void
        {
            var textureName:String;
            switch (_data.itemId) {
                case "crystal1": textureName = "payment/crystal_lot_1"; break;
                case "crystal2": textureName = "payment/crystal_lot_2"; break;
                case "crystal3": textureName = "payment/crystal_lot_3"; break;
                case "gold1": textureName = "payment/money_lot_1"; break;
                case "gold2": textureName = "payment/money_lot_2"; break;
                case "gold3": textureName = "payment/money_lot_3"; break;
            }
            if (textureName) {
                background.texture = AppTheme.instance.assets.getTexture(textureName);
            }
            
            if (_data.giveBonusPrc) {
                bonusPercentLabel.text = "+"+_data.giveBonusPrc+"%";
                bonusFlag.visible = true;
            } else {
                bonusPercentLabel.text = "";
                bonusFlag.visible = false;
            }
            
            
            if (_data.giveBonusCount) {
                bonusLabel.text = "+"+_data.giveBonusCount+" бонус";
            } else {
                bonusLabel.text = "";
            }
            
            valueLabel.text = _data.giveCount;
            
            currencyLabel.text = "За " + _data.price + " голосов";
        }
    }
}
