package ru.elementarium.webclient.view.news {
import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;

import starling.display.Button;
import starling.display.Sprite;

public class NewsView extends ModuleView
    {
        public var _close2Button:feathers.controls.Button;
        public var _prevButton:Button;
        public var _nextButton:Button;

        public var _imageLoader:ImageLoader;

        public function NewsView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.news, false, this) as Sprite;

            _titleLabel.text = "НОВОСТИ";

            validateAndAttach();
        }
}
}
