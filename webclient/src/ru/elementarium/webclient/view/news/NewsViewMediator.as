package ru.elementarium.webclient.view.news
{
import com.junkbyte.console.Cc;
import flash.net.SharedObject;
import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import starling.events.Event;

public class NewsViewMediator extends Mediator
	{
		[Inject]
		public var view:NewsView;
        [Inject]
        public var mainModel:MainModel;
        private var currentPage:int = 0;

		override public function onRegister():void
		{
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._prevButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._nextButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            
            var len:int = mainModel.newsXML.length();
            if (len > 0) {
                var my_so:SharedObject = SharedObject.getLocal("news");
                my_so.data.lastNewsId = int(mainModel.newsXML[len-1].@id);
                my_so.flush();
                
                currentPage = len-1;
                openPage();
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._closeButton:
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                    break;
                case view._close2Button:
                    view.removeFromParent(true);
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                break;
                case view._nextButton:
                    currentPage++;
                    if (currentPage >= mainModel.newsXML.length()) {
                        currentPage = mainModel.newsXML.length() - 1;
                    }
                    openPage();
                break;
                case view._prevButton:
                    currentPage--;
                    if (currentPage <= 0) {
                        currentPage = 0;
                    }
                    openPage();
                break;
            }
        }

        private function openPage():void {
            view._imageLoader.source = ConnectionService.BASE_URL_ASSETS + "news/" + mainModel.newsXML[currentPage].@src;
        }
}
}