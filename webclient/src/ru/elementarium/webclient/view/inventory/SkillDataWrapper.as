package ru.elementarium.webclient.view.inventory {
import ru.muvs.admin.main.vo.ArtikulSkillBaseVO;
import ru.muvs.admin.main.vo.ArtikulSkillRandomVO;
import ru.muvs.admin.main.vo.enum.SkillEnum;
import ru.muvs.elementarium.thrift.skill.DataArtikulRandomSkill;
import ru.muvs.elementarium.thrift.skill.DataSkillType;

public class SkillDataWrapper {

    public static const TYPE_HP:String = "TYPE_HP";
    public static const TYPE_BASE:String = "TYPE_BASE";
    public static const TYPE_DARK:String = "TYPE_DARK";
    public static const TYPE_FIRE:String = "TYPE_FIRE";
    public static const TYPE_LIGHT:String = "TYPE_LIGHT";
    public static const TYPE_NATURE:String = "TYPE_NATURE";
    public static const TYPE_WATER:String = "TYPE_WATER";
    
    public static var DATA:Object = {};

    public var type:String;

    public var baseDamage:int = 0;
    public var randomDamage:int = 0;
    public var randomFromDamage:int = 0;
    public var randomToDamage:int = 0;
    public var baseProtection:int = 0;
    public var randomProtection:int = 0;
    public var randomFromProtection:int = 0;
    public var randomToProtection:int = 0;

    public function SkillDataWrapper(t:String)
    {
        type = t;
    }

    public function isData():Boolean
    {
        return baseDamage || randomDamage || randomFromDamage || randomToDamage ||
                baseProtection || randomProtection || randomFromProtection || randomToProtection;
    }

    public static function initData(currentElement:String, artikulSkillBase:Vector.<ArtikulSkillBaseVO>, artikulSkillRandom:Vector.<ArtikulSkillRandomVO>, randomSkills:Array):void
    {
        DATA[TYPE_HP] = new SkillDataWrapper(TYPE_HP);
        DATA[TYPE_BASE] = new SkillDataWrapper(TYPE_BASE);
        DATA[TYPE_DARK] = new SkillDataWrapper(TYPE_DARK);
        DATA[TYPE_FIRE] = new SkillDataWrapper(TYPE_FIRE);
        DATA[TYPE_LIGHT] = new SkillDataWrapper(TYPE_LIGHT);
        DATA[TYPE_NATURE] = new SkillDataWrapper(TYPE_NATURE);
        DATA[TYPE_WATER] = new SkillDataWrapper(TYPE_WATER);

        for each (var artikulSkill:ArtikulSkillBaseVO in artikulSkillBase) {
            if (artikulSkill.element == currentElement) {
                addBaseVo(artikulSkill);
            }
        }
        for each (var artikulSkillRandomVO:ArtikulSkillRandomVO in artikulSkillRandom) {
            if (artikulSkillRandomVO.element == currentElement) {
                addRandomVo(artikulSkillRandomVO);
            }
        }

        for each (var skillRandom:DataArtikulRandomSkill in randomSkills) {
            addRandomData(skillRandom);
        }

    }

    public static function getByType(type:String):SkillDataWrapper
    {
        return DATA[type];
    }

    public static function addBaseVo(artikulSkill:ArtikulSkillBaseVO):void
    {
        switch (artikulSkill.skill.skill) {
            case SkillEnum.HP:
                getByType(SkillDataWrapper.TYPE_HP).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE:
                getByType(SkillDataWrapper.TYPE_BASE).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.DAMAGE_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).baseDamage = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION:
                getByType(SkillDataWrapper.TYPE_BASE).baseProtection = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).baseProtection = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).baseProtection = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).baseProtection = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).baseProtection = artikulSkill.value;
                break;
            case SkillEnum.PROTECTION_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).baseProtection = artikulSkill.value;
                break;
        }
    }

    public static function addRandomVo(artikulSkill:ArtikulSkillRandomVO):void
    {
        switch (artikulSkill.skill.skill) {
            case SkillEnum.HP:
                getByType(SkillDataWrapper.TYPE_HP).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_HP).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE:
                getByType(SkillDataWrapper.TYPE_BASE).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_BASE).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_DARK).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_FIRE).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_LIGHT).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_NATURE).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.DAMAGE_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).randomFromDamage = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_WATER).randomToDamage = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION:
                getByType(SkillDataWrapper.TYPE_BASE).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_BASE).randomToProtection = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_DARK).randomToProtection = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_FIRE).randomToProtection = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_LIGHT).randomToProtection = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_NATURE).randomToProtection = artikulSkill.valueTo;
                break;
            case SkillEnum.PROTECTION_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).randomFromProtection = artikulSkill.valueFrom;
                getByType(SkillDataWrapper.TYPE_WATER).randomToProtection = artikulSkill.valueTo;
                break;
        }
    }

    public static function addRandomData(skillRandom:DataArtikulRandomSkill):void
    {
        switch (skillRandom.value.type) {
            case DataSkillType.HP:
                getByType(SkillDataWrapper.TYPE_HP).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE:
                getByType(SkillDataWrapper.TYPE_BASE).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.DAMAGE_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).randomDamage = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION:
                getByType(SkillDataWrapper.TYPE_BASE).randomProtection = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION_DARK:
                getByType(SkillDataWrapper.TYPE_DARK).randomProtection = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION_FIRE:
                getByType(SkillDataWrapper.TYPE_FIRE).randomProtection = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION_LIGHT:
                getByType(SkillDataWrapper.TYPE_LIGHT).randomProtection = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION_NATURE:
                getByType(SkillDataWrapper.TYPE_NATURE).randomProtection = skillRandom.value.value;
                break;
            case DataSkillType.PROTECTION_WATER:
                getByType(SkillDataWrapper.TYPE_WATER).randomProtection = skillRandom.value.value;
                break;
        }
    }

}
}
