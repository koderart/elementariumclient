package ru.elementarium.webclient.view.inventory.components {
import com.junkbyte.console.Cc;
import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.dragDrop.IDragSource;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.helper.DragToCanvasHelper;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.admin.main.vo.ArtikulVO;
import starling.display.Sprite;
import starling.text.TextField;

public class InventoryItemRenderer extends LayoutGroupListItemRenderer implements IDragSource
    {
        private var _sprite:Sprite;

        private var imageLoader:ImageLoader;
        private var countLabel:TextField;

        private var initialized:Boolean = false;

        private var _select:TapToSelect;
    
        public function InventoryItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);

            new DragToCanvasHelper(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_item) as Sprite;
                addChild(_sprite);

                imageLoader = _sprite.getChildByName("imageLoader") as ImageLoader;
                imageLoader.pixelSnapping = true;
                countLabel = _sprite.getChildByName("countLabel") as TextField;
                countLabel.text = "";
                countLabel.touchable = false;

                if (data) {
                    drawData();
                }
            }
        }
    
        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();

            }
        }

        private function drawData():void
        {
            var artikul:ArtikulVO = MainModel.artikulData[_data.artikulId];
            
            imageLoader.source = ConnectionService.BASE_URL_HTTP+artikul.image.url;
            countLabel.text = "";
            if (_data.count > 1) {
                countLabel.text = _data.count.toString();
            }

        }
}
}
