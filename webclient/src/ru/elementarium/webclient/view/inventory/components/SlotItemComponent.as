package ru.elementarium.webclient.view.inventory.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlot;
import starling.display.DisplayObject;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;

public class SlotItemComponent extends Sprite
    {
        public static const SLOT_OFF_ALERT_EVENT:String = "slotOffAlertEvent";
    
        private var _sprite:Sprite;

        private var imageLoader:ImageLoader;
        public var data:DataArtikulSlot;

        public function SlotItemComponent(itemSlot:DataArtikulSlot)
        {
            super();
            data = itemSlot;
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_item_slot) as Sprite;
            _sprite.addEventListener(TouchEvent.TOUCH, touchSpriteHandler);
            addChild(_sprite);

            imageLoader = _sprite.getChildByName("image_loader") as ImageLoader;
            imageLoader.pixelSnapping = true;
            imageLoader.width = 50;
            imageLoader.height = 50;
            imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.getArtikulVO(itemSlot.artikul.artikulId).image.url;
        }

        private function touchSpriteHandler(event:TouchEvent):void {
            var image:DisplayObject = event.currentTarget as DisplayObject;
            var touch:Touch = event.getTouch(image);
            if (touch && touch.phase == TouchPhase.ENDED)
            {
                dispatchEventWith(SLOT_OFF_ALERT_EVENT, true, data);
            }
        }
}
}
