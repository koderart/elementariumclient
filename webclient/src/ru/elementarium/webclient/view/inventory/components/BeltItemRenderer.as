package ru.elementarium.webclient.view.inventory.components {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.renderers.LayoutGroupListItemRenderer;
import feathers.utils.touch.TapToSelect;

import ru.elementarium.webclient.model.MainModel;

import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;

import starling.display.Image;
import starling.display.Sprite;
import starling.text.TextField;

public class BeltItemRenderer extends LayoutGroupListItemRenderer
    {
        private var _sprite:Sprite;

        private var imageLoader:ImageLoader;
        private var countLabel:TextField;
        private var lockImage:Image;

        private var initialized:Boolean = false;

        private var _select:TapToSelect;

        public function BeltItemRenderer()
        {
            super();
            this._select = new TapToSelect(this);
        }

        override protected function initialize():void
        {
            initialized = true;
            if (_sprite == null)
            {
                _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_item_belt) as Sprite;
                addChild(_sprite);


                imageLoader = _sprite.getChildByName("image_loader") as ImageLoader;
                imageLoader.pixelSnapping = true;
                imageLoader.width = 57;
                imageLoader.height = 57;
                countLabel = _sprite.getChildByName("countLabel") as TextField;
                countLabel.text = "";
                countLabel.touchable = false;
                lockImage = _sprite.getChildByName("lockImage") as Image;
                if (data) {
                    drawData();
                }
            }
        }
    
        override public function set data(value:Object):void
        {
            _data = value;
            
            if (!value) return;
            
            if (initialized) {
                drawData();

            }
        }

        private function drawData():void
        {
            if (_data as String) {
                countLabel.text = "";
                imageLoader.source = null;
                lockImage.visible = String(_data).substr(0,5) == "block";

            } else if (_data.artikul) {
                lockImage.visible = false;
                imageLoader.source = ConnectionService.BASE_URL_HTTP + MainModel.getArtikulVO(_data.artikul.artikulId).image.url;
                countLabel.text = _data.artikul.count.toString();
            }
        }
}
}
