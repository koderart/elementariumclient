package ru.elementarium.webclient.view.inventory.components {
import com.junkbyte.console.Cc;

import feathers.controls.Check;
import feathers.controls.ImageLoader;
import feathers.controls.LayoutGroup;

import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.inventory.SkillDataWrapper;
import ru.elementarium.webclient.view.main.PopupView;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.muvs.admin.main.vo.ArtikulSkillBaseVO;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.MoneyVO;
import ru.muvs.admin.main.vo.enum.ColorEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.admin.main.vo.enum.SkillEnum;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlot;
import ru.muvs.elementarium.thrift.player.DataPlayerElement;
import ru.muvs.elementarium.thrift.skill.DataArtikulRandomSkill;
import ru.muvs.elementarium.thrift.skill.DataSkillType;

import starling.display.Button;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class ArtikulAlert extends PopupView
{
    public static const USE_EVENT:String = "useEvent";
    public static const DELETE_EVENT:String = "deleteEvent";
    public static const SELL_EVENT:String = "sellEvent";
    public static const REPAIR_EVENT:String = "repairEvent";
    public static const SLOT_OFF_EVENT:String = "slotOffEvent";
    public static const SLOT_ON_EVENT:String = "slotOnEvent";

    public var _backgroundImage:Image;
    public var _borderImage:Image;
    public var _itemRarity:Image;

    public var _titleLabel:TextField;
    public var _repairTitle:TextField;
    public var _weightyTitle:TextField;
    public var _skillsLabel:TextField;
    public var _imageLoader:ImageLoader;

    public var _buttonLayoutGroup:LayoutGroup;
    public var _layoutGroup:LayoutGroup;
    public var _moneyGroup:LayoutGroup;
    public var _hpGroup:LayoutGroup;
    public var _statGroup:LayoutGroup;
    public var _titleStatGroup:LayoutGroup;
    public var _titleDamageLabel:TextField;
    public var _titleProtectedLabel:TextField;

    public var _closeButton:Button;
    public var _useButton:Button;
    public var _slotButton:Button;
    public var _deleteButton:Button;
    public var _sellButton:Button;
    public var _repairButton:Button;
    public var _checkBox:Check;

    private var _data:DataArtikul;
    private var _artikul:ArtikulVO;
    private var _artikulSlot:DataArtikulSlot;

    public function ArtikulAlert(dataArtikul:DataArtikul, dataArtikulSlot:DataArtikulSlot, userElement:int, hint:Boolean = false)
    {
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_artikul_alert, false, this) as Sprite;
        super();
        _data = dataArtikul;
        _artikulSlot = dataArtikulSlot;
        _artikul = MainModel.getArtikulVO(dataArtikul.artikulId);

        _closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);

        _checkBox.addEventListener( Event.CHANGE, checkBox_changeHandler );
        _checkBox.paddingLeft = 40;
        _checkBox.paddingTop = 10;
        _checkBox.visible = _checkBox.includeInLayout = false;

        _useButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        _slotButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        _repairButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        _deleteButton.addEventListener(Event.TRIGGERED, triggeredHandler);
        _sellButton.addEventListener(Event.TRIGGERED, triggeredHandler);

        _slotButton.visible = false;
        if(_artikul.artikulSlot.length > 0) {
            _slotButton.visible = true;
            if (_artikul.durability > 0 || _artikul.durabilityCurrent > 0) {
                _slotButton.visible = _data.durabilityCurrent>0;
            }
        } else {
            _slotButton.visible = _artikul.belty;
        }
        if (_artikulSlot) {
            _slotButton.scaleX = -1;
        }
        _useButton.visible = _artikul.action.bonusId;
        _sellButton.visible = _artikul.sell;
        _deleteButton.visible = _artikul.remove;
        _repairButton.visible = _artikul.repair && _data.durabilityCurrent < _data.durability;

        if(!_slotButton.visible) {_buttonLayoutGroup.removeChild(_slotButton)}
        if(!_useButton.visible) {_buttonLayoutGroup.removeChild(_useButton)}
        if(!_repairButton.visible || _artikulSlot) {_buttonLayoutGroup.removeChild(_repairButton)}
        if(!_deleteButton.visible || _artikulSlot) {_buttonLayoutGroup.removeChild(_deleteButton)}
        if(!_sellButton.visible || _artikulSlot) {_buttonLayoutGroup.removeChild(_sellButton)}
        
        if (hint || (!_slotButton.visible && !_useButton.visible && !_repairButton.visible && !_deleteButton.visible && !_sellButton.visible)) {
            _buttonLayoutGroup.visible = false;
            _layoutGroup.y = _buttonLayoutGroup.y;
        }
        
        var data :ArtikulVO = MainModel.artikulData[dataArtikul.artikulId];

        _titleLabel.text = data.title.ru;
        _imageLoader.source = ConnectionService.BASE_URL_HTTP+data.image.url;
        switch (data.artikulColor) {
            case ColorEnum.GREY:
                _itemRarity.color = 0x585858;
            break;
            case ColorEnum.GREEN:
                _itemRarity.color = 0x0b750b;
            break;
            case ColorEnum.BLUE:
                _itemRarity.color = 0x0048c0;
            break;
            case ColorEnum.ORANGE:
                _itemRarity.color = 0xf0820b;
                break;
            case ColorEnum.PURPLE:
                _itemRarity.color = 0x691988;
            break;
            case ColorEnum.RED:
                _itemRarity.color = 0xad010f;
            break;
        }
        _moneyGroup.removeChildren(1,-1,true);
        _moneyGroup.visible = _moneyGroup.includeInLayout = data.artikulMoney.length > 0;
        for each (var money:MoneyVO in data.artikulMoney) {
            var moneySprite:Sprite = AppTheme.uiBuilder.create(ParsedLayouts.hint_artikul_money) as Sprite;

            var moneyLabel:TextField = moneySprite.getChildByName("moneyLabel") as TextField;
            moneyLabel.text = money.money.toString();
            var goldIcon:Image = moneySprite.getChildByName("goldIcon") as Image;
            goldIcon.visible = false;

            var crystalIcon:Image = moneySprite.getChildByName("crystalIcon") as Image;
            crystalIcon.visible = false;

            if (money.moneyType == MoneyEnum.GOLD) {
                goldIcon.visible = true;
            }
            if (money.moneyType == MoneyEnum.CRYSTAL) {
                crystalIcon.visible = true;
            }
            _moneyGroup.addChild(moneySprite);
        }

        _skillsLabel.text = "";
        if (data.levelMin.levelId) {
            _skillsLabel.text += MainModel.getLocale("artikul.hint.level") + " " + data.levelMin.number;
        }
        if (data.levelMax.levelId) {
            _skillsLabel.text += " - " + data.levelMax.number;
        }
        if (data.durability > 0 || data.durabilityCurrent > 0) {
            if (hint) {
                dataArtikul.durabilityCurrent = data.durabilityCurrent;
                dataArtikul.durability = data.durability;
            }
            _skillsLabel.text += "\n"+MainModel.getLocale("artikul.hint.durability") + " "+dataArtikul.durabilityCurrent+"/"+dataArtikul.durability;
            if (dataArtikul.durabilityCurrent==0) {
                _repairTitle.text = dataArtikul.durability==0 ? MainModel.getLocale("artikul.hint.durabilityzero") : MainModel.getLocale("artikul.hint.needrepair");
            } else {
                _repairTitle.text = "";
            }
        } else {
            _repairTitle.text = "";
        }
        if (data.weighty) {
            _weightyTitle.text = MainModel.getLocale("artikul.hint.weighty");
        } else {
            _weightyTitle.text = "";
        }

        var currentElement:String;
        switch(userElement) {
            case DataPlayerElement.DARK: currentElement = ElementEnum.DARK; break;
            case DataPlayerElement.FIRE: currentElement = ElementEnum.FIRE; break;
            case DataPlayerElement.LIGHT: currentElement = ElementEnum.LIGHT; break;
            case DataPlayerElement.NATURE: currentElement = ElementEnum.NATURE; break;
            case DataPlayerElement.WATER: currentElement = ElementEnum.WATER; break;
        }

        SkillDataWrapper.initData(currentElement, data.artikulSkillBase, data.artikulSkillRandom, dataArtikul.randomSkills);

        for each (var artikulSkill:ArtikulSkillBaseVO in data.artikulSkillBase) {
            if (artikulSkill.element != currentElement) {
                continue;
            }
            if (artikulSkill.skill.skill == SkillEnum.BAG_CAPACITY || artikulSkill.skill.skill == SkillEnum.BELT_CAPACITY) {
                _skillsLabel.text += "\n" + artikulSkill.skill.title.ru + ": " + artikulSkill.value;
            }
        }

        if (data.description.ru) {
            _skillsLabel.text += "\n" + data.description.ru;
        }

        _titleStatGroup.visible = _titleStatGroup.includeInLayout = false;
        for each (var skill:SkillDataWrapper in SkillDataWrapper.DATA) {
            if (!skill.isData()) {
                continue;
            }
            var statItem:ArtikulStatComponent = new ArtikulStatComponent(skill);
            statItem.drawBase();
            switch (statItem.data.type) {
                case SkillDataWrapper.TYPE_HP:
                    _hpGroup.addChild(statItem);
                    break;
                case SkillDataWrapper.TYPE_BASE:
                    _statGroup.addChildAt(statItem, 0);
                    break;
                default:
                    _statGroup.addChild(statItem);
            }
        }
        if(_statGroup.numChildren > 0) {
            _titleStatGroup.visible = _titleStatGroup.includeInLayout = true;
            _checkBox.visible = _checkBox.includeInLayout = true;
        }

        _layoutGroup.validate();

        _borderImage.height = _layoutGroup.y + _layoutGroup.height;
        _backgroundImage.height = _layoutGroup.y + _layoutGroup.height-20;
    }

    private function checkBox_changeHandler( event:Event ):void
    {
        for (var i:int=0; i < _statGroup.numChildren; i++) {
            if(Check(event.target).isSelected) {
                ArtikulStatComponent(_statGroup.getChildAt(i)).drawRandom();
            } else {
                ArtikulStatComponent(_statGroup.getChildAt(i)).drawBase();
            }
        }
    }

    private function triggeredHandler(event:Event):void
    {
        switch (event.target) {
            case _useButton:
                ELAlert.show( "Использовать "+MainModel.artikulData[_data.artikulId].title.ru+"?", "",
                        [
                            { label: "Использовать", triggered: okButton_useHandler, style:"pinkOkButton"},
                            { label: "Отмена", style:"blueCancelButton"}
                        ]);
                break;
            case _deleteButton:
                dispatchEventWith(DELETE_EVENT, true, _data);
                this.removeFromParent(true);
                break;
            case _sellButton:
                dispatchEventWith(SELL_EVENT, true, _data);
                this.removeFromParent(true);
                break;
            case _repairButton:
                dispatchEventWith(REPAIR_EVENT, true, _data);
                this.removeFromParent(true);
                break;
            case _slotButton:
                if (_artikulSlot) {
                    dispatchEventWith(SLOT_OFF_EVENT, true, _artikulSlot);
                } else {
                    dispatchEventWith(SLOT_ON_EVENT, true, _data);
                }
                this.removeFromParent(true);
                break;
            case _closeButton:
                this.removeFromParent(true);
                break;
        }
    }

    private function okButton_useHandler(event:Event):void {
        dispatchEventWith(USE_EVENT, true, _data);
        this.removeFromParent(true);
    }

}
}
