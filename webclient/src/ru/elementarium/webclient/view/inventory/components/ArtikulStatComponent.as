package ru.elementarium.webclient.view.inventory.components {
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.inventory.SkillDataWrapper;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.text.TextField;

public class ArtikulStatComponent extends Sprite
    {
        private var _sprite:Sprite;
        private var elementIcon:Image;
        private var damageLabel:TextField;
        private var protectionLabel:TextField;

        public var data:SkillDataWrapper;

        public function ArtikulStatComponent(skill:SkillDataWrapper)
        {
            super();
            data = skill;
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_artikul_alert_stat) as Sprite;
            addChild(_sprite);

            elementIcon = _sprite.getChildByName("elementIcon") as Image;
            damageLabel = _sprite.getChildByName("damageLabel") as TextField;
            protectionLabel = _sprite.getChildByName("protectionLabel") as TextField;
        }

        private function drawIcon():void {
            switch (data.type) {
                case SkillDataWrapper.TYPE_HP:
                    _sprite.mask = new Quad(104, 26);
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/ico_hp");
                    break;
                case SkillDataWrapper.TYPE_BASE:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/ico_damage");
                    break;
                case SkillDataWrapper.TYPE_DARK:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/dark");
                    break;
                case SkillDataWrapper.TYPE_FIRE:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/fire");
                    break;
                case SkillDataWrapper.TYPE_LIGHT:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/light");
                    break;
                case SkillDataWrapper.TYPE_NATURE:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/nature");
                    break;
                case SkillDataWrapper.TYPE_WATER:
                    elementIcon.texture = AppTheme.instance.assets.getTexture("inventory/alert/element/water");
                    break;
            }
        }

        public function drawBase():void {
            drawIcon();

            var damage:String = "";
            var all:int = data.baseDamage + data.randomDamage;
            if (all > 0) {
                damage += all.toString();
            }
            if (data.randomDamage > 0) {
                damage += " (" + data.randomDamage + ")";
            }
            damageLabel.text = damage;

            var protection:String = "";
            all = data.baseProtection + data.randomProtection;
            if (all > 0) {
                protection += all.toString();
            }
            if (data.randomProtection > 0) {
                protection += " (" + data.randomProtection + ")";
            }
            protectionLabel.text = protection;
        }

        public function drawRandom():void {
            drawIcon();

            var damage:String = "";
            if (data.baseDamage > 0) {
                damage += data.baseDamage.toString();
            }
            if (data.randomFromDamage > 0 || data.randomToDamage > 0) {
                damage += " [" + data.randomFromDamage + ":" + data.randomToDamage + "]";
            }
            damageLabel.text = damage;

            var protection:String = "";
            if (data.baseProtection > 0) {
                protection += data.baseProtection.toString();
            }
            if (data.randomFromProtection > 0 || data.randomToProtection > 0) {
                protection += " [" + data.randomFromProtection + ":" + data.randomToProtection + "]";
            }
            protectionLabel.text = protection;
        }
}
}
