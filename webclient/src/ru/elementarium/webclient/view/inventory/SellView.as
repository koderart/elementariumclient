package ru.elementarium.webclient.view.inventory {
import com.junkbyte.console.Cc;

import feathers.controls.Button;
import feathers.controls.ImageLoader;
import feathers.controls.TextInput;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.WindowView;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.MoneyVO;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class SellView extends WindowView
    {
        public static const SELL:String = "SELL_EVENT";
    
        public var _artikulLabel:TextField;
        public var _countLabel:TextField;
        public var _okButton:Button;
        public var _allButton:Button;
        public var _minusButton:starling.display.Button;
        public var _plusButton:starling.display.Button;
        public var _imageLoader:ImageLoader;
        public var _countInput:TextInput;
        public var _moneyLabel:TextField;

        private var dataArtikul:DataArtikul;
        private var artikul:ArtikulVO;
        private var currentValue:int = 1;

        public function SellView(dataArtikul:DataArtikul, artikul:ArtikulVO)
        {
            super();
            this.dataArtikul = dataArtikul;
            this.artikul = artikul;

            _content = AppTheme.uiBuilder.create(ParsedLayouts.inventory_sell, false, this) as Sprite;

            _titleLabel.text = "ПРОДАЖА";
            _artikulLabel.text = artikul.title.ru;
            _countLabel.text = dataArtikul.count>1 ? dataArtikul.count.toString() : "";

            _okButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _allButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _minusButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _plusButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _imageLoader.source = ConnectionService.BASE_URL_HTTP + artikul.image.url;
            
            _countInput.restrict = "0-9";
            _countInput.maxChars = 4;

            if (dataArtikul.count == 1) {
                _allButton.includeInLayout = _allButton.visible = false;
                _minusButton.visible = false;
                _plusButton.visible = false;
                _countInput.isEnabled = false;
            }
            
            updateCost();

            validateAndAttach();
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case _minusButton:
                    if (currentValue > 1) {
                        currentValue--;
                        _countInput.text = currentValue.toString();
                    }
                break;
                case _plusButton:
                    if (currentValue < dataArtikul.count) {
                        currentValue++;
                        _countInput.text = currentValue.toString();
                    }

                break;
                case _allButton:
                    currentValue = dataArtikul.count;
                break;
                case _okButton:
                    dispatchEventWith(SELL, false, {artikul:dataArtikul, count:currentValue});
                    removeFromParent(true);
                break;
            }
            updateCost();
        }

        private function updateCost():void {
            _countInput.text = currentValue.toString();
            
            var cost:int;
            switch (MoneyVO(artikul.artikulMoney[0]).moneyType) {
                case MoneyEnum.GOLD:
                    cost = Math.ceil(MoneyVO(artikul.artikulMoney[0]).money * 0.1) * currentValue;
                break;
                case MoneyEnum.CRYSTAL:
                    cost = Math.ceil(MoneyVO(artikul.artikulMoney[0]).money * 5 * 0.1) * currentValue;
                break;
            }
            _moneyLabel.text = cost.toString();
        }

    }
}
