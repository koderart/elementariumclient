package ru.elementarium.webclient.view.inventory {

import com.junkbyte.console.Cc;

import feathers.dragDrop.DragData;
import feathers.dragDrop.DragDropManager;
import feathers.dragDrop.IDropTarget;
import feathers.events.DragDropEvent;

import ru.elementarium.webclient.helper.DragToCanvasHelper;

import starling.display.Quad;

public class DragArea extends Quad implements IDropTarget {
    
    public static const ON_DRAG_DROP:String = "DragArea_ON_DRAG_DROP";
    
    public function DragArea(width:Number, height:Number, color:uint=0xffffff) {
        
        super(width,height);
        
        addEventListener(DragDropEvent.DRAG_ENTER, onDragEnter);
        addEventListener(DragDropEvent.DRAG_EXIT, onDragExit);
        addEventListener(DragDropEvent.DRAG_DROP, onDragDrop);
    }

    private function onDragEnter(event:DragDropEvent, dragData:DragData):void
    {
        DragDropManager.acceptDrag(this);
    }

    private function onDragDrop(event:DragDropEvent, dragData:DragData):void
    {
        this.alpha = 0;
        this.touchable = false;

        dispatchEventWith(ON_DRAG_DROP, false, dragData.getDataForFormat(DragToCanvasHelper.ARTIKUL));
    }

    private function onDragExit(event:DragDropEvent, dragData:DragData):void
    {
    }

}
}
