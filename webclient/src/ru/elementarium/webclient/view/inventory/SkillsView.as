package ru.elementarium.webclient.view.inventory {

import com.junkbyte.console.Cc;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.muvs.elementarium.thrift.skill.DataSkillType;
import starling.display.DisplayObject;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.text.TextField;

public class SkillsView extends Sprite {

    private var _sprite:Sprite;
    private var skillPanel:Sprite;

    public function SkillsView() {
        _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory_skills, false) as Sprite;
        addChild(_sprite);

        skillPanel = _sprite.getChildByName("skillPanel") as Sprite;
    }

    public function draw(skills:Array) {
        TextField(skillPanel.getChildByName("hp")).text = "Здоровье: " + skills[DataSkillType.HP];
        TextField(skillPanel.getChildByName("damage")).text = skills[DataSkillType.DAMAGE];
        TextField(skillPanel.getChildByName("damage_dark")).text = skills[DataSkillType.DAMAGE_DARK];
        TextField(skillPanel.getChildByName("damage_fire")).text = skills[DataSkillType.DAMAGE_FIRE];
        TextField(skillPanel.getChildByName("damage_light")).text = skills[DataSkillType.DAMAGE_LIGHT];
        TextField(skillPanel.getChildByName("damage_nature")).text = skills[DataSkillType.DAMAGE_NATURE];
        TextField(skillPanel.getChildByName("damage_water")).text = skills[DataSkillType.DAMAGE_WATER];
        TextField(skillPanel.getChildByName("protection")).text = skills[DataSkillType.PROTECTION];
        TextField(skillPanel.getChildByName("protection_dark")).text = skills[DataSkillType.PROTECTION_DARK];
        TextField(skillPanel.getChildByName("protection_fire")).text = skills[DataSkillType.PROTECTION_FIRE];
        TextField(skillPanel.getChildByName("protection_light")).text = skills[DataSkillType.PROTECTION_LIGHT];
        TextField(skillPanel.getChildByName("protection_nature")).text = skills[DataSkillType.PROTECTION_NATURE];
        TextField(skillPanel.getChildByName("protection_water")).text = skills[DataSkillType.PROTECTION_WATER];
    }

}
}
