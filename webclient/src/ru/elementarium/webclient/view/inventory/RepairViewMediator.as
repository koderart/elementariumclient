package ru.elementarium.webclient.view.inventory
{
import com.junkbyte.console.Cc;

import org.robotlegs.starling.mvcs.Mediator;
import ru.elementarium.webclient.events.InventoryEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.enum.MoneyEnum;
import ru.muvs.elementarium.thrift.money.DataMoneyType;
import starling.events.Event;

public class RepairViewMediator extends Mediator
	{
		[Inject]
		public var view:RepairView;
        [Inject]
        public var mainModel:MainModel;


		override public function onRegister():void
		{
            var artikul:ArtikulVO = MainModel.artikulData[mainModel.durabilityArtikul.artikulId];
            view._close2Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._crystalButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._goldButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._imageLoader.source = artikul.image.url;
            view._durabilityValue.text = mainModel.durabilityArtikul.durabilityCurrent + "/" + mainModel.durabilityArtikul.durability;
            if (artikul.artikulMoney.length) {
                switch (artikul.artikulMoney[0].moneyType) {
                    case MoneyEnum.GOLD:
                        view._goldValue.text = String(Math.ceil(artikul.artikulMoney[0].money * 0.2));
                        view._crystalValue.text = String(Math.ceil(artikul.artikulMoney[0].money * 0.2 / 5));
                    break;
                    case MoneyEnum.CRYSTAL:
                        view._crystalValue.text = String(Math.ceil(artikul.artikulMoney[0].money * 0.2));
                        view._goldValue.text = String(Math.ceil(artikul.artikulMoney[0].money * 0.2 * 5));
                    break;
                }
            }
        }

        private function triggeredHandler(event:Event):void {

            switch (event.target) {
                case view._close2Button:
                    view.removeFromParent(true);
                break;
                case view._crystalButton:
                    var e:InventoryEvent = new InventoryEvent(InventoryEvent.ARTIKUL_REPAIR_SERVER_COMMAND);
                    e.artikul = mainModel.durabilityArtikul;
                    e.artikulMoney = DataMoneyType.CRYSTAL;
                    dispatch(e);
                    view.removeFromParent(true);
                break;
                case view._goldButton:
                    e = new InventoryEvent(InventoryEvent.ARTIKUL_REPAIR_SERVER_COMMAND);
                    e.artikul = mainModel.durabilityArtikul;
                    e.artikulMoney = DataMoneyType.GOLD;
                    dispatch(e);
                    view.removeFromParent(true);
                break;
            }

        }

}
}