package ru.elementarium.webclient.view.inventory {
import com.junkbyte.console.Cc;

import feathers.controls.ImageLoader;
import feathers.controls.TextInput;
import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.WindowView;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import starling.display.Button;
import starling.display.Sprite;
import starling.events.Event;
import starling.text.TextField;

public class RemoveView extends WindowView
    {
        public static const REMOVE:String = "REMOVE_EVENT";

        public var _imageLoader:ImageLoader;
        public var _artikulLabel:TextField;
        public var _countLabel:TextField;
        public var _countInput:TextInput;
        public var _okButton:feathers.controls.Button;
        public var _allButton:feathers.controls.Button;
        public var _minusButton:Button;
        public var _plusButton:Button;

        private var artikul:ArtikulVO;
        private var dataArtikul:DataArtikul;

        public function RemoveView(dataArtikul:DataArtikul, artikul:ArtikulVO)
        {
            super();
            this.dataArtikul = dataArtikul;
            this.artikul = artikul;

            _content = AppTheme.uiBuilder.create(ParsedLayouts.inventory_remove, false, this) as Sprite;

            _titleLabel.text = "ВЫБРОСИТЬ";
            _artikulLabel.text = artikul.title.ru;
            _countLabel.text = dataArtikul.count>1 ? dataArtikul.count.toString() : "";

            _okButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _allButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _minusButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            _plusButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            _imageLoader.source = ConnectionService.BASE_URL_HTTP + artikul.image.url;

            _countInput.text = "1";
            _countInput.restrict = "0-9";
            _countInput.maxChars = 4;

            if (dataArtikul.count == 1)
            {
                _allButton.includeInLayout = _allButton.visible = false;
                _minusButton.visible = false;
                _plusButton.visible = false;
                _countInput.isEnabled = false;
            }

            validateAndAttach();
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target)
            {
                case _minusButton:
                    var currentValue:int = int(_countInput.text);
                    if (currentValue > 1) {
                        currentValue--;
                        _countInput.text = currentValue.toString();
                    }
                break;
                case _plusButton:
                    currentValue = int(_countInput.text);
                    if (currentValue < dataArtikul.count) {
                        currentValue++;
                        _countInput.text = currentValue.toString();
                    }

                break;
                case _allButton:
                    _countInput.text = dataArtikul.count.toString();
                break;
                case _okButton:
                    dispatchEventWith(REMOVE, false, {artikul:dataArtikul, count:int(_countInput.text)});
                    removeFromParent(true);
                break;
            }
        }

}
}
