package ru.elementarium.webclient.view.inventory {

import feathers.controls.Button;
import feathers.controls.ImageLoader;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.WindowView;

import starling.display.Sprite;
import starling.text.TextField;

public class RepairView extends WindowView
{
        public var _close2Button:Button;
        public var _crystalButton:Button;
        public var _goldButton:Button;
        public var _crystalValue:TextField;
        public var _goldValue:TextField;
        public var _durabilityValue:TextField;
        public var _imageLoader:ImageLoader;

        public function RepairView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.inventory_repair, false, this) as Sprite;
            _titleLabel.text = "ПОЧИНКА ПРЕДМЕТА";
            validateAndAttach();
        }
}
}
