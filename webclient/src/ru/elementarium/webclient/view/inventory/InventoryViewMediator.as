package ru.elementarium.webclient.view.inventory
{
import com.junkbyte.console.Cc;

import feathers.controls.List;
import feathers.data.ListCollection;

import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.events.InventoryEvent;
import ru.elementarium.webclient.events.UserEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.view.inventory.components.ArtikulAlert;
import ru.elementarium.webclient.view.inventory.components.SlotItemComponent;
import ru.elementarium.webclient.view.main.components.ELAlert;
import ru.muvs.admin.main.vo.ArtikulSlotVO;
import ru.muvs.admin.main.vo.ArtikulVO;
import ru.muvs.admin.main.vo.enum.BagTabEnum;
import ru.muvs.admin.main.vo.enum.SlotEnum;
import ru.muvs.elementarium.thrift.artikul.DataArtikul;
import org.robotlegs.starling.mvcs.Mediator;
import ru.muvs.elementarium.thrift.artikul.DataArtikulBelt;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlot;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlotType;
import ru.muvs.elementarium.thrift.player.DataPlayerSex;
import starling.events.Event;

public class InventoryViewMediator extends Mediator
	{
		[Inject]
		public var view:InventoryView;

        [Inject]
        public var mainModel:MainModel;

        private var mainData:Array;
        private var consumableData:Array;
        private var questData:Array;
        private var professionData:Array;
        private var recipeData:Array;

        private var beltListCollection:ListCollection = new ListCollection();;

        private static const MAX_BELT_SIZE:int = 12;
        private static const MAX_ITEM_TO_PAGE:int = 30;

        private var bagCapacity:int = 0;
        private var currentTab:String = BagTabEnum.MAIN;
        private var currentPage:int = 0;

        override public function onRegister():void
		{
            view.closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            view.itemList.addEventListener(Event.CHANGE, itemList_changeHandler);

            view.addEventListener(SlotItemComponent.SLOT_OFF_ALERT_EVENT, slotOffAlertEventHandler);
            view.addEventListener(ArtikulAlert.SLOT_OFF_EVENT, artikulAlertEventHandler);

            view.beltList.dataProvider = beltListCollection;
            view.beltList.addEventListener(Event.CHANGE, beltList_changeHandler);

            addContextListener(InventoryEvent.BAG_LIST_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BAG_ADD_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BAG_CHANGE_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BAG_REMOVE_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BELT_LIST_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BELT_TAKE_ON_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.BELT_TAKE_OFF_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.SLOT_LIST_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.SLOT_TAKE_ON_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.SLOT_TAKE_OFF_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(InventoryEvent.ARTIKUL_REPAIR_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(UserEvent.SKILL_LIST_CLIENT_COMMAND, inventoryEventTypeHandler);
            addContextListener(UserEvent.SKILL_CHANGE_CLIENT_COMMAND, inventoryEventTypeHandler);

            dispatch(new InventoryEvent(InventoryEvent.SLOT_LIST_SERVER_COMMAND));

            view.inventoryTabBar.addEventListener( Event.CHANGE, tab_changeHandler );

            view.updateBgBySex(mainModel.player.sex == DataPlayerSex.MALE);


            view.skillsButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.beltUpButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.beltDownButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.bagLeftButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view.bagRightButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            
            view.dragArea.addEventListener(DragArea.ON_DRAG_DROP, dragArea_DragAreaOnDragDropHandler);
        }

        private function dragArea_DragAreaOnDragDropHandler(event:Event):void
        {
            slotOn(event.data as DataArtikul);
        }

        private function tab_changeHandler( event:Event ):void
        {
            currentPage = 0;
            
            switch (view.inventoryTabBar.selectedIndex) {
                case 0:
                    currentTab = BagTabEnum.MAIN;
                    break;
                case 1:
                    currentTab = BagTabEnum.CONSUMABLE;
                    break;
                case 2:
                    currentTab = BagTabEnum.QUEST;
                    break;
                case 3:
                    currentTab = BagTabEnum.PROFESSION;
                    break;
                case 4:
                    currentTab = BagTabEnum.RECIPE;
                    break;
            }

            updatePageData();
        }

        private function inventoryEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case InventoryEvent.BAG_LIST_CLIENT_COMMAND:
                    mainData = [];
                    consumableData = [];
                    questData = [];
                    professionData = [];
                    recipeData = [];

                    for each (var item:DataArtikul in mainModel.inventoryArtikuls) {
                        if(MainModel.getArtikulVO(item.artikulId).weighty){
                            bagCapacity++;
                        }
                        var tabData:Array = getArtikulTabDataById(item.artikulId);
                        tabData.push(item);
                    }
                    drawCapacity();
                    updatePageData();
                break;
                case InventoryEvent.BAG_REMOVE_CLIENT_COMMAND:
                    if(MainModel.getArtikulVO(mainModel.inventoryArtikul.artikulId).weighty){
                        bagCapacity--;
                        drawCapacity();
                    }
                    tabData = getArtikulTabDataById(mainModel.inventoryArtikul.artikulId);

                    var i:int = tabData.length;
                    while(i-- > 0) {
                        if (mainModel.inventoryArtikul.id == tabData[i].id) {
                            tabData.removeAt(i);
                        }
                    }

                    if (currentTab == MainModel.getArtikulVO(mainModel.inventoryArtikul.artikulId).artikulType.artikulTypeBagTab) {
                        updatePageData();
                    }
                break;
                case InventoryEvent.BAG_ADD_CLIENT_COMMAND:
                    if(MainModel.getArtikulVO(mainModel.inventoryArtikul.artikulId).weighty){
                        bagCapacity++;
                        drawCapacity();
                    }
                    getArtikulTabDataById(mainModel.inventoryArtikul.artikulId).push(mainModel.inventoryArtikul);

                    if (currentTab == MainModel.getArtikulVO(mainModel.inventoryArtikul.artikulId).artikulType.artikulTypeBagTab) {
                        updatePageData();
                    }
                break;
                case InventoryEvent.BAG_CHANGE_CLIENT_COMMAND:
                    tabData = getArtikulTabDataById(mainModel.inventoryArtikul.artikulId);
                    for (i=0; i < tabData.length; i++) {
                        if (mainModel.inventoryArtikul.id == tabData[i].id) {
                            tabData[i] = mainModel.inventoryArtikul;
                        }
                        if (currentTab == MainModel.getArtikulVO(mainModel.inventoryArtikul.artikulId).artikulType.artikulTypeBagTab) {
                            updatePageData();
                        }
                    }
                break;
                case InventoryEvent.BELT_LIST_CLIENT_COMMAND:
                    updateBeltList();
                    break;
                case InventoryEvent.BELT_TAKE_ON_CLIENT_COMMAND:
                    beltListCollection.setItemAt(mainModel.inventoryBelt, mainModel.inventoryBelt.order-1);
                    break;
                case InventoryEvent.BELT_TAKE_OFF_CLIENT_COMMAND:
                    beltListCollection.setItemAt("empty"+Math.random(), mainModel.inventoryBelt.order-1);
                break;
                case InventoryEvent.SLOT_LIST_CLIENT_COMMAND:
                    for each (var itemSlot:DataArtikulSlot in mainModel.inventorySlots) {
                        view.drawSlot(itemSlot);
                    }
                break;
                case InventoryEvent.SLOT_TAKE_ON_CLIENT_COMMAND:
                    view.drawSlot(mainModel.inventorySlot);
                break;
                case InventoryEvent.SLOT_TAKE_OFF_CLIENT_COMMAND:
                    view.hideSlot(mainModel.inventorySlot);
                break;
                case InventoryEvent.ARTIKUL_REPAIR_CLIENT_COMMAND:
                    tabData = getArtikulTabDataById(mainModel.durabilityArtikul.artikulId);
                    for (i=0; i < tabData.length; i++) {
                        if (mainModel.durabilityArtikul.id == tabData[i].id) {
                            tabData[i] = mainModel.durabilityArtikul;
                        }
                    }
                    if (currentTab == MainModel.getArtikulVO(mainModel.durabilityArtikul.artikulId).artikulType.artikulTypeBagTab) {
                        updatePageData();
                    }

                    break;
                case UserEvent.SKILL_LIST_CLIENT_COMMAND:
                case UserEvent.SKILL_CHANGE_CLIENT_COMMAND:
                    view.updateSkills(mainModel.playerSkill);
                    drawCapacity();
                    updateBeltList();
                    break;
            }
        }

        private function drawCapacity():void
        {
            view.bagCapacityLabel.text = MainModel.getLocale("inventory.bag.capacity") + " " + bagCapacity.toString() + "/" + mainModel.playerBagCapacity+".";
            if (bagCapacity <= mainModel.playerBagCapacity) {
                view.bagCapacityLabel.format.color = 0xD5CE76;
            } else {
                view.bagCapacityLabel.format.color = 0xff2515;
                view.bagCapacityLabel.text += " " + MainModel.getLocale("inventory.bag.capacity.noteleport");
            }
        }
    
        private function updateBeltList():void
        {
            beltListCollection.removeAll();
            for (var i:int=0; i<MAX_BELT_SIZE; i++) {
                if (i < mainModel.playerBeltCapacity) {
                    beltListCollection.addItem("empty"+Math.random());
                } else {
                    beltListCollection.addItem("block"+Math.random());
                }
            }

            for each (var itemBelt:DataArtikulBelt in mainModel.inventoryBelts) {
                beltListCollection.setItemAt(itemBelt, itemBelt.order-1);
            }
        }
    
        private function getArtikulTabDataById(id:int):Array
        {
            return getArtikulTabData(MainModel.getArtikulVO(id).artikulType.artikulTypeBagTab);
        }
    
        private function getArtikulTabData(tab:String):Array
        {
            switch (tab) {
                case BagTabEnum.MAIN:
                    return mainData;
                break;
                case BagTabEnum.CONSUMABLE:
                    return consumableData;
                break;
                case BagTabEnum.QUEST:
                    return questData;
                break;
                case BagTabEnum.PROFESSION:
                    return professionData;
                break;
                case BagTabEnum.RECIPE:
                    return recipeData;
                break;
                case BagTabEnum.NONE:
                break;
            }
            return [];
        }

        private function triggeredHandler(event:Event):void
        {
            switch (event.target) {
                case view.closeButton:
                    view.removeFromParent(true);
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                    break;
                case view.skillsButton:
                    if (view.skillsButton.isSelected) {
                        view.hideSkills();
                    } else {
                        view.showSkills(mainModel.playerSkill);
                    }
                    break;
                case view.beltUpButton:
                        view.beltList.verticalScrollPosition -= 58;
                        if (view.beltList.verticalScrollPosition < 0) {
                            view.beltList.verticalScrollPosition = 0;
                        }
                    break;
                case view.beltDownButton:
                    view.beltList.verticalScrollPosition += 58;
                    if (view.beltList.verticalScrollPosition > view.beltList.maxVerticalScrollPosition) {
                        view.beltList.verticalScrollPosition = view.beltList.maxVerticalScrollPosition;
                    }
                    break;
                case view.bagLeftButton:
                    var newPage:int = currentPage-1;
                    if (newPage < 0) {newPage = 0;}
                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;
                case view.bagRightButton:
                    var dataLength:int = getArtikulTabData(currentTab).length;
                        
                    newPage = currentPage+1;
                    if ((newPage*MAX_ITEM_TO_PAGE) >= dataLength ) {newPage = currentPage;}
                        
                    if (newPage != currentPage) {
                        currentPage = newPage;
                        updatePageData();
                    }
                    break;
            }
        }

        private function itemList_changeHandler(event:Event):void
        {
            var list:List = List( event.currentTarget );
            if (!list.selectedItem) {
                return;
            }

            var artikulAlert:ArtikulAlert = new ArtikulAlert(list.selectedItem as DataArtikul, null, mainModel.player.element);
            artikulAlert.addEventListener(ArtikulAlert.USE_EVENT, artikulAlertEventHandler);
            artikulAlert.addEventListener(ArtikulAlert.REPAIR_EVENT, artikulAlertEventHandler);
            artikulAlert.addEventListener(ArtikulAlert.DELETE_EVENT, artikulAlertEventHandler);
            artikulAlert.addEventListener(ArtikulAlert.SELL_EVENT, artikulAlertEventHandler);
            artikulAlert.addEventListener(ArtikulAlert.SLOT_ON_EVENT, artikulAlertEventHandler);

            view.sprite.stage.addChild(artikulAlert);

            list.selectedItem = null;
        }

        private function beltList_changeHandler(event:Event):void
        {
            var list:List = List( event.currentTarget );
            if (list.selectedItem && list.selectedItem is DataArtikulBelt) {
                var e:InventoryEvent = new InventoryEvent(InventoryEvent.BELT_TAKE_OFF_SERVER_COMMAND);
                e.artikulBelt = list.selectedItem as DataArtikulBelt;
                dispatch(e);
            }
            list.selectedItem = null;
        }

        private function updatePageData():void
        {
            view.bagPageLabel.text = (currentPage+1) + "/" + Math.max(1, Math.ceil(getArtikulTabData(currentTab).length / MAX_ITEM_TO_PAGE));
            
            var startIndex:int = currentPage*MAX_ITEM_TO_PAGE;
            
            switch (currentTab) {
                case BagTabEnum.MAIN:
                    view.itemList.dataProvider = new ListCollection(mainData.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
                    break;
                case BagTabEnum.CONSUMABLE:
                    view.itemList.dataProvider = new ListCollection(consumableData.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
                    break;
                case BagTabEnum.QUEST:
                    view.itemList.dataProvider = new ListCollection(questData.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
                    break;
                case BagTabEnum.PROFESSION:
                    view.itemList.dataProvider = new ListCollection(professionData.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
                    break;
                case BagTabEnum.RECIPE:
                    view.itemList.dataProvider = new ListCollection(recipeData.slice(startIndex, startIndex + MAX_ITEM_TO_PAGE));
                    break;
            }
        }

        private function okDeleteEventHandler(event:Event):void
        {
            var e:InventoryEvent = new InventoryEvent(InventoryEvent.BAG_REMOVE_SERVER_COMMAND);
            e.artikul = event.data.artikul as DataArtikul;
            e.count = event.data.count as int;
            dispatch(e);
        }

        private function okSellEventHandler(event:Event):void
        {
            var e:InventoryEvent = new InventoryEvent(InventoryEvent.ARTIKUL_SELL_SERVER_COMMAND);
            e.artikul = event.data.artikul as DataArtikul;
            e.count = event.data.count as int;
            dispatch(e);
        }

        private function slotOffAlertEventHandler(event:Event):void
        {
            var dataArtikulSlot:DataArtikulSlot = event.data as DataArtikulSlot;
            var artikulAlert:ArtikulAlert = new ArtikulAlert(dataArtikulSlot.artikul, dataArtikulSlot, mainModel.player.element);
            artikulAlert.addEventListener(ArtikulAlert.SLOT_OFF_EVENT, artikulAlertEventHandler);
            view.sprite.stage.addChild(artikulAlert);
        }

        private function artikulAlertEventHandler(event:Event):void
        {
            var e:InventoryEvent;
            
            switch(event.type) {
                case ArtikulAlert.USE_EVENT:
                    e = new InventoryEvent(InventoryEvent.ARTIKUL_ACTION_SERVER_COMMAND);
                    e.artikul = event.data as DataArtikul;
                    dispatch(e);
                    break;
                case ArtikulAlert.REPAIR_EVENT:
                    e = new InventoryEvent(InventoryEvent.ARTIKUL_REPAIR);
                    e.artikul = event.data as DataArtikul;
                    dispatch(e);
                    break;
                case ArtikulAlert.DELETE_EVENT:
                    var removeView:RemoveView = new RemoveView(event.data as DataArtikul, MainModel.artikulData[event.data.artikulId]);
                    removeView.addEventListener(RemoveView.REMOVE, okDeleteEventHandler);
                    view.addChild(removeView);
                    break;
                case ArtikulAlert.SELL_EVENT:
                    var sellView:SellView = new SellView(event.data as DataArtikul, MainModel.artikulData[event.data.artikulId]);
                    sellView.addEventListener(SellView.SELL, okSellEventHandler);
                    view.addChild(sellView);
                    break;
                case ArtikulAlert.SLOT_ON_EVENT:
                    slotOn(event.data as DataArtikul);
                    break;
                case ArtikulAlert.SLOT_OFF_EVENT:
                    e = new InventoryEvent(InventoryEvent.SLOT_TAKE_OFF_SERVER_COMMAND);
                    e.artikulSlot = event.data as DataArtikulSlot;
                    dispatch(e);
                    break;
            }
        }
    
        private function slotOn(dataArtikul:DataArtikul):void
        {
            var artikulVO:ArtikulVO = MainModel.artikulData[dataArtikul.artikulId];
            
            var levelMin:int = artikulVO.levelMin.number;
            var levelMax:int = artikulVO.levelMax.number;
            if (levelMin && (levelMin > mainModel.playerLevel) ||
                    levelMax && (levelMax < mainModel.playerLevel)) {
                ELAlert.show("Эта вещь не подходит вам по уровню.", "");
                return;
            }

            var e:InventoryEvent;
            if (artikulVO.action.bonusId) {

                ELAlert.show( "Использовать " + artikulVO.title.ru+"?", "",
                        [
                            { label: "Использовать", triggered: function ():void {
                                e = new InventoryEvent(InventoryEvent.ARTIKUL_ACTION_SERVER_COMMAND);
                                e.artikul = dataArtikul;
                                dispatch(e);
                            }, style:"pinkOkButton"},
                            { label: "Отмена", style:"blueCancelButton"}
                        ]);
                
            } else if (artikulVO.belty) {
                
                e = new InventoryEvent(InventoryEvent.BELT_TAKE_ON_SERVER_COMMAND);
                var artikulBelt:DataArtikulBelt = new DataArtikulBelt();
                artikulBelt.artikul = dataArtikul;
                artikulBelt.order = 1;
                for (var i:int=0; i < mainModel.playerBeltCapacity; i++) {
                    var item:Object = beltListCollection.getItemAt(i);
                    if (item is String && item.substr(0,5) == "empty") {
                        artikulBelt.order = i+1;
                        break;
                    }
                }
                e.artikulBelt = artikulBelt;
                dispatch(e);
            
            } else if (artikulVO.artikulSlot.length > 0) {
                
                e = new InventoryEvent(InventoryEvent.SLOT_TAKE_ON_SERVER_COMMAND);
                var artikulSlot:DataArtikulSlot = new DataArtikulSlot();
                artikulSlot.artikul = dataArtikul as DataArtikul;
                
                switch (artikulVO.artikulSlot[0].artikulSlot) {
                    case SlotEnum.BAG: artikulSlot.type = DataArtikulSlotType.BAG; break;
                    case SlotEnum.BELT: artikulSlot.type = DataArtikulSlotType.BELT; break;
                    case SlotEnum.BOOTS: artikulSlot.type = DataArtikulSlotType.BOOTS; break;
                    case SlotEnum.BRACERS: artikulSlot.type = DataArtikulSlotType.BRACERS; break;
                    case SlotEnum.CHEST: artikulSlot.type = DataArtikulSlotType.CHEST; break;
                    case SlotEnum.GLOVES: artikulSlot.type = DataArtikulSlotType.GLOVES; break;
                    case SlotEnum.HEAD: artikulSlot.type = DataArtikulSlotType.HEAD; break;
                    case SlotEnum.LEGS: artikulSlot.type = DataArtikulSlotType.LEGS; break;
                    case SlotEnum.MINING: artikulSlot.type = DataArtikulSlotType.MINING; break;
                    case SlotEnum.SHOULDERS: artikulSlot.type = DataArtikulSlotType.SHOULDERS; break;
                    case SlotEnum.TALISMAN_LEFT:
                    case SlotEnum.TALISMAN_RIGHT:
                        if (artikulVO.artikulSlot.length > 1) {
                            if (view.getSpriteSlot(DataArtikulSlotType.TALISMAN_LEFT).numChildren > 0) {
                                artikulSlot.type = DataArtikulSlotType.TALISMAN_RIGHT;
                            } else {
                                artikulSlot.type = DataArtikulSlotType.TALISMAN_LEFT;
                            }
                        } else {
                            if (artikulVO.artikulSlot[0].artikulSlot == SlotEnum.TALISMAN_LEFT) {
                                artikulSlot.type = DataArtikulSlotType.TALISMAN_LEFT;
                            } else if (artikulVO.artikulSlot[0].artikulSlot == SlotEnum.TALISMAN_RIGHT) {
                                artikulSlot.type = DataArtikulSlotType.TALISMAN_RIGHT;
                            }
                        }
                        break;
                    case SlotEnum.HAND_BOTH: artikulSlot.type = DataArtikulSlotType.HAND_BOTH; break;
                    case SlotEnum.HAND_LEFT:
                    case SlotEnum.HAND_RIGHT:
                        if (artikulVO.artikulSlot.length > 1) {
                            if (view.getSpriteSlot(DataArtikulSlotType.HAND_LEFT).numChildren > 0) {
                                artikulSlot.type = DataArtikulSlotType.HAND_RIGHT;
                            } else {
                                artikulSlot.type = DataArtikulSlotType.HAND_LEFT;
                            }
                        } else {
                            if (artikulVO.artikulSlot[0].artikulSlot == SlotEnum.HAND_LEFT) {
                                artikulSlot.type = DataArtikulSlotType.HAND_LEFT;
                            } else if (artikulVO.artikulSlot[0].artikulSlot == SlotEnum.HAND_RIGHT) {
                                artikulSlot.type = DataArtikulSlotType.HAND_RIGHT;
                            }
                        }
                        break;
                }
                e.artikulSlot = artikulSlot;
                dispatch(e);
            }
        }
}
}