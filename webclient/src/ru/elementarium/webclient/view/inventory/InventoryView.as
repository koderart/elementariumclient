package ru.elementarium.webclient.view.inventory {
import com.junkbyte.console.Cc;

import feathers.controls.List;
import feathers.controls.TabBar;
import feathers.controls.ToggleButton;
import feathers.controls.renderers.IListItemRenderer;
import feathers.data.ListCollection;
import feathers.dragDrop.DragData;
import feathers.dragDrop.DragDropManager;
import feathers.dragDrop.IDropTarget;
import feathers.events.DragDropEvent;
import feathers.layout.Direction;
import feathers.layout.FlowLayout;
import feathers.layout.VerticalLayout;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.inventory.components.BeltItemRenderer;
import ru.elementarium.webclient.view.inventory.components.InventoryItemRenderer;
import ru.elementarium.webclient.view.inventory.components.SlotItemComponent;
import ru.elementarium.webclient.view.main.AbstractView;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlot;
import ru.muvs.elementarium.thrift.artikul.DataArtikulSlotType;

import starling.display.Button;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;

public class InventoryView extends AbstractView implements IDropTarget
{
        public var closeButton:Button;
        public var itemList:List;
        public var beltList:List;

        public var talismanLeftSprite:Sprite;
        public var talismanRightSprite:Sprite;
        public var headSprite:Sprite;
        public var chestSprite:Sprite;
        public var beltSprite:Sprite;
        public var shouldersSprite:Sprite;
        public var handLeftSprite:Sprite;
        public var handRightSprite:Sprite;
        public var bracersSprite:Sprite;
        public var glovesSprite:Sprite;
        public var legsSprite:Sprite;
        public var bootsSprite:Sprite;
        public var bagSprite:Sprite;
        public var miningSprite:Sprite;

        public var maleBackground:Image;
        public var femaleBackground:Image;

        public var skillsButton:ToggleButton;
        
        private var skillsView:SkillsView;
        
        public var bagCapacityLabel:TextField;

        public var inventoryTabBar:TabBar;

        public var beltUpButton:Button;
        public var beltDownButton:Button;
        public var bagLeftButton:Button;
        public var bagRightButton:Button;
        public var bagPageLabel:TextField;

        public var dragArea:DragArea;

        public function InventoryView()
        {
            _sprite = AppTheme.uiBuilder.create(ParsedLayouts.inventory, false) as Sprite;
            super();
            closeButton = _sprite.getChildByName("closeButton") as Button;

            itemList = _sprite.getChildByName("itemList") as List;
            itemList.itemRendererFactory = function():IListItemRenderer { return new InventoryItemRenderer(); };
            var layout:FlowLayout = new FlowLayout();
            layout.gap = 10;
            layout.padding = 10;
            itemList.layout = layout;

            beltList = _sprite.getChildByName("beltList") as List;
            beltList.itemRendererFactory = function():IListItemRenderer { return new BeltItemRenderer(); };
            var layoutV:VerticalLayout = new VerticalLayout();
            layoutV.gap = 0;
            beltList.layout = layoutV;

            var bodyContainer:Sprite = _sprite.getChildByName("bodyContainer") as Sprite;
            talismanLeftSprite = bodyContainer.getChildByName("talisman_left_sprite") as Sprite;
            talismanRightSprite = bodyContainer.getChildByName("talisman_right_sprite") as Sprite;
            headSprite = bodyContainer.getChildByName("head_sprite") as Sprite;
            chestSprite = bodyContainer.getChildByName("chest_sprite") as Sprite;
            beltSprite = bodyContainer.getChildByName("belt_sprite") as Sprite;
            shouldersSprite = bodyContainer.getChildByName("shoulders_sprite") as Sprite;
            handLeftSprite = bodyContainer.getChildByName("hand_left_sprite") as Sprite;
            handRightSprite = bodyContainer.getChildByName("hand_right_sprite") as Sprite;
            bracersSprite = bodyContainer.getChildByName("bracers_sprite") as Sprite;
            glovesSprite = bodyContainer.getChildByName("gloves_sprite") as Sprite;
            legsSprite = bodyContainer.getChildByName("legs_sprite") as Sprite;
            bootsSprite = bodyContainer.getChildByName("boots_sprite") as Sprite;
            bagSprite = bodyContainer.getChildByName("bag_sprite") as Sprite;
            miningSprite = bodyContainer.getChildByName("mining_sprite") as Sprite;

            skillsButton = _sprite.getChildByName("skillsButton") as ToggleButton;

            inventoryTabBar = new TabBar();
            inventoryTabBar.customTabStyleName = "inventoryTabButton";
            inventoryTabBar.gap = 4;
            inventoryTabBar.direction = Direction.VERTICAL;
            inventoryTabBar.dataProvider = new ListCollection(
                    [
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/equipment")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/equipment_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/potion")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/potion_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/quest")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/quest_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/craft")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/craft_selected"))},
                        { label: "",  defaultIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/recipie")),
                            defaultSelectedIcon:new Image(AppTheme.instance.assets.getTexture("inventory/tab_ico/recipie_selected"))}
                    ]);

            inventoryTabBar.validate();
            var tabPosition:Sprite = _sprite.getChildByName("tabPosition") as Sprite;
            tabPosition.addChild( inventoryTabBar );

            maleBackground = _sprite.getChildByName("maleBackground") as Image;
            femaleBackground = _sprite.getChildByName("femaleBackground") as Image;
            bagCapacityLabel = _sprite.getChildByName("bagCapacityLabel") as TextField;

            beltUpButton = _sprite.getChildByName("beltUpButton") as Button;
            beltDownButton = _sprite.getChildByName("beltDownButton") as Button;
            bagLeftButton = _sprite.getChildByName("bagLeftButton") as Button;
            bagRightButton = _sprite.getChildByName("bagRightButton") as Button;
            bagPageLabel = _sprite.getChildByName("bagPageLabel") as TextField;
            
            dragArea = new DragArea(maleBackground.width + beltList.width, maleBackground.height, 0x7c2d);
            dragArea.alpha = 0;
            dragArea.touchable = false;
            _sprite.addChild(dragArea);
            
            addEventListener(DragDropEvent.DRAG_ENTER, onDragEnter);
            addEventListener(DragDropEvent.DRAG_EXIT, onDragExit);
            addEventListener(DragDropEvent.DRAG_DROP, onDragDrop);

        }
    
        private function onDragEnter(event:DragDropEvent, dragData:DragData):void
        {
            dragArea.alpha = 0.05;
            dragArea.touchable = true;
            DragDropManager.acceptDrag(this);
        }
    
        private function onDragDrop(event:DragDropEvent, dragData:DragData):void
        {
            dragArea.alpha = 0;
            dragArea.touchable = false;
        }
    
        private function onDragExit(event:DragDropEvent, dragData:DragData):void
        {
        }
        
        public function showSkills(skills:Array):void
        {
            if (skillsView) { return; }
            skillsView = new SkillsView();
            skillsView.draw(skills);
            skillsView.x = skillsButton.x - skillsView.width;
            skillsView.y = skillsButton.y - skillsView.height;
            skillsView.addEventListener(TouchEvent.TOUCH, touchHandler);
            _sprite.addChild(skillsView);
        }

        public function updateSkills(skills:Array):void
        {
            if (!skillsView) { return; }
            skillsView.draw(skills);
        }

        public function hideSkills():void
        {
            if (skillsView) {
                _sprite.removeChild(skillsView);
                skillsView.dispose();
                skillsView = null;
            }
        }

        private function touchHandler(event:TouchEvent):void
        {
            var image:DisplayObject = event.target as DisplayObject;
            var touch:Touch = event.getTouch(image);
            if (touch && touch.phase == TouchPhase.ENDED) {
                skillsView.removeEventListener(TouchEvent.TOUCH, touchHandler);
                skillsButton.isSelected = false;
                hideSkills();
            }
        }
    
        public function drawSlot(itemSlot:DataArtikulSlot):void
        {
            if (itemSlot.artikul) {
                var newSlot:SlotItemComponent = new SlotItemComponent(itemSlot);
                getSpriteSlot(itemSlot.type).removeChildren();
                getSpriteSlot(itemSlot.type).addChild(newSlot);
            }
        }

        public function hideSlot(itemSlot:DataArtikulSlot):void
        {
            getSpriteSlot(itemSlot.type).removeChildren();
        }
    
        public function getSpriteSlot(type:int):Sprite
        {
            switch (type) {
                case DataArtikulSlotType.BAG:
                    return bagSprite;
                    break;
                case DataArtikulSlotType.BELT:
                    return beltSprite;
                    break;
                case DataArtikulSlotType.BOOTS:
                    return bootsSprite;
                    break;
                case DataArtikulSlotType.BRACERS:
                    return bracersSprite;
                    break;
                case DataArtikulSlotType.CHEST:
                    return chestSprite;
                    break;
                case DataArtikulSlotType.GLOVES:
                    return glovesSprite;
                    break;
                case DataArtikulSlotType.HAND_BOTH:
                    return handLeftSprite;
                    break;
                case DataArtikulSlotType.HAND_LEFT:
                    return handLeftSprite;
                    break;
                case DataArtikulSlotType.HAND_RIGHT:
                    return handRightSprite;
                    break;
                case DataArtikulSlotType.HEAD:
                    return headSprite;
                    break;
                case DataArtikulSlotType.LEGS:
                    return legsSprite;
                    break;
                case DataArtikulSlotType.MINING:
                    return miningSprite;
                    break;
                case DataArtikulSlotType.SHOULDERS:
                    return shouldersSprite;
                    break;
                case DataArtikulSlotType.TALISMAN_LEFT:
                    return talismanLeftSprite;
                    break;
                case DataArtikulSlotType.TALISMAN_RIGHT:
                    return talismanRightSprite;
                    break;
            }
            return null;
        }
    
        public function updateBgBySex(male:Boolean):void
        {
            if (male) {
                maleBackground.visible = true;
                femaleBackground.visible = false;
            } else {
                maleBackground.visible = false;
                femaleBackground.visible = true;
            }
        }

}
}
