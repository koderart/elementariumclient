package ru.elementarium.webclient.view.arena{
import com.junkbyte.console.Cc;

import feathers.controls.LayoutGroup;

import ru.elementarium.webclient.AppTheme;
import ru.elementarium.webclient.view.ParsedLayouts;
import ru.elementarium.webclient.view.main.ModuleView;

import starling.display.Button;
import starling.display.Sprite;
import starling.text.TextField;

public class ArenaView extends ModuleView
    {
        public var _buttonLayout:LayoutGroup;
    
        public var _messageText:TextField;

        public var _arena1Button:Button;
        public var _arena2Button:Button;
        public var _arena3Button:Button;
        public var _arena4Button:Button;
        public var _arena5Button:Button;
        public var _cancelButton:feathers.controls.Button;

        public function ArenaView()
        {
            super();
            _content = AppTheme.uiBuilder.create(ParsedLayouts.arena, false, this) as Sprite;

            _titleLabel.text = "АРЕНА";

            validateAndAttach();
        }
}
}
