package ru.elementarium.webclient.view.arena
{
import com.junkbyte.console.Cc;

import org.robotlegs.starling.mvcs.Mediator;

import ru.elementarium.webclient.events.BattleEvent;
import ru.elementarium.webclient.events.BattleEventType;
import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.model.Match3Model;
import ru.muvs.elementarium.thrift.battle.DataBattleQueue;

import starling.events.Event;
import starling.filters.ColorMatrixFilter;

public class ArenaViewMediator extends Mediator
	{
		[Inject]
		public var view:ArenaView;
        [Inject]
        public var mainModel:MainModel;
        [Inject]
        public var gameModel:Match3Model;

        private var message:String = "На арене вы можете сражаться с другими игроками.\nВыберите желаемый тип боя и подайте заявку.";

		override public function onRegister():void
		{
            addContextListener(BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND, arenaEventTypeHandler);

            view._arena1Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._arena2Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._arena3Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._arena4Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._arena5Button.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._cancelButton.addEventListener(Event.TRIGGERED, triggeredHandler);
            view._closeButton.addEventListener(Event.TRIGGERED, triggeredHandler);

            updateView();
        }

        private function arenaEventTypeHandler(event:Event):void
        {
            switch (event.type) {
                case BattleEventType.BATTLE_QUEUE_JOIN_CLIENT_COMMAND:
                    updateView();
                    break;

            }
        }

        private function updateView():void
        {
            if (gameModel.queueType == DataBattleQueue.NONE) {
                visibleButtons(true);
                view._messageText.text = message;
            } else {
                visibleButtons(false);
                switch (gameModel.queueType) {
                    case DataBattleQueue.PVP:
                        view._messageText.text = "Вы подали заявку на бой 1 VS 1. Ожидайте начала боя.";
                        break;
                    case DataBattleQueue.P2VP2:
                        view._messageText.text = "Вы подали заявку на бой 2 VS 2. Ожидайте начала боя.";
                        break;
                    case DataBattleQueue.P3VP3:
                        view._messageText.text = "Вы подали заявку на бой 3 VS 3. Ожидайте начала боя.";
                        break;
                    case DataBattleQueue.P4VP4:
                        view._messageText.text = "Вы подали заявку на бой 4 VS 4. Ожидайте начала боя.";
                        break;
                    case DataBattleQueue.P5VP5:
                        view._messageText.text = "Вы подали заявку на бой 5 VS 5. Ожидайте начала боя.";
                        break;
                }
            }
        }

        private function visibleButtons(value:Boolean):void
        {
            if (value) {
                view._arena1Button.filter = null;
                view._arena2Button.filter = null;
                view._arena3Button.filter = null;
                view._arena4Button.filter = null;
                view._arena5Button.filter = null;

                view._cancelButton.filter = getDisableFilter();
                view._buttonLayout.isEnabled = true;
                view._cancelButton.isEnabled = false;
            } else {
                view._arena1Button.filter = getDisableFilter();
                view._arena2Button.filter = getDisableFilter();
                view._arena3Button.filter = getDisableFilter();
                view._arena4Button.filter = getDisableFilter();
                view._arena5Button.filter = getDisableFilter();
                view._cancelButton.filter = null;
                view._buttonLayout.isEnabled = false;
                view._cancelButton.isEnabled = true;
            }
        }

        private function getDisableFilter():ColorMatrixFilter
        {
            var colorFilter:ColorMatrixFilter = new ColorMatrixFilter();
            colorFilter.adjustSaturation(-1);
            return colorFilter;
        }
    
        private function triggeredHandler(event:Event):void
        {
            var e:BattleEvent = new BattleEvent(BattleEvent.BATTLE_QUEUE_JOIN_SERVER_COMMAND);;
            switch (event.target) {
                case view._arena1Button:
                    e.id = DataBattleQueue.PVP;
                    dispatch(e);
                break;
                case view._arena2Button:
                    e.id = DataBattleQueue.P2VP2;
                    dispatch(e);
                break;
                case view._arena3Button:
                    e.id = DataBattleQueue.P3VP3;
                    dispatch(e);
                break;
                case view._arena4Button:
                    e.id = DataBattleQueue.P4VP4;
                    dispatch(e);
                break;
                case view._arena5Button:
                    e.id = DataBattleQueue.P5VP5;
                    dispatch(e);
                break;
                case view._cancelButton:
                    dispatch(new BattleEvent(BattleEvent.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND));
                break;
                case view._closeButton:
                    if (gameModel.queueType != DataBattleQueue.NONE) {
                        dispatch(new BattleEvent(BattleEvent.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND));
                    }
                    mainModel.openArena = false;
                    view.removeFromParent(true);
                    dispatchWith(GameEvent.RESET_MAIN_MENU);
                break;
            }

        }
    }
}