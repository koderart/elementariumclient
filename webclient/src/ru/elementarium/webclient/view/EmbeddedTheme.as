package ru.elementarium.webclient.view
{

	public class EmbeddedTheme
	{
		[Embed(source="/../assets/themes/aeon_desktop.png")]
		public static const aeon_desktop:Class;

		[Embed(source="/../assets/themes/aeon_desktop.xml", mimeType="application/octet-stream")]
		public static const aeon_desktop_xml:Class;

        public function EmbeddedTheme()
		{
		}
	}
}