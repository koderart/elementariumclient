package ru.elementarium.webclient
{
import org.robotlegs.starling.core.IInjector;
import org.robotlegs.starling.core.IMediatorMap;
import org.robotlegs.starling.mvcs.Context;

import ru.elementarium.webclient.controller.*;
import ru.elementarium.webclient.events.*;
import ru.elementarium.webclient.model.*;
import ru.elementarium.webclient.services.*;
import ru.elementarium.webclient.view.achieve.*;
import ru.elementarium.webclient.view.alert.*;
import ru.elementarium.webclient.view.arena.*;
import ru.elementarium.webclient.view.help.*;
import ru.elementarium.webclient.view.inventory.*;
import ru.elementarium.webclient.view.login.*;
import ru.elementarium.webclient.view.login.FractionView;
import ru.elementarium.webclient.view.main.*;
import ru.elementarium.webclient.view.game.*;
import ru.elementarium.webclient.view.match3.*;
import ru.elementarium.webclient.view.news.*;
import ru.elementarium.webclient.view.payment.*;
import ru.elementarium.webclient.view.quest.*;
import ru.elementarium.webclient.view.store.*;

import starling.display.DisplayObjectContainer;

public class AppContext extends Context
	{
        private static var _instance: AppContext;

        public static function get instance():AppContext {
            return _instance;
        }

        public function AppContext()
		{
            _instance = this;
		}

		override protected function get injector():IInjector
		{
			return super.injector;
		}
		
		override protected function set injector(value:IInjector):void
		{
			super.injector = value;
		}

        public function get getMediatorMap():IMediatorMap
        {
            return this.mediatorMap;
        }


		override public function get contextView():DisplayObjectContainer
		{
			return super.contextView;
		}
		
		override public function set contextView(value:DisplayObjectContainer):void
		{
			super.contextView = value;
		}

		override public function startup():void
		{
			//this.commandMap.mapEvent(ContextEventType.STARTUP_COMPLETE, ConnectCommand);
            this.commandMap.mapEvent(GameEvent.ACTIVATE, GameCommand);
            this.commandMap.mapEvent(GameEvent.DEACTIVATE, GameCommand);

            this.commandMap.mapEvent(StaticDataEvent.LOAD, StaticCommand);
            this.commandMap.mapEvent(StaticDataEvent.LOADED, StaticCommand);

			this.commandMap.mapEvent(ConnectionEvent.CONNECT, ConnectionCommand);
			this.commandMap.mapEvent(ConnectionEvent.CONNECTED, ConnectionCommand);
			this.commandMap.mapEvent(ConnectionEvent.VERSION_CLIENT_COMMAND, ConnectionCommand);
			this.commandMap.mapEvent(ConnectionEvent.DISCONNECTED, ConnectionCommand);

            this.commandMap.mapEvent(LoginEvent.LOGIN_EMAIL, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.REGISTER_EMAIL, LoginCommand);
			this.commandMap.mapEvent(LoginEvent.LOGIN_VK, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.REGISTER_VK, LoginCommand);
			this.commandMap.mapEvent(LoginEvent.LOGIN_MY_MAIL, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.REGISTER_MY_MAIL, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.LOGIN_APPLE, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.REGISTER_APPLE, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.PLAYER_UNDEFINED, LoginCommand);
            this.commandMap.mapEvent(LoginEvent.LOGIN_SUCCESS, LoginCommand);

			this.commandMap.mapEvent(Match3Event.MOVE, Match3Command);
			this.commandMap.mapEvent(Match3Event.USE_SPELL, Match3Command);
			this.commandMap.mapEvent(Match3Event.USE_POTION, Match3Command);

            this.commandMap.mapEvent(BattleEvent.BATTLE_JOIN_SERVER_COMMAND, BattleCommand);
            this.commandMap.mapEvent(BattleEvent.BATTLE_QUEUE_JOIN_SERVER_COMMAND, BattleCommand);
            this.commandMap.mapEvent(BattleEvent.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND, BattleCommand);

            this.commandMap.mapEvent(StoreEvent.BUY, StoreCommand);
            
            this.commandMap.mapEvent(InventoryEvent.BAG_LIST_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.BAG_LIST_CLIENT_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.BAG_REMOVE_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.BELT_LIST_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.BELT_TAKE_ON_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.BELT_TAKE_OFF_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.ARTIKUL_ACTION_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.ARTIKUL_REPAIR_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.ARTIKUL_SELL_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.SLOT_LIST_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.SLOT_LIST_CLIENT_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.SLOT_TAKE_ON_SERVER_COMMAND, InventoryCommand);
            this.commandMap.mapEvent(InventoryEvent.SLOT_TAKE_OFF_SERVER_COMMAND, InventoryCommand);

            this.commandMap.mapEvent(UserEvent.MONEY_LIST_SERVER_COMMAND, UserCommand);
            this.commandMap.mapEvent(UserEvent.SKILL_LIST_SERVER_COMMAND, UserCommand);
            this.commandMap.mapEvent(UserEvent.MONEY_LIST_CLIENT_COMMAND, UserCommand);

            this.commandMap.mapEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST, QuestCommand);
            this.commandMap.mapEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_TAKE, QuestCommand);
            this.commandMap.mapEvent(QuestEvent.LOCATION_ACTION_SERVER_QUEST_GIVE, QuestCommand);
            this.commandMap.mapEvent(QuestEvent.QUEST_REFUSE_SERVER_COMMAND, QuestCommand);
            this.commandMap.mapEvent(QuestEvent.QUEST_LIST_SERVER_COMMAND, QuestCommand);
            
            this.commandMap.mapEvent(LocationEvent.LOCATION_ACTION_SERVER_TELEPORT_ENTER, LocationCommand);
            this.commandMap.mapEvent(LocationEvent.LOCATION_ACTION_SERVER_PICKUP_ENTER, LocationCommand);
            this.commandMap.mapEvent(LocationEvent.LOCATION_ACTION_SERVER_BATTLE_JOIN, LocationCommand);
            this.commandMap.mapEvent(LocationEvent.LOCATION_ACTION_SERVER_BATTLE_INFO, LocationCommand);
            this.commandMap.mapEvent(LocationEvent.LOCATION_ACTION_SERVER_MEMBER_RUN, LocationCommand);

            this.commandMap.mapEvent(ChallengeEvent.CHALLENGE_LIST_SERVER_COMMAND, ChallengeCommand);
            
            this.commandMap.mapEvent(ChatEvent.CHAT_MESSAGE_SEND_SERVER_COMMAND, ChatCommand);
            this.commandMap.mapEvent(AchieveEvent.ACHIEVE_LIST_SERVER_COMMAND, AchieveCommand);
            this.commandMap.mapEvent(PaymentEvent.PAYMENT_APPLE_START, PaymentCommand);
            this.commandMap.mapEvent(PaymentEvent.PAYMENT_APPLE_STOP, PaymentCommand);
            this.commandMap.mapEvent(PaymentEvent.PAYMENT_APPLE_MAKE_PURCHASE, PaymentCommand);
            this.commandMap.mapEvent(PaymentEvent.PAYMENT_APPLE_DEFERRED_PURCHASE, PaymentCommand);
            this.commandMap.mapEvent(PaymentEvent.PAYMENT_APPLE_CLIENT_COMMAND, PaymentCommand);
            
            this.injector.mapSingleton(MainModel);
            this.injector.mapSingleton(ConnectionService);
            this.injector.mapSingleton(StaticService);
            this.injector.mapSingleton(Match3Model);

            this.mediatorMap.mapView(AppView, AppMediator);
            this.mediatorMap.mapView(StaticView, StaticMediator);
            this.mediatorMap.mapView(GameView, GameViewMediator);
            this.mediatorMap.mapView(ChatView, ChatViewMediator);
            this.mediatorMap.mapView(LoginView, LoginViewMediator);
            this.mediatorMap.mapView(Match3View, Match3ViewMediator);

            this.mediatorMap.mapView(InventoryView, InventoryViewMediator);
            this.mediatorMap.mapView(StoreView, StoreViewMediator);
            this.mediatorMap.mapView(RegisterTokenView, RegisterTokenViewMediator);
            this.mediatorMap.mapView(ArenaView, ArenaViewMediator);
            this.mediatorMap.mapView(PaymentView, PaymentViewMediator);
            this.mediatorMap.mapView(QuestView, QuestViewMediator);
            this.mediatorMap.mapView(AlertView, AlertViewMediator);
            this.mediatorMap.mapView(HelpView, HelpViewMediator);
            this.mediatorMap.mapView(NewsView, NewsViewMediator);
            this.mediatorMap.mapView(AlertQuestCompleteView, AlertQuestCompleteViewMediator);
            this.mediatorMap.mapView(RepairView, RepairViewMediator);
            this.mediatorMap.mapView(AchieveView, AchieveViewMediator);
            this.mediatorMap.mapView(FractionView, FractionViewMediator);
			super.startup();
		}
	}
}
