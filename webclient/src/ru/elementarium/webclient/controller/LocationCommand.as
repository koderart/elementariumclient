package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.LocationEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class LocationCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;
        [Inject]
        public var event:LocationEvent;

        override public function execute():void
		{
            switch (event.type) {
                case LocationEvent.LOCATION_ACTION_SERVER_TELEPORT_ENTER:
                    console.log("LocationEvent.LOCATION_ACTION_SERVER_TELEPORT_ENTER");
                    connection.enterLocation(event.member);
                    break;
                case LocationEvent.LOCATION_ACTION_SERVER_PICKUP_ENTER:
                    console.log("LocationEvent.LOCATION_ACTION_SERVER_PICKUP_ENTER");
                    connection.enterPickup(event.member);
                    break;
                case LocationEvent.LOCATION_ACTION_SERVER_BATTLE_JOIN:
                    console.log("LocationEvent.LOCATION_ACTION_SERVER_BATTLE_JOIN");
                    connection.joinBattleLocation(event.member);
                    break;
                case LocationEvent.LOCATION_ACTION_SERVER_BATTLE_INFO:
                    console.log("LocationEvent.LOCATION_ACTION_SERVER_BATTLE_INFO");
                    connection.memberInfoLocation(event.member);
                    break;
                case LocationEvent.LOCATION_ACTION_SERVER_MEMBER_RUN:
                    console.log("LocationEvent.LOCATION_ACTION_SERVER_MEMBER_RUN");
                    connection.memberRunLocation(event.runPoint);
                    break;
            }
		}
    }
}
