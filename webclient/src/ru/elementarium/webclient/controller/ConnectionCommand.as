package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.ConnectionEvent;
import ru.elementarium.webclient.events.LoginEvent;
import ru.elementarium.webclient.events.PaymentEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;

    public class ConnectionCommand extends Command
    {
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;
		[Inject]
		public var event:ConnectionEvent;
        [Inject]
        public var mainModel:MainModel;

		override public function execute():void
		{
			switch(event.type) {
				case ConnectionEvent.CONNECT:
                    console.log("ConnectionEvent.CONNECT");
					connection.connect();
					break;
				case ConnectionEvent.CONNECTED:
                    connection.serverVersion();
                    break;
				case ConnectionEvent.VERSION_CLIENT_COMMAND:
                    console.log("ConnectionEvent.CONNECTED");
                    if (event.serverVersion != MainModel.serverVersion) {
                        dispatch(new ConnectionEvent (ConnectionEvent.VERSION_CLIENT_ERROR));
                        return;
                    } 
                    BUILD::mobile {
                        if (MainModel.isMobile) {
                            dispatch(new LoginEvent (LoginEvent.LOGIN_APPLE));
                        } else {
                            dispatch(new LoginEvent(LoginEvent.LOGIN_EMAIL_START));
                        }
                    }

                    BUILD::release {
                        if (mainModel.socialVkData) {
                            dispatch(new LoginEvent(LoginEvent.LOGIN_VK));
                        } else if (mainModel.socialMyMailData) {
                            dispatch(new LoginEvent (LoginEvent.LOGIN_MY_MAIL));
                        } else {
                            dispatch(new LoginEvent(LoginEvent.SOCIAL_ERROR));
                        }
                    }

                    BUILD::test {
                        dispatch(new LoginEvent(LoginEvent.LOGIN_EMAIL_START));
                    }

					break;
                case ConnectionEvent.DISCONNECTED:
                    console.log("ConnectionEvent.DISCONNECTED");
                    //dispatch(new PaymentEvent(PaymentEvent.PAYMENT_APPLE_STOP));
                    break;
			}
            //addContextListener(NetEvent.MAIN_SERVICE_CONNECTED, onMainServiceConnected, NetEvent);
            //addContextListener(NetEvent.MAIN_SERVICE_CONNECITON_FAILED, onMainServiceFailed, NetEvent);
		}
    }
}
