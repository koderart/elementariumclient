package ru.elementarium.webclient.controller
{

import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.StaticDataEvent;
import ru.elementarium.webclient.services.ConnectionService;
import ru.elementarium.webclient.services.StaticService;

    public class StaticCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;
        [Inject]
        public var staticData:StaticService;
        [Inject]
        public var event:StaticDataEvent;

		override public function execute():void
		{
            switch(event.type) {
                case StaticDataEvent.LOAD:
                    console.log("StaticDataEvent.LOAD");
                    staticData.load();
                    break;
                case StaticDataEvent.LOADED:
                    console.log("StaticDataEvent.LOADED");
                    connection.connect();
                    //dispatch(new ConnectionEvent(ConnectionEvent.CONNECT));
                    break;
            }
		}
    }
}
