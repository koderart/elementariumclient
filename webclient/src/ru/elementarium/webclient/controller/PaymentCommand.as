package ru.elementarium.webclient.controller
{
BUILD::mobile {
    import com.milkmangames.nativeextensions.ios.StoreKit;
    import com.milkmangames.nativeextensions.ios.events.StoreKitEvent;
    import com.milkmangames.nativeextensions.ios.events.StoreKitErrorEvent;
}
import com.junkbyte.console.ConsoleChannel;
import org.robotlegs.starling.mvcs.Command;
import ru.elementarium.webclient.events.PaymentEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;

    public class PaymentCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:PaymentEvent;

        [Inject]
        public var mainModel:MainModel;

		override public function execute():void
		{
		    switch (event.type) {
                case PaymentEvent.PAYMENT_APPLE_START:
                    console.log("PaymentEvent.PAYMENT_APPLE_START");
                    BUILD::mobile {
                        if(StoreKit.isSupported()) {
                            StoreKit.create();
                            StoreKit.storeKit.setManualTransactionMode(true);
                            if(!StoreKit.storeKit.isStoreKitAvailable()) { trace("this device has purchases disabled."); return; }

                            StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_SUCCEEDED,onPurchaseSuccess);
                            StoreKit.storeKit.addEventListener(StoreKitErrorEvent.PURCHASE_FAILED, onPurchaseFailed);
                            StoreKit.storeKit.addEventListener(StoreKitEvent.PURCHASE_CANCELLED, onPurchaseCancelled);
                        }
                    }

                    break;
                case PaymentEvent.PAYMENT_APPLE_STOP:
                    console.log("PaymentEvent.PAYMENT_APPLE_STOP");
                    BUILD::mobile {
                        if(StoreKit.isSupported()) {
                            StoreKit.storeKit.removeEventListener(StoreKitEvent.PURCHASE_SUCCEEDED,onPurchaseSuccess);
                            StoreKit.storeKit.removeEventListener(StoreKitErrorEvent.PURCHASE_FAILED, onPurchaseFailed);
                            StoreKit.storeKit.removeEventListener(StoreKitEvent.PURCHASE_CANCELLED, onPurchaseCancelled);
                            StoreKit.storeKit.dispose();
                        }
                    }

                    break;
                case PaymentEvent.PAYMENT_APPLE_MAKE_PURCHASE:
                    console.log("PaymentEvent.PAYMENT_APPLE_MAKE_PURCHASE");
                    BUILD::mobile {
                        if(StoreKit.isSupported()) {
                            StoreKit.storeKit.purchaseProduct(event.inApp);
                        }
                    }
                    break;
                case PaymentEvent.PAYMENT_APPLE_CLIENT_COMMAND:
                    console.log("PaymentEvent.PAYMENT_APPLE_CLIENT_COMMAND");
                    BUILD::mobile {
                        if(StoreKit.isSupported()) {
                            StoreKit.storeKit.manualFinishTransaction(event.transactionId);
                        }
                        purchaseDeferred();
                    }
                    break;
                case PaymentEvent.PAYMENT_APPLE_DEFERRED_PURCHASE:
                    console.log("PaymentEvent.PAYMENT_APPLE_DEFERRED_PURCHASE");
                    purchaseDeferred();
                    break;
            }
		}

        private function purchaseDeferred():void
        {
            if (mainModel.storeKitPurchaseDeferred.length > 0) {
                var payment:Object = mainModel.storeKitPurchaseDeferred.shift();
                connection.paymentApple(payment.productId, payment.transactionId, payment.receipt);
            }
        }
    
    BUILD::mobile {

        
        private function onPurchaseSuccess(e:StoreKitEvent):void
        {
            console.log("onPurchaseSuccess", e.productId);
            if(StoreKit.storeKit.isAppReceiptAvailable()) {
                var receipt:String=StoreKit.storeKit.getAppReceipt();
                if (mainModel.player.id) {
                    connection.paymentApple(e.productId, e.transactionId, receipt);
                } else {
                    mainModel.storeKitPurchaseDeferred.push({productId:e.productId, transactionId:e.transactionId, receipt:receipt})
                }
            }
        }

        private function onPurchaseCancelled(e:StoreKitEvent):void
        {
            console.log("onPurchaseCancelled", e.productId, e.transactionId);
            StoreKit.storeKit.manualFinishTransaction(e.transactionId);
        }

        private function onPurchaseFailed(e:StoreKitErrorEvent):void
        {
            console.log("error purchasing product:", e.text);
        }
        
    }
    
    }
}
