package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;
import ru.elementarium.webclient.events.ChatEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class ChatCommand extends Command
    {
        private var console:ConsoleChannel = new ConsoleChannel("command");

        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:ChatEvent;

        override public function execute():void
        {
            switch (event.type) {
                case ChatEvent.CHAT_MESSAGE_SEND_SERVER_COMMAND:
                    console.log("ChatEvent.CHAT_MESSAGE_SEND_SERVER_COMMAND");
                    connection.sendChat(event.message);
                    break;
            }
        }
    }
}
