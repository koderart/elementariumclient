package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.AppTheme;

import ru.elementarium.webclient.events.LoginEvent;
import ru.elementarium.webclient.events.PaymentEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;
BUILD::mobile {
    import ru.flashpress.uid.FPUniqueId;
}

import starling.utils.AssetManager;

    public class LoginCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;
        [Inject]
        public var event:LoginEvent;
        [Inject]
        public var mainModel:MainModel;

        protected var _assets : AssetManager;
    
        override public function execute():void
		{
            switch(event.type) {
                case LoginEvent.REGISTER_EMAIL:
                    console.log("LoginEvent.REGISTER_EMAIL");
                    connection.registerEmail(event.email, event.pass, event.name, event.sex, event.element, event.fraction);
                    break;
                case LoginEvent.LOGIN_EMAIL:
                    console.log("LoginEvent.LOGIN_EMAIL");
                    connection.loginEmail(event.email,event.pass, event.fraction);
                    break;
                case LoginEvent.LOGIN_MY_MAIL:
                    console.log("LoginEvent.LOGIN_MY_MAIL");
                    connection.loginMyMail(mainModel.socialMyMailData.vid, mainModel.socialMyMailData.str,
                            mainModel.socialMyMailData.sig, event.fraction);
                    break;
                case LoginEvent.REGISTER_MY_MAIL:
                    console.log("LoginEvent.REGISTER_MY_MAIL");
                    connection.registerMyMail(mainModel.socialMyMailData.vid, mainModel.socialMyMailData.str,
                            mainModel.socialMyMailData.sig, event.name, event.sex, event.element, event.fraction);
                    break;
                case LoginEvent.LOGIN_VK:
                    console.log("LoginEvent.LOGIN_VK");
                    connection.loginVK(mainModel.socialVkData.viewer_id, mainModel.socialVkData.auth_key, event.fraction);
                    break;
                case LoginEvent.REGISTER_VK:
                    console.log("LoginEvent.REGISTER_VK");
                    connection.registerVK(mainModel.socialVkData.viewer_id, mainModel.socialVkData.auth_key, event.name, event.sex, event.element, event.fraction);
                    break;
                case LoginEvent.LOGIN_APPLE:
                    console.log("LoginEvent.LOGIN_APPLE");
                    BUILD::mobile {
                        connection.loginApple(FPUniqueId.keychainId, event.fraction);
                    }
                    break;
                case LoginEvent.REGISTER_APPLE:
                    console.log("LoginEvent.REGISTER_APPLE");
                    BUILD::mobile {
                        connection.registerApple(FPUniqueId.keychainId, event.name, event.sex, event.element, event.fraction);
                    }
                    break;
                case LoginEvent.PLAYER_UNDEFINED:
                    console.log("LoginEvent.PLAYER_UNDEFINED");
                    //if (mainModel.socialVkData || mainModel.socialMyMailData) {

                    _assets = new AssetManager(MainModel.scaleFactor);

                    switch (MainModel.scaleFactor) {
                        case 1:
                            _assets.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/"+MainModel.assetsVersion+"/x1/registration.atf");
                            _assets.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/"+MainModel.assetsVersion+"/x1/registration.xml");
                            break;
                        case 2:
                            _assets.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/"+MainModel.assetsVersion+"/x2/registration.atf");
                            _assets.enqueue(ConnectionService.BASE_URL_ASSETS + "textures/"+MainModel.assetsVersion+"/x2/registration.xml");
                            break;
                    }

                    _assets.loadQueue(assetManager_onProgress1);
                    break;
                case LoginEvent.LOGIN_SUCCESS:
                    console.log("LoginEvent.LOGIN_SUCCESS");
                    dispatch(new PaymentEvent(PaymentEvent.PAYMENT_APPLE_DEFERRED_PURCHASE));
                    break;
            }
            
		}
    
        protected function assetManager_onProgress1(progress:Number):void
        {
            if (progress !== 1) {
                return;
            }
            dispatchWith(LoginEvent.REGISTER_LOADED, false, _assets);
        }

    }
}
