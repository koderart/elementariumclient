package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;
import ru.elementarium.webclient.events.AchieveEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class AchieveCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");

        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:AchieveEvent;

		override public function execute():void
		{
		    switch (event.type) {
                case AchieveEvent.ACHIEVE_LIST_SERVER_COMMAND:
                    console.log("AchieveEvent.ACHIEVE_LIST_SERVER_COMMAND");
                    connection.getAchieves();
                    break;
		    }
		}
    }
}
