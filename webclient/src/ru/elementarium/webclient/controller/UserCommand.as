package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.UserEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class UserCommand extends Command
    {
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:UserEvent;

        override public function execute():void
        {
            switch (event.type) {
                case UserEvent.MONEY_LIST_SERVER_COMMAND:
                    console.log("UserEvent.MONEY_LIST_SERVER_COMMAND");
                    connection.getMoney();
                    break;
                case UserEvent.SKILL_LIST_SERVER_COMMAND:
                    console.log("UserEvent.SKILL_LIST_SERVER_COMMAND");
                    connection.getSkill();
                    break;
                case UserEvent.MONEY_LIST_CLIENT_COMMAND:
                    console.log("UserEvent.MONEY_LIST_CLIENT_COMMAND");
                    connection.getSkill();
                    break;
            }
        }
    }
}
