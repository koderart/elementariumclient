package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;
import ru.elementarium.webclient.events.QuestEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class QuestCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:QuestEvent;

		override public function execute():void
		{
            switch (event.type) {
                case QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST:
                    console.log("QuestEvent.LOCATION_ACTION_SERVER_QUEST_LIST");
                    connection.getQuestList(event.member);
                    break;
                case QuestEvent.LOCATION_ACTION_SERVER_QUEST_TAKE:
                    console.log("QuestEvent.LOCATION_ACTION_SERVER_QUEST_TAKE");
                    connection.takeQuest(event.member,event.quest);
                    break;
                case QuestEvent.LOCATION_ACTION_SERVER_QUEST_GIVE:
                    console.log("QuestEvent.LOCATION_ACTION_SERVER_QUEST_GIVE");
                    connection.giveQuest(event.member,event.quest);
                    break;
                case QuestEvent.QUEST_REFUSE_SERVER_COMMAND:
                    console.log("QuestEvent.QUEST_REFUSE_SERVER_COMMAND");
                    connection.refuseQuest(event.quest);
                    break;
                case QuestEvent.QUEST_LIST_SERVER_COMMAND:
                    console.log("QuestEvent.QUEST_LIST_SERVER_COMMAND");
                    connection.getQuestAllList();
                    break;
            }
		}
    }
}
