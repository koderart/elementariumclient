package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.ChallengeEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class ChallengeCommand extends Command
    {
        private var console:ConsoleChannel = new ConsoleChannel("command");

        [Inject]
        public var connection:ConnectionService;
        [Inject]
        public var event:ChallengeEvent;

		override public function execute():void
		{
            switch (event.type) {
                case ChallengeEvent.CHALLENGE_LIST_SERVER_COMMAND:
                    console.log("ChallengeEvent.CHALLENGE_LIST_SERVER_COMMAND");
                    connection.getChallengeList();
                    break;
            }
		}
    }
}
