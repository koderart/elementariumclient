package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.StoreEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class StoreCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:StoreEvent;

		override public function execute():void
		{
            switch (event.type) {
                case StoreEvent.BUY:
                    console.log("StoreEvent.BUY");
                    connection.storeBuy(event.store,event.section, event.artikulId, event.count, event.moneyType);
                    break;
            }
		}
    }
}
