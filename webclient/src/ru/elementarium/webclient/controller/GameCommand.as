package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import flash.utils.clearTimeout;
import flash.utils.setTimeout;

import org.robotlegs.starling.mvcs.Command;
import ru.elementarium.webclient.events.GameEvent;
import ru.elementarium.webclient.model.MainModel;
import ru.elementarium.webclient.services.ConnectionService;

public class GameCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:GameEvent;

        [Inject]
        public var mainModel:MainModel;

		override public function execute():void
		{
            switch (event.type) {
                case GameEvent.ACTIVATE:
                    console.log("GameEvent.ACTIVATE");
                    clearTimeout(mainModel.timerDisconnect);
                    break;
                case GameEvent.DEACTIVATE:
                    console.log("GameEvent.DEACTIVATE");
                    connection.disconnect();
                    //mainModel.timerDisconnect = setTimeout(disconnect, 10000);
                    break;
            }
		}

		private function disconnect():void
        {
            if (connection.connected) {
                connection.disconnect();
            }
        }
    }
}
