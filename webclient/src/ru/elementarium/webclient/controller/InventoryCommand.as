package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.InventoryEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class InventoryCommand extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:InventoryEvent;

        override public function execute():void
		{
		    switch (event.type) {
                case InventoryEvent.SLOT_LIST_SERVER_COMMAND:
                    console.log("InventoryEvent.SLOT_LIST_SERVER_COMMAND");
                    connection.slotList();
                    break;
                case InventoryEvent.SLOT_LIST_CLIENT_COMMAND:
                    console.log("InventoryEvent.SLOT_LIST_CLIENT_COMMAND");
                    connection.bagList();
                    break;
                case InventoryEvent.BAG_LIST_SERVER_COMMAND:
                    console.log("InventoryEvent.BAG_LIST_SERVER_COMMAND");
                    connection.bagList();
                    break;
                case InventoryEvent.BAG_LIST_CLIENT_COMMAND:
                    console.log("InventoryEvent.BAG_LIST_CLIENT_COMMAND");
                    connection.beltList();
                    break;
                case InventoryEvent.BELT_LIST_SERVER_COMMAND:
                    console.log("InventoryEvent.BELT_LIST_SERVER_COMMAND");
                    connection.beltList();
                    break;
                case InventoryEvent.BAG_REMOVE_SERVER_COMMAND:
                    connection.bagRemove(event.artikul, event.count);
                    break;
                case InventoryEvent.BELT_TAKE_ON_SERVER_COMMAND:
                    connection.beltOn(event.artikulBelt);
                    break;
                case InventoryEvent.BELT_TAKE_OFF_SERVER_COMMAND:
                    connection.beltOff(event.artikulBelt);
                    break;
                case InventoryEvent.ARTIKUL_ACTION_SERVER_COMMAND:
                    connection.artikulUse(event.artikul);
                    break;
                case InventoryEvent.ARTIKUL_REPAIR_SERVER_COMMAND:
                    connection.artikulRepair(event.artikul, event.artikulMoney);
                    break;
                case InventoryEvent.ARTIKUL_SELL_SERVER_COMMAND:
                    connection.artikulSell(event.artikul, event.count);
                    break;
                case InventoryEvent.SLOT_TAKE_ON_SERVER_COMMAND:
                    connection.slotOn(event.artikulSlot);
                    break;
                case InventoryEvent.SLOT_TAKE_OFF_SERVER_COMMAND:
                    connection.slotOff(event.artikulSlot);
                    break;
		    }
		}
    }
}
