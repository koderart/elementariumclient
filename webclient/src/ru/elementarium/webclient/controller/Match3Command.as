package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.Match3Event;
import ru.elementarium.webclient.services.ConnectionService;

    public class Match3Command extends Command
	{
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:Match3Event;

		override public function execute():void
		{
		    switch (event.type) {
                case Match3Event.MOVE:
                    console.log("Match3Event.MOVE");
                    connection.moveGems(event.swapFirstCell,event.swapSecondCell, event.source, event.destination);
		            break;
                case Match3Event.USE_SPELL:
                    console.log("Match3Event.USE_SPELL");
                    connection.useSpell(event.idSpell, event.source, event.destination);
		            break;
                case Match3Event.USE_POTION:
                    console.log("Match3Event.USE_POTION");
                    connection.usePotion(event.artikulBelt, event.source, event.destination);
		            break;
		    }
		}

    }
}
