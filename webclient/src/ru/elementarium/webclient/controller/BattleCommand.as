package ru.elementarium.webclient.controller
{
import com.junkbyte.console.ConsoleChannel;

import org.robotlegs.starling.mvcs.Command;

import ru.elementarium.webclient.events.BattleEvent;
import ru.elementarium.webclient.services.ConnectionService;

    public class BattleCommand extends Command
    {
        private var console:ConsoleChannel = new ConsoleChannel("command");
        [Inject]
        public var connection:ConnectionService;

        [Inject]
        public var event:BattleEvent;

        override public function execute():void
        {
            switch (event.type) {
                case BattleEvent.BATTLE_JOIN_SERVER_COMMAND:
                    console.log("BattleEvent.BATTLE_JOIN_SERVER_COMMAND");
                    connection.startMob(event.id);
                    break;
                case BattleEvent.BATTLE_QUEUE_JOIN_SERVER_COMMAND:
                    console.log("BattleEvent.BATTLE_JOIN_SERVER_COMMAND");
                    connection.enterRoom(event.id);
                    break;
                case BattleEvent.BATTLE_QUEUE_UN_JOIN_SERVER_COMMAND:
                    console.log("BattleEvent.BATTLE_JOIN_SERVER_COMMAND");
                    connection.exitRoom();
                    break;
            }
        }
    }
}
