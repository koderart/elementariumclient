package
{
    import com.junkbyte.console.Cc;

    import flash.desktop.NativeApplication;
    import flash.display.DisplayObject;
    import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.UncaughtErrorEvent;
    import flash.filters.GlowFilter;
    import flash.geom.Rectangle;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
    import ru.elementarium.webclient.AppContext;
    import ru.elementarium.webclient.AppTheme;
    import ru.elementarium.webclient.model.MainModel;
    import ru.elementarium.webclient.view.main.AppView;
    import starling.core.Starling;

    public class App extends Sprite
	{

        [Embed(source="../html-template/background.jpg", mimeType="image/jpg")]
        public var backgroundImage:Class;

        private var _starling:Starling;
        private var _context:AppContext;
        private var _appTheme:AppTheme;
        private var _progressText:TextField;
        private var _progressError:Boolean;
        private var _background:DisplayObject;
        private var stageWidth:int;
        private var stageHeight:int;

        public function App()
        {
            if(this.stage)
            {
                this.stage.scaleMode = StageScaleMode.NO_SCALE;
                this.stage.align = StageAlign.TOP_LEFT;
            }
            this.mouseEnabled = this.mouseChildren = false;
            this.loaderInfo.addEventListener(Event.COMPLETE, loaderInfo_completeHandler);
            loaderInfo.uncaughtErrorEvents.addEventListener(UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtErrorHandler);

            Cc.startOnStage(this.stage, "adminlog");
            Cc.height = 300;
            Cc.config.commandLineAllowed = true;
            Cc.remoting = true;
            if (MainModel.isMobile) {
                Cc.config.style.big();
                Cc.config.style.updateStyleSheet();
            }
        }
    
        private function uncaughtErrorHandler(event:UncaughtErrorEvent):void
        {
            if (event.error is Error) {
                var error:Error = event.error as Error;
                Cc.error(error.getStackTrace());
            } else if (event.error is ErrorEvent) {
                var errorEvent:ErrorEvent = event.error as ErrorEvent;
                Cc.error(errorEvent.text);
            }
        }
    
        private function loaderInfo_completeHandler(event:Event):void
		{
            BUILD::mobile {
                stageWidth = stage.fullScreenWidth;
                stageHeight = stage.fullScreenHeight;
            }
            BUILD::release {
                stageWidth = stage.stageWidth;
                stageHeight = stage.stageHeight;
            }
            BUILD::test {
                stageWidth = stage.stageWidth;
                stageHeight = stage.stageHeight;
            }

            _background = new backgroundImage();
            _background.x = (stageWidth - _background.width)/2;
            _background.y = (stageHeight - _background.height)/2;
            addChild(_background);

            //Starling.handleLostContext = false;
			Starling.multitouchEnabled = false;

            stage.color = 0;
            
            var artSize: Rectangle = new Rectangle(0, 0, 1024, 576);//768
            var deviceSize:Rectangle =  new Rectangle(0, 0, stageWidth, stageHeight);
            artSize.height = deviceSize.height*artSize.width/deviceSize.width;
            var scaleX: Number = deviceSize.width/artSize.width;
            var newW: int = artSize.width*scaleX;
            var newH: int = artSize.height*scaleX;
            var viewPort:Rectangle = new Rectangle((deviceSize.width-newW)/2, (deviceSize.height-newH)/2, newW, newH);
            BUILD::mobile {
                _starling = new Starling(Main, stage, viewPort, null, "auto", "auto");
                _starling.antiAliasing = 0;
                _starling.stage.stageWidth = artSize.width;
                _starling.stage.stageHeight = artSize.height;

                NativeApplication.nativeApplication.addEventListener(
                        Event.ACTIVATE, function (e:*):void {
                            if (_appTheme && _progressError) {
                                _progressError = false;
                                _appTheme.downloadAppThemeAssets();
                            }
                        }
                );
            }
            BUILD::release {
                _starling = new Starling(Main, this.stage);
                this.stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
            }
            BUILD::test {
                _starling = new Starling(Main, this.stage);
                this.stage.addEventListener(Event.RESIZE, stage_resizeHandler, false, int.MAX_VALUE, true);
            }
			_starling.enableErrorChecking = false;
			_starling.skipUnchangedFrames = true;
            _starling.start();
            _starling.addEventListener("rootCreated", starling_rootCreatedHandler);
        }

        private function starling_rootCreatedHandler(event:Object):void
        {
            _appTheme = new AppTheme();
            _appTheme.addEventListener("complete", appTheme_completeHandler);
            _appTheme.addEventListener("progress", appTheme_progressHandler);
            _appTheme.addEventListener("ioError", appTheme_ioErrorHandler);
            
            var fontSize:int = 18;
            BUILD::mobile {
                fontSize = 30;
            }
            _progressText = new TextField();
            _progressText.defaultTextFormat = new TextFormat("_sans", fontSize, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.CENTER);
            _progressText.filters = [new GlowFilter(0x000000)];
            _progressText.width = 1000;
            _progressText.x = (stageWidth-1000)/2;
            _progressText.y = stageHeight/2 + 30;
            addChild(_progressText);
        }

        private var appView:AppView;
        
        private function appTheme_completeHandler(event:Object):void {
            this._context = new AppContext();
            this._context.contextView =this._starling.stage;
            appView = new AppView();
            this._starling.stage.addChild(appView);
            removeChild(_progressText);
            removeChild(_background);
        }

        private function appTheme_progressHandler(event:Object):void {
            _progressText.text = event.data as String;
        }

        private function appTheme_ioErrorHandler(event:Object):void {
            _progressText.text = "Ошибка загрузки. Проверьте интернет подключение.";
            _progressError = true;
        }

        private function stage_resizeHandler(event:Event):void
		{
			this._starling.stage.stageWidth = this.stage.stageWidth;
			this._starling.stage.stageHeight = this.stage.stageHeight;

            _background.x = (this.stage.stageWidth - _background.width)/2;
            _background.y = (this.stage.stageHeight - _background.height)/2;

            const viewPort:Rectangle = this._starling.viewPort;
			viewPort.width = this.stage.stageWidth;
			viewPort.height = this.stage.stageHeight;

            try
			{
				this._starling.viewPort = viewPort;
			}
			catch(error:Error) {}
			//this._starling.showStatsAt(HAlign.LEFT, VAlign.BOTTOM);
		}
    }
}