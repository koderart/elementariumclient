var flashInserted = false;

var network = {resolveCall:function (obj)
{
	trace ("resolving social api call");
	trace (obj);
	
	if (obj && obj.hasOwnProperty ("source")) 
	{
		
			var method = obj.method;
			var args = obj.args || {};
			args.test_mode = 1;
		
		if (obj.source == "api") 
		{
			
			VK.api(obj.method, obj.args, function() 
			{
				if (arguments && arguments.length > 0) 
				{
					args = arguments[0].response;
				} else 
				{
					args = null;
					trace ("api error: "+obj.method, "error");
				}
				sendToFlash ({source:"api", method:obj.method, args:args});
			});	
		} else if (obj.source == "external") 
		{
			VK.callMethod (obj.method, args, function () 
			{
				if (arguments && arguments.length > 0) 
				{
					args = arguments;
				} else 
				{
					args = null;
					trace ("external call error: "+obj.method, "error");
				}
				
				sendToFlash ({source:"external", method:obj.method, args:args});
			});
		}
	} 
}};

function setupApi () 
{
	// не работает 
	VK.addCallback ("onToggleFlash", function (show) {
		trace ("toggle flash");
	});
	
	
	VK.addCallback ("onWindowBlur", function () {
		trace ("lost focus");
		sendToFlash ({source:"external", method:"onWindowBlur"});
		hideFlash (true);
	});
	
	VK.addCallback ("onWindowFocus", function () {
		trace ("get focus");
		sendToFlash ({source:"external", method:"onWindowFocus"});
		if (state == 1) showFlash ();
        var $j = window.jQuery;
        if (state == 2) $j("#pageFrameContainer").css ("z-index", 1);
	});

    VK.addCallback ("onApplicationAdded", function () {
        trace ("onApplicationAdded");
        sendToFlash ({source:"external", method:"onApplicationAdded"});
    });

    VK.addCallback ("onSettingsChanged", function () {
        trace ("onSettingsChanged");
        sendToFlash ({source:"external", method:"onSettingsChanged"});
    });


    /*
     VK.api('account.getAppPermissions', {test_mode:1}, function(data) {

         var notifications = data.response & 1;
         var friends = data.response & 2;
         //var photos = data.response & 4;

           if  (!notifications || !friends )//|| !photos)
           {
               // data.response is object
               VK.callMethod("showSettingsBox", 1|2|4);

           } else
           {
               if (!flashInserted)
               {
                   flashInserted = true;
                   insertFlash ();
                   VK.callMethod ("showInstallBox");
               }
           }
     });
     */
	
	VK.callMethod ("setTitle", "“Элементариум” - бесплатная браузерная онлайн игра");
	
	flashInserted = true;
	insertFlash ("vk");
	
}


