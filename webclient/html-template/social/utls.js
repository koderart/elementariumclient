jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
}

jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
}

jQuery.fn.visibilityToggle = function() {
    return this.css('visibility', function(i, visibility) {
        return (visibility == 'visible') ? 'hidden' : 'visible';
    });
}

var getParams = {};


function parseGet () 
{
	
	var prmstr = window.location.search.substr(1);
	//var prmstr = "api_url=http://api.vk.com/api.php&api_id=3760406&api_settings=8455&viewer_id=10338660&viewer_type=2&sid=d2a760655824d79a460d035e3692db9052a79bc281c21498a6307f18c526c0eabac0107a4d33deee4613e&secret=1400860634&access_token=d2a92dcbf4e153c4738a3961a5681641b54f08aa734a47f5310d83107291e1b67bb972ce213db8e34cfed&user_id=10338660&group_id=0&is_app_user=1&auth_key=37eea5a786fefafe1b3f03227caaaa51&language=0&parent_language=0&ad_info=ElsdCQBcQlZsAwJbAwJSXHt4AUQ8HTJXUVBBJRVBNwoIFjI2HA8H&is_secure=0&ads_app_id=3760406_6330c746e53e6b7518&referrer=menu&lc_name=809c4550&hash="
	var prmarr = prmstr.split ("&");
	
	
	for ( var i = 0; i < prmarr.length; i++) 
	{
    	var tmparr = prmarr[i].split("=");
    	getParams[tmparr[0]] = tmparr[1];
    	trace (tmparr[0]+" = "+tmparr[1]);
	}
}

function trace (s, level) 
{
	if (typeof console === "undefined") 
	{
		//alert (s);
		
	} else 
	{
		switch (level)
		{
			case 'log':
				if (console.log !== undefined) console.log (s);
			break;
			case 'debug':
				if (console.debug !== undefined) 
						console.debug (s);
					else
						console.log ("DEBUG: "+s);
			break;
			case 'info':
				if (console.info !== undefined) 
						console.info (s);
					else
						console.log ("INFO: "+s);
			break;
			case 'warn':
				if (console.warn !== undefined) 
						console.warn (s);
					else
						console.log ("WARN: "+s);
			break;
			case 'error':
				if (console.error !== undefined) 
						console.error (s);
					else
						console.log ("ERROR: "+s);
			break;
			default:
			 console.log (s);
			break;
			
		}
	}
}

var jsReady = false;

function isReady () 
{
	return jsReady;
}

var _flashReady = false;

function flashReady () 
{
	return _flashReady;
}



function getFlash () {
	var $j = window.jQuery;
	return $j("#webclient")[0];
}

function toggleFlash () 
{
	var $j = window.jQuery;
	$j("#webclient").visibilityToggle ();
}

function showFlash () 
{
	var $j = window.jQuery;
	$j("#webclient").visible ();
}

function hideFlash (fs)
{
    fs = typeof fs !== 'undefined' ? fs : true;
	var $j = window.jQuery;
	$j("#webclient").invisible ();
	if (fs) exitFSMode ();
}


function sendToFlash (value) 
{
	trace ("send to flash");
	trace (value);
	var flash = getFlash ();
	if (flash != null) flash.getJsData (value);
}

function getFromFlash (data)
{
	if (network && network.hasOwnProperty ("resolveCall")) 
	{
		network.resolveCall (data);
	}
}

function reloadPage () 
{
	location.reload ();
}

function exitFSMode () 
{
	var fs = window.fullScreenApi.isFullScreen ();
	if (fs) window.fullScreenApi.toggleFullScreen ();
}

function getUrlVars()
{
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}



function insertFlash (mode)
{
	var swfVersionStr = "13.0.0";
	var xiSwfUrlStr = "expressInstall.swf";

	var flashvars = {};
    if (mode == "vk") {
        flashvars.query =  encodeURIComponent (JSON.stringify(getUrlVars ()));
        flashvars.vk = encodeURIComponent (JSON.stringify (getParams));
    } else if (mode == "mymail") {
        flashvars.mailru = true;
    }

	var params = {};
	params.quality = "high";
	params.bgcolor = "#383838";
	params.allowscriptaccess = "sameDomain";
	params.allowFullScreenInteractive = "true";
	params.wmode = "direct";

	var attributes = {};
	attributes.id = "webclient";
	attributes.name = "webclient";
	attributes.align = "middle";
	attributes.wmode = "direct";

	swfobject.embedSWF(
		"webclient.swf?1.1.1", "flashContent",
		"100%", "100%",
		swfVersionStr, xiSwfUrlStr,
		flashvars, params, attributes);
	// JavaScript enabled so display the flashContent div in case it is not replaced with a swf object.
	swfobject.createCSS("#flashContent", "display:block;text-align:left;");

}
