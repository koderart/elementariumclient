package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.MoneyEnum;

[Bindable]
	public class SocialPaymentVkVO
	{
		public var bankVkItemId:int;
		public var itemType:String = MoneyEnum.GOLD;
		public var itemId:String;
		public var title:LocaleVO;
		public var photoUrl:ImageVO;
        public var price:int;
        public var giveCount:int;
        public var giveBonusPrc:int;
        public var giveBonusCount:int;

		public function SocialPaymentVkVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                photoUrl = new ImageVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            bankVkItemId = obj.bankVkItemId;
            itemType = obj.itemType;
            itemId = obj.itemId;
            title = new LocaleVO(obj.title);
            photoUrl = new ImageVO(obj.photoUrl);
            price = obj.price;
            giveCount = obj.giveCount;
            giveBonusPrc = obj.giveBonusPrc;
            giveBonusCount = obj.giveBonusCount;
        }
        
        public function clone():SocialPaymentVkVO
        {
            var result:SocialPaymentVkVO = new SocialPaymentVkVO();
            result.bankVkItemId = this.bankVkItemId;
            result.itemType = this.itemType;
            result.itemId = this.itemId;
            result.title = this.title.clone();
            result.photoUrl = this.photoUrl.clone();
            result.price = this.price;
            result.giveCount = this.giveCount;
            result.giveBonusPrc = this.giveBonusPrc;
            result.giveBonusCount = this.giveBonusCount;

            return result;
        }
	}
}