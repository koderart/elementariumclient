package ru.muvs.admin.main.vo
{
[Bindable]
public class LocationPassableHexVO
{
    public var x:int;
    public var y:int;
    public var unsafe:Boolean;


    public function LocationPassableHexVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
        }
    }

    public function parseObject(obj:Object):void
    {
        x = obj.x;
        y = obj.y;
        unsafe = obj.unsafe;
    }

    public function clone():LocationPassableHexVO
    {
        var result:LocationPassableHexVO = new LocationPassableHexVO();
        result.x = this.x;
        result.y = this.y;
        result.unsafe = this.unsafe;
        return result;
    }
}
}