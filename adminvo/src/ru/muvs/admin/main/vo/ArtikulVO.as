package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.battle.PotionVO;
import ru.muvs.admin.main.vo.enum.*;

[Bindable]
	public class ArtikulVO
	{
		public var artikulId:int;
		public var title:LocaleVO;
		public var description:LocaleVO;
		public var artikulType:ArtikulTypeVO;
		public var artikulColor:String = ColorEnum.NONE;
		public var image:ImageVO;
		public var levelMin:LevelVO;
		public var levelMax:LevelVO;
        public var stack:int;
        public var stackBattle:int;
        public var durabilityCurrent:int;
        public var durability:int;
        public var weightSkillRandom:int;
        public var weighty:Boolean;
        public var belty:Boolean;
        public var passed:Boolean;
        public var nominal:Boolean;
        public var remove:Boolean = true;
        public var sell:Boolean = true;
        public var repair:Boolean = true;
        public var action:BonusVO;
        public var potion:PotionVO;
        public var artikulSlot:Vector.<ArtikulSlotVO>;
        public var artikulMoney:Vector.<MoneyVO>;
        public var artikulEquals:Vector.<ArtikulEqualsVO>;
        public var artikulSkillBase:Vector.<ArtikulSkillBaseVO>;
        public var artikulSkillRandom:Vector.<ArtikulSkillRandomVO>;

		public function ArtikulVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                artikulType = new ArtikulTypeVO();
                image = new ImageVO();
                levelMin = new LevelVO();
                levelMax = new LevelVO();
                action = new BonusVO();
                potion = new PotionVO();
                artikulSlot = new Vector.<ArtikulSlotVO>();
                artikulMoney = new Vector.<MoneyVO>();
                artikulEquals = new Vector.<ArtikulEqualsVO>();
                artikulSkillBase = new Vector.<ArtikulSkillBaseVO>();
                artikulSkillRandom = new Vector.<ArtikulSkillRandomVO>();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            var o:Object;
            artikulId = obj.artikulId;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            artikulType = new ArtikulTypeVO(obj.artikulType);
            artikulColor = obj.artikulColor;
            image = new ImageVO(obj.image);
            levelMin = new LevelVO(obj.levelMin);
            levelMax = new LevelVO(obj.levelMax);
            stack = obj.stack;
            stackBattle = obj.stackBattle;
            durabilityCurrent = obj.durabilityCurrent;
            durability = obj.durability;
            weightSkillRandom = obj.weightSkillRandom;
            weighty = obj.weighty;
            belty = obj.belty;
            passed = obj.passed;
            nominal = obj.nominal;
            remove = obj.remove;
            sell = obj.sell;
            repair = obj.repair;
            action = new BonusVO(obj.action);
            potion = new PotionVO(obj.potion);
            artikulSlot = new Vector.<ArtikulSlotVO>();
            for each(o in obj.artikulSlot) {
                artikulSlot.push(new ArtikulSlotVO(o));
            }
            artikulMoney = new Vector.<MoneyVO>();
            for each(o in obj.artikulMoney) {
                artikulMoney.push(new MoneyVO(o));
            }
            artikulEquals = new Vector.<ArtikulEqualsVO>();
            for each(o in obj.artikulEquals) {
                artikulEquals.push(new ArtikulEqualsVO(o));
            }
            artikulSkillBase = new Vector.<ArtikulSkillBaseVO>();
            for each(o in obj.artikulSkillBase) {
                artikulSkillBase.push(new ArtikulSkillBaseVO(o));
            }
            artikulSkillRandom = new Vector.<ArtikulSkillRandomVO>();
            for each(o in obj.artikulSkillRandom) {
                artikulSkillRandom.push(new ArtikulSkillRandomVO(o));
            }
        }
        
        public function clone():ArtikulVO
        {
            var result:ArtikulVO = new ArtikulVO();
            result.artikulId = this.artikulId;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.artikulType = this.artikulType.clone();
            result.artikulColor = this.artikulColor;
            result.image = this.image.clone();
            result.levelMin = this.levelMin.clone();
            result.levelMax = this.levelMax.clone();
            result.stack = this.stack;
            result.stackBattle = this.stackBattle;
            result.durabilityCurrent = this.durabilityCurrent;
            result.durability = this.durability;
            result.weightSkillRandom = this.weightSkillRandom;
            result.weighty = this.weighty;
            result.belty = this.belty;
            result.passed = this.passed;
            result.nominal = this.nominal;
            result.remove = this.remove;
            result.sell = this.sell;
            result.repair = this.repair;
            result.action = this.action.clone();
            result.potion = this.potion.clone();

            result.artikulSlot = new Vector.<ArtikulSlotVO>();
            for each(var slot:ArtikulSlotVO in this.artikulSlot) {
                result.artikulSlot.push(slot.clone());
            }
            result.artikulMoney = new Vector.<MoneyVO>();
            for each(var money:MoneyVO in this.artikulMoney) {
                result.artikulMoney.push(money.clone());
            }
            result.artikulEquals = new Vector.<ArtikulEqualsVO>();
            for each(var artikul:ArtikulEqualsVO in this.artikulEquals) {
                result.artikulEquals.push(artikul.clone());
            }
            result.artikulSkillBase = new Vector.<ArtikulSkillBaseVO>();
            for each(var skill:ArtikulSkillBaseVO in this.artikulSkillBase) {
                result.artikulSkillBase.push(skill.clone());
            }
            result.artikulSkillRandom = new Vector.<ArtikulSkillRandomVO>();
            for each(var skillRandom:ArtikulSkillRandomVO in this.artikulSkillRandom) {
                result.artikulSkillRandom.push(skillRandom.clone());
            }

            return result;
        }
	}
}