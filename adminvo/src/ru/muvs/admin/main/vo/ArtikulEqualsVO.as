package ru.muvs.admin.main.vo
{
    [Bindable]
	public class ArtikulEqualsVO
	{
		public var artikulEqualId:int;
		public var artikul:int;
		public var equalArtikul:int;
		public var equalArtikulCount:int;

		public function ArtikulEqualsVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            artikulEqualId = obj.artikulEqualId;
            artikul = obj.artikul;
            equalArtikul = obj.equalArtikul;
        }
        
        public function clone():ArtikulEqualsVO
        {
            var result:ArtikulEqualsVO = new ArtikulEqualsVO();
            result.artikulEqualId = this.artikulEqualId;
            result.artikul = this.artikul;
            result.equalArtikul = this.equalArtikul;
            result.equalArtikulCount = this.equalArtikulCount;
            
            return result;
        }
	}
}