package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.MoneyEnum;

[Bindable]
	public class SocialPaymentMailVO
	{
		public var bankMyMailServiceId:int;
		public var serviceType:String = MoneyEnum.GOLD;
		public var serviceId:String; //to int
		public var serviceName:LocaleVO;
        public var price:int;
        public var giveCount:int;
        public var giveBonusPrc:int;
        public var giveBonusCount:int;

		public function SocialPaymentMailVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                serviceName = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            bankMyMailServiceId = obj.bankMyMailServiceId;
            serviceType = obj.serviceType;
            serviceId = obj.serviceId;
            serviceName = new LocaleVO(obj.serviceName);
            price = obj.price;
            giveCount = obj.giveCount;
            giveBonusPrc = obj.giveBonusPrc;
            giveBonusCount = obj.giveBonusCount;
        }
        
        public function clone():SocialPaymentMailVO
        {
            var result:SocialPaymentMailVO = new SocialPaymentMailVO();
            result.bankMyMailServiceId = this.bankMyMailServiceId;
            result.serviceType = this.serviceType;
            result.serviceId = this.serviceId;
            result.serviceName = this.serviceName.clone();
            result.price = this.price;
            result.giveCount = this.giveCount;
            result.giveBonusPrc = this.giveBonusPrc;
            result.giveBonusCount = this.giveBonusCount;

            return result;
        }
	}
}