package ru.muvs.admin.main.vo
{
[Bindable]
	public class ChallengeLockSkillVO
	{
        public var challengeLockId:int;
		public var LOCK:String;
		public var challengeLock:String;
		public var skill:SkillVO;
		public var value:int;

		public function ChallengeLockSkillVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeLockId = obj.challengeLockId;
            LOCK = obj.LOCK;
            challengeLock = obj.challengeLock;
            skill = new SkillVO(obj.skill);
            value = obj.value;
        }

        public function clone():ChallengeLockSkillVO
        {
            var result:ChallengeLockSkillVO = new ChallengeLockSkillVO();
            result.challengeLockId = this.challengeLockId;
            result.LOCK = this.LOCK;
            result.challengeLock = this.challengeLock;
            result.skill = this.skill.clone();
            result.value = this.value;
            
            return result;
        }
	}
}