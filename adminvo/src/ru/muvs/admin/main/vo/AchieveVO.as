package ru.muvs.admin.main.vo
{

[Bindable]
public class AchieveVO
{
    public var achieveId:int;
    public var title:LocaleVO;
    public var description:LocaleVO;
    public var image:ImageVO;
    public var bonus:BonusVO;
    public var achieveSkill:Array;
    public var achieveGoal:Array;
    public var achieveLock:Array;

    public function AchieveVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            title = new LocaleVO();
            description = new LocaleVO();
            image = new ImageVO();
            bonus = new BonusVO();
            achieveGoal = [];
            achieveLock = [];
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        achieveId = obj.achieveId;
        title = new LocaleVO(obj.title);
        description = new LocaleVO(obj.description);
        image = new ImageVO(obj.image);
        bonus = new BonusVO(obj.bonus);
        achieveSkill = [];
        for each(o in obj.achieveSkill) {
            achieveSkill.push(new AchieveSkillVO(o));
        }

        achieveGoal = [];
        for each(o in obj.achieveGoal) {
            achieveGoal.push(new AchieveGoalVO(o));
        }

        achieveLock = [];
        for each(o in obj.achieveLock) {
            achieveLock.push(new AchieveLockVO(o));
        }
    }

    public function clone():AchieveVO
    {
        var result:AchieveVO = new AchieveVO();
        result.achieveId = this.achieveId;
        result.title = this.title.clone();
        result.description = this.description.clone();
        result.image = this.image.clone();
        result.bonus = this.bonus.clone();

        result.achieveSkill = [];
        for each(var skill:AchieveSkillVO in this.achieveSkill) {
            result.achieveSkill.push(skill.clone());
        }
        result.achieveGoal = [];
        for each(var goal:AchieveGoalVO in this.achieveGoal) {
            result.achieveGoal.push(goal.clone());
        }
        result.achieveLock = [];
        for each(var lock:AchieveLockVO in this.achieveLock) {
            result.achieveLock.push(lock.clone());
        }
        return result;
    }
}
}