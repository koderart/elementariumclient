package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.LevelVO;
import ru.muvs.admin.main.vo.enum.FractionEnum;

[Bindable]
	public class BonusLockVO
	{
        public var bonusLockId:int;
		
        public var level:LevelVO;
        public var quest:int;
        public var element:String;
        public var artikul:int;
        public var fraction:String = FractionEnum.NONE;
        public var rank:RankVO;
    
        // for table
        public var questTitle:String;
        public var artikulTitle:String;

        private var _lock:String;
        public function get LOCK():String {
            return _lock;
        }
        public function set LOCK(value:String):void {
            _lock = value;
        }
        public function get bonusLock():String {
            return _lock;
        }
        public function set bonusLock(value:String):void {
            _lock = value;
        }

        public function BonusLockVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                level = new LevelVO();
                rank = new RankVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            bonusLockId = obj.bonusLockId;
            LOCK = obj.LOCK;
            bonusLock = obj.bonusLock;
            level = new LevelVO(obj.level);
            quest = obj.quest;
            element = obj.element;
            artikul = obj.artikul;
            fraction = obj.fraction;
            rank = new RankVO(obj.rank);
        }

        public function clone():BonusLockVO
        {
            var result:BonusLockVO = new BonusLockVO();
            result.bonusLockId = this.bonusLockId;
            result.LOCK = this.LOCK;
            result.bonusLock = this.bonusLock;
            result.level = this.level.clone();
            result.quest = this.quest;
            result.element = this.element;
            result.artikul = this.artikul;
            result.fraction = this.fraction;
            result.rank = this.rank.clone();
            
            return result;
        }
	}
}