package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.ArenaEnum;

[Bindable]
	public class ArenaVO
	{
		public var arenaId:int;
		public var arena:String = ArenaEnum.PVP;
		public var bonusWin:BonusVO;
		public var bonusLose:BonusVO;

        public function ArenaVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            arenaId = obj.arenaId;
            arena = obj.arena;
            if (obj.bonusWin) {bonusWin = new BonusVO(obj.bonusWin);}
            if (obj.bonusLose) {bonusLose = new BonusVO(obj.bonusLose);}
        }
        
        public function clone():ArenaVO
        {
            var result:ArenaVO = new ArenaVO();
            result.arenaId = this.arenaId;
            result.arena = this.arena;
            if (this.bonusWin) {result.bonusWin = this.bonusWin.clone();}
            if (this.bonusLose) {result.bonusLose = this.bonusLose.clone();}
            return result;
        }
	}
}