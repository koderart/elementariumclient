package ru.muvs.admin.main.vo
{

[Bindable]
public class LocationImageVO
{
    public var locationImageId:int;
    public var positionX:int;
    public var positionY:int;
    public var image:ImageVO;

    public function LocationImageVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            image = new ImageVO();
        }
    }

    public function parseObject(obj:Object):void
    {
        locationImageId = obj.locationImageId;
        positionX = obj.positionX;
        positionY = obj.positionY;
        image = new ImageVO(obj.image);
    }

    public function clone():LocationImageVO
    {
        var result:LocationImageVO = new LocationImageVO();
        result.locationImageId = this.locationImageId;
        result.positionX = this.positionX;
        result.positionY = this.positionY;
        result.image = this.image.clone();
        return result;
    }
}
}