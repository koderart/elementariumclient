package ru.muvs.admin.main.vo
{

[Bindable]
	public class ImageVO
	{
		public var imageId:int;
		public var label:String;
		public var url:String;

		public function ImageVO(obj:Object=null)
		{
		    //Cc.error("ImageVO", obj.url)
            if (obj) {
                parseObject(obj);
            }
        }

        public function parseObject(obj:Object):void
        {
            imageId = obj.imageId;
            label = obj.label;
            url = obj.url;
        }

        public function clone():ImageVO
        {
            var result:ImageVO = new ImageVO();
            result.imageId = this.imageId;
            result.label = this.label;
            result.url = this.url;
            
            return result;
        }
	}
}