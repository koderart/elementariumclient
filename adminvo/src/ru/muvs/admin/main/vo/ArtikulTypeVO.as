package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.BagTabEnum;

[Bindable]
	public class ArtikulTypeVO
	{
		public var artikulTypeId:int;
		public var title:LocaleVO;
		public var artikulTypeBagTab:String = BagTabEnum.MAIN;

		public function ArtikulTypeVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            artikulTypeId = obj.artikulTypeId;
            artikulTypeBagTab = obj.artikulTypeBagTab;
            title = new LocaleVO(obj.title);
        }
        
        public function clone():ArtikulTypeVO
        {
            var result:ArtikulTypeVO = new ArtikulTypeVO();
            result.artikulTypeId = this.artikulTypeId;
            result.artikulTypeBagTab = this.artikulTypeBagTab;
            result.title = this.title.clone();
            
            return result;
        }
	}
}