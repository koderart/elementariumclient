package ru.muvs.admin.main.vo
{

[Bindable]
	public class AchieveSkillVO
	{
		public var achieveSkillId:int;
		public var skill:SkillVO;
		public var value:int;

		public function AchieveSkillVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            achieveSkillId = obj.achieveSkillId;
            skill = new SkillVO(obj.skill);
            value = obj.value;
        }

        public function clone():AchieveSkillVO
        {
            var result:AchieveSkillVO = new AchieveSkillVO();
            result.achieveSkillId = this.achieveSkillId;
            result.skill = this.skill.clone();
            result.value = this.value;
            
            return result;
        }
	}
}