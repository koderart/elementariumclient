package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionEnum {

    public static const TAKE_STEP:String = "TAKE_STEP";
    public static const GIVE_STEP:String = "GIVE_STEP";
    public static const TAKE_POINT:String = "TAKE_POINT";
    public static const GIVE_POINT:String = "GIVE_POINT";
    public static const TAKE_BELT:String = "TAKE_BELT";
    public static const GIVE_BELT:String = "GIVE_BELT";
    public static const RUN_CELL:String = "RUN_CELL";
    public static const SWAP_CELL:String = "SWAP_CELL";
    public static const REMOVE_CELL:String = "REMOVE_CELL";
    public static const REMOVE_CELL_TYPE:String = "REMOVE_CELL_TYPE";
    public static const REPLACE_CELL:String = "REPLACE_CELL";
    public static const REPLACE_CELL_TYPE:String = "REPLACE_CELL_TYPE";
    public static const REMOVE_CELL_ALL:String = "REMOVE_CELL_ALL";
    public static const REMOVE_CELL_LINE:String = "REMOVE_CELL_LINE";
    public static const REMOVE_CELL_FORM:String = "REMOVE_CELL_FORM";
    public static const TAKE_POSITIVE_BUFF:String = "TAKE_POSITIVE_BUFF";
    public static const TAKE_NEGATIVE_BUFF:String = "TAKE_NEGATIVE_BUFF";
    public static const TAKE_NEUTRAL_BUFF:String = "TAKE_NEUTRAL_BUFF";
    public static const GIVE_BUFF:String = "GIVE_BUFF";
    public static const DAMAGE:String = "DAMAGE";
    public static const DAMAGE_OVER_PROTECTION:String = "DAMAGE_OVER_PROTECTION";
    public static const HEAL:String = "HEAL";
    public static const RESURRECTION:String = "RESURRECTION";
    public static const TAKE_POINT_PERCENT:String = "TAKE_POINT_PERCENT";
    public static const GIVE_POINT_PERCENT:String = "GIVE_POINT_PERCENT";
    public static const FORK:String = "FORK";
    public static const CALL:String = "CALL";

    public static const data:Array= [  TAKE_STEP, GIVE_STEP, TAKE_POINT,
        GIVE_POINT, TAKE_BELT, GIVE_BELT, RUN_CELL,SWAP_CELL, REMOVE_CELL, REMOVE_CELL_TYPE, REPLACE_CELL,
        REPLACE_CELL_TYPE, REMOVE_CELL_ALL, REMOVE_CELL_LINE, REMOVE_CELL_FORM, TAKE_POSITIVE_BUFF,
        TAKE_NEGATIVE_BUFF, TAKE_NEUTRAL_BUFF, GIVE_BUFF, DAMAGE, DAMAGE_OVER_PROTECTION, HEAL, RESURRECTION, 
        TAKE_POINT_PERCENT, GIVE_POINT_PERCENT, FORK, CALL];
    public static const LABELS:Object = {
        TAKE_STEP:"Забрать ход",
        GIVE_STEP:"Дать ход",
        TAKE_POINT:"Забрать ману",
        GIVE_POINT:"Дать ману",
        TAKE_BELT:"Забрать из пояса",
        GIVE_BELT:"Дать в пояс",
        RUN_CELL:"Проиграть поле",
        SWAP_CELL:"Поменять камень",
        REMOVE_CELL:"Удалить камень",
        REMOVE_CELL_TYPE:"Удалить камни типа",
        REPLACE_CELL:"Заменить камень",
        REPLACE_CELL_TYPE:"Заменить камни типа",
        REMOVE_CELL_ALL:"Удалить все камни",
        REMOVE_CELL_LINE:"Удалить линию камней",
        REMOVE_CELL_FORM:"Удалить область камней",
        TAKE_POSITIVE_BUFF:"Забрать положительный баф",
        TAKE_NEGATIVE_BUFF:"Забрать отрицательный баф",
        TAKE_NEUTRAL_BUFF:"Забрать нейтральный баф",
        GIVE_BUFF:"Выдать баф",
        DAMAGE:"Урон",
        DAMAGE_OVER_PROTECTION:"Урон поверх защиты",
        HEAL:"Лечение",
        RESURRECTION:"Воскрешение",
        TAKE_POINT_PERCENT:"Забрать ману в процентах",
        GIVE_POINT_PERCENT:"Дать ману в процентах",
        FORK:"Ветка",
        CALL:"Вызвать"
    };
}
}
