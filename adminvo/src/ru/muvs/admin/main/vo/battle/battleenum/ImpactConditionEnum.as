package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactConditionEnum {

    public static const TRUE:String = "TRUE";
    public static const PROBABILITY:String = "PROBABILITY";
    public static const CONJUNCTION:String = "CONJUNCTION";
    public static const DISJUNCTION:String = "DISJUNCTION";
    public static const HAVE_POINT:String = "HAVE_POINT";
    public static const HAVE_SLOT:String = "HAVE_SLOT";
    public static const HAVE_BELT:String = "HAVE_BELT";
    public static const HAVE_CELL:String = "HAVE_CELL";

    public static const data:Array= [   TRUE, PROBABILITY, CONJUNCTION, DISJUNCTION,
        HAVE_POINT, HAVE_SLOT, HAVE_BELT, HAVE_CELL];
    public static const LABELS:Object = {   TRUE:"Истина",
                                            PROBABILITY:"Вероятность",
                                            CONJUNCTION:"И",
                                            DISJUNCTION:"ИЛИ",
                                            HAVE_POINT:"Наличие маны",
                                            HAVE_SLOT:"Наличие слота",
                                            HAVE_BELT:"Наличие пояса",
                                            HAVE_CELL:"Наличие камней"
    };
}
}
