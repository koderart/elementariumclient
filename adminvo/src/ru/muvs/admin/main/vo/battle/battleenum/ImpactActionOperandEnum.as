package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionOperandEnum {

    public static const VALUE:String = "VALUE";
    public static const RANGE:String = "RANGE";
    public static const OPERATOR:String = "OPERATOR";
    public static const AREA_CELL:String = "AREA_CELL";
    public static const SKILL_SELF:String = "SKILL_SELF";
    public static const SKILL_ENEMY:String = "SKILL_ENEMY";
    public static const POINT_SELF:String = "POINT_SELF";
    public static const POINT_ENEMY:String = "POINT_ENEMY";
    public static const COUNT_ALLIES:String = "COUNT_ALLIES";
    public static const COUNT_ENEMIES:String = "COUNT_ENEMIES";

    public static const data:Array= [   VALUE, RANGE, OPERATOR, AREA_CELL,
                                        SKILL_SELF, SKILL_ENEMY, POINT_SELF, POINT_ENEMY,
                                        COUNT_ALLIES, COUNT_ENEMIES];
    public static const LABELS:Object = {   VALUE:"Значение",
                                            RANGE:"Диапазон",
                                            OPERATOR:"Оператор",
                                            AREA_CELL:"Количество камней на поле",
                                            SKILL_SELF:"Своя характеристика",
                                            SKILL_ENEMY:"Характеристика врага",
                                            POINT_SELF:"Своя мана",
                                            POINT_ENEMY:"Мана врага",
                                            COUNT_ALLIES:"Количество союзников",
                                            COUNT_ENEMIES:"Количество противников"
    };
}
}
