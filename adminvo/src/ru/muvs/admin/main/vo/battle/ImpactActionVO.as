package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class ImpactActionVO
	{
		public var battleImpactActionId:int;
        public var actionTarget:ImpactActionTargetVO;
        public var next:ImpactActionVO;

        // TAKE_POINT
        // GIVE_POINT
        public var point:String; //[NONE, DARK, FIRE, LIGHT, WATER, NATURE]
        
        // TAKE_BELT
        // GIVE_BELT
        public var artikul:ArtikulVO;

        // REMOVE_CELL_TYPE
        public var type:String //[NONE, DARK, FIRE, LIGHT, WATER, NATURE],

        // REPLACE_CELL
        // REPLACE_CELL_TYPE
        public var source:String; //[NONE, DARK, FIRE, LIGHT, WATER, NATURE],
        public var destination:String; //[NONE, DARK, FIRE, LIGHT, WATER, NATURE],

        // REMOVE_CELL_FORM
        public var width:int;
        public var height:int;

        // GIVE_BUFF
        public var buff:BuffVO;
        
        // DAMAGE
        public var damage:String; //[DAMAGE, DAMAGE_DARK, DAMAGE_FIRE, DAMAGE_LIGHT, DAMAGE_WATER, DAMAGE_NATURE],
    
        // HEAL
        public var heal:String; //[HEAL, HEAL_DARK, HEAL_FIRE, HEAL_LIGHT, HEAL_WATER, HEAL_NATURE],

        // RESURRECTION
        public var probability:int;
        public var health:int;

        // TAKE_POINT_PERCENT
        // GIVE_POINT_PERCENT
        public var percent:int;

        public var actionOperator: ImpactActionOperatorVO;

        public var count: int;

        // FORK
        public var teeth:Vector.<ImpactActionToohtVO>;

        // CALL
        public var mob:MobVO;
    
        private var _action:String;
        public function get ACTION():String {
            return _action;
        }
        public function set ACTION(value:String):void {
            _action = value;
        }
        public function get impactAction():String {
            return _action;
        }
        public function set impactAction(value:String):void {
            _action = value;
        }

        public function ImpactActionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                actionTarget = new ImpactActionTargetVO();
                //next = new BattleImpactActionVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactActionId = obj.battleImpactActionId;
            impactAction = obj.impactAction;
            actionTarget = new ImpactActionTargetVO(obj.actionTarget);
            if (obj.next) {next = new ImpactActionVO(obj.next);}
            point = obj.point;
            if (obj.artikul) {artikul = new ArtikulVO(obj.artikul);}
            type = obj.type;
            source = obj.source;
            destination = obj.destination;
            width = obj.width;
            height = obj.height;
            if (obj.buff) {buff = new BuffVO(obj.buff);}
            damage = obj.damage;
            heal = obj.heal;
            if (obj.actionOperator) {actionOperator = new ImpactActionOperatorVO(obj.actionOperator);}
            count = obj.count;
            probability = obj.probability;
            health = obj.health;
            percent = obj.percent;
            teeth = new Vector.<ImpactActionToohtVO>();
            for each(var o:Object in obj.teeth) {
                teeth.push(new ImpactActionToohtVO(o));
            }
            if (obj.mob) {mob = new MobVO(obj.mob);}
        }
        
        public function clone():ImpactActionVO
        {
            var result:ImpactActionVO = new ImpactActionVO();
            result.battleImpactActionId = this.battleImpactActionId;
            result.impactAction = this.impactAction;
            result.actionTarget = this.actionTarget.clone();
            if (this.next) {result.next = this.next.clone();}
            result.point = this.point;
            if(this.artikul) {result.artikul = this.artikul.clone();}
            result.type = this.type;
            result.source = this.source;
            result.destination = this.destination;
            result.width = this.width;
            result.height = this.height;
            if (this.buff) {result.buff = this.buff.clone();}
            result.damage = this.damage;
            result.heal = this.heal;
            if(this.actionOperator) {result.actionOperator = this.actionOperator.clone();}
            result.count = this.count;
            result.probability = this.probability;
            result.health = this.health;
            result.percent = this.percent;
            result.teeth = new Vector.<ImpactActionToohtVO>();
            for each(var item:ImpactActionToohtVO in this.teeth) {
                result.teeth.push(item.clone());
            }
            if (this.mob) result.mob = this.mob.clone();
            
            return result;
        }
	}
}