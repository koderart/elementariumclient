package ru.muvs.admin.main.vo.battle
{
[Bindable]
	public class ImpactActionOperatorVO
	{
        public var battleImpactActionOperatorId:int;
        public var firstOperand:ImpactActionOperandVO;
        public var secondOperand:ImpactActionOperandVO;

        private var _operator:String;
        public function get OPERATOR():String {
            return _operator;
        }
        public function set OPERATOR(value:String):void {
            _operator = value;
        }
        public function get actionOperator():String {
            return _operator;
        }
        public function set actionOperator(value:String):void {
            _operator = value;
        }

        public function ImpactActionOperatorVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                firstOperand = new ImpactActionOperandVO();
                secondOperand = new ImpactActionOperandVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactActionOperatorId = obj.battleImpactActionOperatorId;
            actionOperator = obj.actionOperator;
            firstOperand = new ImpactActionOperandVO(obj.firstOperand);
            secondOperand = new ImpactActionOperandVO(obj.secondOperand);
        }
        
        public function clone():ImpactActionOperatorVO
        {
            var result:ImpactActionOperatorVO = new ImpactActionOperatorVO();
            result.battleImpactActionOperatorId = this.battleImpactActionOperatorId;
            result.actionOperator = this.actionOperator;
            result.firstOperand = this.firstOperand.clone();
            result.secondOperand = this.secondOperand.clone();

            return result;
        }
}
}