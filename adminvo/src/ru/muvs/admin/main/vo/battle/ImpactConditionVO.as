package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class ImpactConditionVO
	{
		public var battleImpactConditionId:int;

        // PROBABILITY
        public var probability:int;

        // CONJUNCTION
        // DISJUNCTION
        public var condition:ImpactConditionVO;
        public var next:ImpactConditionVO;
        
        // HAVE_POINT
        public var point:String;// [NONE, DARK, FIRE, LIGHT, WATER, NATURE],
        // HAVE_CELL
        public var type:String;// [NONE, DARK, FIRE, LIGHT, WATER, NATURE],

        public var count:int;

        // HAVE_SLOT
        public var artikul:ArtikulVO;

        private var _condition:String;
        public function get CONDITION():String {
            return _condition;
        }
        public function set CONDITION(value:String):void {
            _condition = value;
        }
        public function get impactCondition():String {
            return _condition;
        }
        public function set impactCondition(value:String):void {
            _condition = value;
        }

        public function ImpactConditionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactConditionId = obj.battleImpactConditionId;
            impactCondition = obj.impactCondition;
            probability = obj.probability;
            condition = new ImpactConditionVO(obj.condition);
            next = new ImpactConditionVO(obj.next);
            point = obj.point;
            count = obj.count;
            artikul = new ArtikulVO(obj.artikul);
        }
        
        public function clone():ImpactConditionVO
        {
            var result:ImpactConditionVO = new ImpactConditionVO();
            result.battleImpactConditionId = this.battleImpactConditionId;
            result.impactCondition = this.impactCondition;
            result.probability = this.probability;
            if (this.condition) {result.condition = this.condition.clone();}
            if (this.next) {result.next = this.next.clone();}
            result.point = this.point;
            result.count = this.count;
            if (this.artikul) {result.artikul = this.artikul.clone();}

            return result;
        }
	}
}