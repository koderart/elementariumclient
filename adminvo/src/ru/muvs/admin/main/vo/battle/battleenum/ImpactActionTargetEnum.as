package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionTargetEnum {

    public static const SELF:String = "SELF";
    public static const ENEMY:String = "ENEMY";
    public static const ENEMIES:String = "ENEMIES";
    public static const ENEMIES_ALL:String = "ENEMIES_ALL";
    public static const ENEMY_RANDOM:String = "ENEMY_RANDOM";
    public static const ENEMIES_RANDOM:String = "ENEMIES_RANDOM";
    public static const ALLY:String = "ALLY";
    public static const ALLIES:String = "ALLIES";
    public static const ALLIES_ALL:String = "ALLIES_ALL";
    public static const ALLY_RANDOM:String = "ALLY_RANDOM";
    public static const ALLIES_RANDOM:String = "ALLIES_RANDOM";

    public static const data:Array= [   SELF, ENEMY, ENEMIES, ENEMIES_ALL,
        ENEMY_RANDOM, ENEMIES_RANDOM, ALLY, ALLIES,ALLIES_ALL, ALLY_RANDOM, ALLIES_RANDOM];
    public static const LABELS:Object = {   SELF:"Себя",
                                            ENEMY:"Врага",
                                            ENEMIES:"Врагов",
                                            ENEMIES_ALL:"Всех врагов",
                                            ENEMY_RANDOM:"Случайный враг",
                                            ENEMIES_RANDOM:"Случайные враги",
                                            ALLY:"Союзник",
                                            ALLIES:"Союзники",
                                            ALLIES_ALL:"Все союзники",
                                            ALLY_RANDOM:"Случайный союзник",
                                            ALLIES_RANDOM:"Случайные союзники"
    };
}
}
