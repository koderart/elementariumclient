package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class ImpactVO
	{
		public var battleImpactId:int;
        public var title:LocaleVO;
        public var description:LocaleVO;
        public var impactExpression:ImpactExpressionVO;

		public function ImpactVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                impactExpression = new ImpactExpressionVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactId = obj.battleImpactId;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            impactExpression = new ImpactExpressionVO(obj.impactExpression);
        }
        
        public function clone():ImpactVO
        {
            var result:ImpactVO = new ImpactVO();
            result.battleImpactId = this.battleImpactId;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.impactExpression = this.impactExpression.clone();

            return result;
        }
	}
}