package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class ImpactActionOperandVO
	{
		public var battleImpactActionOperandId:int;

        // VALUE
        public var value:int;

        // RANGE
        public var from:int;
        public var to:int;

        // OPERATOR
        public var operator:ImpactActionOperatorVO;

        // AREA_CELL
        public var type:String; //[NONE, DARK, FIRE, LIGHT, WATER, NATURE],

        // SKILL_SELF
        // SKILL_ENEMY
        public var skill:SkillVO;

        // POINT_SELF
        // POINT_ENEMY
        public var point:String; //[NONE, DARK, FIRE, LIGHT, WATER, NATURE],

        private var _operand:String;
        public function get OPERAND():String {
            return _operand;
        }
        public function set OPERAND(value:String):void {
            _operand = value;
        }
        public function get actionOperand():String {
            return _operand;
        }
        public function set actionOperand(value:String):void {
            _operand = value;
        }

        public function ImpactActionOperandVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}

        public function parseObject(obj:Object):void
        {
            battleImpactActionOperandId = obj.battleImpactActionOperandId;
            actionOperand = obj.actionOperand;
            value = obj.value;
            from = obj.from;
            to = obj.to;
            if (obj.operator) {operator = new ImpactActionOperatorVO(obj.operator);}
            type = obj.type;
            if (obj.skill) {skill = new SkillVO(obj.skill);}
            point = obj.point;
        }
        
        public function clone():ImpactActionOperandVO
        {
            var result:ImpactActionOperandVO = new ImpactActionOperandVO();
            result.battleImpactActionOperandId = this.battleImpactActionOperandId;
            result.actionOperand = this.actionOperand;
            result.value = this.value;
            result.from = this.from;
            result.to = this.to;
            if (this.operator) {result.operator = this.operator.clone();}
            result.type = this.type;
            if (this.skill) {result.skill = this.skill.clone();}
            result.point = this.point;

            return result;
        }
}
}