package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionOperatorEnum {

    public static const ADDITION:String = "ADDITION";
    public static const SUBTRACTION:String = "SUBTRACTION";
    public static const MULTIPLICATION:String = "MULTIPLICATION";
    public static const DIVISION:String = "DIVISION";
    public static const PERCENT:String = "PERCENT";

    public static const data:Array= [   ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION,
                                        PERCENT];
    public static const LABELS:Object = {   ADDITION:"Сложить",
                                            SUBTRACTION:"Вычесть",
                                            MULTIPLICATION:"Умножить",
                                            DIVISION:"Разделить",
                                            PERCENT:"Процент"
    };
}
}
