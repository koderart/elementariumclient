package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class PotionGroupVO
	{
		public var battlePotionGroupId:int;
		public var title:LocaleVO;
        public var potions:Vector.<PotionGroupItemVO>;

		public function PotionGroupVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battlePotionGroupId = obj.battlePotionGroupId;
            title = new LocaleVO(obj.title);
            potions = new Vector.<PotionGroupItemVO>();
            for each(var o:Object in obj.potions) {
                potions.push(new PotionGroupItemVO(o));
            }
        }
        
        public function clone():PotionGroupVO
        {
            var result:PotionGroupVO = new PotionGroupVO();
            result.battlePotionGroupId = this.battlePotionGroupId;
            result.title = this.title.clone();
            result.potions = new Vector.<PotionGroupItemVO>();
            for each(var item:PotionGroupItemVO in this.potions) {
                result.potions.push(item.clone());
            }

            return result;
        }
	}
}