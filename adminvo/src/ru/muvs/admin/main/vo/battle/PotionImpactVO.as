package ru.muvs.admin.main.vo.battle
{

[Bindable]
	public class PotionImpactVO
	{
		public var battlePotionImpactId:int;
        public var element:String;
        public var impact:ImpactVO;

		public function PotionImpactVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                impact = new ImpactVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battlePotionImpactId = obj.battlePotionImpactId;
            element = obj.element;
            impact = new ImpactVO(obj.impact);
        }
        
        public function clone():PotionImpactVO
        {
            var result:PotionImpactVO = new PotionImpactVO();
            result.battlePotionImpactId = this.battlePotionImpactId;
            result.element = this.element;
            result.impact = this.impact.clone();

            return result;
        }
	}
}