package ru.muvs.admin.main.vo.battle
{

[Bindable]
	public class PotionGroupItemVO
	{
		public var battlePotionGroupPotionId:int;
		public var potion:PotionVO;

		public function PotionGroupItemVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                potion = new PotionVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battlePotionGroupPotionId = obj.battlePotionGroupPotionId;
            potion = new PotionVO(obj.potion);
        }
        
        public function clone():PotionGroupItemVO
        {
            var result:PotionGroupItemVO = new PotionGroupItemVO();
            result.battlePotionGroupPotionId = this.battlePotionGroupPotionId;
            result.potion = this.potion.clone();

            return result;
        }
	}
}