package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class MagicGroupVO
	{
		public var battleMagicGroupId:int;
		public var title:LocaleVO;
        public var magics:Vector.<MagicGroupItemVO>;

		public function MagicGroupVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleMagicGroupId = obj.battleMagicGroupId;
            title = new LocaleVO(obj.title);
            magics = new Vector.<MagicGroupItemVO>();
            for each(var o:Object in obj.magics) {
                magics.push(new MagicGroupItemVO(o));
            }
        }
        
        public function clone():MagicGroupVO
        {
            var result:MagicGroupVO = new MagicGroupVO();
            result.battleMagicGroupId = this.battleMagicGroupId;
            result.title = this.title.clone();
            result.magics = new Vector.<MagicGroupItemVO>();
            for each(var item:MagicGroupItemVO in this.magics) {
                result.magics.push(item.clone());
            }

            return result;
        }
	}
}