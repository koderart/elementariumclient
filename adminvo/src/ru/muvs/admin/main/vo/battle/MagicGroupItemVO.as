package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class MagicGroupItemVO
	{
		public var battleMagicGroupMagicId:int;
		public var magic:MagicVO;

		public function MagicGroupItemVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                magic = new MagicVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleMagicGroupMagicId = obj.battleMagicGroupMagicId;
            magic = new MagicVO(obj.magic);
        }
        
        public function clone():MagicGroupItemVO
        {
            var result:MagicGroupItemVO = new MagicGroupItemVO();
            result.battleMagicGroupMagicId = this.battleMagicGroupMagicId;
            result.magic = this.magic.clone();

            return result;
        }
	}
}