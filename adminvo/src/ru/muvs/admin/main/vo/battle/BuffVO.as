package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;
import ru.muvs.admin.main.vo.battle.battleenum.BuffEnum;
import ru.muvs.admin.main.vo.enum.ElementEnum;

[Bindable]
	public class BuffVO
	{
		public var battleBuffId:int;
        public var title:LocaleVO;
		public var description:LocaleVO;
        public var image:ImageVO;
        public var duration:int;
        public var permanent:Boolean;
        public var positive:Boolean;
        public var negative:Boolean;
        public var before:ImpactVO;
        public var after:ImpactVO;

        // INCREASE_SKILL_CURRENT
        // REDUCE_SKILL_CURRENT
        // INCREASE_SKILL_TOTAL
        // REDUCE_SKILL_TOTAL
        // INCREASE_SKILL
        // REDUCE_SKILL
        public var skill: SkillVO;
        public var value: int;
        public var percent:Boolean;
    
        // TICK
        public var frequency:int;
        public var impact: ImpactVO;

        // STUN
        public var step:int;

        // REFLECT_DAMAGE_TO_DAMAGE
        // REFLECT_DAMAGE_TO_HEAL
        // INVERT_IN_DAMAGE_TO_HEAL
        // INVERT_OUT_DAMAGE_TO_HEAL
        public var damageFrom:String;
        public var damageTo:String;
        public var healTo:String;
        public var reflect:int;

        // CELL_REMOVED
        // CELL_ADDED
        public var type:String = ElementEnum.NONE;
        public var count:int;

        private var _buff:String = BuffEnum.INCREASE_SKILL_CURRENT;
        public function get BUFF():String {
            return _buff;
        }
        public function set BUFF(value:String):void {
            _buff = value;
        }
        public function get battleBuff():String {
            return _buff;
        }
        public function set battleBuff(value:String):void {
            _buff = value;
        }

        public function BuffVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                image = new ImageVO();
                before = new ImpactVO();
                after = new ImpactVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleBuffId = obj.battleBuffId;
            battleBuff = obj.battleBuff;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            image = new ImageVO(obj.image);
            duration = obj.duration;
            permanent = obj.permanent;
            positive = obj.positive;
            negative = obj.negative;
            before = new ImpactVO(obj.before);
            after = new ImpactVO(obj.after);

            skill = new SkillVO(obj.skill);
            value = obj.value;
            percent = obj.percent;
            frequency = obj.frequency;
            impact = new ImpactVO(obj.impact);
            step = obj.step;
            damageFrom = obj.damageFrom;
            damageTo = obj.damageTo;
            healTo = obj.healTo;
            reflect = obj.reflect;
            type = obj.type;
            count = obj.count;
        }

        public function clone():BuffVO
        {
            var result:BuffVO = new BuffVO();
    
            result.battleBuffId = this.battleBuffId;
            result.battleBuff = this.battleBuff;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.image = this.image.clone();
            result.duration = this.duration;
            result.permanent = this.permanent;
            result.positive = this.positive;
            result.negative = this.negative;
            result.before = this.before.clone();
            result.after = this.after.clone();

            if (this.skill) {result.skill = this.skill.clone();}
            result.value = this.value;
            result.percent = this.percent;
            result.frequency = this.frequency;
            if (this.impact) {result.impact = this.impact.clone();}
            result.step = this.step;
            result.damageFrom = this.damageFrom;
            result.damageTo = this.damageTo;
            result.healTo = this.healTo;
            result.reflect = this.reflect;
            result.type = this.type;
            result.count = this.count;

            return result;
        }

}
}