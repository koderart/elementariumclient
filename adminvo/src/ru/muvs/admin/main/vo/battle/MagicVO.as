package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class MagicVO
	{
		public var battleMagicId:int;
		public var title:LocaleVO;
		public var description:LocaleVO;
        public var image:ImageVO;
        public var element:String;
        public var level:LevelVO;
        public var impact:ImpactVO;
        public var cooldown:int;
        public var visible:Boolean = true;

		public function MagicVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                image = new ImageVO();
                level = new LevelVO();
                impact = new ImpactVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleMagicId = obj.battleMagicId;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            image = new ImageVO(obj.image);
            element = obj.element;
            level = new LevelVO(obj.level);
            impact = new ImpactVO(obj.impact);
            cooldown = obj.cooldown;
            visible = obj.visible;
        }
        
        public function clone():MagicVO
        {
            var result:MagicVO = new MagicVO();
            result.battleMagicId = this.battleMagicId;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.image = this.image.clone();
            result.element = this.element;
            result.level = this.level.clone();
            result.impact = this.impact.clone();
            result.cooldown = this.cooldown;
            result.visible = this.visible;

            return result;
        }
	}
}