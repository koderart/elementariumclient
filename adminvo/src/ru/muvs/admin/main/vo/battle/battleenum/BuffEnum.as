package ru.muvs.admin.main.vo.battle.battleenum {
public final class BuffEnum {

    public static const INCREASE_SKILL_CURRENT:String = "INCREASE_SKILL_CURRENT";
    public static const REDUCE_SKILL_CURRENT:String = "REDUCE_SKILL_CURRENT";
    public static const INCREASE_SKILL_TOTAL:String = "INCREASE_SKILL_TOTAL";
    public static const REDUCE_SKILL_TOTAL:String = "REDUCE_SKILL_TOTAL";
    public static const INCREASE_SKILL:String = "INCREASE_SKILL";
    public static const REDUCE_SKILL:String = "REDUCE_SKILL";
    public static const TICK:String = "TICK";
    public static const STUN:String = "STUN";
    public static const REFLECT_DAMAGE_TO_DAMAGE:String = "REFLECT_DAMAGE_TO_DAMAGE";
    public static const REFLECT_DAMAGE_TO_HEAL:String = "REFLECT_DAMAGE_TO_HEAL";
    public static const INVERT_IN_DAMAGE_TO_HEAL:String = "INVERT_IN_DAMAGE_TO_HEAL";
    public static const INVERT_OUT_DAMAGE_TO_HEAL:String = "INVERT_OUT_DAMAGE_TO_HEAL";
    public static const CELL_REMOVED:String = "CELL_REMOVED";
    public static const CELL_ADDED:String = "CELL_ADDED";

    public static const data:Array= [   INCREASE_SKILL_CURRENT, REDUCE_SKILL_CURRENT, INCREASE_SKILL_TOTAL, REDUCE_SKILL_TOTAL,
                                        INCREASE_SKILL, REDUCE_SKILL, TICK, STUN, REFLECT_DAMAGE_TO_DAMAGE, REFLECT_DAMAGE_TO_HEAL,
                                        INVERT_IN_DAMAGE_TO_HEAL, INVERT_OUT_DAMAGE_TO_HEAL, CELL_REMOVED, CELL_ADDED];
    public static const LABELS:Object = {   INCREASE_SKILL_CURRENT:"+ Характеристика(тек)",
                                            REDUCE_SKILL_CURRENT:"- Характеристика(тек)",
                                            INCREASE_SKILL_TOTAL:"+ Характеристика(макс)",
                                            REDUCE_SKILL_TOTAL:"- Характеристика(макс)",
                                            INCREASE_SKILL:"+ Характеристика(тек и макс)",
                                            REDUCE_SKILL:"- Характеристика(тек и макс)",
                                            TICK:"Тик",
                                            STUN:"Оглушить",
                                            REFLECT_DAMAGE_TO_DAMAGE:"Отразить урон в урон",
                                            REFLECT_DAMAGE_TO_HEAL:"Отразить урон в лечение",
                                            INVERT_IN_DAMAGE_TO_HEAL:"Перевернуть вход. урон в лечение",
                                            INVERT_OUT_DAMAGE_TO_HEAL:"Перевернуть исход. урон в лечение",
                                            CELL_REMOVED:"Удалилось камней",
                                            CELL_ADDED:"Добавилось камней"
    };
}
}
