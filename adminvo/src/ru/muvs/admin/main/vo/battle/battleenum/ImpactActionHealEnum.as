package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionHealEnum {

    public static const HEAL:String = "HEAL";
    public static const HEAL_DARK:String = "HEAL_DARK";
    public static const HEAL_FIRE:String = "HEAL_FIRE";
    public static const HEAL_LIGHT:String = "HEAL_LIGHT";
    public static const HEAL_WATER:String = "HEAL_WATER";
    public static const HEAL_NATURE:String = "HEAL_NATURE";

    public static const data:Array= [   HEAL, HEAL_DARK, HEAL_FIRE, HEAL_LIGHT,
                                        HEAL_WATER, HEAL_NATURE];
    public static const LABELS:Object = {   HEAL:"Физический",
                                            HEAL_DARK:"Тьма",
                                            HEAL_FIRE:"Огонь",
                                            HEAL_LIGHT:"Свет",
                                            HEAL_WATER:"Вода",
                                            HEAL_NATURE:"Природа"
    };
}
}
