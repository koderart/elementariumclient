package ru.muvs.admin.main.vo.battle.battleenum {
public final class ImpactActionDamageEnum {

    public static const DAMAGE:String = "DAMAGE";
    public static const DAMAGE_DARK:String = "DAMAGE_DARK";
    public static const DAMAGE_FIRE:String = "DAMAGE_FIRE";
    public static const DAMAGE_LIGHT:String = "DAMAGE_LIGHT";
    public static const DAMAGE_WATER:String = "DAMAGE_WATER";
    public static const DAMAGE_NATURE:String = "DAMAGE_NATURE";

    public static const data:Array= [   DAMAGE, DAMAGE_DARK, DAMAGE_FIRE, DAMAGE_LIGHT,
                                        DAMAGE_WATER, DAMAGE_NATURE];
    public static const LABELS:Object = {   DAMAGE:"Физический",
                                            DAMAGE_DARK:"Тьмой",
                                            DAMAGE_FIRE:"Огнем",
                                            DAMAGE_LIGHT:"Светом",
                                            DAMAGE_WATER:"Водой",
                                            DAMAGE_NATURE:"Природой"
    };
}
}
