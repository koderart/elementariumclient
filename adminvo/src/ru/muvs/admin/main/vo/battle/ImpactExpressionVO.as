package ru.muvs.admin.main.vo.battle
{
[Bindable]
	public class ImpactExpressionVO
	{
		public var battleImpactExpressionId:int;
        public var impactCondition:ImpactConditionVO;
        public var trueAction:ImpactActionVO;
        public var falseAction:ImpactActionVO;

		public function ImpactExpressionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactExpressionId = obj.battleImpactExpressionId;
            if (obj.impactCondition) {impactCondition = new ImpactConditionVO(obj.impactCondition);}
            if (obj.trueAction) {trueAction = new ImpactActionVO(obj.trueAction);}
            if (obj.falseAction) {falseAction = new ImpactActionVO(obj.falseAction);}
        }
        
        public function clone():ImpactExpressionVO
        {
            var result:ImpactExpressionVO = new ImpactExpressionVO();
            result.battleImpactExpressionId = this.battleImpactExpressionId;
            if(this.impactCondition) {result.impactCondition = this.impactCondition.clone();}
            if(this.trueAction) {result.trueAction = this.trueAction.clone();}
            if(this.falseAction) {result.falseAction = this.falseAction.clone();}

            return result;
        }
	}
}