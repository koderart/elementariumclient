package ru.muvs.admin.main.vo.battle
{
[Bindable]
	public class ImpactActionTargetVO
	{
		public var battleImpactActionTargetId:int;

        // ENEMIES
        // ENEMIES_RANDOM
        // ALLIES
        // ALLIES_RANDOM
        public var count:int;

        private var _target:String;
        public function get TARGET():String {
            return _target;
        }
        public function set TARGET(value:String):void {
            _target = value;
        }
        public function get actionTarget():String {
            return _target;
        }
        public function set actionTarget(value:String):void {
            _target = value;
        }

        public function ImpactActionTargetVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battleImpactActionTargetId = obj.battleImpactActionTargetId;
            actionTarget = obj.actionTarget;
            count = obj.count;
        }
        
        public function clone():ImpactActionTargetVO
        {
            var result:ImpactActionTargetVO = new ImpactActionTargetVO();
            result.battleImpactActionTargetId = this.battleImpactActionTargetId;
            result.actionTarget = this.actionTarget;
            result.count = this.count;

            return result;
        }
	}
}