package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class PotionVO
	{
		public var battlePotionId:int;
		public var title:LocaleVO;
		public var description:LocaleVO;
        public var potionImpact:Vector.<PotionImpactVO>;
        public var cooldown:int;

		public function PotionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                potionImpact = new Vector.<PotionImpactVO>();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            battlePotionId = obj.battlePotionId;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            potionImpact = new Vector.<PotionImpactVO>();
            for each(var o:Object in obj.potionImpact) {
                potionImpact.push(new PotionImpactVO(o));
            }
            cooldown = obj.cooldown;
        }
        
        public function clone():PotionVO
        {
            var result:PotionVO = new PotionVO();
            result.battlePotionId = this.battlePotionId;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.potionImpact = new Vector.<PotionImpactVO>();
            for each(var item:PotionImpactVO in this.potionImpact) {
                result.potionImpact.push(item.clone());
            }
            result.cooldown = this.cooldown;

            return result;
        }
	}
}