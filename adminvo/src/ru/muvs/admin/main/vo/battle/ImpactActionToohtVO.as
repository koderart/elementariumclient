package ru.muvs.admin.main.vo.battle
{
import ru.muvs.admin.main.vo.*;

[Bindable]
	public class ImpactActionToohtVO
	{
		public var forkBattleImpactActionToothId:int;
        public var action:ImpactActionVO;
        public var weight:int;

		public function ImpactActionToohtVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            forkBattleImpactActionToothId = obj.forkBattleImpactActionToothId;
            weight = obj.weight;
            if (obj.action) {action = new ImpactActionVO(obj.action);}
        }
        
        public function clone():ImpactActionToohtVO
        {
            var result:ImpactActionToohtVO = new ImpactActionToohtVO();
            result.forkBattleImpactActionToothId = this.forkBattleImpactActionToothId;
            result.weight = this.weight;
            if(this.action) {result.action = this.action.clone();}

            return result;
        }
	}
}