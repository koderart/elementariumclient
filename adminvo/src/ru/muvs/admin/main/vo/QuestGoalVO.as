package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.ElementEnum;
import ru.muvs.admin.main.vo.enum.MoneyEnum;

[Bindable]
	public class QuestGoalVO
	{
		public var questGoalId:int;
        //artikul collect, have
		public var artikul:ArtikulVO;
		public var count:int;
        public var remove:Boolean;

        //mob
        public var mob:MobVO;
        
        //money
        public var moneyType:String = MoneyEnum.NONE;
        public var money:int;
        public var take:Boolean;
        
        //palyer
        public var element:String = ElementEnum.NONE;

        private var _questGoal:String;
        public function get GOAL():String {
            return _questGoal;
        }
        public function set GOAL(value:String):void {
            _questGoal = value;
        }
        public function get questGoal():String {
            return _questGoal;
        }
        public function set questGoal(value:String):void {
            _questGoal = value;
        }
    
        public function QuestGoalVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
        }

        public function parseObject(obj:Object):void
        {
            questGoalId = obj.questGoalId;
            questGoal = obj.questGoal;
            if (obj.artikul) artikul = new ArtikulVO(obj.artikul);
            if (obj.mob) mob = new MobVO(obj.mob);
            count = obj.count;
            remove = obj.remove;
            moneyType = obj.moneyType;
            money = obj.money;
            take = obj.take;
            element = obj.element;
        }

        public function clone():QuestGoalVO
        {
            var result:QuestGoalVO = new QuestGoalVO();
            result.questGoalId = this.questGoalId;
            result.questGoal = this.questGoal;
            if (this.artikul) result.artikul = this.artikul.clone();
            if (this.mob) result.mob = this.mob.clone();
            result.count = this.count;
            result.remove = this.remove;
            result.moneyType = this.moneyType;
            result.money = this.money;
            result.take = this.take;
            result.element = this.element;
            
            return result;
        }
	}
}