package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.ElementEnum;

[Bindable]
	public class AchieveGoalVO
	{
		public var achieveGoalId:int;
		public var GOAL:String;
		public var achieveGoal:String;
		public var mob:MobVO;
		public var count:int;
        public var element:String = ElementEnum.NONE;

		public function AchieveGoalVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                mob = new MobVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            achieveGoalId = obj.achieveGoalId;
            GOAL = obj.GOAL;
            achieveGoal = obj.achieveGoal;
            mob = new MobVO(obj.mob);
            count = obj.count;
            element = obj.element;
        }

        public function clone():AchieveGoalVO
        {
            var result:AchieveGoalVO = new AchieveGoalVO();
            result.achieveGoalId = this.achieveGoalId;
            result.GOAL = this.GOAL;
            result.achieveGoal = this.achieveGoal;
            result.mob = this.mob.clone();
            result.count = this.count;
            result.element = this.element;
            
            return result;
        }
	}
}