package ru.muvs.admin.main.vo
{
    [Bindable]
	public class AreaCellTypeVO
	{
		public var areaCellTypeId:int;
		public var title:LocaleVO;

		public function AreaCellTypeVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            areaCellTypeId = obj.areaCellTypeId;
            title = new LocaleVO(obj.title);
        }
        
        public function clone():AreaCellTypeVO
        {
            var result:AreaCellTypeVO = new AreaCellTypeVO();
            result.areaCellTypeId = this.areaCellTypeId;
            result.title = this.title.clone();
            
            return result;
        }
	}
}