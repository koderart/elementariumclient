    package ru.muvs.admin.main.vo
{

[Bindable]
	public class ChallengeGoalPlayerVO
	{
		public var challengeGoalId:int;
		public var GOAL:String;
		public var challengeGoal:String;
		public var element:String;
		public var count:int;

		public function ChallengeGoalPlayerVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeGoalId = obj.challengeGoalId;
            GOAL = obj.GOAL;
            challengeGoal = obj.challengeGoal;
            element = obj.element;
            count = obj.count;
        }

        public function clone():ChallengeGoalPlayerVO
        {
            var result:ChallengeGoalPlayerVO = new ChallengeGoalPlayerVO();
            result.challengeGoalId = this.challengeGoalId;
            result.GOAL = this.GOAL;
            result.challengeGoal = this.challengeGoal;
            result.element = this.element;
            result.count = this.count;
            
            return result;
        }
	}
}