package ru.muvs.admin.main.vo
{

[Bindable]
	public class NpcGiveVO
	{
        public var npcGiveQuestId:int;
		public var quest:QuestVO;

		public function NpcGiveVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                quest = new QuestVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            npcGiveQuestId = obj.npcGiveQuestId;
            quest = new QuestVO(obj.quest);
        }

        public function clone():NpcGiveVO
        {
            var result:NpcGiveVO = new NpcGiveVO();
            result.npcGiveQuestId = this.npcGiveQuestId;
            result.quest = this.quest.clone();
            
            return result;
        }
	}
}