package ru.muvs.admin.main.vo
{

[Bindable]
	public class ChallengeGoalArtikulVO
	{
		public var challengeGoalId:int;
		public var GOAL:String;
		public var challengeGoal:String;
		public var artikul:ArtikulVO;
		public var count:int;

		public function ChallengeGoalArtikulVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                artikul = new ArtikulVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeGoalId = obj.challengeGoalId;
            GOAL = obj.GOAL;
            challengeGoal = obj.challengeGoal;
            artikul = new ArtikulVO(obj.artikul);
            count = obj.count;
        }

        public function clone():ChallengeGoalArtikulVO
        {
            var result:ChallengeGoalArtikulVO = new ChallengeGoalArtikulVO();
            result.challengeGoalId = this.challengeGoalId;
            result.GOAL = this.GOAL;
            result.challengeGoal = this.challengeGoal;
            result.artikul = this.artikul.clone();
            result.count = this.count;
            
            return result;
        }
	}
}