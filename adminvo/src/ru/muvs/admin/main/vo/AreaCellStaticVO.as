package ru.muvs.admin.main.vo
{
    [Bindable]
	public class AreaCellStaticVO
	{
		public var areaCellStaticId:int;
		public var areaCellType:AreaCellTypeVO;
		public var positionX:int;
		public var positionY:int;

		public function AreaCellStaticVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                areaCellType = new AreaCellTypeVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            areaCellStaticId = obj.areaCellStaticId;
            areaCellType = new AreaCellTypeVO(obj.areaCellType);
            positionX = obj.positionX;
            positionY = obj.positionY;
        }
        
        public function clone():AreaCellStaticVO
        {
            var result:AreaCellStaticVO = new AreaCellStaticVO();
            result.areaCellStaticId = this.areaCellStaticId;
            result.areaCellType = this.areaCellType.clone();
            result.positionX = this.positionX;
            result.positionY = this.positionY;
            
            return result;
        }
	}
}