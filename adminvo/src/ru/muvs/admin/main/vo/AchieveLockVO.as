package ru.muvs.admin.main.vo
{

[Bindable]
	public class AchieveLockVO
	{
        public var achieveLockId:int;
		public var LOCK:String;
		public var achieveLock:String;
		public var level:LevelVO;
		public var element:Array = [];

		public function AchieveLockVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                level = new LevelVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            achieveLockId = obj.achieveLockId;
            LOCK = obj.LOCK;
            achieveLock = obj.achieveLock;
            level = new LevelVO(obj.level);
            element = obj.element;
        }

        public function clone():AchieveLockVO
        {
            var result:AchieveLockVO = new AchieveLockVO();
            result.achieveLockId = this.achieveLockId;
            result.LOCK = this.LOCK;
            result.achieveLock = this.achieveLock;
            result.level = this.level.clone();
            if (this.element) result.element = this.element.concat();
            
            return result;
        }
	}
}