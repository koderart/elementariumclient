package ru.muvs.admin.main.vo
{

[Bindable]
	public class MobSkillVO
	{
		public var mobSkillId:int;
		public var skill:SkillVO;
		public var level:int;
		public var value:int = 0;

		public function MobSkillVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            mobSkillId = obj.mobSkillId;
            skill = new SkillVO(obj.skill);
            level = obj.level;
            value = obj.value;
        }
        
        public function clone():MobSkillVO
        {
            var result:MobSkillVO = new MobSkillVO();
            result.mobSkillId = this.mobSkillId;
            result.skill = this.skill.clone();
            result.level = this.level;
            result.value = this.value;
            
            return result;
        }
	}
}