package ru.muvs.admin.main.vo
{

[Bindable]
	public class ChallengeGoalMobVO
	{
		public var challengeGoalId:int;
		public var GOAL:String;
		public var challengeGoal:String;
		public var mob:MobVO;
		public var count:int;

		public function ChallengeGoalMobVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                mob = new MobVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeGoalId = obj.challengeGoalId;
            GOAL = obj.GOAL;
            challengeGoal = obj.challengeGoal;
            mob = new MobVO(obj.mob);
            count = obj.count;
        }

        public function clone():ChallengeGoalMobVO
        {
            var result:ChallengeGoalMobVO = new ChallengeGoalMobVO();
            result.challengeGoalId = this.challengeGoalId;
            result.GOAL = this.GOAL;
            result.challengeGoal = this.challengeGoal;
            result.mob = this.mob.clone();
            result.count = this.count;
            
            return result;
        }
	}
}