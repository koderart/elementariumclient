package ru.muvs.admin.main.vo
{
    [Bindable]
	public class AreaCellDisableVO
	{
		public var areaCellDisableId:int;
		public var positionX:int;
		public var positionY:int;

		public function AreaCellDisableVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                
            }
		}
        
        public function parseObject(obj:Object):void
        {
            areaCellDisableId = obj.areaCellDisableId;
            positionX = obj.positionX;
            positionY = obj.positionY;
        }
        
        public function clone():AreaCellDisableVO
        {
            var result:AreaCellDisableVO = new AreaCellDisableVO();
            result.areaCellDisableId = this.areaCellDisableId;
            result.positionX = this.positionX;
            result.positionY = this.positionY;
            
            return result;
        }
	}
}