package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.ElementEnum;

    [Bindable]
	public class ArtikulSkillRandomVO
	{
		public var artikulSkillRandomId:int;
		public var skill:SkillVO;
        public var element:String = ElementEnum.NONE;
        public var probability:int;
        public var weight:int;
		public var valueFrom:int;
		public var valueTo:int;

        public function ArtikulSkillRandomVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            artikulSkillRandomId = obj.artikulSkillRandomId;
            skill = new SkillVO(obj.skill);
            element = obj.element;
            probability = obj.probability;
            weight = obj.weight;
            valueFrom = obj.valueFrom;
            valueTo = obj.valueTo;
        }
        
        public function clone():ArtikulSkillRandomVO
        {
            var result:ArtikulSkillRandomVO = new ArtikulSkillRandomVO();
            result.artikulSkillRandomId = this.artikulSkillRandomId;
            result.skill = this.skill.clone();
            result.element = this.element;
            result.probability = this.probability;
            result.weight = this.weight;
            result.valueFrom = this.valueFrom;
            result.valueTo = this.valueTo;
            
            return result;
        }
	}
}