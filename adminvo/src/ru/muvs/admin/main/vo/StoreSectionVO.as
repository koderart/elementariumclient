package ru.muvs.admin.main.vo
{

[Bindable]
	public class StoreSectionVO
	{
		public var storeSectionId:int;
        public var title:LocaleVO;
        public var order:int;
        public var storeSectionArtikul:Vector.<StoreSectionArtikulVO>;
        
		public function StoreSectionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                storeSectionArtikul = new Vector.<StoreSectionArtikulVO>();
            }
        }

        public function parseObject(obj:Object):void
        {
            var o:Object;
            storeSectionId = obj.storeSectionId;
            title = new LocaleVO(obj.title);
            order = obj.order;
            storeSectionArtikul = new Vector.<StoreSectionArtikulVO>();
            for each(o in obj.storeSectionArtikul) {
                storeSectionArtikul.push(new StoreSectionArtikulVO(o));
            }
        }

        public function clone():StoreSectionVO
        {
            var result:StoreSectionVO = new StoreSectionVO();
            result.storeSectionId = this.storeSectionId;
            result.title = this.title.clone();
            result.order = this.order;
            result.storeSectionArtikul = new Vector.<StoreSectionArtikulVO>();
            for each(var sectionArtikul:StoreSectionArtikulVO in this.storeSectionArtikul) {
                result.storeSectionArtikul.push(sectionArtikul.clone());
            }
            return result;
        }
	}
}