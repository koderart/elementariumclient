package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.battle.MagicVO;

[Bindable]
	public class MobVO
	{
		public var mobId:int;
		public var title:LocaleVO;
        public var level:int;
        public var cooldown:int;
		public var imageAvatar:ImageVO;
        public var bonusWin:BonusVO;
        public var bonusLose:BonusVO;
        public var mobSkill:Vector.<MobSkillVO>;
        public var magic:MagicVO;

        public function MobVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                imageAvatar = new ImageVO();
                bonusWin = new BonusVO();
                bonusLose = new BonusVO();
                magic = new MagicVO();
                mobSkill = new Vector.<MobSkillVO>();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            var o:Object;
            mobId = obj.mobId;
            title = new LocaleVO(obj.title);
            level = obj.level;
            cooldown = obj.cooldown;
            imageAvatar = new ImageVO(obj.imageAvatar);
            bonusWin = new BonusVO(obj.bonusWin);
            bonusLose = new BonusVO(obj.bonusLose);
            magic = new MagicVO(obj.magic);
            mobSkill = new Vector.<MobSkillVO>();
            for each(o in obj.mobSkill) {
                mobSkill.push(new MobSkillVO(o));
            }
        }
        
        public function clone():MobVO
        {
            var result:MobVO = new MobVO();
            result.mobId = this.mobId;
            result.title = this.title.clone();
            result.level = this.level;
            result.cooldown = this.cooldown;
            result.imageAvatar = this.imageAvatar.clone();
            result.bonusWin = this.bonusWin.clone();
            result.bonusLose = this.bonusLose.clone();
            result.magic = this.magic.clone();

            result.mobSkill = new Vector.<MobSkillVO>();
            for each(var skill:MobSkillVO in this.mobSkill) {
                result.mobSkill.push(skill.clone());
            }

            return result;
        }
	}
}