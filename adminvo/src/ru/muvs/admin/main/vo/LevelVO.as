package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.ElementEnum;

[Bindable]
	public class LevelVO
	{
		public var levelId:int;
		public var number:int;
        public var element : String = ElementEnum.NONE;
        public var exp:int = 0;
        public var levelSkillBase:Vector.<LevelSkillVO>;
        public var bonus:BonusVO;
        
		public function LevelVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                levelSkillBase = new Vector.<LevelSkillVO>();
                bonus = new BonusVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            var o:Object;
            levelId = obj.levelId;
            number = obj.number;
            element = obj.element;
            exp = obj.exp;
            levelSkillBase = new Vector.<LevelSkillVO>();
            for each(o in obj.levelSkillBase) {
                levelSkillBase.push(new LevelSkillVO(o));
            }
            bonus = new BonusVO(obj.bonus); 
        }

        public function clone():LevelVO
        {
            var result:LevelVO = new LevelVO();
            result.levelId = this.levelId;
            result.number = this.number;
            result.element = this.element;
            result.exp = this.exp;
            result.levelSkillBase = new Vector.<LevelSkillVO>();
            for each(var skill:LevelSkillVO in this.levelSkillBase) {
                result.levelSkillBase.push(skill.clone());
            }
            result.bonus = this.bonus.clone();
            return result;
        }
	}
}