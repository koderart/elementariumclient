package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.SkillEnum;

[Bindable]
	public class SkillVO
	{
		public var skillId:int;
		public var title:LocaleVO;
		public var description:LocaleVO;
		public var skill:String = SkillEnum.NONE;

		public function SkillVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            skillId = obj.skillId;
            skill = obj.skill;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
        }
        
        public function clone():SkillVO
        {
            var result:SkillVO = new SkillVO();
            result.skillId = this.skillId;
            result.skill = this.skill;
            result.title = this.title.clone();
            result.description = this.description.clone();
            
            return result;
        }
	}
}