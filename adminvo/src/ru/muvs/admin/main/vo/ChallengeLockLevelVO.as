package ru.muvs.admin.main.vo
{
[Bindable]
	public class ChallengeLockLevelVO
	{
        public var challengeLockId:int;
		public var LOCK:String;
		public var challengeLock:String;
		public var level:LevelVO;

		public function ChallengeLockLevelVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                level = new LevelVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeLockId = obj.challengeLockId;
            LOCK = obj.LOCK;
            challengeLock = obj.challengeLock;
            level = new LevelVO(obj.level);
        }

        public function clone():ChallengeLockLevelVO
        {
            var result:ChallengeLockLevelVO = new ChallengeLockLevelVO();
            result.challengeLockId = this.challengeLockId;
            result.LOCK = this.LOCK;
            result.challengeLock = this.challengeLock;
            result.level = this.level.clone();
            
            return result;
        }
	}
}