package ru.muvs.admin.main.vo
{
[Bindable]
	public class ChallengeLockElementVO
	{
        public var challengeLockId:int;
		public var LOCK:String;
		public var challengeLock:String;
		public var element:String;

		public function ChallengeLockElementVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeLockId = obj.challengeLockId;
            LOCK = obj.LOCK;
            challengeLock = obj.challengeLock;
            element = obj.element;
        }

        public function clone():ChallengeLockElementVO
        {
            var result:ChallengeLockElementVO = new ChallengeLockElementVO();
            result.challengeLockId = this.challengeLockId;
            result.LOCK = this.LOCK;
            result.challengeLock = this.challengeLock;
            result.element = this.element;
            
            return result;
        }
	}
}