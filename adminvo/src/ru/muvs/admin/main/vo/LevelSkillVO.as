package ru.muvs.admin.main.vo
{

[Bindable]
	public class LevelSkillVO
	{
		public var levelSkillBaseId:int;
		public var skill:SkillVO;
		public var value:int = 0;

		public function LevelSkillVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            levelSkillBaseId = obj.levelSkillBaseId;
            skill = new SkillVO(obj.skill);
            value = obj.value;
        }
        
        public function clone():LevelSkillVO
        {
            var result:LevelSkillVO = new LevelSkillVO();
            result.levelSkillBaseId = this.levelSkillBaseId;
            result.skill = this.skill.clone();
            result.value = this.value;
            
            return result;
        }
	}
}