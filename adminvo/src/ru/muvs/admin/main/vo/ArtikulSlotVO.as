package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.SlotEnum;

[Bindable]
	public class ArtikulSlotVO
	{
		public var artikulSlotId:int;
        public var artikulSlot:String = SlotEnum.NONE;

		public function ArtikulSlotVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            }
        }

        public function parseObject(obj:Object):void
        {
            artikulSlotId = obj.artikulSlotId;
            artikulSlot = obj.artikulSlot;
        }

        public function clone():ArtikulSlotVO
        {
            var result:ArtikulSlotVO = new ArtikulSlotVO();
            result.artikulSlotId = this.artikulSlotId;
            result.artikulSlot = this.artikulSlot;

            return result;
        }
	}
}