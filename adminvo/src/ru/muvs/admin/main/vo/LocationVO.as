package ru.muvs.admin.main.vo
{
import flash.geom.Point;

[Bindable]
public class LocationVO
{
    public var locationId:int;
    public var title:LocaleVO;
    public var description:LocaleVO;
    public var hexWidth:int;
    public var hexHeight:int;
    public var offsetWidth:int;
    public var offsetHeight:int;
    public var cemetery:int;
    public var cemeteryPositionHexX:int;
    public var cemeteryPositionHexY:int;
    public var locationDecoration:Vector.<LocationDecorationVO>;
    public var locationImage:Vector.<LocationImageVO>;
    public var locationMember:Vector.<LocationMemberVO>;
    public var locationPassableHex:Vector.<LocationPassableHexVO>;
    public var background:String;


    public function LocationVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            title = new LocaleVO();
            description = new LocaleVO();
            locationDecoration = new Vector.<LocationDecorationVO>();
            locationImage = new Vector.<LocationImageVO>();
            locationMember = new Vector.<LocationMemberVO>();
            locationPassableHex = new Vector.<LocationPassableHexVO>();
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        locationId = obj.locationId;
        title = new LocaleVO(obj.title);
        description = new LocaleVO(obj.description);
        hexWidth = obj.hexWidth;
        hexHeight = obj.hexHeight;
        offsetWidth = obj.offsetWidth;
        offsetHeight = obj.offsetHeight;
        cemetery = obj.cemetery;
        cemeteryPositionHexX = obj.cemeteryPositionHexX;
        cemeteryPositionHexY = obj.cemeteryPositionHexY;
        background = obj.background;

        locationDecoration = new Vector.<LocationDecorationVO>();
        for each(o in obj.locationDecoration) {
            locationDecoration.push(new LocationDecorationVO(o));
        }
        locationImage = new Vector.<LocationImageVO>();
        for each(o in obj.locationImage) {
            locationImage.push(new LocationImageVO(o));
        }
        locationMember = new Vector.<LocationMemberVO>();
        for each(o in obj.locationMember) {
            locationMember.push(new LocationMemberVO(o));
        }
        locationPassableHex = new Vector.<LocationPassableHexVO>();
        for each(o in obj.locationPassableHex) {
            locationPassableHex.push(new LocationPassableHexVO(o));
        }
    }

    public function clone():LocationVO
    {
        var result:LocationVO = new LocationVO();
        result.locationId = this.locationId;
        result.title = this.title.clone();
        result.description = this.description.clone();
        result.hexWidth = this.hexWidth;
        result.hexHeight = this.hexHeight;
        result.offsetWidth = this.offsetWidth;
        result.offsetHeight = this.offsetHeight;
        result.cemetery = this.cemetery;
        result.cemeteryPositionHexX = this.cemeteryPositionHexX;
        result.cemeteryPositionHexY = this.cemeteryPositionHexY;
        result.background = this.background;
        
        result.locationDecoration = new Vector.<LocationDecorationVO>();
        for each(var decor:LocationDecorationVO in this.locationDecoration) {
            result.locationDecoration.push(decor.clone());
        }
        result.locationImage = new Vector.<LocationImageVO>();
        for each(var image:LocationImageVO in this.locationImage) {
            result.locationImage.push(image.clone());
        }
        result.locationMember = new Vector.<LocationMemberVO>();
        for each(var member:LocationMemberVO in this.locationMember) {
            result.locationMember.push(member.clone());
        }
        result.locationPassableHex = new Vector.<LocationPassableHexVO>();
        for each(var hex:LocationPassableHexVO in this.locationPassableHex) {
            result.locationPassableHex.push(hex.clone());
        }
        return result;
    }
}
}