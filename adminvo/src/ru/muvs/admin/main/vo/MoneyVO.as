package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.MoneyEnum;

[Bindable]
	public class MoneyVO
	{
		public var artikulPriceId:int;
		public var moneyType:String = MoneyEnum.GOLD;
		public var money:int;

		public function MoneyVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            }
        }

        public function parseObject(obj:Object):void
        {
            artikulPriceId = obj.artikulPriceId;
            moneyType = obj.moneyType;
            money = obj.money;
        }

        public function clone():MoneyVO
        {
            var result:MoneyVO = new MoneyVO();
            result.artikulPriceId = this.artikulPriceId;
            result.moneyType = this.moneyType;
            result.money = this.money;
            
            return result;
        }
	}
}