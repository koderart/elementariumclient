package ru.muvs.admin.main.vo
{

[Bindable]
	public class NpcTakeVO
	{
        public var npcTakeQuestId:int;
		public var quest:QuestVO;

		public function NpcTakeVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                quest = new QuestVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            npcTakeQuestId = obj.npcTakeQuestId;
            quest = new QuestVO(obj.quest);
        }

        public function clone():NpcTakeVO
        {
            var result:NpcTakeVO = new NpcTakeVO();
            result.npcTakeQuestId = this.npcTakeQuestId;
            result.quest = this.quest.clone();
            
            return result;
        }
	}
}