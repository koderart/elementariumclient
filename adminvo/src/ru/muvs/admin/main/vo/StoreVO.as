package ru.muvs.admin.main.vo
{

[Bindable]
public class StoreVO
{
    public var storeId:int;
    public var title:LocaleVO;
    public var storeSection:Vector.<StoreSectionVO>;

    public function StoreVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            title = new LocaleVO();
            storeSection = new Vector.<StoreSectionVO>();
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        storeId = obj.storeId;
        title = new LocaleVO(obj.title);
        storeSection = new Vector.<StoreSectionVO>();
        for each(o in obj.storeSection) {
            storeSection.push(new StoreSectionVO(o));
        }
    }

    public function clone():StoreVO
    {
        var result:StoreVO = new StoreVO();
        result.storeId = this.storeId;
        result.title = this.title.clone();
        result.storeSection = new Vector.<StoreSectionVO>();
        for each(var section:StoreSectionVO in this.storeSection) {
            result.storeSection.push(section.clone());
        }
        return result;
    }
}
}