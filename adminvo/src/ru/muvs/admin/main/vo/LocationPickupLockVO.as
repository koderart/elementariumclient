
package ru.muvs.admin.main.vo
{

[Bindable]
public class LocationPickupLockVO
{
    public var pickupLocationMemberLockId:int;
    public var artikul:ArtikulVO;
    public var count:int;
    public var quest:QuestVO;

    private var _lock:String;
    public function get LOCK():String {
        return _lock;
    }
    public function set LOCK(value:String):void {
        _lock = value;
    }
    public function get pickupLocationMemberLock():String {
        return _lock;
    }
    public function set pickupLocationMemberLock(value:String):void {
        _lock = value;
    }
    
    public function LocationPickupLockVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            artikul = new ArtikulVO();
            quest = new QuestVO();
        }
    }

    public function parseObject(obj:Object):void
    {
        pickupLocationMemberLockId = obj.pickupLocationMemberLockId;
        pickupLocationMemberLock = obj.pickupLocationMemberLock;
        artikul = new ArtikulVO(obj.artikul);
        quest = new QuestVO(obj.quest);
        count = obj.count;
    }

    public function clone():LocationPickupLockVO
    {
        var result:LocationPickupLockVO = new LocationPickupLockVO();
        result.pickupLocationMemberLockId = this.pickupLocationMemberLockId;
        result.pickupLocationMemberLock = this.pickupLocationMemberLock;
        result.artikul = this.artikul.clone();
        result.quest = this.quest.clone();
        result.count = this.count;
        return result;
    }
}
}