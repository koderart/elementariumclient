package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.FractionEnum;

[Bindable]
	public class QuestLockVO
	{
        public var questLockId:int;
        //artikul
		public var artikul:ArtikulVO;
		public var count:int;
    
        //element
        public var element:Array = [];
        
        //have done, not done
        public var relation:int;
        
        //level
        public var level:LevelVO;

        public var fraction:Array = [];
        public var rank:RankVO;

        private var _questLock:String;
        public function get LOCK():String {
            return _questLock;
        }
        public function set LOCK(value:String):void {
            _questLock = value;
        }
        public function get questLock():String {
            return _questLock;
        }
        public function set questLock(value:String):void {
            _questLock = value;
        }

        // for table
        public var questTitle:String;
    
        public function QuestLockVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
        }

        public function parseObject(obj:Object):void
        {
            questLockId = obj.questLockId;
            LOCK = obj.LOCK;
            questLock = obj.questLock;
            if (obj.artikul) artikul = new ArtikulVO(obj.artikul);
            count = obj.count;
            element = obj.element;
            relation = obj.relation;
            fraction = obj.fraction;
            if (obj.rank) rank = new RankVO(obj.rank);
            if (obj.level) level = new LevelVO(obj.level);
        }

        public function clone():QuestLockVO
        {
            var result:QuestLockVO = new QuestLockVO();
            result.questLockId = this.questLockId;
            result.questLock = this.questLock;
            if (this.artikul) result.artikul = this.artikul.clone();
            result.count = this.count;
            if (this.element) result.element = this.element.concat();
            result.relation = this.relation;
            if (this.fraction) result.fraction = this.fraction.concat();
            if (this.rank) result.rank = this.rank.clone();
            if (this.level) result.level = this.level.clone();
            
            return result;
        }
	}
}