package ru.muvs.admin.main.vo
{
[Bindable]
	public class ChallengeLockArtikulVO
	{
        public var challengeLockId:int;
		public var LOCK:String;
		public var challengeLock:String;
		public var artikul:ArtikulVO;
		public var count:int;

		public function ChallengeLockArtikulVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                artikul = new ArtikulVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            challengeLockId = obj.challengeLockId;
            LOCK = obj.LOCK;
            challengeLock = obj.challengeLock;
            artikul = new ArtikulVO(obj.artikul);
            count = obj.count;
        }

        public function clone():ChallengeLockArtikulVO
        {
            var result:ChallengeLockArtikulVO = new ChallengeLockArtikulVO();
            result.challengeLockId = this.challengeLockId;
            result.LOCK = this.LOCK;
            result.challengeLock = this.challengeLock;
            result.artikul = this.artikul.clone();
            result.count = this.count;
            
            return result;
        }
	}
}