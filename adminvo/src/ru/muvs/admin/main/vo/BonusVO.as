package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.*;

[Bindable]
	public class BonusVO
	{
		public var bonusId:int;
		public var title:LocaleVO;
        public var weight:int;
        public var bonusAction:Array;
        public var bonusLock:Array;

		public function BonusVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                bonusAction = [];
                bonusLock = [];
            }
		}
        
        public function parseObject(obj:Object):void
        {
            var o:Object;
            bonusId = obj.bonusId;
            title = new LocaleVO(obj.title);
            weight = obj.weight;
            bonusAction = [];
            for each(o in obj.bonusAction) {
                bonusAction.push(new BonusActionVO(o));
            }
            bonusLock = [];
            for each(o in obj.bonusLock) {
                bonusLock.push(new BonusLockVO(o));
            }
        }
        
        public function clone():BonusVO
        {
            var result:BonusVO = new BonusVO();
            result.bonusId = this.bonusId;
            result.title = this.title.clone();
            result.weight = this.weight;
            result.bonusAction = [];
            for each(var o:Object in this.bonusAction) {
                result.bonusAction.push(o.clone());
            }
            result.bonusLock = [];
            for each(o in this.bonusLock) {
                result.bonusLock.push(o.clone());
            }

            return result;
        }
	}
}