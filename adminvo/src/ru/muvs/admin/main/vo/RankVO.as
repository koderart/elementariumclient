package ru.muvs.admin.main.vo
{

[Bindable]
	public class RankVO
	{
		public var rankId:int;
		public var number:int;
		public var honor:int;
        public var title:LocaleVO;
        public var description:LocaleVO;
        public var image:ImageVO;

		public function RankVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                description = new LocaleVO();
                image = new ImageVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            rankId = obj.rankId;
            number = obj.number;
            honor = obj.honor;
            title = new LocaleVO(obj.title);
            description = new LocaleVO(obj.description);
            image = new ImageVO(obj.image);
        }

        public function clone():RankVO
        {
            var result:RankVO = new RankVO();
            result.rankId = this.rankId;
            result.number = this.number;
            result.honor = this.honor;
            result.title = this.title.clone();
            result.description = this.description.clone();
            result.image = this.image.clone();
            return result;
        }
	}
}