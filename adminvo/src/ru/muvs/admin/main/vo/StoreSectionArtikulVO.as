package ru.muvs.admin.main.vo
{

[Bindable]
	public class StoreSectionArtikulVO
	{
		public var storeSectionArtikulId:int;
		public var artikul:ArtikulVO;
        
		public function StoreSectionArtikulVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                artikul = new ArtikulVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            storeSectionArtikulId = obj.storeSectionArtikulId;
            artikul = new ArtikulVO(obj.artikul);
        }

        public function clone():StoreSectionArtikulVO
        {
            var result:StoreSectionArtikulVO = new StoreSectionArtikulVO();
            result.storeSectionArtikulId = this.storeSectionArtikulId;
            result.artikul = this.artikul.clone();
            return result;
        }
	}
}