package ru.muvs.admin.main.vo
{

[Bindable]
public class LocationTeleportLockVO
{
    public var teleportLocationMemberLockId:int;
    public var element:Array = [];
    public var fraction:Array = [];
    public var level:LevelVO;
    public var artikul:ArtikulVO;
    public var count:int;
    
    private var _lock:String;
    public function get LOCK():String {
        return _lock;
    }
    public function set LOCK(value:String):void {
        _lock = value;
    }
    public function get teleportLocationMemberLock():String {
        return _lock;
    }
    public function set teleportLocationMemberLock(value:String):void {
        _lock = value;
    }
    
    public function LocationTeleportLockVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            level = new LevelVO();
            artikul = new ArtikulVO();
        }
    }

    public function parseObject(obj:Object):void
    {
        teleportLocationMemberLockId = obj.teleportLocationMemberLockId;
        teleportLocationMemberLock = obj.teleportLocationMemberLock;
        element = obj.element;
        fraction = obj.fraction;
        level = new LevelVO(obj.level);
        artikul = new ArtikulVO(obj.artikul);
        count = obj.count;
    }

    public function clone():LocationTeleportLockVO
    {
        var result:LocationTeleportLockVO = new LocationTeleportLockVO();
        result.teleportLocationMemberLockId = this.teleportLocationMemberLockId;
        result.teleportLocationMemberLock = this.teleportLocationMemberLock;
        if (this.element) result.element = this.element.concat();
        if (this.fraction) result.fraction = this.fraction.concat();
        result.level = this.level.clone();
        result.artikul = this.artikul.clone();
        result.count = this.count;
        return result;
    }
}
}