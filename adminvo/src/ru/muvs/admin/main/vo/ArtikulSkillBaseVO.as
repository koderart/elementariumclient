package ru.muvs.admin.main.vo
{

    [Bindable]
	public class ArtikulSkillBaseVO
	{
		public var artikulSkillBaseId:int;
		public var skill:SkillVO;
		public var element:String;
		public var value:int;
        
		public function ArtikulSkillBaseVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                skill = new SkillVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            artikulSkillBaseId = obj.artikulSkillBaseId;
            skill = new SkillVO(obj.skill);
            element = obj.element;
            value = obj.value;
        }
        
        public function clone():ArtikulSkillBaseVO
        {
            var result:ArtikulSkillBaseVO = new ArtikulSkillBaseVO();
            result.artikulSkillBaseId = this.artikulSkillBaseId;
            result.skill = this.skill.clone();
            result.element = this.element;
            result.value = this.value;
            
            return result;
        }
	}
}