package ru.muvs.admin.main.vo
{

[Bindable]
	public class NpcVO
	{
		public var npcId:int;
		public var title:LocaleVO;
		public var imageAvatar:ImageVO;
        public var npcTakeQuest:Vector.<NpcTakeVO>;
        public var npcGiveQuest:Vector.<NpcGiveVO>;

        public function NpcVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                imageAvatar = new ImageVO();
                npcTakeQuest = new Vector.<NpcTakeVO>();
                npcGiveQuest = new Vector.<NpcGiveVO>();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            var o:Object;
            npcId = obj.npcId;
            title = new LocaleVO(obj.title);
            imageAvatar = new ImageVO(obj.imageAvatar);
            npcTakeQuest = new Vector.<NpcTakeVO>();
            for each(o in obj.npcTakeQuest) {
                npcTakeQuest.push(new NpcTakeVO(o));
            }
            npcGiveQuest = new Vector.<NpcGiveVO>();
            for each(o in obj.npcGiveQuest) {
                npcGiveQuest.push(new NpcGiveVO(o));
            }
        }
        
        public function clone():NpcVO
        {
            var result:NpcVO = new NpcVO();
            result.npcId = this.npcId;
            result.title = this.title.clone();
            result.imageAvatar = this.imageAvatar.clone();

            result.npcTakeQuest = new Vector.<NpcTakeVO>();
            for each(var take:NpcTakeVO in this.npcTakeQuest) {
                result.npcTakeQuest.push(take.clone());
            }
            result.npcGiveQuest = new Vector.<NpcGiveVO>();
            for each(var give:NpcGiveVO in this.npcGiveQuest) {
                result.npcGiveQuest.push(give.clone());
            }

            return result;
        }
	}
}