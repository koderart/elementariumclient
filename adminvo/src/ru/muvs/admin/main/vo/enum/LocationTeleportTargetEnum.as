package ru.muvs.admin.main.vo.enum {
public final class LocationTeleportTargetEnum {
    public static const SINGLE:String = "SINGLE";

    public static const data:Array= [SINGLE];
    public static const LABELS:Object = {SINGLE:"Одиночный"};
}
}
