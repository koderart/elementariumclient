package ru.muvs.admin.main.vo.enum {
public final class SlotEnum {

    public static const NONE:String = "NONE";
    public static const TALISMAN_LEFT:String = "TALISMAN_LEFT";
    public static const TALISMAN_RIGHT:String = "TALISMAN_RIGHT";
    public static const HEAD:String = "HEAD";
    public static const CHEST:String = "CHEST";
    public static const BELT:String = "BELT";
    public static const SHOULDERS:String = "SHOULDERS";
    public static const HAND_LEFT:String = "HAND_LEFT";
    public static const HAND_RIGHT:String = "HAND_RIGHT";
    public static const HAND_BOTH:String = "HAND_BOTH";
    public static const BRACERS:String = "BRACERS";
    public static const GLOVES:String = "GLOVES";
    public static const LEGS:String = "LEGS";
    public static const BOOTS:String = "BOOTS";
    public static const BAG:String = "BAG";
    public static const MINING:String = "MINING";

    public static const data:Array = [TALISMAN_LEFT,TALISMAN_RIGHT,HEAD,CHEST,BELT,SHOULDERS,HAND_LEFT,HAND_RIGHT,HAND_BOTH,BRACERS,GLOVES,LEGS,BOOTS,BAG,MINING];
    
    public static const LABELS:Object = {NONE:"", 
                                         TALISMAN_LEFT:"Левый оберег", 
                                         TALISMAN_RIGHT:"Правый оберег",
                                         HEAD:"Шлем",
                                         CHEST:"Броня",
                                         BELT:"Пояс",
                                         SHOULDERS:"Наплечники",
                                         HAND_LEFT:"Левая рука",
                                         HAND_RIGHT:"Правая рука",
                                         HAND_BOTH:"Обе руки",
                                         BRACERS:"Наручи",
                                         GLOVES:"Перчатки",
                                         LEGS:"Поножи",
                                         BOOTS:"Сапоги",
                                         BAG:"Сумка",
                                         MINING:"Инструмент"};
}
}
