package ru.muvs.admin.main.vo.enum {
public final class SkillEnum {

    public static const NONE:String = "NONE";
    public static const HP:String = "HP";
    public static const EXP:String = "EXP";
    public static const LEVEL:String = "LEVEL";
    public static const DAMAGE:String = "DAMAGE";
    public static const PROTECTION:String = "PROTECTION";
    public static const DAMAGE_DARK:String = "DAMAGE_DARK";
    public static const PROTECTION_DARK:String = "PROTECTION_DARK";
    public static const DAMAGE_FIRE:String = "DAMAGE_FIRE";
    public static const PROTECTION_FIRE:String = "PROTECTION_FIRE";
    public static const DAMAGE_LIGHT:String = "DAMAGE_LIGHT";
    public static const PROTECTION_LIGHT:String = "PROTECTION_LIGHT";
    public static const DAMAGE_WATER:String = "DAMAGE_WATER";
    public static const PROTECTION_WATER:String = "PROTECTION_WATER";
    public static const DAMAGE_NATURE:String = "DAMAGE_NATURE";
    public static const PROTECTION_NATURE:String = "PROTECTION_NATURE";
    public static const BAG_CAPACITY:String = "BAG_CAPACITY";
    public static const BELT_CAPACITY:String = "BELT_CAPACITY";
    public static const HONOR:String = "HONOR";
    public static const RANK:String = "RANK";
    

    public static const data:Array= [NONE,HP,EXP,LEVEL,DAMAGE,PROTECTION,DAMAGE_DARK,PROTECTION_DARK,DAMAGE_FIRE,PROTECTION_FIRE,DAMAGE_LIGHT,
        PROTECTION_LIGHT,DAMAGE_WATER,PROTECTION_WATER,DAMAGE_NATURE,PROTECTION_NATURE, BAG_CAPACITY, BELT_CAPACITY, HONOR, RANK];
}
}
