package ru.muvs.admin.main.vo.enum {
public final class QuestGoalEnum {

    public static const COLLECT_ARTIKUL:String = "COLLECT_ARTIKUL";
    public static const HAVE_ARTIKUL:String = "HAVE_ARTIKUL";
    public static const KILL_MOB:String = "KILL_MOB";
    public static const KILL_PLAYER:String = "KILL_PLAYER";
    public static const HAVE_MONEY:String = "HAVE_MONEY";

    public static const data:Array= [COLLECT_ARTIKUL, HAVE_ARTIKUL, KILL_MOB, KILL_PLAYER, HAVE_MONEY];
    public static const LABELS:Object = {COLLECT_ARTIKUL:"Собрать артикул",
                                         HAVE_ARTIKUL:"Иметь артикул",
                                         KILL_MOB:"Убийства моба",
                                         KILL_PLAYER:"Убийства игроков",
                                         HAVE_MONEY:"Наличие денег"
    };
}
}
