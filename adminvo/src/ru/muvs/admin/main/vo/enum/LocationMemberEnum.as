package ru.muvs.admin.main.vo.enum {
public final class LocationMemberEnum {
    public static const TELEPORT:String = "TELEPORT";
    public static const NPC:String = "NPC";
    public static const MOB:String = "MOB";
    public static const PICKUP:String = "PICKUP";

    public static const data:Array= [TELEPORT, NPC, MOB, PICKUP];
    public static const LABELS:Object = {TELEPORT:"Телепорт",NPC:"НПЦ", MOB:"Моб", PICKUP:"Объект"};
}
}
