package ru.muvs.admin.main.vo.enum {
public final class BagTabEnum {
    public static const NONE:String = "NONE";
    public static const MAIN:String = "MAIN";
    public static const CONSUMABLE:String = "CONSUMABLE";
    public static const QUEST:String = "QUEST";
    public static const PROFESSION:String = "PROFESSION";
    public static const RECIPE:String = "RECIPE";

    public static const data:Array= [NONE, MAIN, CONSUMABLE, QUEST, PROFESSION, RECIPE];
    public static const LABELS:Object = {NONE:"",MAIN:"Основная",CONSUMABLE:"Расходка",QUEST:"Квестовые",PROFESSION:"Профессиональные",RECIPE:"Рецепты"};
}
}
