package ru.muvs.admin.main.vo.enum {
public final class BonusLockEnum {

    public static const LEVEL_MIN:String = "LEVEL_MIN";
    public static const LEVEL_MAX:String = "LEVEL_MAX";
    public static const QUEST_HAVE_NOT_DONE:String = "QUEST_HAVE_NOT_DONE";
    public static const ELEMENT:String = "ELEMENT";
    public static const ARTIKUL_HAVE:String = "ARTIKUL_HAVE";
    public static const FRACTION:String = "FRACTION";
    public static const RANK_MIN:String = "RANK_MIN";
    public static const RANK_MAX:String = "RANK_MAX";
    
    public static const data:Array= [LEVEL_MIN,LEVEL_MAX,QUEST_HAVE_NOT_DONE, ELEMENT, ARTIKUL_HAVE, FRACTION, RANK_MIN, RANK_MAX];
    public static const LABELS:Object = {LEVEL_MIN:"Минимальный уровень",LEVEL_MAX:"Максимальный уровень",
        QUEST_HAVE_NOT_DONE:"Наличие квеста",ELEMENT:"Стихия", ARTIKUL_HAVE:"Наличие предмета",
        FRACTION:"Фракция", RANK_MIN:"Минимальный ранг", RANK_MAX:"Максимальный ранг"};
}
}
