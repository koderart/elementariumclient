package ru.muvs.admin.main.vo.enum {
public final class FractionEnum {
    public static const NONE:String = "NONE";
    public static const FIRST:String = "FIRST";
    public static const SECOND:String = "SECOND";

    public static const data:Array= [FIRST, SECOND];
    public static const LABELS:Object = {NONE:"",FIRST:"АЛЬЯНС",SECOND:"ЛЕГИОН"};
}
}
