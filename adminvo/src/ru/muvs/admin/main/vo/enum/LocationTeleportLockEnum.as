package ru.muvs.admin.main.vo.enum {
public final class LocationTeleportLockEnum {
    public static const ELEMENT:String = "ELEMENT";
    public static const LEVEL_MIN:String = "LEVEL_MIN";
    public static const LEVEL_MAX:String = "LEVEL_MAX";
    public static const HAVE_ARTIKUL:String = "HAVE_ARTIKUL";
    public static const FRACTION:String = "FRACTION";

    public static const data:Array= [ELEMENT, LEVEL_MIN, LEVEL_MAX, HAVE_ARTIKUL, FRACTION];
    public static const LABELS:Object = {ELEMENT:"Стихия",LEVEL_MIN:"Минимальный уровень",LEVEL_MAX:"Максимальный уровень",
                                        HAVE_ARTIKUL:"Наличие артикула", FRACTION:"Фракция"};
}
}
