package ru.muvs.admin.main.vo.enum {
public final class LocationPickupLockEnum {

    public static const HAVE_SLOT:String = "HAVE_SLOT";
    public static const HAVE_ARTIKUL:String = "HAVE_ARTIKUL";
    public static const HAVE_QUEST_NOT_DONE:String = "HAVE_QUEST_NOT_DONE";

    public static const data:Array= [HAVE_SLOT, HAVE_ARTIKUL, HAVE_QUEST_NOT_DONE];
    public static const LABELS:Object = {HAVE_SLOT:"Наличие слота", HAVE_ARTIKUL:"Наличие артикула", HAVE_QUEST_NOT_DONE:"Наличие квеста"};
}
}
