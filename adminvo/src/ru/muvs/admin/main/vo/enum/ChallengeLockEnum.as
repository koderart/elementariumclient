package ru.muvs.admin.main.vo.enum {
public final class ChallengeLockEnum {

    public static const ELEMENT:String = "ELEMENT";
    public static const LEVEL_MIN:String = "LEVEL_MIN";
    public static const LEVEL_MAX:String = "LEVEL_MAX";
    public static const HAVE_ARTIKUL:String = "HAVE_ARTIKUL";
    public static const HAVE_SKILL:String = "HAVE_SKILL";

    public static const data:Array= [ELEMENT, LEVEL_MIN,LEVEL_MAX, HAVE_ARTIKUL, HAVE_SKILL];
    public static const LABELS:Object = {ELEMENT:"Стихия", LEVEL_MIN:"Минимальный уровень", LEVEL_MAX:"Максимальный уровень",HAVE_ARTIKUL:"Иметь артикул", HAVE_SKILL:"Иметь характеристику"};
}
}
