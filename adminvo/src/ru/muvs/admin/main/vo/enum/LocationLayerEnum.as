package ru.muvs.admin.main.vo.enum {
public final class LocationLayerEnum {
    public static const DOWN:String = "DOWN";
    public static const MIDDLE:String = "MIDDLE";
    public static const TOP:String = "TOP";
    public static const TOPMOST:String = "TOPMOST";

    public static const data:Array= [DOWN, MIDDLE, TOP, TOPMOST];
    public static const LABELS:Object = {DOWN:"Нижний",MIDDLE:"Средний",TOP:"Высокий",TOPMOST:"Самый высокий"};
}
}
