package ru.muvs.admin.main.vo.enum {
public final class ChallengeGoalEnum {

    public static const ACTION_ARTIKUL:String = "ACTION_ARTIKUL";
    public static const KILL_MOB:String = "KILL_MOB";
    public static const KILL_PLAYER:String = "KILL_PLAYER";

    public static const data:Array= [ACTION_ARTIKUL, KILL_MOB, KILL_PLAYER];
    public static const LABELS:Object = {ACTION_ARTIKUL:"Использовать артикул",
                                         KILL_MOB:"Убийства моба",
                                         KILL_PLAYER:"Убийства игроков"
    };
}
}
