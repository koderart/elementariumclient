package ru.muvs.admin.main.vo.enum {
public final class AreaDownSideEnum {
    public static const NONE:String = "NONE";
    public static const TOP:String = "TOP";
    public static const DOWN:String = "DOWN";
    public static const LEFT:String = "LEFT";
    public static const RIGHT:String = "RIGHT";

    public static const data:Array= [NONE,TOP,DOWN,LEFT,RIGHT];
}
}
