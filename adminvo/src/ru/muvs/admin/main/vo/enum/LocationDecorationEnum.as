package ru.muvs.admin.main.vo.enum {
public final class LocationDecorationEnum {
    public static const IMAGE:String = "IMAGE";
    public static const ANIMATION:String = "ANIMATION";
    public static const PARTICLE:String = "PARTICLE";

    public static const data:Array= [IMAGE, ANIMATION, PARTICLE];
    public static const LABELS:Object = {IMAGE:"Изображение",ANIMATION:"Кадровая анимация",PARTICLE:"Анимация частиц"};
}
}
