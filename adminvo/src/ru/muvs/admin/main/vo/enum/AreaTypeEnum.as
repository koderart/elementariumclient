package ru.muvs.admin.main.vo.enum {
public final class AreaTypeEnum {
    public static const NONE:String = "NONE";
    public static const BATTLE:String = "BATTLE";
    public static const FARM:String = "FARM";

    public static const data:Array= [NONE,BATTLE,FARM];
}
}
