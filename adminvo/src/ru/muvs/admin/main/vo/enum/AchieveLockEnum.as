package ru.muvs.admin.main.vo.enum {
public final class AchieveLockEnum {

    public static const LEVEL:String = "LEVEL";
    public static const ELEMENT:String = "ELEMENT";

    public static const data:Array= [LEVEL,ELEMENT];
    public static const LABELS:Object = {LEVEL:"Уровень", ELEMENT:"Стихия"};
}
}
