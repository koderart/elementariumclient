package ru.muvs.admin.main.vo.enum {
public final class BonusActionEnum {

    public static const CHILD:String = "CHILD";
    public static const SKILL_MOD_PLUS:String = "SKILL_MOD_PLUS";
    public static const SKILL_MOD_MINUS:String = "SKILL_MOD_MINUS";
    public static const SKILL_MOD_PRC_PLUS:String = "SKILL_MOD_PRC_PLUS";
    public static const SKILL_MOD_PRC_MINUS:String = "SKILL_MOD_PRC_MINUS";
    public static const ARTIKUL_GIVE:String = "ARTIKUL_GIVE";
    public static const ARTIKUL_TAKE:String = "ARTIKUL_TAKE";
    public static const MONEY_GIVE:String = "MONEY_GIVE";
    public static const MONEY_TAKE:String = "MONEY_TAKE";
    public static const SLOT_GIVE:String = "SLOT_GIVE";
    public static const SLOT_TAKE:String = "SLOT_TAKE";

    public static const data:Array= [CHILD, SKILL_MOD_PLUS, SKILL_MOD_MINUS, SKILL_MOD_PRC_PLUS, SKILL_MOD_PRC_MINUS, ARTIKUL_GIVE, ARTIKUL_TAKE, MONEY_GIVE, MONEY_TAKE, SLOT_GIVE, SLOT_TAKE];
    public static const LABELS:Object = {CHILD:"Подбонус",
                                         SKILL_MOD_PLUS:"+ характеристика",
                                         SKILL_MOD_MINUS:"- характеристика",
                                         SKILL_MOD_PRC_PLUS:"+ % характеристика",
                                         SKILL_MOD_PRC_MINUS:"- % характеристика",
                                         ARTIKUL_GIVE:"Выдать артикул",
                                         ARTIKUL_TAKE:"Забрать артикул",
                                         MONEY_GIVE:"Выдать деньги",
                                         MONEY_TAKE:"Забрать деньги",
                                         SLOT_GIVE:"Положить в слот",
                                         SLOT_TAKE:"Забрать из слота"
    };
}
}
