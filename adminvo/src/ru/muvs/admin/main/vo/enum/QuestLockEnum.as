package ru.muvs.admin.main.vo.enum {
public final class QuestLockEnum {

    public static const LEVEL_MIN:String = "LEVEL_MIN";
    public static const LEVEL_MAX:String = "LEVEL_MAX";
    public static const ELEMENT:String = "ELEMENT";
    public static const QUEST_HAVE_DONE:String = "QUEST_HAVE_DONE";
    public static const QUEST_HAVE_NOT_DONE:String = "QUEST_HAVE_NOT_DONE";
    public static const ARTIKUL:String = "ARTIKUL";
    public static const FRACTION:String = "FRACTION";
    public static const RANK_MIN:String = "RANK_MIN";
    public static const RANK_MAX:String = "RANK_MAX";

    public static const data:Array= [LEVEL_MIN,LEVEL_MAX, ELEMENT, QUEST_HAVE_DONE, QUEST_HAVE_NOT_DONE, ARTIKUL,
    FRACTION, RANK_MIN, RANK_MAX];
    public static const LABELS:Object = {LEVEL_MIN:"Минимальный уровень",LEVEL_MAX:"Максимальный уровень", ELEMENT:"Стихия",
        QUEST_HAVE_DONE:"Выполненный квест", QUEST_HAVE_NOT_DONE:"Активный квест", ARTIKUL:"Наличие артикула",
        FRACTION:"Фракция", RANK_MIN:"Минимальный ранг", RANK_MAX:"Максимальный ранг"};
}
}
