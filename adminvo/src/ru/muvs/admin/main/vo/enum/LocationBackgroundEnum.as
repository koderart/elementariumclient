package ru.muvs.admin.main.vo.enum {
public final class LocationBackgroundEnum {
    public static const WHITE:String = "WHITE";
    public static const BLACK:String = "BLACK";

    public static const data:Array= [WHITE, BLACK];
    public static const LABELS:Object = {WHITE:"Белый", BLACK:"Черный"};
}
}
