package ru.muvs.admin.main.vo.enum {
public final class AchieveGoalEnum {

    public static const KILL_MOB:String = "KILL_MOB";
    public static const KILL_PLAYER:String = "KILL_PLAYER";

    public static const data:Array= [KILL_MOB, KILL_PLAYER];
    public static const LABELS:Object = {KILL_MOB:"Убийства моба",
                                         KILL_PLAYER:"Убийства игроков"
    };
}
}
