package ru.muvs.admin.main.vo.enum {
public final class MoneyEnum {
    public static const NONE:String = "NONE";
    public static const GOLD:String = "GOLD";
    public static const CRYSTAL:String = "CRYSTAL";

    public static const data:Array= [GOLD, CRYSTAL];
    public static const LABELS:Object = {NONE:"",GOLD:"Золото",CRYSTAL:"Кристалл"};
}
}
