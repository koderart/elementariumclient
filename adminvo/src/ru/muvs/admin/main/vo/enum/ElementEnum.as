package ru.muvs.admin.main.vo.enum {
public final class ElementEnum {
    
    public static const NONE:String = "NONE";
    public static const DARK:String = "DARK";
    public static const FIRE:String = "FIRE";
    public static const LIGHT:String = "LIGHT";
    public static const WATER:String = "WATER";
    public static const NATURE:String = "NATURE";

    public static const data:Array = [NONE,DARK,FIRE,LIGHT,WATER,NATURE];

    public static const LABELS:Object = {NONE:"",DARK:"ТЬМА",FIRE:"ОГОНЬ",LIGHT:"СВЕТ",WATER:"ВОДА",NATURE:"ПРИРОДА"};
}
}
