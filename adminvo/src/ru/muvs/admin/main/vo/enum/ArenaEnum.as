package ru.muvs.admin.main.vo.enum {
public final class ArenaEnum {
    public static const PVP:String = "PVP";
    public static const P2VP2:String = "P2VP2";
    public static const P3VP3:String = "P3VP3";
    public static const P4VP4:String = "P4VP4";
    public static const P5VP5:String = "P5VP5";

    public static const data:Array= [PVP, P2VP2, P3VP3, P4VP4, P5VP5];
    public static const LABELS:Object = {PVP:"1 на 1",P2VP2:"2 на 2",P3VP3:"3 на 3",
                                         P4VP4:"4 на 4",P5VP5:"5 на 5"};
}
}
