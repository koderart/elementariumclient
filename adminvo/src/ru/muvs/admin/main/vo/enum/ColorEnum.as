package ru.muvs.admin.main.vo.enum {
public final class ColorEnum {
    public static const NONE:String = "NONE";
    public static const GREY:String = "GREY";
    public static const GREEN:String = "GREEN";
    public static const BLUE:String = "BLUE";
    public static const ORANGE:String = "ORANGE";
    public static const PURPLE:String = "PURPLE";
    public static const RED:String = "RED";

    public static const data:Array = [NONE, GREY, GREEN, BLUE, ORANGE, PURPLE, RED];
    
    public static const LABELS:Object = {NONE:"",GREY:"СЕРЫЙ",GREEN:"ЗЕЛЕНЫЙ",BLUE:"СИНИЙ",ORANGE:"ОРАНЖЕВЫЙ",PURPLE:"ФИОЛЕТОВЫЙ",RED:"КРАСНЫЙ"};
    public static const COLORS:Object = {NONE:0xFFFFFF,GREY:0x666666,GREEN:0x00FF00,BLUE:0x0000FF,ORANGE:0xAA3F00,PURPLE:0xAA0076,RED:0xFF0000};
}
}
