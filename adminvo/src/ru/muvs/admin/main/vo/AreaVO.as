package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.AreaDownSideEnum;
import ru.muvs.admin.main.vo.enum.AreaTypeEnum;

[Bindable]
	public class AreaVO
	{
		public var areaId:int;
		public var title:LocaleVO;
		public var areaType:String = AreaTypeEnum.NONE;
		public var areaDownSide:String = AreaDownSideEnum.NONE;
		public var width:int = 1;
		public var height:int = 1;
		public var areaCellDrop:Vector.<AreaCellDropVO>;
		public var areaCellStatic:Vector.<AreaCellStaticVO>;
		public var areaCellDisable:Vector.<AreaCellDisableVO>;

		public function AreaVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                title = new LocaleVO();
                areaCellDrop = new Vector.<AreaCellDropVO>();
                areaCellStatic = new Vector.<AreaCellStaticVO>();
                areaCellDisable = new Vector.<AreaCellDisableVO>();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            var o:Object;
            areaId = obj.areaId;
            title = new LocaleVO(obj.title);
            areaType = obj.areaType;
            areaDownSide = obj.areaDownSide;
            width = obj.width;
            height = obj.height;
            areaCellDrop = new Vector.<AreaCellDropVO>();
            for each(o in obj.areaCellDrop) {
                areaCellDrop.push(new AreaCellDropVO(o));
            }
            areaCellStatic = new Vector.<AreaCellStaticVO>();
            for each(o in obj.areaCellStatic) {
                areaCellStatic.push(new AreaCellStaticVO(o));
            }
            areaCellDisable = new Vector.<AreaCellDisableVO>();
            for each(o in obj.areaCellDisable) {
                areaCellDisable.push(new AreaCellDisableVO(o));
            }
        }
        
        public function clone():AreaVO
        {
            var result:AreaVO = new AreaVO();
            result.areaId = this.areaId;
            result.title = this.title.clone();
            result.areaType = this.areaType;
            result.areaDownSide = this.areaDownSide;
            result.width = this.width;
            result.height = this.height;

            result.areaCellDrop = new Vector.<AreaCellDropVO>();
            for each(var cell1:AreaCellDropVO in this.areaCellDrop) {
                result.areaCellDrop.push(cell1.clone());
            }
            result.areaCellStatic = new Vector.<AreaCellStaticVO>();
            for each(var cell2:AreaCellStaticVO in this.areaCellStatic) {
                result.areaCellStatic.push(cell2.clone());
            }
            result.areaCellDisable = new Vector.<AreaCellDisableVO>();
            for each(var sell3:AreaCellDisableVO in this.areaCellDisable) {
                result.areaCellDisable.push(sell3.clone());
            }

            return result;
        }
	}
}