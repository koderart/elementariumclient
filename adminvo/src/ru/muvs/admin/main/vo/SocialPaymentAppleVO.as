package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.MoneyEnum;

[Bindable]
	public class SocialPaymentAppleVO
	{
		public var bankAppleInAppId:int;
		public var inAppType:String = MoneyEnum.GOLD;
		public var inApp:String;
        public var price:int;
        public var giveCount:int;
        public var giveBonusPrc:int;
        public var giveBonusCount:int;

		public function SocialPaymentAppleVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
            }
		}
        
        public function parseObject(obj:Object):void
        {
            bankAppleInAppId = obj.bankAppleInAppId;
            inAppType = obj.inAppType;
            inApp = obj.inApp;
            price = obj.price;
            giveCount = obj.giveCount;
            giveBonusPrc = obj.giveBonusPrc;
            giveBonusCount = obj.giveBonusCount;
        }
        
        public function clone():SocialPaymentAppleVO
        {
            var result:SocialPaymentAppleVO = new SocialPaymentAppleVO();
            result.bankAppleInAppId = this.bankAppleInAppId;
            result.inAppType = this.inAppType;
            result.inApp = this.inApp;
             result.price = this.price;
            result.giveCount = this.giveCount;
            result.giveBonusPrc = this.giveBonusPrc;
            result.giveBonusCount = this.giveBonusCount;

            return result;
        }
	}
}