package ru.muvs.admin.main.vo
{

[Bindable]
	public class BonusActionVO
	{
		public var bonusActionId:int;
		public var probability:int;
		public var weight:int;

        //ARTIKUL_GIVE, ARTIKUL_TAKE
		public var artikul:int;
		public var countFrom:int;
		public var countTo:int;
        public var randomSkill:Boolean;

        //CHILD
        public var child:BonusVO;
        
        //MONEY_GIVE, MONEY_TAKE
        public var moneyType:String;
        public var moneyFrom:int;
        public var moneyTo:int;

        //SKILL_MOD_PLUS, SKILL_MOD_MINUS, SKILL_MOD_PRC_PLUS, SKILL_MOD_PRC_MINUS
        public var skill:SkillVO;
        public var value:int;

        //SLOT_GIVE, SLOT_TAKE
        //artikul
        
        /**
         for admin greed
         */
        public var artikulTitle:String;

        private var _action:String;
        public function get ACTION():String {
            return _action;
        }
        public function set ACTION(value:String):void {
            _action = value;
        }
        public function get bonusAction():String {
            return _action;
        }
        public function set bonusAction(value:String):void {
            _action = value;
        }

        public function BonusActionVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                child = new BonusVO();
                skill = new SkillVO();
            }
        }

        public function parseObject(obj:Object):void
        {
            bonusActionId = obj.bonusActionId;
            ACTION = obj.ACTION;
            bonusAction = obj.bonusAction;
            probability = obj.probability;
            weight = obj.weight;
            artikul = obj.artikul;
            countFrom = obj.countFrom;
            countTo = obj.countTo;
            randomSkill = obj.randomSkill;
            
            child = new BonusVO(obj.child);

            moneyType = obj.moneyType;
            moneyFrom = obj.moneyFrom;
            moneyTo = obj.moneyTo;
            
            skill = new SkillVO(obj.skill);
            value = obj.value;
        }

        public function clone():BonusActionVO
        {
            var result:BonusActionVO = new BonusActionVO();
            result.bonusActionId = this.bonusActionId;
            result.ACTION = this.ACTION;
            result.bonusAction = this.bonusAction;
            result.probability = this.probability;
            result.weight = this.weight;
             result.artikul = this.artikul;
            result.countFrom = this.countFrom;
            result.countTo = this.countTo;
            result.randomSkill = this.randomSkill;
            
            result.child = this.child.clone();
            
            result.moneyType = this.moneyType;
            result.moneyFrom = this.moneyFrom;
            result.moneyTo = this.moneyTo;

            result.skill = this.skill.clone();
            result.value = this.value;
            
            return result;
        }
	}
}