package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.BonusVO;
import ru.muvs.admin.main.vo.enum.LocationLayerEnum;

[Bindable]
public class LocationMemberVO
{
    public var locationMemberId:int;
    public var positionX:int;
    public var positionY:int;
    public var positionHexX:int;
    public var positionHexY:int;
    public var layer:String = LocationLayerEnum.MIDDLE;

    // TELEPORT
    public var image:ImageVO;
    public var target:int;
    public var targetPositionHexX:int;
    public var targetPositionHexY:int;
    public var teleportLock:Vector.<LocationTeleportLockVO>;

    // NPC
    public var npc:NpcVO;

    // MOB
    public var mob:MobVO;

    public var model:String;    
    public var label:Boolean = true;    
    public var speed:int;
    public var pause:int;
    public var left:Boolean;

    //PICKUP
    //image
    //label
    public var action:BonusVO;
    public var cooldown:int;
    public var pickupLock:Vector.<LocationPickupLockVO>;

    private var _member:String;
    public function get MEMBER():String {
        return _member;
    }
    public function set MEMBER(value:String):void {
        _member = value;
    }
    public function get locationMember():String {
        return _member;
    }
    public function set locationMember(value:String):void {
        _member = value;
    }
    
    public function LocationMemberVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            teleportLock = new Vector.<LocationTeleportLockVO>();
            pickupLock = new Vector.<LocationPickupLockVO>();
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        locationMemberId = obj.locationMemberId;
        locationMember = obj.locationMember;
        positionX = obj.positionX;
        positionY = obj.positionY;
        positionHexX = obj.positionHexX;
        positionHexY = obj.positionHexY;
        layer = obj.layer;
        image = obj.image ? new ImageVO(obj.image) : null;
        npc = obj.npc ? new NpcVO(obj.npc) : null;
        mob = obj.mob ? new MobVO(obj.mob) : null;
        target = obj.target;
        targetPositionHexX = obj.targetPositionHexX;
        targetPositionHexY = obj.targetPositionHexY;
        teleportLock = new Vector.<LocationTeleportLockVO>();
        for each(o in obj.teleportLock) {
            teleportLock.push(new LocationTeleportLockVO(o));
        }
        model = obj.model;
        label = obj.label;
        speed = obj.speed;
        pause = obj.pause;
        left = obj.left;
        action = obj.action ? new BonusVO(obj.action) : null;
        cooldown = obj.cooldown;
        pickupLock = new Vector.<LocationPickupLockVO>();
        for each(o in obj.pickupLock) {
            pickupLock.push(new LocationPickupLockVO(o));
        }
    }

    public function clone():LocationMemberVO
    {
        var result:LocationMemberVO = new LocationMemberVO();
        result.locationMemberId = this.locationMemberId;
        result.locationMember = this.locationMember;
        result.positionX = this.positionX;
        result.positionY = this.positionY;
        result.positionHexX = this.positionHexX;
        result.positionHexY = this.positionHexY;
        result.layer = this.layer;
        if (this.image) {result.image = this.image.clone();}
        if (this.npc) {result.npc = this.npc.clone();}
        if (this.mob) {result.mob = this.mob.clone();}
        result.target = this.target;
        result.targetPositionHexX = this.targetPositionHexX;
        result.targetPositionHexY = this.targetPositionHexY;
        for each(var lockTeleport:LocationTeleportLockVO in this.teleportLock) {
            result.teleportLock.push(lockTeleport.clone());
        }

        result.model = this.model;
        result.label = this.label;
        result.speed = this.speed;
        result.pause = this.pause;
        result.left = this.left;
        if (this.action) {result.action = this.action.clone();}
        result.cooldown = this.cooldown;
        for each(var lockPickup:LocationPickupLockVO in this.pickupLock) {
            result.pickupLock.push(lockPickup.clone());
        }

        return result;
    }
}
}