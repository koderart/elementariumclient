package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.*;

[Bindable]
public class ChallengeVO
{
    public var challengeId:int;
    public var title:LocaleVO;
    public var description:LocaleVO;
    public var bonus:BonusVO;
    public var challengeGoal:Array;
    public var challengeLock:Array;

    public function ChallengeVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            title = new LocaleVO();
            description = new LocaleVO();
            bonus = new BonusVO();
            challengeGoal = [];
            challengeLock = [];
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        challengeId = obj.challengeId;
        title = new LocaleVO(obj.title);
        description = new LocaleVO(obj.description);
        bonus = new BonusVO(obj.bonus);
        challengeGoal = [];
        for each(o in obj.challengeGoal) {
            switch (o.GOAL) {
                case ChallengeGoalEnum.ACTION_ARTIKUL:
                    challengeGoal.push(new ChallengeGoalArtikulVO(o));
                break;
                case ChallengeGoalEnum.KILL_MOB:
                    challengeGoal.push(new ChallengeGoalMobVO(o));
                break;
                case ChallengeGoalEnum.KILL_PLAYER:
                    challengeGoal.push(new ChallengeGoalPlayerVO(o));
                break;
            }
        }

        challengeLock = [];
        for each(o in obj.challengeLock) {
            switch (o.LOCK) {
                case ChallengeLockEnum.LEVEL_MAX:
                case ChallengeLockEnum.LEVEL_MIN:
                    challengeLock.push(new ChallengeLockLevelVO(o));
                break;
                case ChallengeLockEnum.ELEMENT:
                    challengeLock.push(new ChallengeLockElementVO(o));
                break;
                case ChallengeLockEnum.HAVE_ARTIKUL:
                    challengeLock.push(new ChallengeLockArtikulVO(o));
                break;
                case ChallengeLockEnum.HAVE_SKILL:
                    challengeLock.push(new ChallengeLockSkillVO(o));
                break;
            }
        }
    }

    public function clone():ChallengeVO
    {
        var result:ChallengeVO = new ChallengeVO();
        result.challengeId = this.challengeId;
        result.title = this.title.clone();
        result.description = this.description.clone();
        result.bonus = this.bonus.clone();

        result.challengeGoal = [];
        for each(var o:Object in this.challengeGoal) {
            result.challengeGoal.push(o.clone());
        }
        result.challengeLock = [];
        for each(o in this.challengeLock) {
            result.challengeLock.push(o.clone());
        }
        return result;
    }
}
}