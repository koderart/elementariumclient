package ru.muvs.admin.main.vo
{
    [Bindable]
	public class AreaCellDropVO
	{
		public var areaCellDropId:int;
		public var areaCellType:AreaCellTypeVO;
		public var weight:int;

		public function AreaCellDropVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            } else {
                areaCellType = new AreaCellTypeVO();
            }
		}
        
        public function parseObject(obj:Object):void
        {
            areaCellDropId = obj.areaCellDropId;
            areaCellType = new AreaCellTypeVO(obj.areaCellType);
            weight = obj.weight;
        }
        
        public function clone():AreaCellDropVO
        {
            var result:AreaCellDropVO = new AreaCellDropVO();
            result.areaCellDropId = this.areaCellDropId;
            result.areaCellType = this.areaCellType.clone();
            result.weight = this.weight;
            
            return result;
        }
	}
}