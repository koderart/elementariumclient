package ru.muvs.admin.main.vo
{
	[Bindable]
	public class LocaleVO
	{
		public var localizationId:int;
		public var ru:String;
		public var en:String;
		public var token:String;

		public function LocaleVO(obj:Object=null)
		{
            if (obj) {
                parseObject(obj);
            }
        }

        public function parseObject(obj:Object):void
        {
            localizationId = obj.localizationId;
            ru = obj.ru;
            en = obj.en;
            token = obj.token;
        }

        public function clone():LocaleVO
        {
            var result:LocaleVO = new LocaleVO();
            result.localizationId = this.localizationId;
            result.ru = this.ru;
            result.en = this.en;
            result.token = this.token;
            
            return result;
        }
	}
}