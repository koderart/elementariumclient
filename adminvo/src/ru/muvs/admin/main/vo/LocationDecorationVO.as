package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.LocationLayerEnum;

[Bindable]
public class LocationDecorationVO
{
    public var locationDecorationId:int;
    public var positionX:int;
    public var positionY:int;
    public var layer:String = LocationLayerEnum.MIDDLE;

    // TELEPORT
    public var image:ImageVO;

    // ANIMATION
    public var source:String;
    public var speed:int;
    public var pause:int;
    
    
    private var _decoration:String;
    public function get DECORATION():String {
        return _decoration;
    }
    public function set DECORATION(value:String):void {
        _decoration = value;
    }
    public function get locationDecoration():String {
        return _decoration;
    }
    public function set locationDecoration(value:String):void {
        _decoration = value;
    }
    
    public function LocationDecorationVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
        }
    }

    public function parseObject(obj:Object):void
    {
        locationDecoration = obj.locationDecoration;
        locationDecorationId = obj.locationDecorationId;
        positionX = obj.positionX;
        positionY = obj.positionY;
        layer = obj.layer;
        source = obj.source;
        speed = obj.speed;
        pause = obj.pause;
        image = new ImageVO(obj.image);
    }

    public function clone():LocationDecorationVO
    {
        var result:LocationDecorationVO = new LocationDecorationVO();
        result.locationDecoration = this.locationDecoration;
        result.locationDecorationId = this.locationDecorationId;
        result.positionX = this.positionX;
        result.positionY = this.positionY;
        result.layer = this.layer;
        result.source = this.source;
        result.speed = this.speed;
        result.pause = this.pause;
        if (this.image) {result.image = this.image.clone();}
        return result;
    }
}
}