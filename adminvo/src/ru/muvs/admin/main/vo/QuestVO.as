package ru.muvs.admin.main.vo
{
import ru.muvs.admin.main.vo.enum.*;

[Bindable]
public class QuestVO
{
    public var questId:int;
    public var title:LocaleVO;
    public var description:LocaleVO;
    public var location:LocaleVO;
    public var cooldown:int;
    public var bonus:BonusVO;
    public var questGoal:Array;
    public var questLock:Array;

    public function QuestVO(obj:Object=null)
    {
        if (obj) {
            parseObject(obj);
        } else {
            title = new LocaleVO();
            description = new LocaleVO();
            location = new LocaleVO();
            bonus = new BonusVO();
            questGoal = [];
            questLock = [];
        }
    }

    public function parseObject(obj:Object):void
    {
        var o:Object;
        questId = obj.questId;
        title = new LocaleVO(obj.title);
        description = new LocaleVO(obj.description);
        location = new LocaleVO(obj.location);
        cooldown = obj.cooldown;
        bonus = new BonusVO(obj.bonus);
        questGoal = [];
        for each(o in obj.questGoal) {
            questGoal.push(new QuestGoalVO(o));
        }

        questLock = [];
        for each(o in obj.questLock) {
            questLock.push(new QuestLockVO(o));
        }
    }

    public function clone():QuestVO
    {
        var result:QuestVO = new QuestVO();
        result.questId = this.questId;
        result.title = this.title.clone();
        result.description = this.description.clone();
        result.location = this.location.clone();
        result.cooldown = this.cooldown;
        result.bonus = this.bonus.clone();

        result.questGoal = [];
        for each(var o:Object in this.questGoal) {
            result.questGoal.push(o.clone());
        }
        result.questLock = [];
        for each(o in this.questLock) {
            result.questLock.push(o.clone());
        }
        return result;
    }
}
}